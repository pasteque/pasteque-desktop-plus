//    POS-Tech
//    Based upon Openbravo POS
//
//    Copyright (C) 2007-2009 Openbravo, S.L.
//                       2012 Scil (http://scil.coop)
//
//    This file is part of POS-Tech.
//
//    POS-Tech is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    POS-Tech is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with POS-Tech.  If not, see <http://www.gnu.org/licenses/>.


package fr.pasteque.pos.payment;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Date;

import javax.swing.JPanel;
import javax.swing.JTextArea;

import fr.pasteque.basic.BasicException;
import fr.pasteque.beans.JCalendarDialog;
import fr.pasteque.data.loader.ImageLoader;
import fr.pasteque.format.Formats;
import fr.pasteque.pos.admin.CurrencyInfo;
import fr.pasteque.pos.customers.CustomerInfoExt;
import fr.pasteque.pos.forms.AppLocal;
import fr.pasteque.pos.util.RoundUtils;
import fr.pasteque.pos.widgets.JEditorCurrency;
import fr.pasteque.pos.widgets.JEditorKeys;
import fr.pasteque.pos.widgets.JEditorString;
import fr.pasteque.pos.widgets.WidgetsBuilder;

public class JPaymentPaper extends javax.swing.JPanel implements JPaymentInterface {
    
    /**
	 * 
	 */
	private static final long serialVersionUID = -6926404199040961424L;

	private JPaymentNotifier m_notifier;
    

    private double partAmount;
    private double m_dTicket;
    private double m_dTotal;
    private CurrencyInfo currency;
    
    private String m_sPaper; // "paperin", "paperout"
    
    public final class Name {
        public static final String PAPERIN = "paperin";
        public static final String PAPEROUT = "paperout";
    }
    
    
    // private String m_sCustomer; 
    
    
    /** Creates new form JPaymentTicket */
    public JPaymentPaper(JPaymentNotifier notifier) {
        
        m_notifier = notifier;
        
        initComponents();
        
        m_jTendered.addPropertyChangeListener("Edition", new RecalculateState());
        m_jTendered.addEditorKeys(m_jKeys);
    }
    
    public void activate(CustomerInfoExt customerext, double dTotal,
            double partAmount, CurrencyInfo currency, String transID) {
        
        m_dTotal = dTotal;
        this.partAmount = partAmount;
        this.currency = currency;
        
        m_jTendered.reset();
        m_jTendered.activate();
        
        printState();        
    }
    
    public Component getComponent() {
        return this;
    }
    
    public PaymentInfo executePayment() {
    	m_sPaper = m_dTicket < 0 ? JPaymentPaper.Name.PAPEROUT : JPaymentPaper.Name.PAPERIN ;
    	PaymentInfo retour = new PaymentInfoTicket(m_dTicket, this.currency, m_sPaper);
    	try {
			retour.setEcheance((Date) Formats.TIMESTAMP.parseValue(jTxtGoalDate.getText()));
		} catch (BasicException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	retour.setNote(m_jNotes.getText());
    	retour.setTransactionID(m_jReference.getText());
        return retour;
    }    
    
    private void printState() {

        Double value = m_jTendered.getDoubleValue();
        if (value == null) {
            m_dTicket = this.partAmount;
        } else {
            m_dTicket = value;
        } 
        
        Formats.setAltCurrency(this.currency);
        m_jMoneyEuros.setText(Formats.CURRENCY.formatValue(new Double(m_dTicket)));
        
        int iCompare = RoundUtils.compare(m_dTicket, m_dTotal);
        
        // it is allowed to pay more
        //m_notifier.setStatus(m_dTicket > 0.0, iCompare >= 0);
        //EDU - 2015-07-01 On ne peut plus payer plus si on utilise un avoir d'un montant supérieur 
        //on en crée un nouveau négatif.
        m_notifier.setStatus(m_dTicket >= 0.0, iCompare == 0);
    }
    
    private class RecalculateState implements PropertyChangeListener {
        public void propertyChange(PropertyChangeEvent evt) {
            printState();
        }
    }    
    
    private void initComponents() {
        setLayout(new java.awt.BorderLayout());
        btnDateStart = new javax.swing.JButton();
        jLabelGoalDate = new javax.swing.JLabel();
        jLabelReference = new javax.swing.JLabel();
        jTxtGoalDate = new javax.swing.JTextField();
        m_jReference = new JEditorString();
        
        
        // Setup right part (inputContainer): keyboard
        JPanel inputContainer = new JPanel();
        inputContainer = new javax.swing.JPanel();
        m_jKeys = new JEditorKeys();
        m_jTendered = new JEditorCurrency();
        jPanel1 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        
        jPanel1.setLayout(new javax.swing.BoxLayout(jPanel1, javax.swing.BoxLayout.Y_AXIS));
        jPanel1.add(m_jKeys);

        jPanel3.setBorder(javax.swing.BorderFactory.createEmptyBorder(5, 5, 5, 5));
        jPanel3.setLayout(new java.awt.BorderLayout());
        jPanel3.add(m_jTendered, java.awt.BorderLayout.CENTER);

        jPanel1.add(jPanel3);
        
        inputContainer.setLayout(new java.awt.BorderLayout());
        inputContainer.add(jPanel1, java.awt.BorderLayout.NORTH);
        
        // Setup left part (paymentInfoContainer): payment info (amount)
        
        JPanel paymentInfoContainer = new JPanel();
        GridBagConstraints constraints = new GridBagConstraints();
        paymentInfoContainer.setLayout(new GridBagLayout());
        
        givenLabel = WidgetsBuilder.createLabel(AppLocal.getIntString("Label.InputCash"));
        m_jMoneyEuros = WidgetsBuilder.createLabel();
        
        //paymentInfoContainer.setLayout(new BorderLayout());

        //JPanel changeContainer = new JPanel();
        //changeContainer.setLayout(new GridLayout(1, 2, 8 ,8));
        givenLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        //changeContainer.add(givenLabel);
        m_jMoneyEuros.setBackground(new java.awt.Color(153, 153, 255));
        m_jMoneyEuros.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        m_jMoneyEuros.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(javax.swing.UIManager.getDefaults().getColor("Button.darkShadow")), javax.swing.BorderFactory.createEmptyBorder(1, 4, 1, 4)));
        m_jMoneyEuros.setOpaque(true);
        //changeContainer.add(m_jMoneyEuros);
        //paymentInfoContainer.add(changeContainer, BorderLayout.NORTH);
        
      	constraints.fill = GridBagConstraints.NONE;
      	constraints.ipady = 0;       
      	constraints.weighty = 0;   
      	constraints.anchor = GridBagConstraints.EAST; 
      	constraints.insets = new Insets(5,0,0,3);  
      	constraints.gridx = 2;       
      	constraints.gridwidth = 1;   
      	constraints.gridy = 0;       
      	
      	paymentInfoContainer.add(givenLabel,constraints);
      	
      	constraints.fill = GridBagConstraints.HORIZONTAL;
      	constraints.ipady = 0;       
      	constraints.weighty = 0;   
      	constraints.insets = new Insets(5,0,0,0); 
      	constraints.gridx = 3;       
      	constraints.gridwidth = 2;   
      	constraints.gridy = 0;       
      	
      	paymentInfoContainer.add(m_jMoneyEuros,constraints);
        
        jLabelReference = WidgetsBuilder.createLabel(AppLocal.getIntString("Label.Reference"));
      	constraints.fill = GridBagConstraints.NONE;
      	constraints.ipady = 0;       
      	constraints.weighty = 0;
      	constraints.insets = new Insets(5,0,0,3);  
      	constraints.gridx = 2;       
      	constraints.gridwidth = 1;   
      	constraints.gridy = 1;       
      	
      	paymentInfoContainer.add(jLabelReference,constraints);
      	m_jReference.setMinimumSize(new java.awt.Dimension(90, 22));
        m_jReference.addEditorKeys(m_jKeys);
        
      	constraints.fill = GridBagConstraints.HORIZONTAL;
      	constraints.ipady = 0;       
      	constraints.weighty = 0;
      	constraints.insets = new Insets(5,0,0,0); 
      	constraints.gridx = 3;       
      	constraints.gridwidth = 2;   
      	constraints.gridy = 1;       
      	
      	paymentInfoContainer.add(m_jReference,constraints);
        
        //jPanelCenter.add(m_jReference, BorderLayout.NORTH);
        //jPanelCenter.add(m_jNotes, BorderLayout.CENTER);
        
        jLabelGoalDate = WidgetsBuilder.createLabel(AppLocal.getIntString("Label.GoalDate"));
        
        jTxtGoalDate.setPreferredSize(new java.awt.Dimension(200, 25));
        jTxtGoalDate.setMinimumSize(new java.awt.Dimension(120, 22));
        
        btnDateStart.setIcon(ImageLoader.readImageIcon("calendar.png"));
        btnDateStart.setPreferredSize(new java.awt.Dimension(50, 25));
        btnDateStart.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDateStartActionPerformed(evt);
            }
        });
        
      	constraints.fill = GridBagConstraints.NONE;
      	constraints.ipady = 0;       
      	constraints.weighty = 0; 
      	constraints.insets = new Insets(5,0,5,3);  
      	constraints.gridx = 1;       
      	constraints.gridwidth = 1;   
      	constraints.gridy = 2;       
      	
      	paymentInfoContainer.add(jLabelGoalDate,constraints);
        
      	constraints.fill = GridBagConstraints.HORIZONTAL; 
      	constraints.anchor = GridBagConstraints.CENTER; 
      	constraints.insets = new Insets(5,0,5,0);  
      	constraints.gridx = 2;       
      	constraints.gridwidth = 2;     
      	
      	paymentInfoContainer.add(jTxtGoalDate,constraints);
        
      	constraints.fill = GridBagConstraints.NONE;
      	constraints.anchor = GridBagConstraints.EAST;   
      	constraints.gridx = 4;       
      	constraints.gridwidth = 1;      
      	
      	paymentInfoContainer.add(btnDateStart,constraints);
      	
      	constraints.fill = GridBagConstraints.NONE;
      	constraints.anchor = GridBagConstraints.CENTER; //bottom of space
      	constraints.insets = new Insets(10,0,0,0);  //top padding
      	constraints.gridx = 0;       //aligned with button left
      	constraints.gridwidth = 5;   //5 columns wide
      	constraints.gridy = 3;       //4th row
      	
      	paymentInfoContainer.add(WidgetsBuilder.createLabel(AppLocal.getIntString("Label.Notes")),constraints);
        
        m_jNotes = new javax.swing.JTextArea();
        m_jNotes.setFont(WidgetsBuilder.getFont(WidgetsBuilder.SIZE_SMALL));
        m_jNotes.setEditable(true);
        m_jNotes.setFocusable(true);
        m_jNotes.setRows(3);
      	m_jNotes.setLineWrap(true);
      	m_jNotes.setWrapStyleWord(true);
        //jPanelCenter.add(m_jNotes, BorderLayout.CENTER);
      	
      	constraints.fill = GridBagConstraints.BOTH;
      	constraints.ipady = 0;       //reset to default
      	constraints.weighty = 1.0;   //request any extra vertical space
      	constraints.anchor = GridBagConstraints.PAGE_END; //bottom of space
      	constraints.insets = new Insets(5,0,30,0);  //top padding
      	constraints.gridx = 0;       //aligned with button left
      	constraints.gridwidth = 5;   //5 columns wide
      	constraints.gridy = 4;       //4th row
      	constraints.gridheight = 3;
      	
      	paymentInfoContainer.add(m_jNotes,constraints);
        
        //jPanelCenter.add(comp);
        //paymentInfoContainer.add(jPanelCenter , BorderLayout.CENTER);
        // Add all to main container
        add(paymentInfoContainer, BorderLayout.CENTER);
        add(inputContainer, BorderLayout.LINE_END);
        
    }
    
    public void clearComponents() {
        jTxtGoalDate.setText("");
        m_jReference.setText("");
    	m_jNotes.setText("");
    }
    
    private void btnDateStartActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDateStartActionPerformed
        Date date;
            try {
                date = (Date) Formats.TIMESTAMP.parseValue(jTxtGoalDate.getText());
            } catch (BasicException e) {
                date = null;
            }        
            date = JCalendarDialog.showCalendarTimeHours(this, date);
            if (date != null) {
            	jTxtGoalDate.setText(Formats.TIMESTAMP.formatValue(date));
            }
    }//GEN-LAST:event_btnDateStartActionPerformed
    
    
    private javax.swing.JLabel givenLabel;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private JEditorKeys m_jKeys;
    private javax.swing.JLabel m_jMoneyEuros;
    private JEditorCurrency m_jTendered;
    private JTextArea m_jNotes;
    private javax.swing.JLabel jLabelReference;
	private JEditorString m_jReference;
	private javax.swing.JLabel jLabelGoalDate;
	private javax.swing.JTextField jTxtGoalDate;
	private javax.swing.JButton btnDateStart;
	
    
}
