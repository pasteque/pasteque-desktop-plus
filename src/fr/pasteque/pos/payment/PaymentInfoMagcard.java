//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2008-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.payment;

import fr.pasteque.pos.admin.CurrencyInfo;
import fr.pasteque.pos.forms.AppLocal;

import java.io.Serializable;

public class PaymentInfoMagcard extends PaymentInfo implements Serializable {
     
    /**
	 * 
	 */
	private static final long serialVersionUID = 8740325385335598740L;

	protected double m_dTotal;
    

    protected String track1;
    protected String track2;
    protected String track3;
    
    
    protected String m_sAuthorization;    
    protected String m_sErrorMessage;
    protected String m_sReturnMessage;
    
    /** Creates a new instance of PaymentInfoMagcard */
    public PaymentInfoMagcard(String sHolderName, String sCardNumber,
            String sExpirationDate, String track1, String track2, String track3,
            String sTransactionID, double dTotal, CurrencyInfo currency, String sSecurity) {
        m_sHolderName = sHolderName;
        m_sCardNumber = sCardNumber;
        m_sExpirationDate = sExpirationDate;
        this.track1 = track1;
        this.track2 = track2;
        this.track3 = track3;
        this.currency = currency;
   
        m_transactionID = sTransactionID;
        m_dTotal = dTotal;
        
        m_sAuthorization = null;
        m_sErrorMessage = null;
        m_sReturnMessage = null;
        m_sSecurity = sSecurity;
    }
    
    /** Creates a new instance of PaymentInfoMagcard */
    public PaymentInfoMagcard(String sHolderName, String sCardNumber,
            String sExpirationDate, String sTransactionID, double dTotal,
            CurrencyInfo currency , String m_sSecurity) {
        this(sHolderName, sCardNumber, sExpirationDate, null, null, null,
                sTransactionID, dTotal, currency, m_sSecurity);
    }
    
    public PaymentInfo copyPayment(){
        PaymentInfoMagcard p = new PaymentInfoMagcard(m_sHolderName,
                m_sCardNumber, m_sExpirationDate, track1, track2, track3,
                m_transactionID, m_dTotal, this.currency , this.m_sSecurity);
        p.m_sAuthorization = m_sAuthorization;
        p.m_sErrorMessage = m_sErrorMessage;
        return p;
    }    
    
    public String getName() {
        return "magcard";
    }
    public double getTotal() {
        return m_dTotal;
    }         
    
    public boolean isPaymentOK() {
        return m_sAuthorization != null;
    }    


    
    /**
     * Get tracks of magnetic card.
     *   Framing characters: 
     *    - start sentinel (SS)
     *    - end sentinel (ES) 
     *    - LRC 
     * @param framingChar 
     *    true: including framing characters
     *    false: exluding framing characters
     * @return tracks of the magnetic card
     */
    public String getTrack1(boolean framingChar) {
        return (framingChar)
            ? track1
            : track1.substring(1, track1.length()-2);
    }
    public String getTrack2(boolean framingChar) {
        return (framingChar)
            ? track2
            : track2.substring(1, track2.length()-2);
    }
    public String getTrack3(boolean framingChar) {
        return (framingChar)
            ? track3
            : track3.substring(1, track3.length()-2);
    }
    
    public String getAuthorization() {
        return m_sAuthorization;
    }

    public String getMessage() {
        return m_sErrorMessage;
    }
    
    public void paymentError(String sMessage, String moreInfo) {
        m_sAuthorization = null;
        m_sErrorMessage = sMessage + "\n" + moreInfo;
    }    
    
    public void setReturnMessage(String returnMessage){
        m_sReturnMessage = returnMessage;
    }
    
    public String getReturnMessage(){
        return m_sReturnMessage;
    }
    
    public void paymentOK(String sAuthorization, String sTransactionId, String sReturnMessage) {
        m_sAuthorization = sAuthorization;
        m_transactionID = sTransactionId;
        m_sReturnMessage = sReturnMessage;
        m_sErrorMessage = null;
    }  


    public String printAuthorization() {
        return m_sAuthorization== null ? "" :m_sAuthorization;
    }
    public String printTransactionID() {
        return m_transactionID== null ? "" :m_transactionID;
    }
    
    
    @Override
    public void setNote(String note){
    	this.note = "" ;
    	if(m_sHolderName != null && !m_sHolderName.isEmpty()) {
    		this.note = AppLocal.getIntString("label.cardholder").concat(" : ").concat(m_sHolderName).concat("\r\n");
    	}
    	if(m_sCardNumber != null && !m_sCardNumber.isEmpty()) {
    		this.note = this.note.concat(AppLocal.getIntString("label.cardnumber").concat(" : ").concat(m_sCardNumber).concat("\r\n"));
    	}
    	if(m_sExpirationDate != null && !m_sExpirationDate.isEmpty()) {
    		this.note = this.note.concat(AppLocal.getIntString("label.cardexpdate").concat(" : ").concat(m_sExpirationDate).concat("\r\n"));
    	}
    	if(m_sSecurity != null && !m_sSecurity.isEmpty()) {
    		this.note = this.note.concat(AppLocal.getIntString("label.cardsecurity").concat(" : ").concat(m_sSecurity).concat("\r\n"));
    	}
    	this.note = this.note.concat(note);
    }
}
