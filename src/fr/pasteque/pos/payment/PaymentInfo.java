//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2008-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.payment;

import fr.pasteque.basic.BasicException;
import fr.pasteque.format.Formats;
import fr.pasteque.pos.admin.CurrencyInfo;
import fr.pasteque.pos.forms.AppLocal;
import fr.pasteque.pos.forms.DataLogicSales;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.JSONObject;

/** Generic PaymentInfo as stored in database.
 * Use subclasses when creating a new PaymentInfo.
 */
public abstract class PaymentInfo implements Serializable {
	
    /**
	 * 
	 */
	
    public final class Type {
        public static final String PREPAID = "prepaid";
        public static final String DEBT = "debt";
        public static final String DEBT_PAID = "debtpaid";
        public static final String PAPER_OUT = "paperout";
        public static final String PAPER_IN = "paperin";
        public static final String MAGCARD = "magcard";
        public static final String CHEQUE = "cheque";
        public static final String TRANSFER = "transfer";
        public static final String CASH = "cash";
        public static final String CASH_IN = "cashin";
        public static final String CASH_OUT = "cashout";
        public static final String CASH_OUT_FEE = "place";
        public static final String CASH_OUT_BUY = "buyout";
        public static final String CASH_REFUND = "cashrefund";
    }
    
	private static final long serialVersionUID = 3355931213745599744L;
	protected CurrencyInfo currency;
	protected double amount;
	protected String m_sName;
	protected String m_transactionID;
	protected Date echeance;
	protected String note;
    protected String m_sHolderName;
    protected String m_sCardNumber;

	protected String m_sExpirationDate;
    protected String m_sSecurity;

    public Date getEcheance() {
		return echeance;
	}
	public String getNote() {
		return note;
	}
	public CurrencyInfo getCurrency() {
        return this.currency;
    }
    public String getName() {
        return m_sName;
    }   
    /** Get total of paiement in payment currency. */
    public double getTotal() {
        return this.amount;
    }
    
    public String getTransactionID(){
        return m_transactionID;
    }
    
    public void setTransactionID(String m_transactionID) {
		this.m_transactionID = m_transactionID;
	}
	public void setEcheance(Date echeance) {
		this.echeance = echeance;
	}
	public void setNote(String note) {
		this.note = note;
	}
	
    public boolean isDeffered() {
    	return echeance != null;
    }
    
    public String printEcheance() {

    	Format formatter = new SimpleDateFormat("dd MMMM yyyy");

    	return echeance != null ? formatter.format(echeance) : "" ;
    }
    
    public String getHolderName() {
        return (m_sHolderName == null ? "" : m_sHolderName );
    }
    public String getCardNumber() {
        return m_sCardNumber;
    }
    public String getExpirationDate() {
        return m_sExpirationDate;
    }    
    
    public String getSecurity() {
		return m_sSecurity;
	}

	public void setSecurity(String m_sSecurity) {
		this.m_sSecurity = m_sSecurity;
	}
	
    public void setM_sHolderName(String m_sHolderName) {
		this.m_sHolderName = m_sHolderName;
	}
	public void setM_sCardNumber(String m_sCardNumber) {
		this.m_sCardNumber = m_sCardNumber;
	}
	public void setM_sExpirationDate(String m_sExpirationDate) {
		this.m_sExpirationDate = m_sExpirationDate;
	}
	public String printCardNumber() {
        // hide start numbers
        if (m_sCardNumber != null && m_sCardNumber.length() > 4) {
            return m_sCardNumber.substring(0, m_sCardNumber.length()-4).replaceAll(".", "*") +
                    m_sCardNumber.substring(m_sCardNumber.length() - 4);
        } else {
            return "****";
        }
    }
    public String printExpirationDate() {
        return m_sExpirationDate== null ? "" : m_sExpirationDate;
    }
	
    public abstract PaymentInfo copyPayment();

    public JSONObject toJSON() {
        JSONObject o = new JSONObject();
        o.put("type", this.getName());
		//EDU : Vendredi 08 Avril 2016 18h - Andrezieux s'est retrouvé avec un nombre non fini
		//On patch
		if(Double.isNaN(this.getTotal()) || Double.isInfinite(this.getTotal())) {
			o.put("amount", 0D);
		} else {
			o.put("amount", this.currency.convertToMain(this.getTotal()));
		}
        o.put("currencyId", this.currency.getID());
		if(Double.isNaN(this.getTotal()) || Double.isInfinite(this.getTotal())) {
			o.put("currencyAmount", 0D);
		} else {
			o.put("currencyAmount", this.getTotal());
		}
        o.put("transId", this.getTransactionID());
        //o.put("returnMessage", this.getr);
        o.put("note", this.getNote());
        if(this.getEcheance() != null) {
        	o.put("echeance", this.getEcheance().getTime());
        }
        
        /* TODO en // avec le PaymentDetail DTO
        *
        if(this.getCardNumber() != null) {
        	o.put("cardNumber", this.getCardNumber());
        }
         if(this.getSecurity() != null) {
        	o.put("security", this.getSecurity());
        }
        if(this.getExpirationDate() != null) {
        	o.put("expiration", this.getExpirationDate());
        }
        if(!this.getHolderName().isEmpty()) {
        	o.put("holder", this.getHolderName());
        }*/
        return o;
    }

    

	public static PaymentInfo readJSON(JSONObject o) throws BasicException {
        String type = o.getString("type");
        PaymentInfo retour = null;
        //double amount = o.getDouble("amount");
        int currencyId = o.getInt("currencyId");
        double currencyAmount = o.getDouble("currencyAmount");

        DataLogicSales dlSales = new DataLogicSales();
        retour = new PaymentInfoGeneric(type, currencyAmount,
                dlSales.getCurrency(currencyId));
        if(!o.isNull("echeance")) {
            retour.setEcheance( new Date(o.getLong("echeance")));
        }
        
        if(!o.isNull("note")) {
        	analyseNote(retour , o.getString("note"));
        }
        
        if(!o.isNull("holder")) {
            retour.setM_sHolderName(o.getString("holder"));
        }
        if(!o.isNull("security")) {
            retour.setSecurity( o.getString("security"));
        }
        if(!o.isNull("expiration")) {
            retour.setM_sExpirationDate( o.getString("expiration"));
        }
        if(!o.isNull("cardNumber")) {
            retour.setM_sCardNumber(o.getString("cardNumber"));
        }

        return retour;
    }

    private static void analyseNote(PaymentInfo retour , String note) {
		
    	String[] liste = note.split("\r\n");
    	String cardholderPrefix = AppLocal.getIntString("label.cardholder").concat(" : ");
    	String expirationPrefix = AppLocal.getIntString("label.cardexpdate").concat(" : ");
    	String cardNumberPrefix = AppLocal.getIntString("label.cardnumber").concat(" : ");
    	String securityPrefix = AppLocal.getIntString("label.cardsecurity").concat(" : ");
    	for(String item : liste) {
    		
    		byte ptext[];
			try {
				ptext = item.getBytes("ISO_8859_1");
				item = new String(ptext, "UTF-8"); 
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			} 
	        if(item.startsWith(cardholderPrefix)) {
	            retour.setM_sHolderName(item.substring(cardholderPrefix.length()));
	        }
	        if(item.startsWith(securityPrefix)) {
	            retour.setSecurity( item.substring(securityPrefix.length()));
	        }
	        if(item.startsWith(expirationPrefix)) {
	            retour.setM_sExpirationDate( item.substring(expirationPrefix.length()));
	        }
	        if(item.startsWith(cardNumberPrefix)) {
	            retour.setM_sCardNumber(item.substring(cardNumberPrefix.length()));
	        }
    	}
		
	}
    
	public String printTotal() {
        Formats.setAltCurrency(this.currency);
        return Formats.CURRENCY.formatValue(new Double(getTotal()));
    }

}
