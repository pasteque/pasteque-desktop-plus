//    POS-Tech
//    Based upon Openbravo POS
//
//    Copyright (C) 2007-2009 Openbravo, S.L.
//                       2012 Scil (http://scil.coop)
//
//    This file is part of POS-Tech.
//
//    POS-Tech is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    POS-Tech is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with POS-Tech.  If not, see <http://www.gnu.org/licenses/>.


package fr.pasteque.pos.payment;

import java.awt.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Date;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import fr.pasteque.pos.admin.CurrencyInfo;
import fr.pasteque.pos.customers.CustomerInfoExt;
import fr.pasteque.pos.forms.AppLocal;
import fr.pasteque.pos.forms.AppView;
import fr.pasteque.pos.util.RoundUtils;
import fr.pasteque.basic.BasicException;
import fr.pasteque.beans.JCalendarDialog;
import fr.pasteque.data.loader.ImageLoader;
import fr.pasteque.format.Formats;
import fr.pasteque.pos.widgets.JEditorCurrencyPositive;
import fr.pasteque.pos.widgets.JEditorKeys;
import fr.pasteque.pos.widgets.JEditorString;
import fr.pasteque.pos.widgets.JEditorStringNumber;
import fr.pasteque.pos.widgets.WidgetsBuilder;

/**
 *
 * @author  adrianromero
 */
public class JPaymentMagcard extends javax.swing.JPanel implements JPaymentInterface {

    /**
	 * 
	 */
	private static final long serialVersionUID = 6326380620414049596L;
	//private PaymentPanel m_cardpanel;
    private PaymentGateway m_paymentgateway;
    private JPaymentNotifier m_notifier;
    private String transaction;

    private double m_dPaid;
    private double m_dTotal;
    private double partAmount;
    private CurrencyInfo currency;
    
    /** Creates new form JPaymentCash */
    public JPaymentMagcard(AppView app, JPaymentNotifier notifier) {
        
        m_notifier = notifier;
        
        initComponents();  
        
        m_jTendered.addPropertyChangeListener("Edition", new RecalculateState());
        m_jTendered.addEditorKeys(m_jKeys);

        m_paymentgateway = PaymentGatewayFac.getPaymentGateway(app.getProperties());
        if (m_paymentgateway == null) {
            jlblMessage.setText(AppLocal.getIntString("message.nopaymentgateway"));
        } else {
            // TODO: In case card reader is connected to POS
            //m_cardpanel = PaymentPanelFac.getPaymentPanel(app.getProperties().getProperty("payment.magcardreader"), notifier);
            //add(m_cardpanel.getComponent(), BorderLayout.CENTER);
            jlblMessage.setText(null);
        }

    }
    
    public void activate(CustomerInfoExt customerext, double dTotal,
            double partAmount, CurrencyInfo currency, String transID) {
        this.transaction = transID;
        this.currency = currency;
        
        if (m_paymentgateway == null) {
            jlblMessage.setText(AppLocal.getIntString("message.nopaymentgateway"));
            m_notifier.setStatus(false, false);
        } else {
            jlblMessage.setText(null);
        }

        m_dTotal = dTotal;
        this.partAmount = partAmount;
        
        
        m_jTendered.reset();
        m_jTendered.activate();
        
        printState();
        
    }
    public PaymentInfo executePayment() {
        PaymentInfoMagcard payinfo = new PaymentInfoMagcard(m_jOwner.getText(), m_jCard.getText(), 
        		m_jExpiration.getText(), this.transaction, m_dPaid, this.currency, m_jSecurity.getText());
        payinfo.setNote(m_jNotes.getText());
    	try {
    		payinfo.setEcheance((Date) Formats.TIMESTAMP.parseValue(jTxtGoalDate.getText()));
		} catch (BasicException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	
        return payinfo;
        /*        
        jlblMessage.setText(null);

        PaymentInfoMagcard payinfo = m_cardpanel.getPaymentInfoMagcard();

        m_paymentgateway.execute(payinfo);
        if (payinfo.isPaymentOK()) {
            return payinfo;
        } else {
            jlblMessage.setText(payinfo.getMessage());
            return null;
            }*/
    }
    public Component getComponent() {
        return this;
    }

    private void printState() {
        
        Double value = m_jTendered.getDoubleValue();
        if (value == null) {
            m_dPaid = partAmount;
        } else {
            m_dPaid = value;
        } 

        Formats.setAltCurrency(this.currency);
        m_jMoneyEuros.setText(Formats.CURRENCY.formatValue(new Double(m_dPaid)));
        
        int iCompare = RoundUtils.compare(m_dPaid, m_dTotal);
        
        // if iCompare > 0 then the payment is not valid
        m_notifier.setStatus(m_dPaid > 0.0 && iCompare <= 0, iCompare == 0);
    }
    
    private class RecalculateState implements PropertyChangeListener {
        public void propertyChange(PropertyChangeEvent evt) {
            printState();
        }
    }     
    
    private void initComponents() {
        setLayout(new java.awt.BorderLayout());
    	
        btnDateStart = new javax.swing.JButton();
        jLabelGoalDate = new javax.swing.JLabel();
        jTxtGoalDate = new javax.swing.JTextField();
        m_jSecurity = new JEditorStringNumber();
    	m_jCard = new JEditorStringNumber();
    	m_jOwner = new JEditorString();
    	m_jExpiration = new JEditorString();
    	chkDeffered = new JCheckBox(AppLocal.getIntString("Label.PostponedPayment"));
        
        
        jlblMessage = WidgetsBuilder.createLabel();
        jlblMessage.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        // Setup right part (inputContainer): keyboard
        inputContainer = new javax.swing.JPanel();
        m_jKeys = new fr.pasteque.pos.widgets.JEditorKeys();
        m_jTendered = new JEditorCurrencyPositive();
        jPanel1 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        
        jPanel1.setLayout(new javax.swing.BoxLayout(jPanel1, javax.swing.BoxLayout.Y_AXIS));
        jPanel1.add(m_jKeys);

        jPanel3.setBorder(javax.swing.BorderFactory.createEmptyBorder(5, 5, 5, 5));
        jPanel3.setLayout(new java.awt.BorderLayout());
        jPanel3.add(m_jTendered, java.awt.BorderLayout.CENTER);

        jPanel1.add(jPanel3);
        
        inputContainer.setLayout(new java.awt.BorderLayout());
        inputContainer.add(jPanel1, java.awt.BorderLayout.NORTH);

        // Setup left part (paymentInfoContainer): payment info (amount)
        paymentInfoContainer = new javax.swing.JPanel();
        GridBagConstraints constraints = new GridBagConstraints();
        paymentInfoContainer.setLayout(new GridBagLayout());
        givenLabel = WidgetsBuilder.createLabel(AppLocal.getIntString("Label.InputCash"));
        m_jMoneyEuros = WidgetsBuilder.createLabel();
        
        //paymentInfoContainer.setLayout(new BorderLayout());

        //JPanel changeContainer = new JPanel();
        //changeContainer.setLayout(new GridLayout(1, 2, 8 ,8));
        givenLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        //changeContainer.add(givenLabel);
        m_jMoneyEuros.setBackground(new java.awt.Color(153, 153, 255));
        m_jMoneyEuros.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        m_jMoneyEuros.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(javax.swing.UIManager.getDefaults().getColor("Button.darkShadow")), javax.swing.BorderFactory.createEmptyBorder(1, 4, 1, 4)));
        m_jMoneyEuros.setOpaque(true);
        //changeContainer.add(m_jMoneyEuros);
        //paymentInfoContainer.add(changeContainer, BorderLayout.NORTH);
        //paymentInfoContainer.add(jlblMessage, BorderLayout.SOUTH);
        
      	constraints.fill = GridBagConstraints.NONE;
      	constraints.ipady = 0;       
      	constraints.weighty = 0;   
      	constraints.anchor = GridBagConstraints.EAST; 
      	constraints.insets = new Insets(5,0,0,3);  
      	constraints.gridx = 2;       
      	constraints.gridwidth = 1;   
      	constraints.gridy = 0;       
      	
      	paymentInfoContainer.add(givenLabel,constraints);
      	
      	constraints.fill = GridBagConstraints.HORIZONTAL;
      	constraints.ipady = 0;       
      	constraints.weighty = 0;   
      	constraints.insets = new Insets(5,0,0,0); 
      	constraints.gridx = 3;       
      	constraints.gridwidth = 2;   
      	constraints.gridy = 0;       
      	
      	paymentInfoContainer.add(m_jMoneyEuros,constraints);
            

      	constraints.fill = GridBagConstraints.NONE;
      	constraints.ipady = 0;       
      	constraints.weighty = 0;
      	constraints.insets = new Insets(5,0,0,0); 
      	constraints.gridx = 2;       
      	constraints.gridwidth = 3;   
      	constraints.gridy = 1;       
      	
      	paymentInfoContainer.add(chkDeffered,constraints);
      	
        jLabelGoalDate = WidgetsBuilder.createLabel(AppLocal.getIntString("Label.GoalDate"));
        
        jTxtGoalDate.setPreferredSize(new java.awt.Dimension(200, 25));
        jTxtGoalDate.setMinimumSize(new java.awt.Dimension(120, 22));
        
        btnDateStart.setIcon(ImageLoader.readImageIcon("calendar.png"));
        btnDateStart.setPreferredSize(new java.awt.Dimension(50, 25));
        btnDateStart.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDateStartActionPerformed(evt);
            }
        });
        
      	constraints.fill = GridBagConstraints.NONE;
      	constraints.ipady = 0;       
      	constraints.weighty = 0; 
      	constraints.insets = new Insets(5,0,5,3);  
      	constraints.gridx = 1;       
      	constraints.gridwidth = 1;   
      	constraints.gridy = 2;       
      	
      	paymentInfoContainer.add(jLabelGoalDate,constraints);
        
      	constraints.fill = GridBagConstraints.HORIZONTAL; 
      	constraints.anchor = GridBagConstraints.CENTER; 
      	constraints.insets = new Insets(5,0,5,0);  
      	constraints.gridx = 2;       
      	constraints.gridwidth = 2;     
      	
      	paymentInfoContainer.add(jTxtGoalDate,constraints);
        
      	constraints.fill = GridBagConstraints.NONE;
      	constraints.anchor = GridBagConstraints.EAST;   
      	constraints.gridx = 4;       
      	constraints.gridwidth = 1;      
      	
      	paymentInfoContainer.add(btnDateStart,constraints);
      	
      	constraints.fill = GridBagConstraints.NONE;
      	constraints.ipady = 0;       
      	constraints.weighty = 0;
      	constraints.insets = new Insets(10,0,0,3);  
      	constraints.gridx = 1;       
      	constraints.gridwidth = 2;   
      	constraints.gridy = 3;       
      	
      	paymentInfoContainer.add( WidgetsBuilder.createLabel(AppLocal.getIntString("label.cardholder")),constraints);
        
      	m_jOwner.addEditorKeys(m_jKeys);
      	
      	constraints.fill = GridBagConstraints.HORIZONTAL;
      	constraints.ipady = 0;       
      	constraints.weighty = 0;
      	constraints.insets = new Insets(5,0,0,0); 
      	constraints.gridx = 3;       
      	constraints.gridwidth = 2;     
      	
      	paymentInfoContainer.add(m_jOwner,constraints);
      	
      	constraints.fill = GridBagConstraints.NONE;
      	constraints.ipady = 0;       
      	constraints.weighty = 0;
      	constraints.insets = new Insets(5,0,0,3);  
      	constraints.gridx = 1;       
      	constraints.gridwidth = 2;   
      	constraints.gridy = 4;       
      	
      	paymentInfoContainer.add( WidgetsBuilder.createLabel(AppLocal.getIntString("label.cardnumber")),constraints);
        
      	m_jCard.addEditorKeys(m_jKeys);
      	
      	constraints.fill = GridBagConstraints.HORIZONTAL;
      	constraints.ipady = 0;       
      	constraints.weighty = 0;
      	constraints.insets = new Insets(5,0,0,0); 
      	constraints.gridx = 3;       
      	constraints.gridwidth = 2;   
      	
      	paymentInfoContainer.add(m_jCard,constraints);
      	
      	constraints.fill = GridBagConstraints.NONE;
      	constraints.ipady = 0;       
      	constraints.weighty = 0;
      	constraints.insets = new Insets(5,0,0,3);  
      	constraints.gridx = 1;       
      	constraints.gridwidth = 2;   
      	constraints.gridy = 5;       
      	
      	paymentInfoContainer.add( WidgetsBuilder.createLabel(AppLocal.getIntString("label.cardexpdate")),constraints);
        
      	m_jExpiration.addEditorKeys(m_jKeys);
      	
      	constraints.fill = GridBagConstraints.HORIZONTAL;
      	constraints.ipady = 0;       
      	constraints.weighty = 0;
      	constraints.insets = new Insets(5,0,0,0); 
      	constraints.gridx = 3;       
      	constraints.gridwidth = 2;  
      	
      	paymentInfoContainer.add(m_jExpiration,constraints);
      	
      	constraints.fill = GridBagConstraints.NONE;
      	constraints.ipady = 0;       
      	constraints.weighty = 0;
      	constraints.insets = new Insets(5,0,0,3);  
      	constraints.gridx = 1;       
      	constraints.gridwidth = 2;   
      	constraints.gridy = 6; 
      	
      	paymentInfoContainer.add( WidgetsBuilder.createLabel(AppLocal.getIntString("label.cardsecurity")),constraints);
        
      	m_jSecurity.addEditorKeys(m_jKeys);
      	
      	constraints.fill = GridBagConstraints.HORIZONTAL;
      	constraints.ipady = 0;       
      	constraints.weighty = 0;
      	constraints.insets = new Insets(5,0,0,0); 
      	constraints.gridx = 3;       
      	constraints.gridwidth = 2;   
      	
      	paymentInfoContainer.add(m_jSecurity,constraints);
      	
      	
      	constraints.fill = GridBagConstraints.NONE;
      	constraints.anchor = GridBagConstraints.CENTER; //bottom of space
      	constraints.insets = new Insets(10,0,0,0);  //top padding
      	constraints.gridx = 0;       //aligned with button left
      	constraints.gridwidth = 5;   //5 columns wide
      	constraints.gridy = 7;       //4th row
      	
      	paymentInfoContainer.add(WidgetsBuilder.createLabel(AppLocal.getIntString("Label.Notes")),constraints);
        
        m_jNotes = new javax.swing.JTextArea();
        m_jNotes.setFont(WidgetsBuilder.getFont(WidgetsBuilder.SIZE_SMALL));
        m_jNotes.setEditable(true);
        m_jNotes.setFocusable(true);
        m_jNotes.setRows(3);
      	m_jNotes.setLineWrap(true);
      	m_jNotes.setWrapStyleWord(true);
        //jPanelCenter.add(m_jNotes, BorderLayout.CENTER);
      	
      	constraints.fill = GridBagConstraints.BOTH;
      	constraints.ipady = 0;       //reset to default
      	constraints.weighty = 1.0;   //request any extra vertical space
      	constraints.anchor = GridBagConstraints.PAGE_END; //bottom of space
      	constraints.insets = new Insets(5,0,30,0);  //top padding
      	constraints.gridx = 0;       //aligned with button left
      	constraints.gridwidth = 5;   //5 columns wide
      	constraints.gridy = 8;       //4th row
      	constraints.gridheight = 3;
      	
      	paymentInfoContainer.add(m_jNotes,constraints);
        
        // Add all to main container
        add(paymentInfoContainer, java.awt.BorderLayout.CENTER);
        add(inputContainer, java.awt.BorderLayout.LINE_END);
    }
    
    public void clearComponents() {
        jTxtGoalDate.setText("");
        m_jSecurity.setText("");
    	m_jCard.setText("");
    	m_jOwner.setText("");
    	m_jExpiration.setText("");
    	m_jNotes.setText("");
    }
    
    private void btnDateStartActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDateStartActionPerformed
        Date date;
            try {
                date = (Date) Formats.TIMESTAMP.parseValue(jTxtGoalDate.getText());
            } catch (BasicException e) {
                date = null;
            }        
            date = JCalendarDialog.showCalendarTimeHours(this, date);
            if (date != null) {
            	jTxtGoalDate.setText(Formats.TIMESTAMP.formatValue(date));
            }
    }//GEN-LAST:event_btnDateStartActionPerformed

    private JLabel givenLabel;
    private JPanel jPanel1;
    private JPanel inputContainer;
    private JPanel jPanel3;
    private JPanel paymentInfoContainer;
    private JEditorKeys m_jKeys;
    private JLabel m_jMoneyEuros;
    private JEditorCurrencyPositive m_jTendered;
    private JLabel jlblMessage;
    private JTextArea m_jNotes;
	private JEditorStringNumber m_jCard;
	private JEditorString m_jOwner;
	private JEditorString m_jExpiration;
	private JEditorStringNumber m_jSecurity;
	private javax.swing.JLabel jLabelGoalDate;
	private javax.swing.JTextField jTxtGoalDate;
	private javax.swing.JButton btnDateStart;
	private javax.swing.JCheckBox chkDeffered;

}
