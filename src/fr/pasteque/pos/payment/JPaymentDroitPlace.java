package fr.pasteque.pos.payment;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.JTextArea;

import fr.pasteque.format.Formats;
import fr.pasteque.pos.admin.CurrencyInfo;
import fr.pasteque.pos.customers.CustomerInfoExt;
import fr.pasteque.pos.forms.AppLocal;
import fr.pasteque.pos.util.RoundUtils;
import fr.pasteque.pos.widgets.JEditorCurrencyPositive;
import fr.pasteque.pos.widgets.JEditorString;
import fr.pasteque.pos.widgets.WidgetsBuilder;

public class JPaymentDroitPlace extends javax.swing.JPanel implements JPaymentInterface {
    
    /**
	 * 
	 */
	private static final long serialVersionUID = -7395697319642651230L;

	private JPaymentNotifier m_notifier;

    private double m_dPaid;
    private double m_dTotal;
    private double partAmount;
    private CurrencyInfo currency;
    
    /** Creates new form JPaymentCash */
    public JPaymentDroitPlace(JPaymentNotifier notifier) {
        
        m_notifier = notifier;
        
        initComponents();  
        
        m_jTendered.addPropertyChangeListener("Edition", new RecalculateState());
        m_jTendered.addEditorKeys(m_jKeys);
    }
    
    public void activate(CustomerInfoExt customerext, double dTotal,
            double partAmount, CurrencyInfo currency, String transID) {
        
        m_dTotal = dTotal;
        this.partAmount = partAmount;
        this.currency = currency;
        
        m_jTendered.reset();
        m_jTendered.activate();
        
        printState();
        
    }
    public PaymentInfo executePayment() {
    	PaymentInfo retour =  new PaymentInfoTicket(m_dPaid, this.currency, "ddp");  
    	retour.setNote(m_jNotes.getText());
    	retour.setTransactionID(m_jReference.getText());
    	
    	return retour;
    }
    public Component getComponent() {
        return this;
    }

    private void printState() {
        
        Double value = m_jTendered.getDoubleValue();
        if (value == null) {
            m_dPaid = this.partAmount;
        } else {
            m_dPaid = value;
        } 

        Formats.setAltCurrency(this.currency);
        m_jMoneyEuros.setText(Formats.CURRENCY.formatValue(new Double(m_dPaid)));
        
        int iCompare = RoundUtils.compare(m_dPaid, m_dTotal);
        
        // if iCompare > 0 then the payment is not valid
        m_notifier.setStatus(m_dPaid > 0.0 && iCompare <= 0, iCompare == 0);
    }
    
    private class RecalculateState implements PropertyChangeListener {
        public void propertyChange(PropertyChangeEvent evt) {
            printState();
        }
    }     
    
    private void initComponents() {
        setLayout(new java.awt.BorderLayout());
    	m_jReference = new JEditorString();
    	

        // Setup right part (inputContainer): keyboard
        inputContainer = new javax.swing.JPanel();
        m_jKeys = new fr.pasteque.pos.widgets.JEditorKeys();
        m_jTendered = new JEditorCurrencyPositive();
        jPanel1 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        
        jPanel1.setLayout(new javax.swing.BoxLayout(jPanel1, javax.swing.BoxLayout.Y_AXIS));
        jPanel1.add(m_jKeys);

        jPanel3.setBorder(javax.swing.BorderFactory.createEmptyBorder(5, 5, 5, 5));
        jPanel3.setLayout(new java.awt.BorderLayout());
        jPanel3.add(m_jTendered, java.awt.BorderLayout.CENTER);

        jPanel1.add(jPanel3);
        
        inputContainer.setLayout(new java.awt.BorderLayout());
        inputContainer.add(jPanel1, java.awt.BorderLayout.NORTH);

        // Setup left part (paymentInfoContainer): payment info (amount)
        paymentInfoContainer = new javax.swing.JPanel();
        GridBagConstraints constraints = new GridBagConstraints();
        paymentInfoContainer.setLayout(new GridBagLayout());
        
        givenLabel = WidgetsBuilder.createLabel(AppLocal.getIntString("Label.InputCash"));
        m_jMoneyEuros = WidgetsBuilder.createLabel();
        
        //paymentInfoContainer.setLayout(new BorderLayout());

        //JPanel changeContainer = new JPanel();
        //changeContainer.setLayout(new GridLayout(1, 2, 8 ,8));
        givenLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        //changeContainer.add(givenLabel);
        m_jMoneyEuros.setBackground(new java.awt.Color(153, 153, 255));
        m_jMoneyEuros.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        m_jMoneyEuros.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(javax.swing.UIManager.getDefaults().getColor("Button.darkShadow")), javax.swing.BorderFactory.createEmptyBorder(1, 4, 1, 4)));
        m_jMoneyEuros.setOpaque(true);
        //changeContainer.add(m_jMoneyEuros);
        //paymentInfoContainer.add(changeContainer, BorderLayout.NORTH);
        
      	constraints.fill = GridBagConstraints.NONE;
      	constraints.ipady = 0;       
      	constraints.weighty = 0;   
      	constraints.anchor = GridBagConstraints.EAST; 
      	constraints.insets = new Insets(5,0,0,3);  
      	constraints.gridx = 1;       
      	constraints.gridwidth = 1;   
      	constraints.gridy = 0;       
      	
      	paymentInfoContainer.add(givenLabel,constraints);
      	
      	constraints.fill = GridBagConstraints.HORIZONTAL;
      	constraints.ipady = 0;       
      	constraints.weighty = 0;   
      	constraints.weightx = 1.0;   //request any extra horizontal space
      	constraints.insets = new Insets(5,0,0,0); 
      	constraints.gridx = 2;       
      	constraints.gridwidth = 3;   
      	constraints.gridy = 0;       
      	
      	paymentInfoContainer.add(m_jMoneyEuros,constraints);
      	
      	constraints.fill = GridBagConstraints.NONE;
      	constraints.ipady = 0;       
      	constraints.weighty = 0;
      	constraints.weightx = 0;  
      	constraints.insets = new Insets(5,0,0,3);  
      	constraints.gridx = 1;       
      	constraints.gridwidth = 1;   
      	constraints.gridy = 1;       
      	
      	paymentInfoContainer.add(WidgetsBuilder.createLabel(AppLocal.getIntString("Label.Reference")),constraints);
        
        m_jReference.addEditorKeys(m_jKeys);
        
      	constraints.fill = GridBagConstraints.HORIZONTAL;
      	constraints.ipady = 0;       
      	constraints.weighty = 0;
      	constraints.weightx = 1.0;   //request any extra horizontal space
      	constraints.insets = new Insets(5,0,0,0); 
      	constraints.gridx = 2;       
      	constraints.gridwidth = 3;   
      	constraints.gridy = 1;       
      	
      	paymentInfoContainer.add(m_jReference,constraints);
      	   	
      	
      	constraints.fill = GridBagConstraints.NONE;
      	constraints.anchor = GridBagConstraints.CENTER; //bottom of space
      	constraints.insets = new Insets(10,0,0,0);  //top padding
      	constraints.gridx = 1;       //aligned with button left
      	constraints.gridwidth = 5;   //5 columns wide
      	constraints.gridy = 5;       //4th row
      	
      	paymentInfoContainer.add(WidgetsBuilder.createLabel(AppLocal.getIntString("Label.Notes")),constraints);
        
        m_jNotes = new javax.swing.JTextArea();
        m_jNotes.setFont(WidgetsBuilder.getFont(WidgetsBuilder.SIZE_SMALL));
        m_jNotes.setEditable(true);
        m_jNotes.setFocusable(true);
        m_jNotes.setRows(3);
      	m_jNotes.setLineWrap(true);
      	m_jNotes.setWrapStyleWord(true);
        //jPanelCenter.add(m_jNotes, BorderLayout.CENTER);
      	
      	constraints.fill = GridBagConstraints.BOTH;
      	constraints.ipady = 0;       //reset to default
      	constraints.weighty = 1.0;   //request any extra vertical space
      	constraints.weightx = 1.0;   //request any extra vertical space
      	constraints.anchor = GridBagConstraints.PAGE_END; //bottom of space
      	constraints.insets = new Insets(5,0,30,0);  //top padding
      	constraints.gridx = 1;       //aligned with button left
      	constraints.gridwidth = 5;   //5 columns wide
      	constraints.gridy = 6;       //4th row
      	constraints.gridheight = 3;
      	
      	paymentInfoContainer.add(m_jNotes,constraints);
        
        // Add all to main container
        add(paymentInfoContainer, java.awt.BorderLayout.CENTER);
        add(inputContainer, java.awt.BorderLayout.LINE_END);
    }
    
    
    public void clearComponents() {
    	m_jReference.setText("");
    	m_jNotes.setText("");
    }
    
    
    private javax.swing.JLabel givenLabel;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel inputContainer;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel paymentInfoContainer;
    private fr.pasteque.pos.widgets.JEditorKeys m_jKeys;
    private javax.swing.JLabel m_jMoneyEuros;
    private JEditorCurrencyPositive m_jTendered;
    private JTextArea m_jNotes;
	private JEditorString m_jReference;


}
