//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.config;

import fr.pasteque.data.user.DirtyManager;
import fr.pasteque.pos.config.JPanelConfigGeneral.KnownConnections;
import fr.pasteque.pos.config.JPanelConfigGeneral.KnownPorts;
import fr.pasteque.pos.forms.AppLocal;
import fr.pasteque.pos.util.StringParser;
import fr.pasteque.pos.widgets.WidgetsBuilder;

import java.awt.Component;

/**
 *
 * @author adrian
 */
public class ParametersPrinter extends javax.swing.JPanel implements ParametersConfig {

	public class PrinterNames {
		public static final String CHECK = "check";
		public static final String RECEIPT = "receipt";
		public static final String OTHER = "standard";
	}
    /**
	 * 
	 */
	private static final long serialVersionUID = -4216128684720802290L;
	private String othersizename = PrinterNames.OTHER;

    /** Creates new form ParametersPrinter */
    public ParametersPrinter(String [] printernames , boolean choice) {
        initComponents();
        
        if(choice) {
	        jPrinters.addItem("(Default)");
	        jPrinters.addItem("(Show dialog)");
        }
        for (String name : printernames) {
            jPrinters.addItem(name);
        }
        for (KnownConnections field : KnownConnections.values()) {
        	jConnect.addItem( field.toString());
        }
        for (KnownPorts field : KnownPorts.values()) {
        	jPorts.addItem( field.toString());
        }
        
        toggleConnect(!choice);
        jConnect.setSelectedItem(KnownConnections.DRIVER.toString());

    }
    
    public void toggleConnect(boolean visible) {
        jConnect.setVisible(visible);
        jPorts.setVisible(visible);
        jlblConnPrinter.setVisible(visible);
        jlblPrinterPort.setVisible(visible);
    }

    //TODO plus fin que ça on doit pouvoir changer le port dans certain cas 
    //On doit aussi pouvoir agir sur le fonctionnement de l'affichage des drivers
    
    public Component getComponent() {
        return this;
    }

    public void addDirtyManager(DirtyManager dirty) {
        jPrinters.addActionListener(dirty);
        jReceiptPrinter.addActionListener(dirty);
        jCheckPrinter.addActionListener(dirty);
    }

    public void setParameters(StringParser p) {
    	String connect = p.nextToken(',');
        String printer = p.nextToken(',');
        String sizename = p.nextToken(',');

        jConnect.setSelectedItem(connect);
        
        if(sizename.isEmpty()) {
        	//Rétro Compatibilité
        	if(connect.equals(jConnect.getSelectedItem())) {
        		sizename = PrinterNames.OTHER ;
        	} else {
        	
        		sizename = printer;
        		printer = connect;
        		//Pas forcément nécessaire c'est la selection par défaut
        		connect = KnownConnections.DRIVER.toString();
        	}
        }
    	
    	
        jPrinters.setSelectedItem(printer);
        jPorts.setSelectedItem(printer);
        
        jReceiptPrinter.setSelected(PrinterNames.RECEIPT.equals(sizename));
        jCheckPrinter.setSelected(PrinterNames.CHECK.equals(sizename));
        othersizename = PrinterNames.RECEIPT.equals(sizename) || PrinterNames.CHECK.equals(sizename) ? PrinterNames.OTHER : sizename;
    }

    public String getParameters() {
        return comboValue(jConnect.getSelectedItem()) + "," + printerValue() + "," + boolValue(jReceiptPrinter.isSelected() , jCheckPrinter.isSelected());
    }
    
    public String printerValue() {
    	String retour = "";
    	if(KnownConnections.DRIVER.toString().equals(jConnect.getSelectedItem())) {
    		retour += comboValue(jPrinters.getSelectedItem());
    	} else {
    		retour += comboValue(jPorts.getSelectedItem());
    	}
    	return retour;
    }

    private static String comboValue(Object value) {
        return value == null ? "" : value.toString();
    }

    private String boolValue(boolean receiptvalue, boolean checkValue) {
        return receiptvalue ? PrinterNames.RECEIPT : checkValue ? PrinterNames.CHECK : othersizename;
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPrinters = new javax.swing.JComboBox<String>();
        jPorts = new javax.swing.JComboBox<String>();
        jConnect = new javax.swing.JComboBox<String>();
        jReceiptPrinter = new javax.swing.JCheckBox();
        jCheckPrinter = new javax.swing.JCheckBox();
        
        jPorts.setEditable(true);
        
        
        jlblConnPrinter = WidgetsBuilder.createLabel(AppLocal.getIntString("label.machinedisplayconn"));

        jlblPrinterPort = WidgetsBuilder.createLabel(AppLocal.getIntString("label.machineprinterport"));
        
        jReceiptPrinter.setSelected(true);
        jReceiptPrinter.setText(AppLocal.getIntString("label.receiptprinter")); // NOI18N
        
        jCheckPrinter.setSelected(false);
        jCheckPrinter.setText(AppLocal.getIntString("label.checkprinter")); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
//        layout.setHorizontalGroup(
//            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
//            .addGap(0, 430, Short.MAX_VALUE)
//            .addGroup(layout.createSequentialGroup()
//                .addContainerGap()
//                .addComponent(jPrinters, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)
//                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
//                .addComponent(jReceiptPrinter)
//                .addContainerGap(129, Short.MAX_VALUE)
//                .addComponent(jCheckPrinter))
////            .addGap(0, 430, Short.MAX_VALUE)
////            .addGroup(layout.createSequentialGroup()
////                .addContainerGap()
////                .addComponent(jlblConnPrinter)
////                .addComponent(jConnect)
////                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
////                .addComponent(jlblPrinterPort)
////                .addComponent(jPorts))
//                
//        );
        
        layout.setHorizontalGroup(
        		layout.createSequentialGroup()
        		.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        				.addComponent(jPrinters , javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)
        				.addComponent(jReceiptPrinter)
        				)
        		.addGap(0, 30, Short.MAX_VALUE)		
        		.addGroup(layout.createSequentialGroup()
        				.addComponent(jlblConnPrinter)
        				.addGap(0, 10, Short.MAX_VALUE)
        				.addComponent(jConnect)
        				)
        	    .addGap(0, 30, Short.MAX_VALUE)
        	    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                		.addGroup(layout.createSequentialGroup()
                				.addComponent(jlblPrinterPort)
                				.addGap(0, 10, Short.MAX_VALUE)
                				.addComponent(jPorts)
                				)
        				.addComponent(jCheckPrinter)
        				)
        		);
        
        layout.setVerticalGroup(
        		layout.createSequentialGroup()
        		.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
        				.addComponent(jPrinters)
        				.addComponent(jlblConnPrinter)
        				.addComponent(jConnect)
        				.addComponent(jlblPrinterPort)
        				.addComponent(jPorts))
        	    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
        	    		.addComponent(jReceiptPrinter)
        	    		.addComponent(jCheckPrinter))
        		);
//        layout.setVerticalGroup(
//            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
//            .addGap(0, 61, Short.MAX_VALUE)
//            .addGroup(layout.createSequentialGroup()
//                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
//                    .addComponent(jPrinters, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
//                    .addComponent(jReceiptPrinter)
//                    .addComponent(jCheckPrinter))
//                .addContainerGap(37, Short.MAX_VALUE))
////            .addGroup(layout.createSequentialGroup()
////                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
////                    .addComponent(jlblConnPrinter)
////                    .addComponent(jConnect)
////                    .addComponent(jlblPrinterPort)
////                    .addComponent(jPorts))
////                .addContainerGap(37, Short.MAX_VALUE))
//        );
        
        
        jConnect.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jConnectActionPerformed(evt);
            }
        });
        
    }// </editor-fold>//GEN-END:initComponents
    
    private void jConnectActionPerformed(java.awt.event.ActionEvent evt) {
        if(KnownConnections.DRIVER.toString().equals(jConnect.getSelectedItem())) {
        	jPrinters.setVisible(true);
            jPorts.setVisible(false);
            jlblPrinterPort.setVisible(false);
        } else {
        	jPrinters.setVisible(false);	
            jPorts.setVisible(true);
            jlblPrinterPort.setVisible(true);
        }
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> jPrinters;
    private javax.swing.JComboBox<String> jConnect;
    private javax.swing.JComboBox<String> jPorts;
    private javax.swing.JCheckBox jReceiptPrinter;
    private javax.swing.JCheckBox jCheckPrinter;
    private javax.swing.JLabel jlblConnPrinter;
    private javax.swing.JLabel jlblPrinterPort;
    // End of variables declaration//GEN-END:variables

}
