//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2007-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.ticket;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;

import fr.pasteque.format.DoubleUtils;
import fr.pasteque.format.Formats;

public class TicketTaxInfo implements Serializable {

	private static final long serialVersionUID = -3223753930296653779L;

	private TaxInfo tax;
	// Mtt HT
	private double subTotal;
	// Mtt taxe
	private double taxTotal;

	/** Creates a new instance of TicketTaxInfo */
	public TicketTaxInfo(TaxInfo tax) {
		this.tax = tax;
		this.subTotal = 0.0;
		this.taxTotal = 0.0;
	}

	public TaxInfo getTaxInfo() {
		return tax;
	}

	public void add(double dValue) {
		dValue = DoubleUtils.fixDecimals(dValue);
		// soit on est sur un taxe a montant fixe (sans assiette)
		// soit sur une taxe avec un taux
		if (tax.getAmount() != null && tax.getAmount() != 0D) {
			subTotal += dValue;
			taxTotal += dValue;
		} else {
			subTotal += dValue;
			taxTotal = DoubleUtils.fixDecimals(subTotal * tax.getRate());
		}
	}

	public double getSubTotal() {
		return subTotal;
	}

	public double getTax() {
		return taxTotal;
	}

	public double getTotal() {
		return subTotal + taxTotal;
	}

	public String printSubTotal() {
		Formats.setAltRoundingMode(null);
		return Formats.CURRENCY.formatValue(new Double(getSubTotal()));
	}

	public String printTax() {
		return Formats.CURRENCY.formatValue((new BigDecimal(getTax())).setScale(7, RoundingMode.HALF_UP).doubleValue());
	}

	public String printTotal() {
		return Formats.CURRENCY.formatValue(new Double(getTotal()));
	}
}
