package fr.pasteque.pos.ticket;

import java.io.Serializable;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import fr.pasteque.format.DateUtils;

public class SalesLocationInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3757173356684040474L;
	
	private String tourName;
	private int tourId;
	private long id;
	private String city;
	private String inseeNb;
	private int pubNb;
	private boolean paid;
	private double fee;
	private Date startDate;
	private Date endDate;
	private List<String> salesSessions = new ArrayList<String>();
	private Date nextStartDate;
	

	public String getTourName() {
		return tourName;
	}
	
	public Date getNextStartDate() {
		return nextStartDate;
	}
	
	public void setNextStartDate(Date nextStartDate) {
		this.nextStartDate = nextStartDate;
	}

	public int getTourId() {
		return tourId;
	}

	public long getId() {
		return id;
	}

	public String getCity() {
		return city;
	}

	public String getInseeNb() {
		return inseeNb;
	}

	public int getPubNb() {
		return pubNb;
	}

	public boolean isPaid() {
		return paid;
	}

	public double getFee() {
		return fee;
	}

	public Date getStartDate() {
		return startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public SalesLocationInfo(JSONObject o , String tourName) {
		this.tourName = o.isNull("tourName") ? o.getString("tourName") : tourName;
		this.tourId = o.getInt("tourId");
		this.id = o.getLong("id");
		this.city = o.getString("city");
		this.inseeNb = o.getString("inseeNb"); 
		this.pubNb = o.getInt("pubNb");
		this.paid = o.getBoolean("paid");
		this.fee = o.getDouble("fee");
		this.startDate = DateUtils.readMilliTimestamp(o.getLong("startDate"));
		this.endDate = DateUtils.readMilliTimestamp(o.getLong("endDate"));
		if( ! o.isNull("nextStartDate")) {
			try {
				this.nextStartDate = DateUtils.readMilliTimestamp(o.getLong("nextStartDate"));
			} 
			catch(Exception e) {
				this.nextStartDate = null;
			}
		}
	}
	
	public String toString() {
		DateFormat df = DateFormat.getDateTimeInstance();
		return this.city + " - " + df.format(startDate) + " - " + df.format(endDate) ;
	}
	
	//EDU - Obsolète Utile lorsqu'on passait l'intégralité des lieux de ventes dans l'objet
	public static ArrayList<SalesLocationInfo> extractList(JSONObject o) {
		ArrayList<SalesLocationInfo> returnedList = new ArrayList<SalesLocationInfo>();
		JSONArray toursList = o.getJSONArray("tours");
		JSONArray citiesList;
		JSONObject tpmJSON;
		int maxTours = toursList.length();
		int maxCities = 0 ;
		while(0 < maxTours-- ) {
			tpmJSON = toursList.getJSONObject(maxTours).getJSONObject("tour");
			citiesList = tpmJSON.getJSONArray("salesLocations");
			maxCities = citiesList.length();
			while(0 < maxCities--) {
				returnedList.add(new SalesLocationInfo(citiesList.getJSONObject(maxCities) , tpmJSON.getString("name")) );
			}
		}

        Collections.sort(returnedList, new SalesLocationInfoStartDateComparator());
        
		return returnedList;
	}
	
	public JSONObject toJSON() {
        JSONObject o = new JSONObject();
        
        		
        if (this.city == null) {
            o.put("city", JSONObject.NULL);
        } else {
        	o.put("city", this.city);
        }
        if (this.inseeNb == null) {
            o.put("inseeNb", JSONObject.NULL);
        } else {
        	o.put("inseeNb", this.inseeNb);
        }
        if (this.startDate == null) {
            o.put("startDate", JSONObject.NULL);
        } else {
        	o.put("startDate", this.startDate.getTime());
        }
        if (this.endDate == null) {
            o.put("endDate", JSONObject.NULL);
        } else {
        	o.put("endDate", this.endDate.getTime());
        }
        if (this.nextStartDate == null) {
            o.put("nextStartDate", JSONObject.NULL);
        } else {
        	o.put("nextStartDate", this.nextStartDate.getTime());
        }
        o.put("tourId", this.tourId);
        o.put("id", this.id);
        o.put("pubNb", this.pubNb);
        o.put("paid", this.paid);
        o.put("fee", this.fee);
		
        //TODO : Vérifier que tout est OK
        if (this.salesSessions == null) {
            o.put("salesSessions", JSONObject.NULL);
        } else {
        	o.put("salesSessions", this.salesSessions);
        }
        
        return o;
	}
}

class SalesLocationInfoStartDateComparator implements Comparator<SalesLocationInfo> {
    public int compare(SalesLocationInfo sL1, SalesLocationInfo sL2) {
        return sL1.getStartDate().compareTo(sL2.getEndDate());
    }
}
