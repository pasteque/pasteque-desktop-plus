//    POS-Tech
//    Based upon Openbravo POS
//
//    Copyright (C) 2007-2009 Openbravo, S.L.
//                       2012 Scil (http://scil.coop)
//
//    This file is part of POS-Tech.
//
//    POS-Tech is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    POS-Tech is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with POS-Tech.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.ticket;

import java.util.*;
import java.io.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import fr.pasteque.pos.payment.PaymentInfo;
import fr.pasteque.format.DoubleUtils;
import fr.pasteque.format.Formats;
import fr.pasteque.basic.BasicException;
import fr.pasteque.data.loader.LocalRes;
import fr.pasteque.pos.admin.InseeInfo;
import fr.pasteque.pos.caching.InseeCache;
import fr.pasteque.pos.customers.CustomerInfoExt;
import fr.pasteque.pos.customers.DataLogicCustomers;
import fr.pasteque.pos.forms.AppLocal;
import fr.pasteque.pos.forms.AppUser;
import fr.pasteque.pos.forms.DataLogicSystem;
import fr.pasteque.pos.payment.PaymentInfoMagcard;
import fr.pasteque.pos.util.RoundUtils;
import fr.pasteque.pos.util.StringUtils;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author adrianromero
 */
public class TicketInfo implements Serializable {

	private static final long serialVersionUID = 2765650092387265178L;

	public static final int RECEIPT_NORMAL = 0;
	public static final int RECEIPT_REFUND = 1;
	public static final int RECEIPT_PAYMENT = 2;
	
	public static int  ORDER ;

	private static DateFormat m_dateformat = new SimpleDateFormat("hh:mm");

	private String m_sId;
	private int tickettype;
	private long m_iTicketId;
	private java.util.Date m_dDate;
	private Properties attributes;
	private UserInfo m_User;
	private CustomerInfoExt m_Customer;
	private String m_sActiveCash;
	private List<TicketLineInfo> m_aLines;
	private List<PaymentInfo> payments;
	private List<TicketTaxInfo> taxes;
	//private String m_sResponse;
	private Integer customersCount;
	private Integer tariffAreaId;
	private Integer discountProfileId;
	private String inseeNum;
	private double discountRate;
	private double offersEconomy;
	private Long parentTicketId;
	private String address;
	private String m_sNote;

	private boolean openedPaymentDialog;
	
	private String dateProchainDeballage;
	
	
	/** Creates new TicketModel */
	public TicketInfo() {
		tickettype = RECEIPT_NORMAL;
		m_iTicketId = 0; // incrementamos
		m_dDate = new Date();
		attributes = new Properties();
		m_User = null;
		m_Customer = null;
		m_sActiveCash = null;
		m_aLines = new ArrayList<TicketLineInfo>(); // vacio de lineas

		payments = new ArrayList<PaymentInfo>();
		taxes = null;
		inseeNum = null;
		//m_sResponse = null;
		offersEconomy = 0D;
		parentTicketId = null;
		address = "";
		m_sNote = "";
		ORDER =  0;
	}

	//Attention les lignes de composants ne sont pas dans le JSON
	public TicketInfo(JSONObject o) throws BasicException {
		this.m_sNote = "";
		this.m_sId = o.getString("id");
		this.m_iTicketId = o.getLong("ticketId");
		this.m_dDate = new Date(o.getLong("date"));
		this.m_sActiveCash = o.getString("cashId");
		updateAddress();
		DataLogicSystem dlSystem = new DataLogicSystem();
		AppUser user = dlSystem.getPeople(o.getString("userId"));
		String locationId = null;
		if(m_sActiveCash != null && !m_sActiveCash.isEmpty()) {
			CashSession cashSession;
			
			try {
				cashSession = dlSystem.getCashSessionById(m_sActiveCash);
			
				CashRegisterInfo cash = dlSystem.getCashRegister(cashSession.getCashRegisterId());
				locationId = cash.getLocationId();
			} catch(Exception ex) {
				locationId = null;
			}
		}

		this.m_User = new UserInfo(user.getId(), user.getName());
		if (!o.isNull("customerId")) {
			DataLogicCustomers dlCust = new DataLogicCustomers();
			this.m_Customer = dlCust.getCustomer(o.getString("customerId"));
		}
		this.tickettype = o.getInt("type");
		if (!o.isNull("custCount")) {
			this.customersCount = o.getInt("custCount");
		}
		if (!o.isNull("tariffAreaId")) {
			this.tariffAreaId = o.getInt("tariffAreaId");
		}
		if (!o.isNull("discountProfileId")) {
			this.discountProfileId = o.getInt("discountProfileId");
		}
		if (!o.isNull("inseeNum")) {
			this.inseeNum = o.getString("inseeNum");
		}
		if (!o.isNull("parentTicketId")) {
			this.parentTicketId = o.getLong("parentTicketId");
		}
		this.discountRate = o.getDouble("discountRate");
		this.m_aLines = new ArrayList<TicketLineInfo>();
		JSONArray jsLines = o.getJSONArray("lines");
		for (int i = 0; i < jsLines.length(); i++) {
			JSONObject jsLine = jsLines.getJSONObject(i);
			TicketLineInfo line = new TicketLineInfo(jsLine);
			line.setLocationId(locationId);
			this.m_aLines.add(line);
		}
		this.payments = new ArrayList<PaymentInfo>();
		JSONArray jsPayments = o.getJSONArray("payments");
		for (int i = 0; i < jsPayments.length(); i++) {
			JSONObject jsPay = jsPayments.getJSONObject(i);
			this.payments.add(PaymentInfo.readJSON(jsPay));
		}
		if (!o.isNull("note")) {
			this.m_sNote = o.getString("note");
		}
		this.attributes = new Properties();
	}

	public JSONObject toJSON() {
		JSONObject o = new JSONObject();
		if (this.m_sId != null) {
			o.put("id", this.m_sId);
		}
		o.put("date", this.m_dDate.getTime());
		o.put("userId", m_User.getId());
		if (this.m_Customer != null) {
			o.put("customerId", this.m_Customer.getId());
		} else {
			o.put("customerId", JSONObject.NULL);
		}
		o.put("type", this.tickettype);
		o.put("ticketId", this.m_iTicketId);
		if (this.customersCount != null) {
			o.put("custCount", this.customersCount);
		} else {
			o.put("custCount", JSONObject.NULL);
		}
		if (this.tariffAreaId != null) {
			o.put("tariffAreaId", this.tariffAreaId);
		} else {
			o.put("tariffAreaId", JSONObject.NULL);
		}
		if (this.discountProfileId != null) {
			o.put("discountProfileId", this.discountProfileId);
		} else {
			o.put("discountProfileId", JSONObject.NULL);
		}
		//EDU : Vendredi 08 Avril 2016 18h - Andrezieux s'est retrouvé avec un nombre non fini
		//On patch
		if(Double.isNaN(this.discountRate) || Double.isInfinite(this.discountRate)) {
			o.put("discountRate", 1D);
		} else {
			o.put("discountRate", this.discountRate);
		}
		if (this.inseeNum != null) {
			o.put("inseeNum", this.inseeNum);
		} else {
			o.put("inseeNum", JSONObject.NULL);
		}
		if (this.parentTicketId != null) {
			o.put("parentTicketId", this.parentTicketId);
		} else {
			o.put("parentTicketId", JSONObject.NULL);
		}
		if(m_sNote != null && !m_sNote.isEmpty()) {
			o.put("note", this.m_sNote);
		} else {
			o.put("note", JSONObject.NULL);
		}
		JSONArray lines = new JSONArray();
		for (TicketLineInfo l : this.m_aLines) {
			if(! l.isSubproduct()) {
				lines.put(l.toJSON());
			} else {
				//Gérer le cas où on a une commande perdue dans un produit composé
				if(l.isOrder() && l.parent != null && !l.parent.isOrder()) {
					TicketLineInfo copy = l.copyTicketLine();
					copy.setSubproduct(false);
					copy.setMultiply(copy.getMultiply()*-1);
					copy.toggleOrder();
					copy.setParent(null);
					lines.put(copy.toJSON());
					//On fait une copie - on passe la quantité en retour - on passe en livré-recep
					copy.toggleOrder();
					copy.setMultiply(copy.getMultiply()*-1);
					lines.put(copy.toJSON());
					//On repasse la quantité en + - on repasse en commande
				}
				//Si l.parent == null ? cas arrivé le 15/10/2016 sur le camion 16
				//On n'a plus que le parent dans la liste.
			}
		}

		o.put("lines", lines);
		JSONArray payments = new JSONArray();
		for (PaymentInfo p : this.payments) {
			payments.put(p.toJSON());
		}
		o.put("payments", payments);
		return o;
	}

	/** Serialize as shared ticket */
	public byte[] serialize() throws IOException {
		ByteArrayOutputStream bos = new ByteArrayOutputStream(2048);
		ObjectOutputStream out = new ObjectOutputStream(bos);
		out.writeObject(m_sId);
		out.writeInt(tickettype);
		out.writeLong(m_iTicketId);
		out.writeObject(m_Customer);
		out.writeObject(m_dDate);
		out.writeObject(attributes);
		out.writeObject(m_aLines);
		out.writeObject(this.customersCount);
		out.writeObject(this.tariffAreaId);
		out.writeObject(this.discountProfileId);
		out.writeObject(this.inseeNum);
		out.writeDouble(this.discountRate);
		out.writeDouble(this.offersEconomy);
		out.writeObject(this.parentTicketId);
		out.writeObject(this.address);
		out.writeObject(this.m_sNote);
		out.flush();
		byte[] data = bos.toByteArray();
		out.close();
		return data;
	}

	/** Deserialize as shared ticket */
	@SuppressWarnings("unchecked")
	public TicketInfo(byte[] data) throws IOException {
		ByteArrayInputStream bis = new ByteArrayInputStream(data);
		ObjectInputStream in = new ObjectInputStream(bis);
		try {
			m_sId = (String) in.readObject();
			tickettype = in.readInt();
			m_iTicketId = in.readLong();
			m_Customer = (CustomerInfoExt) in.readObject();
			m_dDate = (Date) in.readObject();
			attributes = (Properties) in.readObject();
			m_aLines = (List<TicketLineInfo>) in.readObject();
			this.customersCount = (Integer) in.readObject();
			this.tariffAreaId = (Integer) in.readObject();
			this.discountProfileId = (Integer) in.readObject();
			this.inseeNum = (String) in.readObject();
			this.discountRate = in.readDouble();
			this.offersEconomy = in.readDouble();
			this.parentTicketId = (Long) in.readObject();
			this.address = (String) in.readObject();
			this.m_sNote = (String) in.readObject();
		} catch (ClassNotFoundException cnfe) {
			// Should never happen
			cnfe.printStackTrace();
		}
		in.close();
		m_User = null;
		m_sActiveCash = null;

		payments = new ArrayList<PaymentInfo>();
		taxes = null;
	}

	public TicketInfo copyTicket() {
		TicketInfo t = new TicketInfo();
		if (this.m_sId != null) {
			t.m_sId = new String(this.m_sId);
		}
		t.tickettype = tickettype;
		t.m_iTicketId = m_iTicketId;
		t.m_dDate = m_dDate;
		t.m_sActiveCash = m_sActiveCash;
		t.attributes = (Properties) attributes.clone();
		t.m_User = m_User;
		t.m_Customer = m_Customer;
		if (this.customersCount != null) {
			t.customersCount = this.customersCount;
		}
		t.m_aLines = new ArrayList<TicketLineInfo>();
		for (TicketLineInfo l : m_aLines) {
			t.m_aLines.add(l.copyTicketLine());
		}
		t.refreshLines();

		t.payments = new LinkedList<PaymentInfo>();
		for (PaymentInfo p : payments) {
			t.payments.add(p.copyPayment());
		}
		if (this.tariffAreaId != null) {
			t.tariffAreaId = new Integer(this.tariffAreaId);
		}
		if (this.discountProfileId != null) {
			t.discountProfileId = new Integer(this.discountProfileId);
		}
		t.inseeNum = this.inseeNum ;
		t.discountRate = this.discountRate;
		t.offersEconomy = this.offersEconomy;
		t.parentTicketId = this.parentTicketId;
		t.address = this.address;
		t.m_sNote = this.m_sNote;
		// taxes are not copied, must be calculated again.
		return t;
	}

	public String getId() {
		return m_sId;
	}

	public int getTicketType() {
		return tickettype;
	}

	public void setTicketType(int tickettype) {
		this.tickettype = tickettype;
	}

	public long getTicketId() {
		return m_iTicketId;
	}

	public void setTicketId(long iTicketId) {
		m_iTicketId = iTicketId;
		// refreshLines();
	}

	public String getName(Object info) {

		StringBuffer name = new StringBuffer();

		if (getCustomerId() != null) {
			name.append(m_Customer.toString());
			name.append(" - ");
		}

		if (info == null) {
			if (m_iTicketId == 0) {
				name.append("(" + m_dateformat.format(m_dDate) + " " + Long.toString(m_dDate.getTime() % 1000) + ")");
			} else {
				name.append(Long.toString(m_iTicketId));
			}
		} else {
			name.append(info.toString());
		}

		return name.toString();
	}

	public String getName() {
		return getName(null);
	}

	public java.util.Date getDate() {
		return m_dDate;
	}

	public void setDate(java.util.Date dDate) {
		m_dDate = dDate;
	}

	public UserInfo getUser() {
		return m_User;
	}

	public void setUser(UserInfo value) {
		m_User = value;
	}

	public CustomerInfoExt getCustomer() {
		return m_Customer;
	}

	public void setCustomer(CustomerInfoExt value) {
		m_Customer = value;
	}

	public String getCustomerId() {
		if (m_Customer == null) {
			return null;
		} else {
			return m_Customer.getId();
		}
	}

	@SuppressWarnings("deprecation")
	public String getTransactionID() {
		return (getPayments().size() > 0) ? (getPayments().get(getPayments().size() - 1)).getTransactionID() : StringUtils.getCardNumber(); // random
																																			// transaction
																																			// ID
	}

	public String getReturnMessage() {
		return ((getPayments().get(getPayments().size() - 1)) instanceof PaymentInfoMagcard) ? ((PaymentInfoMagcard) (getPayments()
				.get(getPayments().size() - 1))).getReturnMessage() : LocalRes.getIntString("button.ok");
	}

	public void setActiveCash(String value) {
		m_sActiveCash = value;
		updateAddress();
	}

	public String getActiveCash() {
		return m_sActiveCash;
	}

	public String getProperty(String key) {
		return attributes.getProperty(key);
	}

	public String getProperty(String key, String defaultvalue) {
		return attributes.getProperty(key, defaultvalue);
	}

	public void setProperty(String key, String value) {
		attributes.setProperty(key, value);
	}

	public Properties getProperties() {
		return attributes;
	}

	public Integer getCustomersCount() {
		return this.customersCount;
	}

	public void setCustomersCount(Integer count) {
		this.customersCount = count;
	}

	public boolean hasCustomersCount() {
		return this.customersCount != null;
	}

	public Integer getTariffArea() {
		return this.tariffAreaId;
	}

	public void setTariffArea(Integer value) {
		this.tariffAreaId = value;
	}

	public Integer getDiscountProfileId() {
		return this.discountProfileId;
	}

	public void setDiscountProfileId(Integer id) {
		this.discountProfileId = id;
	}

	public double getDiscountRate() {
		updateDiscountRate();
		return this.discountRate;
	}

	public void setDiscountRate(double discountRate) {
		this.discountRate = discountRate;
		updateDiscountRate();
	}

	public String getInseeNum() {
		return inseeNum;
	}

	public void setInseeNum(String inseeNum) {
		this.inseeNum = inseeNum;
	}

	public TicketLineInfo getLine(int index) {
		return m_aLines.get(index);
	}

	public void addLine(TicketLineInfo oLine) {

		oLine.setTicket(m_sId, m_aLines.size());
		m_aLines.add(oLine);
	}

	public void insertLine(int index, TicketLineInfo oLine) {
		m_aLines.add(index, oLine);
		refreshLines();
	}

	public void setLine(int index, TicketLineInfo oLine) {
		oLine.setTicket(m_sId, index);
		m_aLines.set(index, oLine);
	}

	public void removeLine(int index) {
		m_aLines.remove(index);
		refreshLines();
	}

	private void refreshLines() {
		for (int i = 0; i < m_aLines.size(); i++) {
			getLine(i).setTicket(m_sId, i);
		}
	}

	public int getLinesCount() {
		return m_aLines.size();
	}

	public double getArticlesCount() {
		double dArticles = 0.0;
		TicketLineInfo oLine;

		//EDU On considère le nombre de lignes sans les composants d'offre
		for (Iterator<TicketLineInfo> i = m_aLines.iterator(); i.hasNext();) {
			oLine = i.next();
			if( ! oLine.isSubproduct()) {
				if (oLine.isProductScale()) {
					if (oLine.getPrice() >= 0) {
						dArticles += 1;
					} else {
						dArticles -= 1;
					}
				} else {
					if (oLine.getPrice() >= 0) {
						dArticles += oLine.getMultiply();
					} else {
						dArticles -= oLine.getMultiply();
					}
				}
			}
		}

		return dArticles;
	}
	
	public double getArticlesDeliveredCount() {
		double dArticles = 0.0;
		TicketLineInfo oLine;

		//EDU On considère le nombre de produits livrés ( = sur le tapis de la caisse )
		for (Iterator<TicketLineInfo> i = m_aLines.iterator(); i.hasNext();) {
			oLine = i.next();
			
			if( oLine.hasNoChild() && !oLine.isOrder() && oLine.getProductID() != null ) {
				if (oLine.isProductScale()) {
					if (oLine.getPrice() >= 0) {
						dArticles += 1;
					} 
				} else {
					if (oLine.getPrice() >= 0 && oLine.getMultiply() > 0) {
						dArticles += oLine.getMultiply();
					} 
				}
			}
		}

		return dArticles;
	}
	
	public double getArticlesOrderedCount() {
		double dArticles = 0.0;
		TicketLineInfo oLine;

		//EDU On considère le nombre de produits livrés ( = sur le tapis de la caisse )
		for (Iterator<TicketLineInfo> i = m_aLines.iterator(); i.hasNext();) {
			oLine = i.next();
			
			if( oLine.hasNoChild() && oLine.isOrder() && oLine.getProductID() != null ) {
				if (oLine.isProductScale()) {
					if (oLine.getPrice() >= 0) {
						dArticles += 1;
					} 
				} else {
					if (oLine.getPrice() >= 0 && oLine.getMultiply() > 0) {
						dArticles += oLine.getMultiply();
					} 
				}
			}
		}

		return dArticles;
	}
	
	public int getTicketLinesCount() {
		int dLines = 0;
		TicketLineInfo oLine;

		//EDU On considère le nombre de lignes sans les composants d'offre
		for (Iterator<TicketLineInfo> i = m_aLines.iterator(); i.hasNext();) {
			oLine = i.next();
			if( ! oLine.isSubproduct()) {
				dLines++;
			}
		}

		return dLines;
	}

	/** le sous-total sera prix HT + Mtt Taxes (sans TVA) DEEE, ECOMOB, ... **/
	public double getSubTotal() {
		double sum = 0D;
		double taxes = 0D;
		for (TicketLineInfo line : m_aLines) {
			//On ne prend rien pour les subproducts (=elements d'une offre)
			if(!line.isSubproduct()) {
				sum += line.getSubValue();
				taxes += line.getTaxesVATExcludeAmount();
			}
			
		}
		return DoubleUtils.fixDecimals(sum * (1 - this.discountRate) + taxes);
	}

	/** Montant de la TVA, y compris la TVA des taxes DEEE, ECOMOB, ... **/
	public double getVAT() {
		double sum = 0D;
		double vatTaxes = 0D;
		// On ne peut pas avoir la TVA a partir des TicketTaxInfo, sauf a flaguer les TicketTaxInfo pour savoir si c'est une TVA
		// if (hasTaxesCalculated()) {
		// for (TicketTaxInfo tax : taxes) {
		// // Taxes are already rounded...
		// sum += tax.getTax();
		// }
		// } else {
		// for (TicketLineInfo line : m_aLines) {
		// sum += line.getVATAmount();
		// vatTaxes += line.getTaxesVATAmount();
		// }
		// }
		for (TicketLineInfo line : m_aLines) {
			//On ne prend rien pour les subproducts (=elements d'une offre)
			if(!line.isSubproduct()) {
				sum += line.getVATAmount();
				vatTaxes += line.getTaxesVATAmount();
			}
			
		}
		// S'il y a une remise, la partie taxe (DEEE, ECOMOB, ...) ne peut pas faire partie de la remise.
		if (this.discountRate != 0) {
			return DoubleUtils.fixDecimals((sum - vatTaxes) * (1 - this.discountRate));
		}
		return DoubleUtils.fixDecimals(sum);
	}

	/** Get price before discount */
	public double getFullTotal() {
		double sum = 0.0;
		for (TicketLineInfo line : m_aLines) {
			//On ne prend pas les subproducts (=elements d'une offre)
			if(!line.isSubproduct()) {
				sum += line.getValue();
			}
		}
		return DoubleUtils.fixDecimals(sum);
	}

	/** Get total with discount */
	//EDU Problème sur le nombre de décimales
	//C'est le dernier, donc le suel où on arrondi
	public double getTotal() {
		return RoundUtils.round((getSubTotal() + getVAT()));
	}

	public double getDiscountAmount() {
		// TODO : AME => On doit pouvoir faire plus optimisé
		//EDU ?? total avant discount - total disc incl plutôt ?
		return getFullTotal() - getTotal();
	}

	public double getTotalPaid() {
		double sum = 0.0;
		for (PaymentInfo p : payments) {
			if (!"debtpaid".equals(p.getName())) {
				sum += p.getTotal();
			}
		}
		return DoubleUtils.fixDecimals(sum);
	}

	public List<TicketLineInfo> getLinesOrdered() {
		List<TicketLineInfo> sorted = new ArrayList<TicketLineInfo>();
		for(TicketLineInfo element : m_aLines) {
			if(element.isOrder()) {
				sorted.add(element);
			}
		}
		return sorted;
	}
	
	public List<TicketLineInfo> getLinesDelivered() {
		List<TicketLineInfo> sorted = new ArrayList<TicketLineInfo>();
		for(TicketLineInfo element : m_aLines) {
			if(!element.isOrder()) {
				sorted.add(element);
			}
		}
		return sorted;
	}
	
	public List<TicketLineInfo> getLines() {
		return m_aLines;
	}
	
	public List<String> getAddressLines() {
		return Arrays.asList(address.split("\n"));
	}

	public void setLines(List<TicketLineInfo> l) {
		m_aLines = l;
	}

	public List<PaymentInfo> getPayments() {
		return payments;
	}

	public void setPayments(List<PaymentInfo> l) {
		payments = l;
	}

	public void resetPayments() {
		payments = new ArrayList<PaymentInfo>();
	}

	public List<TicketTaxInfo> getTaxes() {
		return taxes;
	}

	public boolean hasTaxesCalculated() {
		return taxes != null;
	}

	public void setTaxes(List<TicketTaxInfo> l) {
		taxes = l;
	}

	public void resetTaxes() {
		taxes = null;
	}

	public TicketTaxInfo getTaxLine(TaxInfo tax) {
		for (TicketTaxInfo taxline : taxes) {
			if (tax.getId().equals(taxline.getTaxInfo().getId())) {
				return taxline;
			}
		}

		return new TicketTaxInfo(tax);
	}

	public TicketTaxInfo[] getTaxLines() {
		Map<String, TicketTaxInfo> m = new HashMap<String, TicketTaxInfo>();

		for (TicketLineInfo ticketLineInfo : m_aLines) {
			// On traite la TVA dans un premier temps...
			TicketTaxInfo ticketTaxInfoVAT = new TicketTaxInfo(ticketLineInfo.getVAT());
			if (m.containsKey(ticketLineInfo.getVAT().getId())) {
				ticketTaxInfoVAT = m.get(ticketLineInfo.getVAT().getId());
			}
			ticketTaxInfoVAT.add(ticketLineInfo.getSubValue());

			// On traite les autres taxes
			//for (TaxInfo taxInfo : ticketLineInfo.getTaxes()) {
			for (int index = ticketLineInfo.getTaxes().size() ; index-- > 0 ; ) {
				TaxInfo taxInfo = ticketLineInfo.getTaxes().get(index);
				TicketTaxInfo ticketTaxInfo = new TicketTaxInfo(taxInfo);
				if (m.containsKey(taxInfo.getId())) {
					ticketTaxInfo = m.get(taxInfo.getId());
				}
				//double amountold = DoubleUtils.fixDecimals(taxInfo.getAmount() / (1 + ticketLineInfo.getVATRate()) * ticketLineInfo.getMultiply());
				double amount = ticketLineInfo.getSingleTaxAmountExclVAT(index) * ticketLineInfo.getMultiply();
				ticketTaxInfo.add(amount);
				m.put(ticketTaxInfo.getTaxInfo().getId(), ticketTaxInfo);
				ticketTaxInfoVAT.add(amount);
			}
			m.put(ticketTaxInfoVAT.getTaxInfo().getId(), ticketTaxInfoVAT);
		}

		Collection<TicketTaxInfo> avalues = m.values();
		return avalues.toArray(new TicketTaxInfo[avalues.size()]);
	}

	public String printId() {	
		
		if (m_iTicketId > 0) {
			// valid ticket id
			return Formats.INT.formatValue(new Long(m_iTicketId));
		} else {
			return "--";
		}
		
	}
	
	public String getSTicketId() {   
	        
	        if (m_iTicketId > 0) {
	            // valid ticket id
	            return new Long(m_iTicketId).toString();
	        } else {
	            return "";
	        }
	        
	    }

	public String printDate() {
		return Formats.TIMESTAMP.formatValue(m_dDate);
	}

	public String printUser() {
		return m_User == null ? "" : m_User.getName();
	}

	public String printCustomer() {
		return m_Customer == null ? "" : m_Customer.getName();
	}

	public String printArticlesCount() {
		return Formats.DOUBLE.formatValue(new Double(getArticlesCount()));
	}
	
	public String printArticlesStockCount() {
		return String.format("%1$02.0f", getArticlesDeliveredCount());
	}
	
	public String printArticlesOrderCount() {
		return String.format("%1$02.0f", getArticlesOrderedCount());
	}
	
	public String printLinesCount() {
		return String.format("%1$02d", getTicketLinesCount());
	}

	/** Pour affichage caisse et ticket **/
	public String printSubTotal() {
		Formats.setAltRoundingMode(null);
		return Formats.CURRENCY.formatValue(new Double(getSubTotal()));
	}

	/** Pour affichage caisse **/
	public String printVAT() {
		//TODO Repasser par ici
		//Bosser avec des double n'est pas ultime
		return Formats.CURRENCY.formatValue((new BigDecimal(getVAT())).setScale(7, RoundingMode.HALF_UP).doubleValue());
	}

	public String printFullTotal() {
		return Formats.CURRENCY.formatValue(new Double(this.getFullTotal()));
	}

	public String printTotal() {
		return Formats.CURRENCY.formatValue(new Double(getTotal()));
	}

	public String printTotalPaid() {
		return Formats.CURRENCY.formatValue(new Double(getTotalPaid()));
	}

	public String printCustomersCount() {
		if (this.hasCustomersCount()) {
			return Formats.INT.formatValue(this.customersCount);
		} else {
			return "";
		}
	}

	public String printDiscountRate() {
		return Formats.PERCENT.formatValue(this.discountRate);
	}

	public String printDiscountAmount() {
		return Formats.CURRENCY.formatValue(new Double(this.getDiscountAmount()));
	}
	
	public String getCustomerInsee() {
		String retour = m_Customer == null ? null : m_Customer.getPostal() ;
		InseeInfo insee = null;
		try {
			insee = retour == null ? null : InseeCache.getInseeByZipAndName(retour , m_Customer.getCity());
		} catch (BasicException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		retour = insee == null ? null : insee.getInseeNum();
		return retour;
	}

	public double getOffersEconomy() {
		return offersEconomy;
	}

	public void setOffersEconomy(double offersEconomy) {
		this.offersEconomy = offersEconomy;
	}
	
	public String printOffersEconomy() {
		return Formats.CURRENCY.formatValue(new Double(this.getOffersEconomy()));
	}
	
	public String printTotalDiscount() {
		return Formats.CURRENCY.formatValue(new Double(this.getTotalDiscount()));
	}
	
	public String printDiscountRateOneDecimal() {
		return Formats.PERCENTONEDECIMAL.formatValue(this.discountRate);
	}
	
	public boolean isPrintOffersEconomy() {
		return ( this.getOffersEconomy() > 0 ) ;
	}

	public Long getParentTicketId() {
		return parentTicketId;
	}

	public void setParentTicketId(Long parentTicketId) {
		this.parentTicketId = parentTicketId;
	}

	public String getAddress() {
		return address;
	}
	
	public String getSingleLinePaymentMedium() {
		String retour = "";
		if(payments.size() > 0) {
			if(payments.size() == 1 ){
				retour = AppLocal.getIntString("transpayment." + payments.get(0).getName());
				if(payments.get(0).isDeffered()) {
					retour += " " + AppLocal.getIntString("Label.PostponedPayment");
				}
			} else {
				for(PaymentInfo payment : payments) {
					if(retour.length() != 0 ) {
						retour += " - ";
					}
					retour += AppLocal.getIntString("transpayment." + payment.getName());
					if(payment.isDeffered()) {
						retour += " " + AppLocal.getIntString("Label.PostponedPayment");
					}
				}
			}
		}
		return retour;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
	public void updateAddress() {
		DataLogicSystem dlSystem = new DataLogicSystem();
		
		if(m_sActiveCash != null && !m_sActiveCash.isEmpty()) {
			CashSession cashSession;
			try {
				cashSession = dlSystem.getCashSessionById(m_sActiveCash);
			
				CashRegisterInfo cash = dlSystem.getCashRegister(cashSession.getCashRegisterId());
				SalesLocationInfo deballage = cashSession.getSalesLocation();
				String tourName = deballage == null || deballage.getTourName() == null ? "" : " - ".concat(deballage.getTourName());
				address = cash.getLocationName().concat(tourName).concat("\n");

				SalesLocationInfo sLocation = cashSession.getSalesLocation();
				if(sLocation != null) {
					address = address.concat(sLocation.getCity());
				}
				if(cash.getAddress()!= null) {
					address = address.concat("\n").concat(cash.getAddress());
				}
			
			} catch (BasicException e) {
				address = "";
			}
		} else {
			address = "";
		}
	}

	public String getNote() {
		return m_sNote;
	}

	public void setNote(String m_sNote) {
		this.m_sNote = m_sNote;
	}

	public void updateDiscountRate() {
		double discountAmount = RoundUtils.round(this.getFullTotal() * discountRate) ;
		if (  discountRate <= 1 && this.getFullTotal()!= 0  && ( this.getTotal() != 0 ||  discountRate == 1)){
			discountRate = discountAmount / this.getFullTotal();
		}else{
			discountRate = 0;
		}
	}
	// total des remises lignes - remise ticket
	public double getTotalDiscount() {
		double totalDiscount = this.getDiscountAmount();
		for (TicketLineInfo ticketLine : this.m_aLines) {
			totalDiscount += ticketLine.getValueRemise();
		}
		return totalDiscount;
	}
	
	// Si ticket contient une commande
	public boolean HasOrders() {
		for (TicketLineInfo l : this.m_aLines) {
			if (l.isOrder()){
				return true;
			}
		}
		return false;
	}
	
	public boolean isOpenedPaymentDialog() {
		return openedPaymentDialog;
	}

	public void setOpenedPaymentDialog(boolean isOpenedPaymentDialog) {
		openedPaymentDialog = isOpenedPaymentDialog;
		
	}
	
	public boolean canRefund() {
		boolean result = true;
		//Ancienne règle :
		//result = getTicketType() == TicketInfo.RECEIPT_NORMAL ;
		//
		return result;
		
	}

	public boolean isDiscounted() {
		if(this.getDiscountRate() > 0) {
			return true;
		}
		for (TicketLineInfo l : this.m_aLines) {
			if (l.hasDiscount()){
				return true;
			}
		}
		return false;
	}
	
	public String getDateProchainDeballage() {
		return dateProchainDeballage;
	}
	public void setDateProchainDeballage(String dateProchainDeballage) {
		this.dateProchainDeballage = dateProchainDeballage;
	}
	
}
