//    POS-Tech
//    Based upon Openbravo POS
//
//    Copyright (C) 2007-2009 Openbravo, S.L.
//                       2012 Scil (http://scil.coop)
//
//    This file is part of POS-Tech.
//
//    POS-Tech is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    POS-Tech is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with POS-Tech.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.ticket;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

/** Model for Cash register config. */
public class CashRegisterInfo implements Serializable {

	private static final long serialVersionUID = 4404930829404271349L;

	private int id;
	private String label;
	private String locationId;
	private long nextTicketId;
	private String taxCustCategId;
	private String locationName;
	private String zipCode;
	private String address;
	private String tourname;

	private List<SalesLocationInfo> salesLocations = new ArrayList<SalesLocationInfo>();

	public CashRegisterInfo(String label, String locationId, long nextTicketId) {
		this.label = label;
		this.locationId = locationId;
		this.nextTicketId = nextTicketId;
	}

	public CashRegisterInfo(String label, String locationId, long nextTicketId, String taxCustCategId) {
		this.label = label;
		this.locationId = locationId;
		this.nextTicketId = nextTicketId;
		this.taxCustCategId = taxCustCategId;
	}

	public CashRegisterInfo(JSONObject o) {
		this.id = o.getInt("id");
		this.label = o.getString("label");
		this.locationId = o.getString("locationId");
		this.nextTicketId = o.getLong("nextTicketId");
		// TODO : AME => check if(o.get("taxCustCategId") != JSONObject.NULL)
		if (o.get("taxCustCategId") != JSONObject.NULL) {
			this.taxCustCategId = o.getString("taxCustCategId");
		}
        
		//this.setSalesLocations(SalesLocationInfo.extractList(o.getJSONObject("location")));
		this.locationName = o.getJSONObject("location").getString("label");
		this.address = o.getJSONObject("location").get("address") == JSONObject.NULL ? null : ((String) o.getJSONObject("location").get("address")).replace("\\n", "\n");
		if (o.getJSONObject("location").get("tours") != JSONObject.NULL 
				&& o.getJSONObject("location").getJSONArray("tours").length() > 0
				&& o.getJSONObject("location").getJSONArray("tours").getJSONObject(0).get("tour") != JSONObject.NULL 
				&& o.getJSONObject("location").getJSONArray("tours").getJSONObject(0).getJSONObject("tour").get("nom") != JSONObject.NULL) {
			this.tourname = o.getJSONObject("location").getJSONArray("tours").getJSONObject(0).getJSONObject("tour").getString("nom");
		}
		this.zipCode = o.getJSONObject("location").get("inseeNum") == JSONObject.NULL ? null : o.getJSONObject("location").getString("inseeNum");
	}

	public int getId() {
		return this.id;
	}

	public String getLabel() {
		return this.label;
	}

	public String getLocationId() {
		return this.locationId;
	}

	public long getNextTicketId() {
		return this.nextTicketId;
	}
	
	public void setNextTicketId(long nextTicketId) {
		this.nextTicketId = nextTicketId;
	}

	public String getTaxCustCategId() {
		return taxCustCategId;
	}

	public String getLocationName() {
		return locationName;
	}
	
	public void incrementNextTicketId() {
		this.nextTicketId++;
	}

	public List<SalesLocationInfo> getSalesLocations() {
		return salesLocations;
	}

	public void setSalesLocations(List<SalesLocationInfo> salesLocations) {
		this.salesLocations = salesLocations;
	}

	public String getZipCode() {
		return zipCode;
	}

	public String getAddress() {
		return address == null ? "" : address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getTourname() {
		return tourname == null ? "" : tourname;
	}

	public void setTourname(String tourname) {
		this.tourname = tourname;
	}

}
