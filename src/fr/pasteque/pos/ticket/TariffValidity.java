package fr.pasteque.pos.ticket;

import java.io.Serializable;

import org.json.JSONObject;

public class TariffValidity implements Serializable {
	
    /**
	 * 
	 */
	private static final long serialVersionUID = -82573011776571181L;
	private Long id ;
    private Integer areaId ;
    private String locationId ;
    private Long start ;
    private Long end ;
    private Integer priority ;
    
    public TariffValidity () {
        id = null ;
        areaId = null;
        locationId = null;
        start = null;
        end = null;
        priority = 0;
	}
    
	public TariffValidity (JSONObject oValidity) {
        id = oValidity.isNull("id") ? null : oValidity.getLong("id");
        areaId = oValidity.isNull("areaId") ? null : oValidity.getInt("areaId");
        locationId = oValidity.isNull("locationId") ? null : oValidity.getString("locationId");
        start = oValidity.isNull("startDate") ? null : oValidity.getLong("startDate");
        end = oValidity.isNull("endDate") ? null : oValidity.getLong("endDate");
        priority = 0;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getAreaId() {
		return areaId;
	}

	public void setAreaId(Integer areaId) {
		this.areaId = areaId;
	}

	public String getLocationId() {
		return locationId;
	}

	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}

	public Long getStart() {
		return start;
	}

	public void setStart(Long start) {
		this.start = start;
	}

	public Long getEnd() {
		return end;
	}

	public void setEnd(Long end) {
		this.end = end;
	}

	public Integer getPriority() {
		return priority;
	}

	public void setPriority(Integer priority) {
		this.priority = priority==null ? 0 : priority;
	}
	
	

}
