package fr.pasteque.pos.ticket;

import java.io.Serializable;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

public class CompositionInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 747738243915477256L;
	private String parentId;
	private String childId;
	private double quantity;

	public String getParentId() {
		return parentId;
	}

	public String getChildId() {
		return childId;
	}

	public double getQuantity() {
		return quantity;
	}

	public CompositionInfo(JSONObject o) {
		 parentId = o.getString("parentId");
		 childId = o.getString("childId");
		 quantity = o.getDouble("quantity");
	 }
	 
	 public static ArrayList<CompositionInfo> extractList(JSONArray jsonArray) {
			ArrayList<CompositionInfo> returnedList = new ArrayList<CompositionInfo>();
			int max = jsonArray.length();
			while(0 < max-- ) {
					returnedList.add(new CompositionInfo(jsonArray.getJSONObject(max)) );
			}
	        
			return returnedList;
		}
	 
	 @Override
	 public boolean equals(Object obj) {
	     if (obj == null) {
	         return false;
	 	 }
	     if (!(obj instanceof CompositionInfo)) {
	         return false;
	     }
	     CompositionInfo other = (CompositionInfo) obj;
	     if(other == this) {
	    	 return true;
	     }
	     return parentId.equals(other.getParentId()) && childId.equals(other.getChildId()) && quantity == other.getQuantity();
	 }
	 
}
