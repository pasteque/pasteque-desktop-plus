package fr.pasteque.pos.ticket;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONObject;

import fr.pasteque.format.DateUtils;

public class MessageInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3851593848234604157L;
	private String content;
	private String locationId;
	private String peopleId;
	private String productId;
	private String type;
	private Date startDate;
	private Date endDate;
	
	
	 public String getContent() {
		return content;
	}

	public String getLocationId() {
		return locationId;
	}

	public String getPeopleId() {
		return peopleId;
	}

	public String getProductId() {
		return productId;
	}

	public String getType() {
		return type;
	}

	public Date getStartDate() {
		return startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public MessageInfo(JSONObject o) {
		 if (!o.isNull("content")) {
			 content = o.getString("content");
		 }
		 if (!o.isNull("locationId")) {
			 locationId = o.getString("locationId");
		 }
		 if (!o.isNull("peopleId")) {
			 peopleId = o.getString("peopleId");
		 }
		 if (!o.isNull("productId")) {
			 productId = o.getString("productId");
		 }
		 if (!o.isNull("type")) {
			 type = o.getString("type");
		 }
		 if (!o.isNull("startDate")) {
			 startDate = DateUtils.readMilliTimestamp(o.getLong("startDate"));
		 }
		 if (!o.isNull("endDate")) {
			 endDate = DateUtils.readMilliTimestamp(o.getLong("endDate"));
		 }
	 }
	 
	 public static ArrayList<MessageInfo> extractList(JSONArray jsonArray) {
			ArrayList<MessageInfo> returnedList = new ArrayList<MessageInfo>();
			int max = jsonArray.length();
			while(0 < max-- ) {
					returnedList.add(new MessageInfo(jsonArray.getJSONObject(max)) );
			}
			
	        
			return returnedList;
		}
}
