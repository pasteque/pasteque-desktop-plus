//    POS-Tech
//    Based upon Openbravo POS
//
//    Copyright (C) 2007-2009 Openbravo, S.L.
//                       2012 Scil (http://scil.coop)
//
//    This file is part of POS-Tech.
//
//    POS-Tech is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    POS-Tech is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with POS-Tech.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.ticket;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import org.json.JSONArray;
import org.json.JSONObject;

import fr.pasteque.basic.BasicException;
import fr.pasteque.data.loader.ImageLoader;
import fr.pasteque.format.DoubleUtils;
import fr.pasteque.format.Formats;
import fr.pasteque.pos.forms.DataLogicSales;
import fr.pasteque.pos.util.RoundUtils;
import fr.pasteque.pos.util.StringUtils;

/**
 *
 * @author adrianromero
 */
public class TicketLineInfo implements Serializable,Comparable<TicketLineInfo> {

	private static final long serialVersionUID = 6608012948284450199L;
	public static final char ORDER = 'O' ;
	public static final char SALE = 'S' ;
	public static final String PRODUCT_NAME_PREFIX = "Produit : ";
	private String m_sTicket;
	private int m_iLine;
	private double multiply;
	private double price;
	private double discountRate;
	private TaxInfo vat;
	private Properties attributes;
	private String productid;
	private String attsetinstid;
	private boolean subproduct;
	private List<TaxInfo> taxes = new ArrayList<TaxInfo>();
	private char type;
	private boolean orderAvailable = false;
	private String comments = null;
	private Integer tariffAreaId = null;
	private Integer parentLine = null;
	private Long parentId = null;
	//EDU afin de ne pas remettre tout le cache en cause et devoir prévoir les outils pour intercepter un changement de lieu 
	private String locationId;
	//EDU - il faut pouvoir remonter à la ref parente si on est un composant
	TicketLineInfo  parent= null;
	//EDU - ll faut pouvoir passer aux enfants si on est dans une ref composée
	List <TicketLineInfo>  children= null;
	private int orderOfLine;
	private Date createdDate;
	SimpleDateFormat  formater = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
	
	//Plus simple pour vérifier qu'il n'y a pas d'altération dans l'ordre
	//Il faut conserver à l'écran 
	//ref prix
	// |ss ref
	// |ss ref	
	//Plus simple pour vérifier la cohérence en cas de modif (qté par exemple)

	//La genèse de l'histoire pour ces lignes
	//On souhaite afficher les articles dans la table afin de pouvoir n'avoir qu'une partie de la compo en commande
	//
	//Il faudra toutefois jouer plus finement pour les exports ( si une partie seulement de la compo est en commande,
	//il faut faire comme si ce n'était pas le cas, puis ajouter un retour sur la ref seule et une vente en commande sur la ref seule
	//Chaque fois que le composant est une vente du même type que le parent, elle ne doit pas apparaitre dans l'export
	//
	//Lors des mouvements il y du coup une contrainte complémentaire : faire bouger les éleemnts associés en simultané
	//
	//Lors de l'ajout on l'enlèvement d'éléments, il faut le faire sur les ref propres -> une nouvelle ligne si il s'agit d'un composant
	//Il faut aussi repasser sur l'ensemble pour vérifier qu'on n'a pas validé/enlevé de compo 
	//

	public TicketLineInfo(String productname, String producttaxcategory, double dMultiply, double dPrice, TaxInfo vat) {
		Properties props = new Properties();
		props.setProperty("product.name", productname);
		props.setProperty("product.taxcategoryid", producttaxcategory);
		init(null, null, dMultiply, dPrice, vat, new ArrayList<TaxInfo>(), props);
	}

	private TicketLineInfo() {
		init(null, null, 0.0, 0.0, null, new ArrayList<TaxInfo>(), new Properties());
	}

	public TicketLineInfo(ProductInfoExt product, double dMultiply, double dPrice, TaxInfo tax, List<TaxInfo> taxes, Properties attributes) {
		String pid;
		if (product == null) {
			pid = null;
		} else {
			pid = product.getID();
			orderAvailable = product.isOrderAvailable();
			attributes.setProperty("product.name", product.getName());
			attributes.setProperty("product.com", "false");
			attributes.setProperty("product.scale", product.isScale() ? "true" : "false");
			if (product.getAttributeSetID() != null) {
				attributes.setProperty("product.attsetid", product.getAttributeSetID());
			}
			attributes.setProperty("product.taxcategoryid", product.getTaxCategoryID());
			if (product.getCategoryID() != null) {
				attributes.setProperty("product.categoryid", product.getCategoryID());
			}
			
		}
		init(pid, null, dMultiply, dPrice, tax, taxes, attributes);
	}

	public TicketLineInfo(TicketLineInfo line) {
		init(line.productid, line.attsetinstid, line.multiply, line.price, line.vat, line.taxes, (Properties) line.attributes.clone());
		this.subproduct = line.isSubproduct();
		this.discountRate = line.discountRate;
		this.type = line.type;
		this.children = line.children;
		this.parent = line.parent;
		this.orderAvailable = line.orderAvailable;
		this.comments = line.comments ;
		this.tariffAreaId = line.tariffAreaId ;
		this.parentId = line.parentId ;
		this.parentLine = line.parentLine ;
		this.locationId = line.locationId;
		this.orderOfLine = line.orderOfLine;
		this.updateChildren(this);
	}

	private void init(String productid, String attsetinstid, double dMultiply, double dPrice, TaxInfo vat, List<TaxInfo> taxes,
			Properties attributes) {
		this.productid = productid;
		this.attsetinstid = attsetinstid;
		multiply = dMultiply;
		price = dPrice;
		this.vat = vat;
		this.taxes = taxes;
		this.attributes = attributes;

		m_sTicket = null;
		m_iLine = -1;
		
		type = SALE;
		
		
		
		this.orderOfLine = TicketInfo.ORDER++;
		
		this.subproduct = false;
		this.createdDate = new Date();
	}

	public void setTicket(String ticket, int line) {
		m_sTicket = ticket;
		m_iLine = line;
	}
	
	public String getTicket() {
		return m_sTicket;
	}

	public JSONObject toJSON() {
		JSONObject o = new JSONObject();
		String notes = null ;
		o.put("dispOrder", this.m_iLine);
		o.put("productId", this.productid);
		o.put("attributes", JSONObject.NULL); // TODO: add attributes
		o.put("quantity", this.multiply);
		o.put("price", this.price);
		o.put("vatId", this.vat.getId());
		List<String> taxesId = new ArrayList<String>();
		for (TaxInfo taxInfo : taxes) {
			taxesId.add(taxInfo.getId());
		}
		o.put("taxes", new JSONArray(taxesId));
		o.put("discountRate", this.discountRate);
		o.put("type", this.type);
		if(this.productid == null) {
			notes = PRODUCT_NAME_PREFIX + this.attributes.getProperty("product.name", " ");
		}
		if(this.comments != null) {
			if(notes != null ) {
				notes = notes.concat("\n").concat(this.comments);
			} else {
				notes = this.comments;
			}
		}
		o.put("note", notes);
		o.put("tariffAreaId", this.tariffAreaId);
		o.put("parentId", this.parentId);
		o.put("parentLine", this.parentLine);
		return o;
	}
	public JSONObject lineDeletedToJSON() {
		JSONObject o = new JSONObject();
		String notes = null ;
		o.put("dispOrder", this.m_iLine);
		o.put("productId", this.productid);
		o.put("attributes", JSONObject.NULL); // TODO: add attributes
		o.put("quantity", this.multiply);
		o.put("price", this.price);
		o.put("vatId", this.vat.getId());
		List<String> taxesId = new ArrayList<String>();
		for (TaxInfo taxInfo : taxes) {
			taxesId.add(taxInfo.getId());
		}
		o.put("taxes", new JSONArray(taxesId));
		o.put("discountRate", this.discountRate);
		o.put("type", this.type);
		if(this.productid == null) {
			notes = PRODUCT_NAME_PREFIX + this.attributes.getProperty("product.name", " ");
		}
		if(notes != null && this.comments != null) {
			notes = notes.concat("\n").concat(this.comments);
		}
		o.put("note", notes);
		o.put("tariffAreaId", this.tariffAreaId);
		o.put("parentId", this.parentId);
		o.put("parentLine", this.parentLine);
		o.put("createdDate",formater.format(this.createdDate));
		return o;
	}
	public TicketLineInfo(JSONObject o) throws BasicException {
		String comments = o.isNull("note") ? null : o.getString("note");
		String productName = getProductName(comments);
		this.m_iLine = o.getInt("dispOrder");
		this.productid = o.isNull("productId") ? null : o.getString("productId");
		DataLogicSales dlSales = new DataLogicSales();
		ProductInfoExt product = dlSales.getProductInfo(this.productid);
		this.attributes = new Properties();

		this.comments = (productName.length() != 0 ? comments.substring(PRODUCT_NAME_PREFIX.length() + productName.length()) : comments) ;
		this.multiply = o.getDouble("quantity");
		this.price = o.getDouble("price");
		this.vat = dlSales.getTax(o.getString("vatId"));
		this.discountRate = o.getDouble("discountRate");
		this.type =   o.getString("type").charAt(0) ;
		this.comments = comments ;
		this.tariffAreaId = o.isNull("tariffAreaId") ? null : o.getInt("tariffAreaId");
		this.parentId = o.isNull("parentId") ? null : o.getLong("parentId");
		this.parentLine = o.isNull("parentLine") ? null : o.getInt("parentLine");
		if (product != null) {
			attributes.setProperty("product.name", product.getName());
			attributes.setProperty("product.com", "false");
			attributes.setProperty("product.scale", product.isScale() ? "true" : "false");
			// TODO: attributes
			attributes.setProperty("product.taxcategoryid", product.getTaxCategoryID());
			if (product.getCategoryID() != null) {
				attributes.setProperty("product.categoryid", product.getCategoryID());
			}
			orderAvailable = product.isOrderAvailable();
		} else {
			attributes.setProperty("product.name", productName);
			if(this.vat != null ) {
				attributes.setProperty("product.taxcategoryid", this.vat.getId());
			}
		}
	}

	private String getProductName(String comment) {
		String retour = "";
		if(comment != null && comment.startsWith(PRODUCT_NAME_PREFIX)) {
			int size = comment.indexOf("\n") ;
			size = size < 0 ? comment.length() :size ;
			retour = comment.substring( PRODUCT_NAME_PREFIX.length(), size);
		}
		return retour;
	}

	public TicketLineInfo copyTicketLine() {
		TicketLineInfo l = new TicketLineInfo();
		// l.m_sTicket = null;
		// l.m_iLine = -1;
		l.productid = productid;
		l.attsetinstid = attsetinstid;
		l.multiply = multiply;
		l.price = price;
		l.vat = vat;
		l.discountRate = this.discountRate;
		l.attributes = (Properties) attributes.clone();
		l.subproduct = this.subproduct;
		l.type = this.type;
		l.orderAvailable = this.orderAvailable;
		l.comments = this.comments;
		l.tariffAreaId = this.tariffAreaId;
		l.parentId = this.parentId;
		l.parentLine = this.parentLine ;
		l.locationId = this.locationId ;
		l.orderOfLine = this.orderOfLine;
		l.createdDate = this.createdDate;
		return l;
	}
	
	//TODO vérifier pertinence des règles pour fusion
	//même produit non nul - on ne fusionne pas une ligne sans ref 
	//même type (on ne fusionne pas une commande avec une livraison) - 
	//même nature - on ne fusionne pas un composant avec le produit vendu en son nom propre -
	//même assemblage - on ne fusionne pas deux lignes de deux assemblages différents
	//même sens - on ne fusionne pas un retour avec une vente
	//Voir pour les questions de prix / remise ...
	/**
	 * Test si les règle de similitude entre deux object sont vraies
	 * @param line ligne testée
	 * @return true si les deux objets sont suffisement semblables pour être fusionnables
	 */
	public boolean isSimilar(TicketLineInfo line) {
		return (line.getProductID() != null 
				&& line.getProductID().equals(productid) 
				&& line.isSubproduct() == subproduct 
				&& ((parent == null && line.getParent() == null ) || (parent != null && parent.equals(line.getParent()))) 
				&& line.getType() == type 
				&& multiply*line.getMultiply() > 0
				&& getDiscountRate() == line.getDiscountRate()
				&& ((locationId == null && line.getLocationId()==null ) || (line.getLocationId().equals(locationId))) );
	}
	
	/**
	 * Agréger les données de la line passée en paramètres avec la ligne courante
	 * @param line
	 * @return
	 */
	public boolean mergeTicketLine(TicketLineInfo line) {

		if(isSimilar(line)) {
			multiply += line.getMultiply() ;
			line.setMultiply(0D);
		} 
		return (line.getMultiply() == 0 ) ;
	}
	
	/**
	 * Décomposer une ligne en deux lignes indépendantes
	 * @param newMultiply quantité pour la nouvelle ligne
	 * @return
	 */
	public TicketLineInfo splitTicketLine(double newMultiply) {
		TicketLineInfo l = copyTicketLine();
		if(multiply == newMultiply) {
			l = null ;
		} else {
			multiply-=newMultiply;
			l.setMultiply(newMultiply);
		}
		return l;
	}

	public int getTicketLine() {
		return m_iLine;
	}

	public String getProductID() {
		return productid;
	}
	
	//Ces quatres méthodes permettent d'avoir un retour visuel
	//TODO faire sauter les chaines en dur.
	public String getEmpl() {
		String response = "";
		DataLogicSales dlSales = new DataLogicSales();
		ProductInfoExt product;
		try {
			product = dlSales.getProductInfo(this.productid);

			if((product != null) && (!product.getMessagesByCriteria("EMPL" , locationId).isEmpty())) {
				response =  product.getMessagesByCriteria("EMPL" , locationId).get(0).getContent() + "&nbsp;";
			}
		} catch (BasicException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return response;
	}
	
	public String getLinked() {
		String response = "";
		DataLogicSales dlSales = new DataLogicSales();
		ProductInfoExt product;
		if(!isSubproduct()) {
			try {
				product = dlSales.getProductInfo(this.productid);
	
				if((product != null) && (!product.getParents().isEmpty())) {
					response += "<img src=\""
					          + ImageLoader.class.getResource("/fr/pasteque/images/link.png")
					          + "\">" ;	
				}
			} catch (BasicException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return response;
	}

	public String getAlert() {
		String response = "";
		DataLogicSales dlSales = new DataLogicSales();
		ProductInfoExt product;
		try {
			product = dlSales.getProductInfo(this.productid);

			if((product != null) && (!product.getMessagesByCriteria("ALERT" , locationId).isEmpty())) {
				response += "<img src=\""
				          + ImageLoader.class.getResource("/fr/pasteque/images/alert.png")
				          + "\">" ;	
			}
		} catch (BasicException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return response;
	}
	
	public String getOrder() {
		String response = "";

		if(getType() != SALE) {
					response += "<img src=\""
					+ ImageLoader.class.getResource("/fr/pasteque/images/order.png")
			        + "\">" ;	
		}
		return response;
	}
	
	public String getProductName() {
		return attributes.getProperty("product.name");
	}

	public String getProductAttSetId() {
		return attributes.getProperty("product.attsetid");
	}

	public String getProductAttSetInstDesc() {
		return attributes.getProperty("product.attsetdesc", "");
	}

	public void setProductAttSetInstDesc(String value) {
		if (value == null) {
			attributes.remove(value);
		} else {
			attributes.setProperty("product.attsetdesc", value);
		}
	}

	public String getProductAttSetInstId() {
		return attsetinstid;
	}

	public void setProductAttSetInstId(String value) {
		attsetinstid = value;
	}

	public boolean isProductCom() {
		return "true".equals(attributes.getProperty("product.com"));
	}

	public boolean isProductScale() {
		return "true".equals(attributes.getProperty("product.scale"));
	}

	public String getProductTaxCategoryID() {
		return (attributes.getProperty("product.taxcategoryid"));
	}

	public String getProductCategoryID() {
		return (attributes.getProperty("product.categoryid"));
	}

	public double getMultiply() {
		return multiply;
	}

	public void setMultiply(double dValue) {
		multiply = DoubleUtils.fixDecimals(dValue);
	}

	public double getDiscountRate() {
		return this.discountRate;
	}

	public void setDiscountRate(double rate) {
		this.discountRate = DoubleUtils.fixDecimals(rate);
	}

	public boolean hasDiscount() {
		return this.discountRate > 0.0;
	}

	/** Get price without discount */
	public double getFullPrice() {
		return this.price;
	}

	/** Get price with discount */
	public double getPrice() {
		return DoubleUtils.fixDecimals(this.price * (1.0 - this.discountRate));
	}

	public void setPrice(double dValue) {
		price = DoubleUtils.fixDecimals(dValue);
	}

	/** Prix fort (sans remise) total avec toutes les taxes TVA + (DEEE + ECOMOB ....) */
	public double getFullPriceTax() {
		return DoubleUtils.fixDecimals((price * (1.0 + getVATRate())) + getTaxesVATIncludeAmount());
	}

	/** Mtt TTC dans taxes autres que la TVA */
	public double getTaxesVATIncludeAmount() {
		double amount = 0D;
		for (int index = getTaxes().size() ; index-- > 0 ; ) {
			amount += getSingleTaxAmountInclVAT(index);
		}
		return amount;
	}
	
	/** obtenir le montant hors TVA des taxes pour un élément donné de taxes à partir de son index dans le tableau */
	public double getSingleTaxAmountExclVAT(int index) {
		return getSingleTaxAmountInclVAT(index) / (1 + vat.getRate());
	}
	
	/** obtenir le montant des taxes pour un élément donné de taxes à partir de son index dans le tableau */
	public double getSingleTaxAmountInclVAT(int index) {
		List<TaxInfo> taxesList = getTaxes() ;
		int rank =  this.taxes.size()-1 ;
		Double coefficient = 1D;
		//Je doit mettre la main sur le composant pour pouvoir appliquer mon coeff
		if (index > rank) { 
			int indice = -1;
			while (++indice < this.children.size() && index > rank) {
				rank += this.children.get(indice).getTaxes().size() ;
				coefficient = this.children.get(indice).multiply;
			}
		}
		return ((taxesList.size() <= index || taxesList.get(index).getAmount() == null ) ? 0D : taxesList.get(index).getAmount()) * coefficient;
	}

	/** Mtt HT des taxes autres que la TVA */
	public double getTaxesVATExcludeAmount() {
		double amount = 0D;
		for (int index = getTaxes().size() ; index-- > 0 ; ) {
			amount += getSingleTaxAmountExclVAT(index);
		}
		return amount;
	}

	/** Mtt TVA dans les taxes autres que la TVA */
	public double getTaxesVATAmount() {
		double vatAmount = 0D;
		for (int index = getTaxes().size() ; index-- > 0 ; ) {
			vatAmount += getSingleTaxAmountInclVAT(index) * getVATRate() / ( 1 + getVATRate());
		}
		return DoubleUtils.fixDecimals(vatAmount);
	}

	/** Prix (avec remise) total avec toutes les taxes TVA + (DEEE + ECOMOB ....) **/
	public double getPriceTax() {
		return DoubleUtils.fixDecimals((price * (1.0 - this.discountRate) * (1.0 + getVATRate())) + getTaxesVATIncludeAmount());
	}
	
	/** Permet de calculer le prix HT a partir d'un prix TTC (On enlève les ECOTAXES d'abord car ils sont indépendant de la TVA) **/
	public void setPriceTax(double dValue) {
		price = DoubleUtils.fixDecimals((dValue - getTaxesVATIncludeAmount()) / (1.0 + getVATRate()));
	}

	public TaxInfo getVAT() {
		return vat;
	}

	public void setVAT(TaxInfo value) {
		vat = value;
	}

	public String getProperty(String key) {
		return attributes.getProperty(key);
	}

	public String getProperty(String key, String defaultvalue) {
		return attributes.getProperty(key, defaultvalue);
	}

	public void setProperty(String key, String value) {
		attributes.setProperty(key, value);
	}

	public Properties getProperties() {
		return attributes;
	}

	public double getVATRate() {
		return vat == null ? 0.0 : vat.getRate();
	}

	/** Le montant total de la ligne (HORS DEEE, ECOMOB) (avec la quantité et la remise) */
	public double getSubValue() {
		return DoubleUtils.fixDecimals(price * (1.0 - this.discountRate) * multiply);
	}

	/** Get price with quantity (without discount) */
	public double getFullSubValue() {
		return DoubleUtils.fixDecimals(this.price * this.multiply);
	}

	/** Le montant total de TVA (sur la base du tarif plein) sur la ligne y compris celui des autres taxes **/
	public double getFullVATAmount() {
		return DoubleUtils.fixDecimals(price * multiply * getVATRate() + (multiply * getTaxesVATAmount()));
	}

	/** Le montant total de TVA (sur la base du tarif remisé) sur la ligne y compris celui des autres taxes **/
	public double getVATAmount() {
		return DoubleUtils.fixDecimals(price * (1.0 - this.discountRate) * multiply * getVATRate() + (multiply * getTaxesVATAmount()));
	}

	//EDU 2015 04 15 - Nous souhaitons avoir un prix de vente en centimes - ie deux décimales. Ou plutôt Currency precision !
	//TODO : valider le hack
	/** Le montant total (avec remise) de la ligne du ticket avec toutes les taxes TVA + (DEEE + ECOMOB ....) **/
	public double getValue() {
		return RoundUtils.round(price * (1.0 - this.discountRate) * multiply * (1.0 + getVATRate()) + (multiply * getTaxesVATIncludeAmount()));
	}
	
	/** Le montant total (sans remise) de la ligne du ticket avec toutes les taxes TVA + (DEEE + ECOMOB ....) **/
	public double getFullValue() {
		return RoundUtils.round(this.price * this.multiply * (1.0 + this.getVATRate()) + (multiply * getTaxesVATIncludeAmount()));
	}
	/** le montant total TTC du remise **/
	public double getValueRemise(){
		return this.getFullValue() - this.getValue();
	}
	public boolean isSubproduct() {
		return this.subproduct;
	}

	public void setSubproduct(boolean subproduct) {
		this.subproduct = subproduct;
	}

	public char getType() {
		return type;
	}
	
	public boolean isOrder() {
		return type == ORDER;
	}
	
	//TODO Parcourir tous les enfants pour les cocher/décocher ?
	public void toggleOrder() {
		if(isOrderAvailable()) {
			type = ( type == ORDER ? SALE : ORDER);
		}
	}

	public void setType(char type) {
		this.type = type;
	}

	public String printName() {
		return StringUtils.encodeXML(attributes.getProperty("product.name"));
	}

	public String printTaxes(TaxInfo taxInfo) {
		double taxesAmount = taxInfo.getAmount() * this.multiply;
		return "Dont " + Formats.CURRENCY.formatValue(taxesAmount) + " " + taxInfo.getName();
	}

	public String printMultiply() {
		return Formats.DOUBLE.formatValue(multiply);
	}

	public String printFullPrice() {
		return Formats.CURRENCY.formatValue(this.getFullPrice());
	}

	public String printPrice() {
		Formats.setAltRoundingMode(null);
		return Formats.CURRENCY.formatValue(getPrice());
	}

	public String printFullPriceTax() {
		return Formats.CURRENCY.formatValue(this.getFullPriceTax());
	}

	/** Affiche le prix unitaire TTC (avec DEEE, ECOMOB, ...) **/
	public String printPriceTax() {
		return Formats.CURRENCY.formatValue(getPriceTax());
	}

	public String printFullVATAmount() {
		return Formats.CURRENCY.formatValue(this.getFullVATAmount());
	}

	public String printVATAmount() {
		return Formats.CURRENCY.formatValue(getVATAmount());
	}

	public String printVATRate() {
		return Formats.PERCENT.formatValue(getVATRate());
	}

	public String printFullSubValue() {
		Formats.setAltRoundingMode(null);
		return Formats.CURRENCY.formatValue(this.getFullSubValue());
	}

	public String printSubValue() {
		Formats.setAltRoundingMode(null);
		return Formats.CURRENCY.formatValue(getSubValue());
	}

	public String printFullValue() {
		return Formats.CURRENCY.formatValue(this.getFullValue());
	}

	/** Affiche le prix TTC d'une ligne i.e. prix unitaire * qté **/
	public String printValue() {
		return Formats.CURRENCY.formatValue(getValue());
	}
	
	/** Affiche le montant remise TTC d'une ligne **/
	public String printValueRemise() {
		return Formats.CURRENCY.formatValue(getValueRemise());
	}
	
	public String printDiscountRate() {
		return Formats.PERCENT.formatValue(this.discountRate);
	}
	
	public String printDiscountRateOneDecimal() {
		return Formats.PERCENTONEDECIMAL.formatValue(this.discountRate);
	}
	
	public List<TaxInfo> getTaxes() {
		List<TaxInfo> retour = new ArrayList<TaxInfo>(taxes);
		if(this.children != null && this.children.size() > 0) {
			for(TicketLineInfo childLine : this.children) {
				retour.addAll(childLine.getTaxes());
			}
		}
		return retour;
	}

	public void setTaxes(List<TaxInfo> taxes) {
		this.taxes = taxes;
	}

	public boolean isOrderAvailable() {
		return orderAvailable;
	}

	public TicketLineInfo getParent() {
		return parent;
	}

	public void setParent(TicketLineInfo parent) {
		this.parent = parent;
	}

	public List<TicketLineInfo> getChildren() {
		return children;
	}

	public void setChildren(List<TicketLineInfo> children) {
		this.children = children;
	}

	public void addChild(TicketLineInfo tmpTicketLine) {
		if(this.children == null) {
			this.children = new ArrayList<TicketLineInfo>();
		}
		this.children.add(tmpTicketLine);
		
	}

	//Pour traiter de manière différente les produit dont
	//la somme des composants enfants est non entière 
	//ou égale à 1
	public boolean isChildQuantityAnInt() {
		Double childQuantity = 0D , defaultQuantity , bascule = 1D;
		DataLogicSales dlSales = new DataLogicSales();
		ProductInfoExt product;
		try {
			product = dlSales.getProductInfo(this.productid);

		} catch (BasicException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			product = null ;
		}
		
		if(children != null) {
			for(TicketLineInfo ticketLine : children) {
				childQuantity += ticketLine.multiply ;
			}
		}
		
		if(product !=null) {
			defaultQuantity = product.countChildren();
		} else {
			defaultQuantity = childQuantity + 1;
		}
		
		if(multiply > 1) {
			//dans ce cas il devient important de calculer le 'point de bascule'
			//c'est à dire le nombre maxi d'offres unitaires acceptables
			//C'est nb elements pour l'offre du dessus -1
			if(children != null) {
				for(TicketLineInfo ticketLine : children) {
					try {
						product = dlSales.getProductInfo(ticketLine.productid);
						if(product.getParents().size() > 1) {
							for(CompositionInfo composition : product.getParents() ) {
								if (composition.getQuantity() > 1 ) {
									bascule = composition.getQuantity() - 1 ;
								}
							}
						}
					} catch (BasicException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
			bascule =  bascule > multiply ? multiply : bascule ; 
		}
		
		//On doit retourner faux - ce qui veut dire pas touche dans les cas suivants :
		//On est sur un produit composé dont la somme des quantités des enfants est < 1 
		//On est sur une vente à l'unité dans le cas d'un panachage un promo sur deux produits identiques on en vend un rouge un bleu
		//Et qu'on n'a pas assez de prosuits identiques pour satisfaire la promo du dessus
		// on a un prix par deux on prends deux bleux on bascule
		// on a un prix par trois, on a deux bleus on ne bascule pas
		return ( childQuantity == childQuantity.intValue() && (multiply > bascule || !childQuantity.equals(defaultQuantity*bascule) || !childQuantity.equals(bascule)));
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Integer getTariffAreaId() {
		return tariffAreaId;
	}

	public void setTariffAreaId(Integer tariffAreaId) {
		this.tariffAreaId = tariffAreaId;
	}
	
	public boolean hasNoChild() {
		return ( children == null || children.size() == 0 );
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public Integer getParentLine() {
		return parentLine;
	}

	public void setParentLine(Integer parentLine) {
		this.parentLine = parentLine;
	}

	/**
	 * Mettre à jour la ligne parente pour qu'elle point sur la ligne courante
	 * @param line la ligne d'origine qu'il faut aller remplacer dans la ligne parente
	 */
	public void updateParent(TicketLineInfo line) {
		if(this.parent != null && line.parent == this.parent) {
			try{
				this.parent.getChildren().set(this.parent.getChildren().indexOf(line) , this );
			} 
			catch (Exception e) {
				//TODO gérer les cas qui aterrissent ici 
			}
		}
		
		updateChildren(line);
		
	}
		
		/**
		 * Mettre à jour les enfants  pour qu'ils point sur la ligne courante
		 * @param line la ligne d'origine qu'il faut aller remplacer dans la ligne parente
		 */
		public void updateChildren(TicketLineInfo line) {
			if(this.children != null && line.children == this.children) {
				try{
					for(TicketLineInfo child :  this.getChildren()) {
						child.setParent(this);
					}
					
				} 
				catch (Exception e) {
					//TODO gérer les cas qui aterrissent ici
				}
			}
		
		}

		public String getLocationId() {
			return locationId;
		}

		public void setLocationId(String locationId) {
			this.locationId = locationId;
		}

		public int getOrderOfLine() {
			return orderOfLine;
		}

		@Override
		public int compareTo(TicketLineInfo other) {
			if (this.getChildren()!=null && this.getChildren().contains(other))
				 
				return -1;
			else if (this.getChildren()!=null && this.getChildren().get(0).getOrderOfLine() < other.getOrderOfLine()){
				return -1;
			}
			else if(this.getParent()!=null && this.getParent().equals(other ) && this.getOrderOfLine() < other.getOrderOfLine())
				
				return 1;
			else if (this.getOrderOfLine() < other.getOrderOfLine())
			     
				return -1;
			else if (this.getOrderOfLine() >  other.getOrderOfLine())
			      
				return 1;
			else
			      
				return 0;
			
		}
		
		public double alreadyReturned() {
			double result = 0D;
			
			return result;
		}
		
		//EDU Test pour déterminer si le retour est légitime et dans quelle proportion
		//Si c'est une commande elle doit être sur la session de vente en cours
		//Dans tous les cas il faut ne pas avoir déjà fait trop de retour sur la ligne.
		//Il faut que la quantité vendue initialement soit positive
		public double canRefund() {
			double result = this.getMultiply();
			result -= alreadyReturned();
			return result;
		}
		
		
}
