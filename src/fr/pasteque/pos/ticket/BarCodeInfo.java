package fr.pasteque.pos.ticket;

import java.io.Serializable;
import java.util.ArrayList;
import org.json.JSONArray;

public class BarCodeInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9085105601680905151L;

	private String barcode;
	private String productId;
	
	 public String getBarcode() {
		return barcode;
	}

	public String getProductId() {
		return productId;
	}

	public BarCodeInfo(String barcode , String productId) {
		 this.barcode = barcode;
		 this.productId = productId;
	 }
	 
	 public static ArrayList<BarCodeInfo> extractList(JSONArray o , String productId) {
			ArrayList<BarCodeInfo> returnedList = new ArrayList<BarCodeInfo>();
			int max = o.length();
			while(0 < max-- ) {
				returnedList.add(new BarCodeInfo(o.getString(max) , productId) );
			}
	        
			return returnedList;
		}
}
