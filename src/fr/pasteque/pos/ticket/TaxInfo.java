//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2007-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.ticket;

import java.io.Serializable;

import fr.pasteque.data.loader.IKeyed;

import java.util.Date;

import org.json.JSONObject;

/**
 *
 * @author adrianromero
 */
public class TaxInfo implements Serializable, IKeyed {

	private static final long serialVersionUID = -2705212098856473043L;
	private String id;
	private String name;
	private String taxcategoryid;
	private Date validfrom;
	private Date validto;
	private String taxcustcategoryid;
	private String parentid;

	private Double rate;
	private Double amount;
	private boolean cascade;
	private Integer order;

	/** Creates new TaxInfo */
	public TaxInfo(String id, String name, String taxcategoryid, Date validfrom, Date validto, String taxcustcategoryid, String parentid,
			Double rate, Double amount, boolean cascade, Integer order) {
		this.id = id;
		this.name = name;
		this.taxcategoryid = taxcategoryid;
		this.validfrom = validfrom;
		this.validto = validto;
		this.taxcustcategoryid = taxcustcategoryid;
		this.parentid = parentid;

		this.rate = rate;
		this.amount = amount;
		this.cascade = cascade;
		this.order = order;
	}

	public TaxInfo(JSONObject o) {
		this.id = o.getString("id");
		this.name = o.getString("label");
		this.taxcategoryid = o.getString("taxCatId");
		if (!o.isNull("startDate")) {
			this.validfrom = new Date(o.getLong("startDate"));
		}
		if (!o.isNull("endTo")) {
			this.validto = new Date(o.getLong("endTo"));
		}
		if (!o.isNull("taxCustCategoryId")) {
			this.taxcustcategoryid = o.getString("taxCustCategoryId");
		}
		if (!o.isNull("rate")) {
			this.rate = o.getDouble("rate");
		}
		if (!o.isNull("amount")) {
			this.amount = o.getDouble("amount");
		}
		this.order = null;
		this.parentid = null; // TODO: support for parent tax
		this.cascade = false; // TODO: support for tax cascade
	}

	public Object getKey() {
		return id;
	}

	public void setId(String value) {
		id = value;
	}

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String value) {
		name = value;
	}

	public String getTaxCategoryId() {
		return taxcategoryid;
	}

	public void setTaxCategoryId(String value) {
		taxcategoryid = value;
	}

	public Date getValidFrom() {
		return validfrom;
	}

	public Date getValidTo() {
		return validto;
	}

	public void setValidTo(Date validto) {
		this.validto = validto;
	}

	public String getTaxCustCategoryId() {
		return taxcustcategoryid;
	}

	public void setTaxCustCategoryId(String value) {
		taxcustcategoryid = value;
	}

	public String getParentId() {
		return parentid;
	}

	public void setParentId(String value) {
		parentid = value;
	}

	public double getRate() {
		return rate;
	}

	public void setRate(double value) {
		rate = value;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public boolean isCascade() {
		return cascade;
	}

	public void setCascade(boolean value) {
		cascade = value;
	}

	public Integer getOrder() {
		return order;
	}

	public Integer getApplicationOrder() {
		return order == null ? Integer.MAX_VALUE : order.intValue();
	}

	public void setOrder(Integer value) {
		order = value;
	}

	@Override
	public String toString() {
		return name;
	}
}
