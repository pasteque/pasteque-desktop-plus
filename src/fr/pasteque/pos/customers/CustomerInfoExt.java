//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2007-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.customers;

import fr.pasteque.format.Formats;
import fr.pasteque.pos.forms.AppLocal;
import fr.pasteque.pos.util.RoundUtils;

import java.util.Date;

import org.json.JSONObject;

/**
 *
 * @author adrianromero
 */
public class CustomerInfoExt extends CustomerInfo {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4903612793225581769L;
	protected String taxcustomerid;
	protected Integer discountProfileId;
	protected String notes;
	protected boolean visible;
	protected String card;
	protected Double maxdebt;
	protected Date curdate;
	protected Double curdebt;
	protected double prepaid;
	protected String firstname;
	protected String lastname;
	protected String email;
	protected String phone;
	protected String phone2;
	protected String fax;
	protected String address;
	protected String address2;
	protected String postal;
	protected String city;
	protected String region;
	protected String country;
	protected Date dateofbirth;
	protected Integer levelSearch;
	protected String type;
	protected String source;
	protected boolean newsletter;
	protected boolean partenaires;
	protected String civilite;
	private Date dateCreation;
	private Date dateUpdate;
	protected boolean isParent;

	/** Creates a new instance of UserInfoBasic */
	public CustomerInfoExt(String id) {
		super(id);
	} 

	public CustomerInfoExt(JSONObject o) {
		super(null);
		if (!o.isNull("id")) {
			this.id = o.getString("id");
		}
		if (!o.isNull("number")) {
			this.taxid = o.getString("number");
		}
		if (!o.isNull("dispName")) {
			this.name = o.getString("dispName");
		}
		if (!o.isNull("key")) {
			this.searchkey = o.getString("key");
		}
		if (!o.isNull("custTaxId")) {
			this.taxcustomerid = o.getString("custTaxId");
		}
		if (!o.isNull("discountProfileId")) {
			this.discountProfileId = o.getInt("discountProfileId");
		}
		if (!o.isNull("note")) {
			this.notes = o.getString("note");
		}

		if (!o.isNull("visible")) {
			this.visible = o.getBoolean("visible");
		}else {
			this.visible = false;
		}

		if (!o.isNull("card")) {
			this.card = o.getString("card");
		}
		if (!o.isNull("maxDebt")) {
			this.maxdebt = o.getDouble("maxDebt");
		}
		if (!o.isNull("debtDate")) {
			this.curdate = new Date(o.getLong("debtDate"));
		}
		if (!o.isNull("currDebt")) {
			this.curdebt = o.getDouble("currDebt");
		}
		this.prepaid = o.getDouble("prepaid");
		if (!o.isNull("firstName")) {
			this.firstname = o.getString("firstName");
		}
		if (!o.isNull("lastName")) {
			this.lastname = o.getString("lastName");
		}
		if (!o.isNull("email")) {
			this.email = o.getString("email");
		}
		if (!o.isNull("phone1")) {
			this.phone = o.getString("phone1");
		}
		if (!o.isNull("phone2")) {
			this.phone2 = o.getString("phone2");
		}
		if (!o.isNull("fax")) {
			this.fax = o.getString("fax");
		}
		if (!o.isNull("addr1")) {
			this.address = o.getString("addr1");
		}
		if (!o.isNull("addr2")) {
			this.address2 = o.getString("addr2");
		}
		if (!o.isNull("zipCode")) {
			this.postal = o.getString("zipCode");
		}
		if (!o.isNull("city")) {
			this.city = o.getString("city");
		}
		if (!o.isNull("region")) {
			this.region = o.getString("region");
		}
		if (!o.isNull("country")) {
			this.country = o.getString("country");
		}
		if (!o.isNull("dateOfBirth")) {
			this.dateofbirth = new Date(o.getLong("dateOfBirth"));
		}

		if (!o.isNull("type")) {
			this.type = o.getString("type");
		}

		if (!o.isNull("source")) {
			this.source = o.getString("source");
		}

		if (!o.isNull("civilite")) {
			this.civilite = o.getString("civilite");
		}

		if (!o.isNull("newsletter")) {
			this.newsletter = o.getBoolean("newsletter");
		}else {
			this.newsletter = false;
		}

		if (!o.isNull("partenaires")) {
			this.partenaires = o.getBoolean("partenaires");
		}else {
			this.partenaires = false;
		}

		if (!o.isNull("dateCreation")) {
			this.dateCreation = new Date(o.getLong("dateCreation"));
		}

		if (!o.isNull("dateUpdate")) {
			this.dateUpdate = new Date(o.getLong("dateUpdate"));
		}

		this.setParent(o.isNull("idParent"));
	}


	public JSONObject toJSON() {
		JSONObject o = new JSONObject();
		if (this.address != null) {
			o.put("address1", this.address);
		}else {
			o.put("address1", JSONObject.NULL);
		}

		if (this.address2 != null) {
			o.put("address2", this.address2);
		}else {
			o.put("address2", JSONObject.NULL);
		}

		if (this.card != null) {
			o.put("cardNumber", this.card);
		}else {
			o.put("cardNumber", JSONObject.NULL);
		}

		if (this.city != null) {
			o.put("city", this.city);
		}else {
			o.put("city", JSONObject.NULL);
		}

		if (this.civilite != null) {
			o.put("civilite", this.civilite);
		}else {
			o.put("civilite", "STE");
		}

		if (this.region != null) {
			o.put("region", this.region);
		}else {
			o.put("region", JSONObject.NULL);
		}

		if (this.country != null) {
			o.put("country", this.country);
		}else {
			o.put("country", JSONObject.NULL);
		}

		if (this.dateCreation != null) {
			o.put("creationDate", this.dateCreation.getTime());
		}else {
			o.put("creationDate", new Date().getTime());
		}

		if (this.dateofbirth != null) {
			o.put("dateOfBirth", this.dateofbirth.getTime());
		}else {
			o.put("dateOfBirth", JSONObject.NULL);
		}

		if (this.email != null) {
			o.put("email", this.email);
		}else {
			o.put("email", JSONObject.NULL);
		}

		if (this.firstname != null) {
			o.put("firstName", this.firstname);
		} else {
			o.put("firstName", JSONObject.NULL);
		}

		if (this.id != null) {
			o.put("id", this.id);
		} else {
			o.put("id", JSONObject.NULL);
		}

		if (this.lastname != null) {
			o.put("lastName", this.lastname);
		} else {
			o.put("lastName", JSONObject.NULL);
		}

		if (this.dateUpdate != null) {
			o.put("lastUpdate", this.dateUpdate.getTime());
		}else {
			o.put("lastUpdate", new Date().getTime());
		}

		if (this.newsletter != false) {
			o.put("newsletter", this.newsletter);
		} else {
			o.put("newsletter", false);
		}

		if (this.notes != null) {
			o.put("note", this.notes);
		} else {
			o.put("note", JSONObject.NULL);
		}

		if (this.partenaires != false) {
			o.put("partenaires", this.partenaires);
		} else {
			o.put("partenaires", false);
		}

		if (this.phone != null) {
			o.put("phone1", this.phone);
		} else {
			o.put("phone1", JSONObject.NULL);
		}

		if (this.phone2 != null) {
			o.put("phone2", this.phone2);
		} else {
			o.put("phone2", JSONObject.NULL);
		}

		if (this.searchkey != null) {
			o.put("searchKey", this.searchkey);
		} else {
			o.put("searchKey", JSONObject.NULL);
		}

		if (this.source != null) {
			o.put("source", this.source);
		} else {
			o.put("source", JSONObject.NULL);
		}

		if (this.type != null) {
			o.put("type", this.type);
		} else {
			o.put("type", JSONObject.NULL);
		}

		if (this.postal != null) {
			o.put("zipCode", this.postal);
		} else {
			o.put("zipCode", JSONObject.NULL);
		}

		if (this.dateCreation != null) {
			o.put("dateCreation", this.dateCreation.getTime());
		}else {
			o.put("dateCreation", JSONObject.NULL);
		}

		if (this.dateUpdate != null) {
			o.put("dateUpdate", this.dateUpdate.getTime());
		}else {
			o.put("dateUpdate", JSONObject.NULL);
		}

		o.put("isParent", this.isParent);

		return o;
	}

	public String getTaxCustCategoryID() {
		return taxcustomerid;
	}

	public void setTaxCustomerID(String taxcustomerid) {
		this.taxcustomerid = taxcustomerid;
	}

	public Integer getDiscountProfileId() {
		return this.discountProfileId;
	}
	public void setDiscountProfileId(Integer id) {
		this.discountProfileId = id;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public boolean isVisible() {
		return visible;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	public String getCard() {
		return card;
	}

	public void setCard(String card) {
		this.card = card;
	}

	public Double getMaxdebt() {
		return maxdebt;
	}

	public String printMaxDebt() {       
		return Formats.CURRENCY.formatValue(RoundUtils.getValue(getMaxdebt()));
	}

	public void setMaxdebt(Double maxdebt) {
		this.maxdebt = maxdebt;
	}

	public Date getCurdate() {
		return curdate;
	}

	public void setCurdate(Date curdate) {
		this.curdate = curdate;
	}

	public Double getCurdebt() {
		return curdebt;
	}

	public String printCurDebt() {       
		return Formats.CURRENCY.formatValue(RoundUtils.getValue(getCurdebt()));
	}

	public void setCurdebt(Double curdebt) {
		this.curdebt = curdebt;
	}

	public void updateCurDebt(Double amount, Date d) {

		curdebt = curdebt == null ? amount : curdebt + amount;

		if (RoundUtils.compare(curdebt, 0.0) > 0) {
			if (curdate == null) {
				// new date
				curdate = d;
			}
		} else if (RoundUtils.compare(curdebt, 0.0) == 0) {
			curdebt = null;
			curdate = null;
		} else { // < 0
			curdate = null;
		}
	}

	public double getPrepaid() {
		return this.prepaid;
	}

	public void setPrepaid(double prepaid) {
		this.prepaid = prepaid;
	}

	/** Update prepaid account. Use positive amount to fill the account
	 * and negative value to use it.
	 */
	public void updatePrepaid(double amount) {
		this.prepaid += amount;
	}

	public String printPrepaid() {
		return Formats.CURRENCY.formatValue(RoundUtils.getValue(getPrepaid()));
	}


	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPhone2() {
		return phone2;
	}

	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getPostal() {
		return postal;
	}

	public void setPostal(String postal) {
		this.postal = postal;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Date getDateofbirth() {
		return dateofbirth;
	}

	public void setDateofbirth(Date dateofbirth) {
		this.dateofbirth = dateofbirth;
	} 

	public Integer getLevelSearch() {
		return levelSearch;
	}

	public void setLevelSearch(Integer levelSearch) {
		this.levelSearch = levelSearch;
	}     
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public boolean isNewsletter() {
		return newsletter;
	}

	public void setNewsletter(boolean newsletter) {
		this.newsletter = newsletter;
	}

	public boolean isPartenaires() {
		return partenaires;
	}

	public void setPartenaires(boolean partenaires) {
		this.partenaires = partenaires;
	}

	public String getCivilite() {
		return civilite;
	}

	public void setCivilite(String civilite) {
		this.civilite = civilite;
	}

	public Date getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}

	public Date getDateUpdate() {
		return dateUpdate;
	}

	public void setDateUpdate(Date dateUpdate) {
		this.dateUpdate = dateUpdate;
	}   	

	public String getNumPhone() {
		return (this.phone!=null? this.phone : (this.phone2!=null?this.phone2 : ""));
	}

	public String getMail() {
		return (email!=null?email:"");
	}
	public String getCodePostal() {
		return (postal!=null?postal:"");
	}
	public String getVille() {
		return (city!=null? city : "");
	}
	public String printCustInfoOld() {
		return AppLocal.getIntString("MsgBox.CustomerInfo", getName(), printPrepaid(), printCurDebt());
	}

	public String printCustInfo() {
		return AppLocal.getIntString("messageBox.CustomerInfo", getName(), printPrepaid(), getCodePostal(),getVille(),getNumPhone(),getMail());
	}

	@Override
	public String toString() {
		return String.format("<html>%s&nbsp;-&nbsp;%s<br>%s&nbsp;-&nbsp;%s",getSearchkey(), getName(), getCodePostal(),getVille());
	} 

	public boolean isParent() {
		return isParent;
	}
	public void setParent(boolean isParent) {
		this.isParent = isParent;
	}

	@Override
	public boolean equals (Object in) {
		if(in == null) {
			return false;
		}else if( in instanceof CustomerInfoExt) {
			CustomerInfoExt inObject = (CustomerInfoExt) in;
			return this.id == null ? false : this.id.equals(inObject.getId());
		}else {
			return false;
		}       
	}
}
