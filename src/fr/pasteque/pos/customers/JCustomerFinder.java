//    POS-Tech
//    Based upon Openbravo POS
//
//    Copyright (C) 2007-2009 Openbravo, S.L.
//                       2012 Scil (http://scil.coop)
//
//    This file is part of POS-Tech.
//
//    POS-Tech is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    POS-Tech is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with POS-Tech.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.customers;

import fr.pasteque.basic.BasicException;
import fr.pasteque.beans.EncryptDecrypt;
import fr.pasteque.data.gui.MessageInf;
import fr.pasteque.data.loader.ImageLoader;
import fr.pasteque.data.user.OseToken;
import fr.pasteque.pos.forms.AppConfig;
import fr.pasteque.pos.forms.AppLocal;
import fr.pasteque.pos.forms.AppUser;
import fr.pasteque.pos.widgets.Criteria;
import fr.pasteque.pos.widgets.JModuleSearch;
import fr.pasteque.pos.widgets.WidgetsBuilder;

import java.awt.Component;
import java.awt.Desktop;
import java.awt.Dialog;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListModel;
import javax.xml.bind.DatatypeConverter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;


/**
 *
 * @author  adrianromero
 */
public class JCustomerFinder extends javax.swing.JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8984588264688342657L;
	private CustomerInfoExt selectedCustomer;
	private DataLogicCustomers dlc;
	private static AppUser appUser;
	private String fileJson = AppConfig.loadedInstance.getProperty("file.userjson");
	private JList<CustomerInfoExt> JList_Result = new JList<CustomerInfoExt>();
	private JList<CustomerInfoExt> JList_Result_Copy = new JList<CustomerInfoExt>();

	/** Creates new form JCustomerFinder */
	private JCustomerFinder(java.awt.Frame parent, boolean modal) {
		super(parent, modal);
	}

	/** Creates new form JCustomerFinder */
	private JCustomerFinder(java.awt.Dialog parent, boolean modal) {
		super(parent, modal);
	}

	public static JCustomerFinder getCustomerFinder(Component parent,
			DataLogicCustomers dlCustomers, AppUser user) {
		Window window = getWindow(parent);
		appUser = user;
		JCustomerFinder myMsg;
		if (window instanceof Frame) { 
			myMsg = new JCustomerFinder((Frame) window, true);
		} else {
			myMsg = new JCustomerFinder((Dialog) window, true);
		}
		myMsg.init(dlCustomers);
		myMsg.applyComponentOrientation(parent.getComponentOrientation());
		return myMsg;
	}

	public CustomerInfoExt getSelectedCustomer() {
		return selectedCustomer;
	}

	private void init(DataLogicCustomers dlCustomers) {
		this.dlc = dlCustomers;
		initComponents();

		getRootPane().setDefaultButton(jcmdOK);

		selectedCustomer = null;
	}
	/**
	 * PG : Lors de l'ouverture de la fenêtre de recherche client,
	 * si aucun client sélectionné on retourne les 5 derniers clients créé,
	 * sinon on retourne le client sélectionné.
	 * @param customer
	 */
	public void search(CustomerInfo customer) {
		if (customer == null || customer.getName() == null || customer.getName().equals("")) {
			// Default filter: show top 5
			automaticLastClientSearch(5);
			//automaticTop10ClientSearch();
		} else {

			searchModule.reloadSearch(fileJson, customer);       
			executeSearch();
		}
	}



	@SuppressWarnings("unchecked")
	public void executeSearch() {

		try {
			/**Modification de la recherche client*/
			List<CustomerInfoExt> results = this.dlc.searchCustomersOpt(searchModule.getJList_Search().getModel(), "customers");

			searchModule.getJList_Result().setModel(new MyListData<CustomerInfoExt>(results));
			if (searchModule.getJList_Result().getModel().getSize() > 0) {
				searchModule.getJList_Result().setSelectedIndex(0);
			}
		} catch (BasicException e) {
			e.printStackTrace();
			MessageInf msg = new MessageInf(MessageInf.SGN_WARNING, AppLocal.getIntString("Label.LoadError"), e);
			msg.show(this);

		}        
	}


	/** Automatic filtering of customers when choosing the form
	 * to choose a client appears. Displays the last 5
	 * of the customer's list by their id
	 */
	@SuppressWarnings("unchecked")
	public void automaticLastClientSearch(Integer maxNumber){
		try {
			searchModule.getJList_Result().setModel(new MyListData<CustomerInfoExt>(this.dlc.getLastCustomerList(maxNumber)));
		} catch (BasicException e) {
			e.printStackTrace();
		}
		if (searchModule.getJList_Result().getModel().getSize() > 0) {
			searchModule.getJList_Result().setSelectedIndex(0);
		}
	}
	private static Window getWindow(Component parent) {
		if (parent == null) {
			return new JFrame();
		} else if (parent instanceof Frame || parent instanceof Dialog) {
			return (Window) parent;
		} else {
			return getWindow(parent.getParent());
		}
	}

	private static class MyListData<T> extends javax.swing.AbstractListModel<T> {

		/**
		 * 
		 */
		private static final long serialVersionUID = 2290182794229424994L;
		private java.util.List<T> m_data;

		public MyListData(java.util.List<T> data) {
			m_data = data;
		}

		public T getElementAt(int index) {
			return m_data.get(index);
		}

		public int getSize() {
			return m_data.size();
		} 
	}   

	private void initComponents() {
		JPanel jPanel2 = new JPanel();
		//        m_jKeys = new JEditorKeys();
		JPanel jPanel3 = new JPanel();
		JPanel jPanelBtsCustomer = new javax.swing.JPanel();
		searchModule = new JModuleSearch(fileJson, JList_Result, JList_Result_Copy);

		JPanel jPanel4 = new JPanel();
		JPanel jPanel8 = new JPanel();
		JPanel jPanel1 = new JPanel();
		jcmdOK = WidgetsBuilder.createButton(ImageLoader.readImageIcon("button_ok.png"),
				AppLocal.getIntString("Button.OK"),
				WidgetsBuilder.SIZE_MEDIUM);
		jcmdCancel = WidgetsBuilder.createButton(ImageLoader.readImageIcon("button_cancel.png"),
				AppLocal.getIntString("Button.Cancel"),
				WidgetsBuilder.SIZE_MEDIUM);

		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
		setTitle(AppLocal.getIntString("form.customertitle")); // NOI18N

		jPanel2.setLayout(new java.awt.BorderLayout());
		//        jPanel2.add(m_jKeys,java.awt.BorderLayout.NORTH);

		// bouton ajout d'un nouveau client
		GridLayout gl = new GridLayout(8, 1, 5, 5);
		jPanelBtsCustomer.setLayout(gl);
		JButton addNewCustomerBtn = WidgetsBuilder.createButton(AppLocal.getIntString("button.newCustomer"),
				WidgetsBuilder.SIZE_MEDIUM);
		addNewCustomerBtn.setFocusPainted(false);
		addNewCustomerBtn.setFocusable(false);
		addNewCustomerBtn.setRequestFocusEnabled(false);
		addNewCustomerBtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				addNewCustomerBtnActionPerformed(evt);
			}

		});
		jPanelBtsCustomer.add(addNewCustomerBtn);

		// bouton actualisation clients
		JButton reloadCustomerBtn = WidgetsBuilder.createButton(AppLocal.getIntString("button.reloadCustomers"),
				WidgetsBuilder.SIZE_MEDIUM);
		reloadCustomerBtn.setFocusPainted(false);
		reloadCustomerBtn.setFocusable(false);
		reloadCustomerBtn.setRequestFocusEnabled(false);
		reloadCustomerBtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				reloadCustomersBtnActionPerformed(evt);
			}
		});
		jPanelBtsCustomer.add(reloadCustomerBtn);

		jPanel2.add(jPanelBtsCustomer,java.awt.BorderLayout.NORTH);
		getContentPane().add(jPanel2, java.awt.BorderLayout.LINE_END);

		jPanel3.setLayout(new java.awt.BorderLayout());

		jPanel3.add(searchModule, java.awt.BorderLayout.PAGE_START);

		jPanel4.setBorder(javax.swing.BorderFactory.createEmptyBorder(5, 5, 5, 5));
		jPanel4.setLayout(new java.awt.BorderLayout());

		searchModule.getSearchBtn().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				searchBtnActionPerformed(evt);
			}
		});

		searchModule.getJList_Result().setFocusable(false);
		searchModule.getJList_Result().setRequestFocusEnabled(false);
		searchModule.getJList_Result().setCellRenderer(new CustomerRenderer());
		searchModule.getJList_Result().addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				jListCustomersMouseClicked(evt);
			}
		});
		searchModule.getJList_Result().addListSelectionListener(new javax.swing.event.ListSelectionListener() {
			public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
				jListCustomersValueChanged(evt);
			}
		});

		jPanel8.setLayout(new java.awt.BorderLayout());

		jcmdOK.setEnabled(false);
		jcmdOK.setFocusPainted(false);
		jcmdOK.setFocusable(false);
		jcmdOK.setRequestFocusEnabled(false);
		jcmdOK.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jcmdOKActionPerformed(evt);
			}
		});
		jPanel1.add(jcmdOK);

		jcmdCancel.setFocusPainted(false);
		jcmdCancel.setFocusable(false);
		jcmdCancel.setRequestFocusEnabled(false);
		jcmdCancel.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jcmdCancelActionPerformed(evt);
			}
		});
		jPanel1.add(jcmdCancel);

		jPanel8.add(jPanel1, java.awt.BorderLayout.LINE_END);

		jPanel3.add(jPanel8, java.awt.BorderLayout.SOUTH);

		getContentPane().add(jPanel3, java.awt.BorderLayout.CENTER);

		java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
		setBounds((screenSize.width-613)/2, (screenSize.height-610)/2, 613, 610);
	}

	private void jcmdOKActionPerformed(java.awt.event.ActionEvent evt) {

		selectedCustomer = (CustomerInfoExt) searchModule.getJList_Result().getSelectedValue();
		dispose();

	}

	private void jcmdCancelActionPerformed(java.awt.event.ActionEvent evt) {

		dispose();

	}

	private void searchBtnActionPerformed(java.awt.event.ActionEvent evt) {
		if (searchModule.getJList_Search().getModel().getSize()>0) {
			executeSearch();
		}
	}

	private void jListCustomersValueChanged(javax.swing.event.ListSelectionEvent evt) {

		jcmdOK.setEnabled(searchModule.getJList_Result().getSelectedValue() != null);

	}

	private void jListCustomersMouseClicked(java.awt.event.MouseEvent evt) {
		selectedCustomer = (CustomerInfoExt) searchModule.getJList_Result().getSelectedValue();
		if (evt.getClickCount() == 2 && (evt.getButton() == MouseEvent.BUTTON1)) {
			dispose();
		}else if (selectedCustomer.getLevelSearch() != 2) {
			if ((evt.getButton() != MouseEvent.BUTTON1) && Desktop.isDesktopSupported()) {
				final Desktop dt = Desktop.getDesktop();
				//TODO rendre ceci paramétrable via les ressources côté serveur
				//TODO gérer l'authentification de l'utilisateur
				//http://127.0.0.1:8080/ose-server/dashboard#/products/fd897f61ef4cbc924fe2d0ed6abb3436/form
				String backOfficeUrl;
				try {
					backOfficeUrl = AppConfig.loadedInstance.getProperty("server.backoffice") + "/"+ AppConfig.loadedInstance.getProperty("server.racine") + "?token="+EncryptDecrypt.encrypt(new OseToken(appUser.getName()).toString())+"#/customers/" + selectedCustomer.getId() + "/form?_action=view";
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					backOfficeUrl = AppConfig.loadedInstance.getProperty("server.backoffice") + "/"+ AppConfig.loadedInstance.getProperty("server.racine") + "#/customers/" + selectedCustomer.getId() + "/form?_action=view";
				}
				if (dt.isSupported(Desktop.Action.BROWSE)) {
					try {
						dt.browse(new URI(backOfficeUrl));
					} catch (URISyntaxException exc) {
						//
					} catch (IOException exc) {
						//
					}
				}
			}
		}else if ((evt.getButton() != MouseEvent.BUTTON1) && selectedCustomer.getLevelSearch() == 2){
			javax.swing.JOptionPane.showMessageDialog(null,"La fiche client n'est pas disponible pour le moment"); 
		}

	}


	private void addNewCustomerBtnActionPerformed(ActionEvent evt) {
		if (Desktop.isDesktopSupported()) {
			final Desktop dt = Desktop.getDesktop();
			//TODO rendre ceci paramétrable via les ressources côté serveur
			//TODO gérer l'authentification de l'utilisateur
			//2021-02 EDU Ajout des critères de recherches utilisés pour préremplir
			ListModel<Criteria> listModel = searchModule.getJList_Search().getModel();

			HashMap<String , Object> listCriteria = new HashMap<String , Object>();

			for( int i  = 0; i < listModel.getSize(); i++){
				// On pose dans le Map lisible par la création  


				Criteria element = listModel.getElementAt(i);

				switch(element.getColumn()) {
				case "lastName":
				case "firstName": 
					listCriteria.put(element.getColumn().toLowerCase(), element.getSearchvalue().get(0));
					break;
				case "phone" :
					listCriteria.put("searchphone", element.getSearchvalue().get(0));
					break;
				case "email" :
					listCriteria.put("searchmail", element.getSearchvalue().get(0));
					break;					

				}


			}
			String criteriaInString = "";
			if(!listCriteria.isEmpty()) {
				ObjectMapper mapper = new ObjectMapper();
				try {
					criteriaInString = mapper.writeValueAsString(listCriteria);
					criteriaInString = DatatypeConverter.printBase64Binary(criteriaInString.getBytes());
					criteriaInString = "&criteria="+criteriaInString;
				} catch (JsonProcessingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			String backOfficeUrl;
			try {
				backOfficeUrl = AppConfig.loadedInstance.getProperty("server.backoffice") + "/" + AppConfig.loadedInstance.getProperty("server.racine") + "?token="+EncryptDecrypt.encrypt(new OseToken(appUser.getName()).toString())+"#/customers/form?_action=create"+criteriaInString;
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				backOfficeUrl = AppConfig.loadedInstance.getProperty("server.backoffice") + "/" + AppConfig.loadedInstance.getProperty("server.racine") + "#/customers/form?_action=create"+criteriaInString;
			}
			if (dt.isSupported(Desktop.Action.BROWSE)) {
				try {
					dt.browse(new URI(backOfficeUrl));
				} catch (URISyntaxException exc) {
					//
				} catch (IOException exc) {
					//
				}
			}
		}

	}
	private void reloadCustomersBtnActionPerformed(ActionEvent evt) {
		DataLogicCustomers dlCust = new DataLogicCustomers();
		dlCust.preloadCustomers(new Date(), AppConfig.loadedInstance);
		search(null);

	}


	private javax.swing.JButton jcmdCancel;
	private javax.swing.JButton jcmdOK;
	//    private JEditorKeys m_jKeys;
	private JModuleSearch searchModule;

}
