//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2007-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.customers;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.ListModel;

import org.json.JSONArray;
import org.json.JSONObject;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.pasteque.basic.BasicException;
import fr.pasteque.data.loader.ServerLoader;
import fr.pasteque.pos.caching.CustomersCache;
import fr.pasteque.pos.forms.AppConfig;
import fr.pasteque.pos.forms.AppProperties;
import fr.pasteque.pos.util.AltEncrypter;
import fr.pasteque.pos.util.URLTextGetter.ServerException;
import fr.pasteque.pos.widgets.Criteria;

/**
 *
 * @author adrianromero
 */
public class DataLogicCustomers {

    private static Logger logger = Logger.getLogger("fr.pasteque.pos.customers.DataLogicCustomers");
    
    // TODO: use local database for caching
    private static List<DiscountProfile> discProfileCache;
    
    /** Load customers list from server 
     * @param lastUpdate */
    private List<CustomerInfoExt> loadCustomers(AppProperties props) throws BasicException {
        try {
            ServerLoader loader = new ServerLoader();
            ServerLoader.Response r ;

			Date realDate = new Date(new Date().getTime() - Integer.parseInt(props.getProperty("cache.loadCustomerSince"))* 60*1000);
			//EDU 2021 07 on uniformise et le delta temps est en paramètre
			//TODO le getAll pourrait avantageusement être remplacé par un getCriteria bien paramétré
			// par exemple sur le centre de profit, la zone géographique , la proba de passage du client ...
			// cependant le bonus octroyé par la présence en cache du client est faible 
			// devant le gain de temps visible par l'utilisateur au démarrage
            r = loader.read("CustomersAPI", "getAll" , "date" , String.valueOf(realDate.getTime()));

            if (r.getStatus().equals(ServerLoader.Response.STATUS_OK)) {
                List<CustomerInfoExt> data = new ArrayList<CustomerInfoExt>();
                JSONArray a = r.getArrayContent();
                for (int i = 0; i < a.length(); i++) {
                    JSONObject o = a.getJSONObject(i);
                    CustomerInfoExt customer = new CustomerInfoExt(o);
                    data.add(customer);
                }
                return data;
            }
        } catch (Exception e) {
            throw new BasicException(e);
        }
        return null;
    }
    
    private List<String> loadTopCustomers() throws BasicException {
        try {
            ServerLoader loader = new ServerLoader();
            ServerLoader.Response r = loader.read("CustomersAPI", "getTop");
            if (r.getStatus().equals(ServerLoader.Response.STATUS_OK)) {
                List<String> data = new ArrayList<String>();
                JSONArray a = r.getArrayContent();
                for (int i = 0; i < a.length(); i++) {
                    data.add(a.getString(i));
                }
                return data;
            }
        } catch (Exception e) {
            throw new BasicException(e);
        }
        return null;
    }
    /** Preload and update cache if possible. Return true if succes. False
     * otherwise and cache is not modified.
     */
    public boolean preloadCustomers(Date lastUpdate , AppProperties props) {
        try {
            logger.log(Level.INFO, "Preloading customers");
            List<CustomerInfoExt> data = this.loadCustomers(props);
            List<String> topIds = this.loadTopCustomers();
            if (data == null) {
                return false;
            }
            try {
                CustomersCache.refreshCustomers(data,lastUpdate);
                CustomersCache.refreshRanking(topIds);
            } catch (BasicException e) {
                e.printStackTrace();
                return false;
            }
            return true;
        } catch (BasicException e) {
            e.printStackTrace();
            return false;
        }
    }

    /** Get all customers */
    public List<CustomerInfoExt> getCustomerList() throws BasicException {
        return CustomersCache.getCustomers();
    }

    /** Get customer from local cache */
    public CustomerInfoExt getCustomer(String id) throws BasicException {
        return CustomersCache.getCustomer(id);
    }
    /** Load a customer from server, update it in cache and return it.
     * This is an asynchronous call.
     */
    public void updateCustomer(final String id,
            final CustomerListener callback) {
        Thread t = new Thread() {
                public void run() {
                    try {
                        logger.log(Level.INFO, "Refreshing customer");
                        ServerLoader loader = new ServerLoader();
                        ServerLoader.Response r = loader.read("CustomersAPI",
                                "get", "id", id);
                        if (r.getStatus().equals(ServerLoader.Response.STATUS_OK)) {
                            //List<CustomerInfoExt> data = new ArrayList<CustomerInfoExt>();
                            JSONObject o = r.getObjContent();
                            CustomerInfoExt customer = new CustomerInfoExt(o);
                            CustomerInfoExt customerCache = CustomersCache.getCustomer(id);
                            if (customerCache != null) {
                            	customer.setLevelSearch(0);
                            }else {
                            	customer.setLevelSearch(1);
                            }
                            CustomersCache.refreshCustomer(customer);
                            if (callback != null) {
                                callback.customerLoaded(customer);
                            }
                        } else {
                            logger.log(Level.WARNING,
                                    "Unable to load customer: "
                                    + r.getStatus());
                        }
                    } catch (Exception e) {
//                    	System.out.print("Client inexistant en local. UpdateClient impossible !");
                        logger.log(Level.WARNING,
                        		"Client inexistant en local. UpdateClient impossible !");
                        if (callback != null) {
                            callback.customerLoaded(null);
                        }
                    }
                }
            };
        t.start();
    }

    public CustomerInfoExt getCustomerByCard(String card)
        throws BasicException {
        return CustomersCache.getCustomerByCard(card);
    }
    
    /** PG : Test s'il y a une location par défaut 
     * 
     * @return
     * @throws SocketTimeoutException
     * @throws ServerException
     * @throws IOException
     */
    public static JSONObject getDefault() throws SocketTimeoutException, ServerException, IOException {

		ServerLoader loader = new ServerLoader();
		ServerLoader.Response r = loader.read("LocationsAPI", "getDefault");
		
		if (r.getStatus().equals(ServerLoader.Response.STATUS_OK)) {
			JSONObject o = r.getObjContent();
			return o;
		} else {
			return null;
		}
	}
    
    /** PG : Récupération url 
     * 
     * @return
     * @throws SocketTimeoutException
     * @throws ServerException
     * @throws IOException
     * @throws BasicException 
     */
    public static List<String> getUrl() throws BasicException {
    	try {
			ServerLoader loader = new ServerLoader();
			ServerLoader.Response r = loader.read("LocationsAPI", "getUrl");
			
			if (r.getStatus().equals(ServerLoader.Response.STATUS_OK)) {
	            List<String> urls = new ArrayList<String>();
	            JSONArray a = r.getArrayContent();
	            for (int i = 0; i < a.length(); i++) {
	            	JSONObject o = a.getJSONObject(i);
	                urls.add(o.getString("url"));
	            }
	            return urls;
	        }
    } catch (Exception e) {
        throw new BasicException(e);
    }
    return null;
	}
    
    /** Search customers list from server
     *  
     * @param listModel
     * @param customersExcept
     * @param location
     * @param url
     * @return
     * @throws BasicException
     */
    private static List<CustomerInfoExt> searchCustomers(ListModel<Criteria> listModel, String customersExcept, String location, String url) throws BasicException {
		List<CustomerInfoExt> data = new ArrayList<CustomerInfoExt>();
		String time = AppConfig.loadedInstance.getProperty("server.backoffice.timeout");
    	try {
            ServerLoader loader = null;
            
            if (location.equals("central")) {
	            String user = AppConfig.loadedInstance.getProperty("db.user");
	    		String password = AppConfig.loadedInstance.getProperty("db.password");
	    		if (password != null && password.startsWith("crypt:")) {
	    			// the password is encrypted
	    			AltEncrypter cypher = new AltEncrypter("cypherkey" + user);
	    			password = cypher.decrypt(password.substring(6));
	    		}
	    		
	            loader = new ServerLoader(url, user, password);
            }else if (location.equals("local")) {
            	
            	loader = new ServerLoader();
            }
            
            ServerLoader.Response r ;
            
            List<String> getCriteriaParms = new LinkedList<String>();
            
			for( int i  = 0; i < listModel.getSize(); i++){
				
				// Conversion de l objet criteria en chaine de charactere au format json
				ObjectMapper mapper = new ObjectMapper();
				String criteriaInString = mapper.writeValueAsString(listModel.getElementAt(i));
				
				getCriteriaParms.add(listModel.getElementAt(i).getColumn());
				getCriteriaParms.add(criteriaInString); 

			}
			
			getCriteriaParms.add("customersExcept"); 
			getCriteriaParms.add(customersExcept);	
			
			getCriteriaParms.add("parentOnly");
			getCriteriaParms.add("true");
			
			String[] arr = getCriteriaParms.toArray(new String[0]);
			
			if(loader != null) {
				if (location.equals("central")) {
					
					 ExecutorService executor = Executors.newSingleThreadExecutor();
				     Future<String> future = executor.submit(loader);
				     
				     try {
				            System.out.println("Started..");
				            System.out.println(future.get(Integer.parseInt(time), TimeUnit.SECONDS));
				            System.out.println("Finished!");
				        } catch (TimeoutException e) {
				            future.cancel(true);
				            System.out.println("Temps de réponse trop long annulation.");
				            return data;
				        }

			        executor.shutdownNow();						
				}
				
	            r = loader.read("CustomersAPI", "getCriteria", arr);
	            
	            if (r.getStatus().equals(ServerLoader.Response.STATUS_OK)) {
	                
	                JSONArray a = r.getArrayContent();
	                for (int z = 0; z < a.length(); z++) {
	                    JSONObject o = a.getJSONObject(z);
	                    CustomerInfoExt customer = new CustomerInfoExt(o);
	                    data.add(customer);
	                }
	                return data;
	            } 
			}else {
				return data;
			}
			
        } catch (Exception e) {
        	return data;
        }
        return null;
    }
    
    
    /**Modification de la recherche client direct par carte de fidélité
     * @throws IOException 
     * @throws ServerException 
     * @throws SocketTimeoutException */
    public static CustomerInfoExt searchCustomersByFidelite(ListModel<Criteria> listModel, String table) throws BasicException {
    	List<CustomerInfoExt> result = new ArrayList<CustomerInfoExt>();
    	List<CustomerInfoExt> result1 = new ArrayList<CustomerInfoExt>();
    	List<CustomerInfoExt> result2 = new ArrayList<CustomerInfoExt>();
    	List<String> customersExceptList= new LinkedList<String>();
    	JSONObject locationDefault = null;
    	String prefix = "";
    	List<String> urls = new ArrayList<String>();
    	String customersExceptString = "";
    		
            // PG : Lance la recherche client sur le BO local 
            //en excluent de la recherche les clients avec un id contenu dans customersExceptString
            result1.addAll(searchCustomers(listModel, customersExceptString, "local", null));
            
            for (CustomerInfoExt customerInfoExt : result1) {
    			customerInfoExt.setLevelSearch(1);
    		}
            result.addAll(result1);
    		
    		
			try {
				locationDefault = getDefault();
			} catch (SocketTimeoutException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ServerException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
    		if (locationDefault != null) {
    		
	    		// PG : Récupére l'ID des clients trouvé dans le BO local pour 
	    		// les concaténer dans customersExceptString 
	       		if(result.size() > 0) {
	       			
	    			for (CustomerInfoExt customer : result) {
	    				customersExceptList.add("\'"+customer.getCard()+"\'");
	    			}
	    		}
	       		
	            for (String s: customersExceptList) {
	            	customersExceptString += prefix + s;
	                prefix = ",";
	            }
	       		
	            urls.addAll(getUrl());
	            
	            for(String url : urls) {
		            // PG : Lance la recherche client sur le BO central et supprime les doublons
		            //en excluent de la recherche les clients avec un id contenu dans customersExceptString
		            result2.addAll(searchCustomers(listModel, customersExceptString, "central", url));
		            for (CustomerInfoExt customerInfoExt : result2) {
		    			customerInfoExt.setLevelSearch(2);
		    		}
		            result.addAll(result2);
	            }
	            
    		}
    	if (result.size() > 0) {
    		return result.get(0);
    	}else {
    		return null; 
    	}
        
    }
    
    /**Modification de la recherche client 
     * @throws IOException 
     * @throws ServerException 
     * @throws SocketTimeoutException */
    public List<CustomerInfoExt> searchCustomersOpt(ListModel<Criteria> listModel, String table) throws BasicException {
    	List<CustomerInfoExt> result = CustomersCache.searchCustomersExactOpt(listModel, table);
    	List<CustomerInfoExt> resultJok = CustomersCache.searchCustomersOpt(listModel,  table);
    	List<CustomerInfoExt> result1 = new ArrayList<CustomerInfoExt>();
    	List<CustomerInfoExt> result2 = new ArrayList<CustomerInfoExt>();
    	List<String> customersExceptList= new LinkedList<String>();
    	JSONObject locationDefault = null;
    	String prefix = "";
    	List<String> urls = new ArrayList<String>();

    		if(resultJok.size() > 0) {
    			resultJok.removeAll(result);
    			result.addAll(resultJok);
    		}
    		
    		for (CustomerInfoExt customerInfoExt : result) {
    			customerInfoExt.setLevelSearch(0);
    		}
    		
    		// PG : Récupére l'ID des clients trouvé dans le cache pour 
    		// les concaténer dans customersExceptString 
    		if (result.size() > 0) {
    			for (CustomerInfoExt customer : result) {
    				customersExceptList.add("\'"+customer.getCard()+"\'");
    			}
    		}
    		
            String customersExceptString = "";
            for (String s: customersExceptList) {
            	customersExceptString += prefix + s;
                prefix = ",";
            }
    		
            // PG : Lance la recherche client sur le BO local 
            //en excluent de la recherche les clients avec un id contenu dans customersExceptString
            result1.addAll(searchCustomers(listModel, customersExceptString, "local", null));
            
            for (CustomerInfoExt customerInfoExt : result1) {
    			customerInfoExt.setLevelSearch(1);
    		}
            result.addAll(result1);
    		
    		
			try {
				locationDefault = getDefault();
			} catch (SocketTimeoutException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ServerException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
    		if (locationDefault != null) {
    		
	    		// PG : Récupére l'ID des clients trouvé dans le BO local pour 
	    		// les concaténer dans customersExceptString 
	       		if(result.size() > 0) {
	       			
	    			for (CustomerInfoExt customer : result) {
	    				customersExceptList.add("\'"+customer.getCard()+"\'");
	    			}
	    		}
	       		
	            for (String s: customersExceptList) {
	            	customersExceptString += prefix + s;
	                prefix = ",";
	            }
	       		
	            urls.addAll(getUrl());
	            
	            for(String url : urls) {
		            // PG : Lance la recherche client sur le BO central et supprime les doublons
		            //en excluent de la recherche les clients avec un id contenu dans customersExceptString
		            result2.addAll(searchCustomers(listModel, customersExceptString, "central", url));
		            for (CustomerInfoExt customerInfoExt : result2) {
		    			customerInfoExt.setLevelSearch(2);
		    		}
		            result.addAll(result2);
	            }
	            
    		}
    		
        return result;
    }

    /** Search customers, use null as argument to disable filter */
    public List<CustomerInfoExt> searchCustomers(String number,
            String searchkey, String name) throws BasicException {
    	List<CustomerInfoExt> result = CustomersCache.searchCustomersExact(number, searchkey, name);
    	List<CustomerInfoExt> resultJok = CustomersCache.searchCustomers(number, searchkey, name); 

    		if(resultJok.size() > 0) {
    			resultJok.removeAll(result);
    			result.addAll(resultJok);
    		}
    		
    		if(name != null) {
    			result.addAll(CustomersCache.searchCustomersSplitted(number, searchkey, name));
    		}
    	
        return result;
    }


    /** Gets the TOP 10 customer's list by number of tickets
     * with their id
     */
    public List<CustomerInfoExt> getTop10CustomerList() throws BasicException {
        return CustomersCache.getTopCustomers();
    }
       
    public int updateCustomerExt(final CustomerInfoExt customer) throws BasicException {
        /*        return new PreparedSentence(s
                , "UPDATE CUSTOMERS SET NOTES = ? WHERE ID = ?"
                , SerializerWriteParams.INSTANCE      
                ).exec(new DataParams() { public void writeValues() throws BasicException {
                        setString(1, customer.getNotes());
                        setString(2, customer.getId());
                        }});*/
        // TODO: reenable customer update
        return 0;
    }

    /*public final SentenceList getReservationsList() {
                return new PreparedSentence(s
            , "SELECT R.ID, R.CREATED, R.DATENEW, C.CUSTOMER, CUSTOMERS.TAXID, CUSTOMERS.SEARCHKEY, COALESCE(CUSTOMERS.NAME, R.TITLE),  R.CHAIRS, R.ISDONE, R.DESCRIPTION " +
              "FROM RESERVATIONS R LEFT OUTER JOIN RESERVATION_CUSTOMERS C ON R.ID = C.ID LEFT OUTER JOIN CUSTOMERS ON C.CUSTOMER = CUSTOMERS.ID " +
              "WHERE R.DATENEW >= ? AND R.DATENEW < ?"
            , new SerializerWriteBasic(new Datas[] {Datas.TIMESTAMP, Datas.TIMESTAMP})
            , new SerializerReadBasic(customerdatas));
        // TODO: enable reservation list
        return null;
    }
    
    public final SentenceExec getReservationsUpdate() {
        return new SentenceExecTransaction(s) {
            public int execInTransaction(Object params) throws BasicException {  
    
                new PreparedSentence(s
                    , "DELETE FROM RESERVATION_CUSTOMERS WHERE ID = ?"
                    , new SerializerWriteBasicExt(customerdatas, new int[]{0})).exec(params);
                if (((Object[]) params)[3] != null) {
                    new PreparedSentence(s
                        , "INSERT INTO RESERVATION_CUSTOMERS (ID, CUSTOMER) VALUES (?, ?)"
                        , new SerializerWriteBasicExt(customerdatas, new int[]{0, 3})).exec(params);                
                }
                return new PreparedSentence(s
                    , "UPDATE RESERVATIONS SET ID = ?, CREATED = ?, DATENEW = ?, TITLE = ?, CHAIRS = ?, ISDONE = ?, DESCRIPTION = ? WHERE ID = ?"
                    , new SerializerWriteBasicExt(customerdatas, new int[]{0, 1, 2, 6, 7, 8, 9, 0})).exec(params);
            }
            };
        // TODO: enable reservation update
        return null;
    }
    
    public final SentenceExec getReservationsDelete() {
                return new SentenceExecTransaction(s) {
            public int execInTransaction(Object params) throws BasicException {  
    
                new PreparedSentence(s
                    , "DELETE FROM RESERVATION_CUSTOMERS WHERE ID = ?"
                    , new SerializerWriteBasicExt(customerdatas, new int[]{0})).exec(params);
                return new PreparedSentence(s
                    , "DELETE FROM RESERVATIONS WHERE ID = ?"
                    , new SerializerWriteBasicExt(customerdatas, new int[]{0})).exec(params);
            }
            };
        // TODO: enable reservation delete
        return null;
    }
    
    public final SentenceExec getReservationsInsert() {
                return new SentenceExecTransaction(s) {
            public int execInTransaction(Object params) throws BasicException {  
    
                int i = new PreparedSentence(s
                    , "INSERT INTO RESERVATIONS (ID, CREATED, DATENEW, TITLE, CHAIRS, ISDONE, DESCRIPTION) VALUES (?, ?, ?, ?, ?, ?, ?)"
                    , new SerializerWriteBasicExt(customerdatas, new int[]{0, 1, 2, 6, 7, 8, 9})).exec(params);

                if (((Object[]) params)[3] != null) {
                    new PreparedSentence(s
                        , "INSERT INTO RESERVATION_CUSTOMERS (ID, CUSTOMER) VALUES (?, ?)"
                        , new SerializerWriteBasicExt(customerdatas, new int[]{0, 3})).exec(params);                
                }
                return i;
            }
            };
        // TODO: enable reservation create
        return null;
    }*/

    private static void loadDiscountProfiles() throws BasicException {
         try {
             ServerLoader loader = new ServerLoader();
             ServerLoader.Response r = loader.read("DiscountProfilesAPI",
                     "getAll");
             if (r.getStatus().equals(ServerLoader.Response.STATUS_OK)) {
                 DataLogicCustomers.discProfileCache = new ArrayList<DiscountProfile>();
                 JSONArray a = r.getArrayContent();
                 for (int i = 0; i < a.length(); i++) {
                     JSONObject o = a.getJSONObject(i);
                     DiscountProfile prof = new DiscountProfile(o);
                     DataLogicCustomers.discProfileCache.add(prof);
                 }
             }
         } catch (Exception e) {
             throw new BasicException(e);
         }
    }

    public List<DiscountProfile> getDiscountProfiles() throws BasicException {
        if (DataLogicCustomers.discProfileCache == null) {
            DataLogicCustomers.loadDiscountProfiles();
        }
        return DataLogicCustomers.discProfileCache;
    }

    public DiscountProfile getDiscountProfile(int id) throws BasicException {
        if (DataLogicCustomers.discProfileCache == null) {
            DataLogicCustomers.loadDiscountProfiles();
        }
        for (DiscountProfile p : DataLogicCustomers.discProfileCache) {
            if (id == p.getId()) {
                return p;
            }
        }
        return null;
    }

    public interface CustomerListener {
        /** Callback for asynchronous customer refresh */
        public void customerLoaded(CustomerInfoExt customer);
    }

	public List<CustomerInfoExt> getLastCustomerList(int maxNumber) throws BasicException{
		return CustomersCache.getLastCustomers(maxNumber);
	}
}
