package fr.pasteque.pos.widgets;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.DefaultListCellRenderer;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.SwingUtilities;

import fr.pasteque.data.loader.ImageLoader;


/**
 * @author Pascal Gattino
 */

public class CustomListRendererSearch extends DefaultListCellRenderer
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final ImageIcon crossIcon = new ImageIcon ("images/cross.png");

    /**
     * Actual renderer.
     */
    private CustomLabel renderer;

    /**
     * Custom renderer constructor.
     * We will use it to create actual renderer component instance.
     * We will also add a custom mouse listener to process close button.
     *
     * @param list our JList instance
     */
    public CustomListRendererSearch(final JList<Criteria>list, final ArrayList<Criteria> listArrayList)
    {
        super();
        renderer = new CustomLabel();

        list.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseReleased(MouseEvent e)
            {
                if ( SwingUtilities.isLeftMouseButton(e))
                {
                    int index = list.locationToIndex(e.getPoint());
                    if (index != -1 && list.isSelectedIndex(index))
                    {
                        Rectangle rect = list.getCellBounds(index, index);
                        Point pointWithinCell = new Point (e.getX() - rect.x, e.getY() - rect.y);
                        Rectangle crossRect = new Rectangle(rect.width-rect.width+5,
                                rect.height/2-12/2,12, 12);
                        if (crossRect.contains(pointWithinCell))
                        {
                            DefaultListModel<Criteria> model = (DefaultListModel<Criteria>)list.getModel();
                            model.remove(index);
                            listArrayList.remove(index);
                        }
                    }
                }
            }
        } );
    }

    /**
     * Returns custom renderer for each cell of the list.vcbc
     *
     * @param list         list to process
     * @param value        cell value (Criteria object in our case)
     * @param index        cell index
     * @param isSelected   whether cell is selected or not
     * @param cellHasFocus whether cell has focus or not
     * @return custom renderer for each cell of the list
     */
    @Override
    public Component getListCellRendererComponent (JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus)
    {
        renderer.setSelected(isSelected);
        renderer.setData((Criteria)value);
        renderer.setToolTipText(renderer.getText());
        return renderer;
    }

    /**
     * Label that has some custom decorations.
     */
    private static class CustomLabel extends JLabel
    {
        /**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		private boolean selected;
		
        public CustomLabel()
        {
            super();
            setOpaque(false);
            setFont(new Font("Serif", Font.BOLD, 14));
            setBorder(BorderFactory.createEmptyBorder(0, 30, 0, 0));            
        }

 

		private void setSelected(boolean selected)
        {
            this.selected = selected;
            setForeground(selected ? Color.BLACK : Color.BLACK);
        }

        private void setData(Criteria value)
        {
            setText(value.toString());
        }

        @Override
        protected void paintComponent (Graphics g)
        {
            Graphics2D g2d = (Graphics2D) g;
            g2d.setRenderingHint (RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            
            
            g2d.drawImage(crossIcon.getImage(), 0,
            		 0, null);

            if (selected)
            {
            	g2d.drawImage(crossIcon.getImage(), 0,
                       0, null);
            }
            super.paintComponent(g);
        }

        @Override
        public Dimension getPreferredSize()
        {
            final Dimension ps = super.getPreferredSize();
            ps.height = 36;
            ps.width = 115;
            return ps;
        }
    }
}


