package fr.pasteque.pos.widgets;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.swing.ListModel;


public interface ISearchCache {

	
    public PreparedStatement searchOpt(ListModel<Criteria> listModel) throws SQLException;
    public PreparedStatement searchExactOpt(ListModel<Criteria> listModel) throws SQLException;
}
