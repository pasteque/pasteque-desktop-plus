package fr.pasteque.pos.widgets;

import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.pasteque.data.loader.ImageLoader;
import fr.pasteque.pos.forms.AppLocal;
import fr.pasteque.pos.widgets.WidgetsBuilder;

import javax.swing.GroupLayout.Alignment;
import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListSelectionModel;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;

import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.FlowLayout;
import java.awt.BorderLayout;


public class JModuleSearch extends javax.swing.JPanel implements DocumentListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ArrayList<Criteria> ListArrayCriteria = new ArrayList<Criteria>();
	private String fileJson = null;
	
    /**
     * Creates new form JModule_Search
     */
    public JModuleSearch(String fileJson, JList<?> JList_Result,  JList<?> JList_Result_Copy) {
    	this.fileJson = fileJson;
    	this.JList_Result = JList_Result;
        this.JList_Result_Copy = JList_Result_Copy;
        initComponents();
        
        jText_Search.getDocument().addDocumentListener(this);
        
    }

	public String getFileJson() {
		return fileJson;
	}
	
	public void setFileJson(String fileJson) {
		this.fileJson = fileJson;
	}
	
	@SuppressWarnings("rawtypes")
	public JList getJList_Result() {
		return JList_Result;
	}
	
	@SuppressWarnings("rawtypes")
	public void setJList_Result(JList jList_Result) {
		JList_Result = jList_Result;
	}

	public javax.swing.JList<Criteria> getJList_Search() {
		return JList_Search;
	}


	public void setJList_Search(javax.swing.JList<Criteria> jList_Search) {
		JList_Search = jList_Search;
	}

	public javax.swing.JTextField getjText_Search() {
		return jText_Search;
	}
	
	public JButton getSearchBtn() {
		return searchBtn;
	}
    
	/** 
	 * PG : Fonction qui remplit la JList_Search avec 
	 * les informations de l'ojet déjà sélectionné
	 */
	public void reloadSearch(String FileJson, Object info) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			Search search = mapper.readValue(new File(fileJson), Search.class);
			for (final Criteria crit : search.getCriteria()) {
				Object searchValueMethod = null;
				try {
					if (crit.getColumn().equals("lastName")){
						searchValueMethod = new PropertyDescriptor("lastname", info.getClass()).getReadMethod().invoke(info);
					}else if (crit.getColumn().equals("firstName")){
						searchValueMethod = new PropertyDescriptor("firstname", info.getClass()).getReadMethod().invoke(info);
					}else {
						searchValueMethod = new PropertyDescriptor(crit.getColumn(), info.getClass()).getReadMethod().invoke(info);
					}
					String searchValue = (String) searchValueMethod;
					if(searchValue != null && !searchValue.isEmpty()) {
						
						List<String> listSearchValue = Arrays.asList(searchValue);
						crit.setSearchvalue(listSearchValue);
						
				    	ListArrayCriteria.add(crit);
				    	crit.fusionCriteria(ListArrayCriteria);
				    	DefaultListModel<Criteria> model = new DefaultListModel<Criteria> ();
				    	for (int y=0; y<ListArrayCriteria.size();y++) {
				    		model.addElement(ListArrayCriteria.get(y));
				    		
				    	}
				    	JList_Search.setModel(model);
				    	JList_Search.setCellRenderer(new CustomListRendererSearch(JList_Search,ListArrayCriteria));
				    	JList_Search.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
				    	JList_Search.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
				    	JList_Search.setLayoutOrientation(JList.HORIZONTAL_WRAP);
				    	JList_Search.setVisibleRowCount(-1);
				    	jText_Search.setText("");
					}
			    	
				} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | IntrospectionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		} catch (JsonParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (JsonMappingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	
	/** 
	 * PG : Fonction qui remplit en automatique la JList_Search
	 */
	public void createJListSearch(String searchValue) {
		
		try {
			/** 
			 * PG : Test si c'est un objet qui a été saisie dans le jText_Search
			 * en verifiant si la chaine de caractere commence par { et termine par }.
			 */
			if (searchValue.substring(0,1).equals("{") &&  searchValue.substring(searchValue.length()- 1,searchValue.length()-0).equals("}")) {
	        	        				
				Search search = new ObjectMapper().readValue(searchValue, Search.class);
					
            	for (int y=0; y<search.getCriteria().size();y++) {
            		ListArrayCriteria.add(search.getCriteria().get(y));
            		search.getCriteria().get(y).fusionCriteria(ListArrayCriteria);
            	}

				DefaultListModel<Criteria> model = new DefaultListModel<Criteria> ();
            	for (int z=0; z<ListArrayCriteria.size();z++) {
            		model.addElement(ListArrayCriteria.get(z));
            	}
            	JList_Search.setModel(model);
            	JList_Search.setCellRenderer(new CustomListRendererSearch(JList_Search,ListArrayCriteria));
            	JList_Search.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
            	JList_Search.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            	JList_Search.setLayoutOrientation(JList.HORIZONTAL_WRAP);
            	JList_Search.setVisibleRowCount(-1);
            	jText_Search.setText(""); 
			}
			
		} catch (JsonParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (JsonMappingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
	}
	
    
    public void initComponents() {
    	JPanel jPanelSearch = new JPanel();
    	FlowLayout flowLayout = (FlowLayout) jPanelSearch.getLayout();
    	flowLayout.setVgap(0);
    	flowLayout.setHgap(0);
    	JPanel jPanelBtn = new JPanel();
    	JPanel jPanelJlist = new JPanel();
        JPanel jPanel1 = new JPanel();
        JPanel jPanel2 = new JPanel();
    	clearBtn = WidgetsBuilder.createButton(null,
                AppLocal.getIntString("button.clean"),
                WidgetsBuilder.SIZE_MEDIUM);
    	searchBtn = WidgetsBuilder.createButton(ImageLoader.readImageIcon("execute.png"),
                 AppLocal.getIntString("button.executefilter"),
                 WidgetsBuilder.SIZE_MEDIUM);
    	
    	jScrollPane1 = new javax.swing.JScrollPane();
 
        jText_Search = new JTextField();
        
        /** 
         * PG: Action execute lorsque l'on clique sur entre et qu'on est dans le champ 
         * jText_Search.
         */
        jText_Search.addKeyListener(new KeyAdapter() {
        	@Override
        	public void keyReleased(KeyEvent e) {
        		if (e.getKeyCode() == KeyEvent.VK_ENTER){
        			String searchValue =  jText_Search.getText();
        			createJListSearch(searchValue);
        		 }
        	}
        });
        JList_Search = new javax.swing.JList<Criteria>();

        jText_Search.addFocusListener(new FocusListener() {
            @Override
            public void focusLost(FocusEvent e) {
            }

            @Override
            public void focusGained(FocusEvent e) {
            	jText_Search.setSelectionStart(0);
            	jText_Search.setSelectionEnd(0);
            	jText_Search.setCaretPosition(jText_Search.getText().length());
            }
        });
        
        jPopupMenu_Search = new JPopuMenuSearch();
        JPopuMenuSearch.addPopup(jText_Search, jPopupMenu_Search);
        
        clearBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                clearBtnActionPerformed(evt);
            }
        });

        searchBtn.setFocusPainted(false);
        searchBtn.setFocusable(false);
        searchBtn.setRequestFocusEnabled(false);

        jText_Search.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        
        
        jPanelJlist.setBorder(javax.swing.BorderFactory.createEmptyBorder(5, 5, 5, 5));
        jPanelJlist.setLayout(new java.awt.BorderLayout());
        jPanelJlist.add(jScrollPane1, java.awt.BorderLayout.CENTER);
        
        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1Layout.setHorizontalGroup(
        	jPanel1Layout.createParallelGroup(Alignment.TRAILING)
        		.addGroup(jPanel1Layout.createSequentialGroup()
        			.addContainerGap()
        			.addGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING)
        				.addComponent(JList_Search, GroupLayout.DEFAULT_SIZE, 396, Short.MAX_VALUE)
        				.addComponent(jText_Search, GroupLayout.DEFAULT_SIZE, 396, Short.MAX_VALUE))
        			.addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
        	jPanel1Layout.createParallelGroup(Alignment.LEADING)
        		.addGroup(jPanel1Layout.createSequentialGroup()
        			.addGap(16)
        			.addComponent(jText_Search, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
        			.addPreferredGap(ComponentPlacement.UNRELATED)
        			.addComponent(JList_Search, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
        			.addContainerGap(16, Short.MAX_VALUE))
        );
        
        
        jScrollPane1.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

    	JList_Result.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
    	
        jScrollPane1.setViewportView(JList_Result);
        
        jPanel1.setLayout(jPanel1Layout);
        jPanelSearch.add(jPanel1);
                
        jPanelBtn.add(clearBtn);
        jPanelBtn.add(searchBtn);
        
        
        jPanel2.setLayout(new java.awt.BorderLayout());
         
        jPanel2.add(jPanelSearch,BorderLayout.NORTH);
        jPanel2.add(jPanelBtn,BorderLayout.CENTER);
        jPanel2.add(jPanelJlist, BorderLayout.SOUTH);
        
   
        this.setLayout(new java.awt.BorderLayout());
        this.add(jPanel2, java.awt.BorderLayout.CENTER);
        
       
    }// </editor-fold>  

	public void insertUpdate(DocumentEvent ev) {
    	jPopupMenu_Search.showPopup(jText_Search, ListArrayCriteria, JList_Search, getFileJson());
    }
   
    public void removeUpdate(DocumentEvent ev) {
    	jPopupMenu_Search.showPopup(jText_Search, ListArrayCriteria, JList_Search, getFileJson());
    }

    public void changedUpdate(DocumentEvent ev) {
    	jPopupMenu_Search.showPopup(jText_Search, ListArrayCriteria, JList_Search, getFileJson());
    }
       
    
    @SuppressWarnings("unused")
	private static class MyListData<T> extends javax.swing.AbstractListModel<T> {
        
        /**
		 * 
		 */
		private static final long serialVersionUID = 2290182794229424994L;
		private java.util.List<T> m_data;
        
		public MyListData(java.util.List<T> data) {
            m_data = data;
        }
        
        public T getElementAt(int index) {
            return m_data.get(index);
        }
        
        public int getSize() {
            return m_data.size();
        } 
    }  
    
    @SuppressWarnings("unchecked")
	protected void clearBtnActionPerformed(ActionEvent evt) {
    	ListArrayCriteria.clear();
    	if (JList_Search.getModel().getSize()>0) {
    		@SuppressWarnings("rawtypes")
			DefaultListModel listModel1 = (DefaultListModel) JList_Search.getModel();
            listModel1.removeAllElements();
    	}
    	
    	if (JList_Result.getModel().getSize()>0) {
    		JList_Result.setModel(JList_Result_Copy.getModel());
    		}    	
	}



    // Variables declaration - do not modify 
    @SuppressWarnings("rawtypes")
	private javax.swing.JList JList_Result;
    @SuppressWarnings("rawtypes")
	private javax.swing.JList JList_Result_Copy;
    private javax.swing.JTextField jText_Search;
    private javax.swing.JList<Criteria> JList_Search;
    private JPopuMenuSearch jPopupMenu_Search;
    private javax.swing.JScrollPane jScrollPane1;
    private JButton searchBtn;
    private JButton clearBtn;

}
