package fr.pasteque.pos.widgets;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;


public class Criteria {


        private String name;
        private String column;
        private List<String> searchvalue;
        
        Criteria(){}
        
        public Criteria(String Name, String Column, List<String> SearchValue)
        {
        	this.name = Name;
        	this.column = Column;
            this.searchvalue = SearchValue;
        }

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getColumn() {
			return column;
		}

		public void setColumn(String column) {
			this.column = column;
		}

		public List<String> getSearchvalue() {
			return searchvalue;
		}

		public void setSearchvalue(List<String> searchvalue) {
			this.searchvalue = searchvalue;
		}
		
		 @Override
        public String toString() {
			 
			 String prefix = "";
			 String concatSearchValue = "";
			 for (String value : searchvalue) {
				 concatSearchValue += prefix + value;
				 prefix = " OU ";
			 }
//			 String concatSearchValue = searchvalue.stream().collect(Collectors.joining(" OU "));
			 return name + ": " + concatSearchValue;            
        }
		 
		/** PG : Fonction qui regroupe les objets Citeria ayant
		 * la même valeur de column et concatene les valeurs de searchValue
		 * @param ListArrayCriteria
		 * @return
		 */
		public ArrayList<Criteria> fusionCriteria(ArrayList<Criteria> ListArrayCriteria) {
			
			Map<String, List<Criteria>> maps =  new HashMap<String, List<Criteria>>();
			for (Criteria criteria : ListArrayCriteria) {
			    String key  = criteria.column;
			    if(maps.containsKey(key)){
			        List<Criteria> list = maps.get(key);
			        list.add(criteria);

			    }else{
			        List<Criteria> list = new ArrayList<Criteria>();
			        list.add(criteria);
			        maps.put(key, list);
			    }

			}
    		
    		for(Entry<String, List<Criteria>> map : maps.entrySet()) {
    			
    			if (map.getValue().size()>1) {
    				String fusionName = null ;
    				String fusionColumn = null;
    				ArrayList<String> fusionSearchValue = new ArrayList<String>();
    				for (Criteria crit:map.getValue()) {
    					fusionName = crit.getName();
    					fusionColumn = crit.getColumn();
    					
    					for (String searchValue: crit.getSearchvalue()) {
    						fusionSearchValue.add(searchValue);
    					}
    					
    					ListArrayCriteria.remove(crit);
    				}

    				Criteria fusionCrit = new Criteria(fusionName, fusionColumn, fusionSearchValue);
    				ListArrayCriteria.add(fusionCrit);
    			}
    			
    		}
			return ListArrayCriteria;
		}
	
}

