package fr.pasteque.pos.widgets;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ListModel;

import fr.pasteque.pos.caching.LocalDB;

public abstract class SearchCacheAbstract implements ISearchCache {
	
    public PreparedStatement searchOpt(ListModel<Criteria> listModel, String table) throws SQLException
    {
        String searchQuery = "SELECT data FROM "+table+" WHERE isParent";
        List<String> values = new ArrayList<>();
        PreparedStatement preparedStatement = null;
        
        try{
        	
        	for( int i  = 0; i < listModel.getSize(); i++){
        		
        		if (listModel.getElementAt(i).getSearchvalue().size()> 1) {
        			searchQuery += " AND (LOWER("+listModel.getElementAt(i).getColumn()+") like LOWER(?)";
        			values.add("%"+listModel.getElementAt(i).getSearchvalue().get(0)+"%");
        			
        			for (int y = 1; y < listModel.getElementAt(i).getSearchvalue().size(); y++) {
        				
        				searchQuery += " OR LOWER("+listModel.getElementAt(i).getColumn()+") like LOWER(?)";
        				values.add("%"+listModel.getElementAt(i).getSearchvalue().get(y)+"%");
        				
        			}
        			searchQuery +=")";
        		}
        		else {
        		searchQuery += " AND LOWER("+listModel.getElementAt(i).getColumn()+") like LOWER(?)";
    			values.add("%"+listModel.getElementAt(i).getSearchvalue().get(0)+"%");
        		}
	    	}
            
        }catch(Exception ex){
            System.out.println(ex.getMessage());
        }
        
        preparedStatement = LocalDB.prepare(searchQuery+" LIMIT 20");
        for (int j=0; j < values.size(); j++) {
         	preparedStatement.setString(j+1, values.get(j));
        	
        }
        
        return preparedStatement;
    }
    
    public PreparedStatement searchExactOpt(ListModel<Criteria> listModel, String table) throws SQLException
    {
        String searchQuery = "SELECT data FROM "+table+" WHERE isParent";
        List<String> values = new ArrayList<>();
        PreparedStatement preparedStatement = null;
        
        try{
        	
        	for( int i  = 0; i < listModel.getSize(); i++){
        		
        		if (listModel.getElementAt(i).getSearchvalue().size()> 1) {
        			searchQuery += " AND (LOWER("+listModel.getElementAt(i).getColumn()+") like LOWER(?)";
        			values.add(listModel.getElementAt(i).getSearchvalue().get(0));
        			
        			for (int y = 1; y < listModel.getElementAt(i).getSearchvalue().size(); y++) {
        				
        				searchQuery += " OR LOWER("+listModel.getElementAt(i).getColumn()+") like LOWER(?)";
        				values.add(listModel.getElementAt(i).getSearchvalue().get(y));
        				
        			}
        			searchQuery +=")";
        		}
        		else {
        		searchQuery += " AND LOWER("+listModel.getElementAt(i).getColumn()+") like LOWER(?)";
    			values.add(listModel.getElementAt(i).getSearchvalue().get(0));
        		}
	    	}
            
        }catch(Exception ex){
            System.out.println(ex.getMessage());
        }
        
        preparedStatement = LocalDB.prepare(searchQuery+" LIMIT 20");
        for (int j=0; j < values.size(); j++) {
         	preparedStatement.setString(j+1, values.get(j));
        	
        }
        
        return preparedStatement;
    }
    
}
