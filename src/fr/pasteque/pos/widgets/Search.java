package fr.pasteque.pos.widgets;

import java.util.ArrayList;
import java.util.List;

public class Search {

        private String type;
        private List<Criteria> criteria = new ArrayList<Criteria>();
        
        Search(){}
                
		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public List<Criteria> getCriteria() {
			return criteria;
		}

		public void setCriteria(List<Criteria> criteria) {
			this.criteria = criteria;
		}
		    	
}
