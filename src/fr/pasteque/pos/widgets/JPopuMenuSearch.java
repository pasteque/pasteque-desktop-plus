package fr.pasteque.pos.widgets;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JPopuMenuSearch extends JPopupMenu {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1148133629926388903L;
	private JPopupMenu jPopupMenu_Search;

	public JPopuMenuSearch() {
	}
	
	
	static void addPopup(Component component, final JPopuMenuSearch jPopupMenu_Search) {
		component.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}
			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}
			private void showMenu(MouseEvent e) {
				jPopupMenu_Search.show(e.getComponent(), e.getX(), e.getY());
			}
		});
	}
	
    ////DEBUT #1
    /** PG: Fonction qui crée un PopupMenu en dessous du JTextField avec comme MenuItems 
     * toutes les informations recherchables qui ont été récupéré du fichier Json + le texte saisie dans le JTextField
     */
    public void showPopup(final JTextField jText_Search, final ArrayList<Criteria> ListArrayCriteria, final JList<Criteria> JList_Search, String fileJson) {
        if (jPopupMenu_Search != null)
        	jPopupMenu_Search.setVisible(false);
        jPopupMenu_Search = null;
        if (jText_Search.getText().length() == 0)
            return;
        jPopupMenu_Search = new JPopupMenu();
        jPopupMenu_Search.setRequestFocusEnabled(false);
        if (jText_Search.getText().length() == 0) {
        	jPopupMenu_Search.setVisible(false);
        } else {
        	jPopupMenu_Search.removeAll();
        	
        	//DEBUT #2
        	/** PG: Rempli le PopupMenu avec comme MenuItems toutes les informations recherchables qui ont été récupéré 
        	 * du fichier Json et en concaténant dans les MenueItems ces informations + le texte saisi dans le JTextField
        	 */
        	ObjectMapper mapper = new ObjectMapper();
        	try {
				Search search = mapper.readValue(new File(fileJson), Search.class);
	        	for (final Criteria crit : search.getCriteria()) {
	        		List<String> saisie = Arrays.asList(jText_Search.getText());
	        		crit.setSearchvalue(saisie);
	       	        		
	        		if (!crit.getSearchvalue().get(0).substring(0,1).equals("{")){
	        		
	        			JMenuItem item=new JMenuItem(crit.toString());
	        		
	        		
		        		//DEBUT #3
		        		/** PG: Action lorsqu'on clique sur un MenuItems, on copie la valeur du MenuItems dans le 
		        		 * JList et on vide le JTextField
		        		 */
		        		item.addActionListener(new ActionListener() {
		        		
		                  public void actionPerformed(ActionEvent e) {
		                	
		                	ListArrayCriteria.add(crit);
		                	crit.fusionCriteria(ListArrayCriteria);
		                	DefaultListModel<Criteria> model = new DefaultListModel<Criteria> ();
		                	for (int y=0; y<ListArrayCriteria.size();y++) {
		                		model.addElement(ListArrayCriteria.get(y));
		                		
		                	}
		                	JList_Search.setModel(model);
		                	JList_Search.setCellRenderer(new CustomListRendererSearch(JList_Search,ListArrayCriteria));
		                	JList_Search.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		                	JList_Search.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		                	JList_Search.setLayoutOrientation(JList.HORIZONTAL_WRAP);
		                	JList_Search.setVisibleRowCount(-1);
		                	jText_Search.setText("");
		                  }
		                  
		                //FIN #3
		                  
		              });
		        		jPopupMenu_Search.add(item);
	        		}
	        	}
        	//FIN #2
        	} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
            if (!jPopupMenu_Search.isShowing())
            	jPopupMenu_Search.show(jText_Search, 0, jText_Search.getHeight());
            else
            	jPopupMenu_Search.revalidate();
            jText_Search.requestFocus();
        }
    }
  //FIN #1

}

