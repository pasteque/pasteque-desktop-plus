package fr.pasteque.pos.admin;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.json.JSONObject;

import fr.pasteque.pos.caching.InseeCache;

public class InseeInfo implements Serializable, Comparator<InseeInfo> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2903345849494953824L;
	
    private String inseeNum;
    private String zipCode;
    private String commune;
    private Double latitude;
    private Double longitude;
    
    
    
	public InseeInfo() {
	}
	
	public InseeInfo(JSONObject o) {
		this.inseeNum = ( o.isNull("insee") ? null : o.getString("insee"));
		this.zipCode = ( o.isNull("zipCode") ? null : o.getString("zipCode"));
		this.commune = ( o.isNull("commune") ? null : o.getString("commune"));
		this.latitude = ( o.isNull("latitude") ? null : o.getDouble("latitude"));
		this.longitude = ( o.isNull("longitude") ? null : o.getDouble("longitude"));
	}
	
	public JSONObject toJSON() {
        JSONObject o = new JSONObject();
        o.put("insee", ( this.inseeNum == null ? JSONObject.NULL : this.inseeNum));
        o.put("zipCode", ( this.zipCode == null ? JSONObject.NULL : this.zipCode));
        o.put("commune", ( this.commune == null ? JSONObject.NULL : this.commune));
        o.put("latitude", ( this.latitude == null ? JSONObject.NULL : this.latitude));
        o.put("longitude", ( this.longitude == null ? JSONObject.NULL : this.longitude));
        return o;
	}
	
	public String getInseeNum() {
		return inseeNum;
	}
	public void setInseeNum(String inseeNum) {
		this.inseeNum = inseeNum;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	public String getCommune() {
		return commune;
	}
	public void setCommune(String commune) {
		this.commune = commune;
	}
	public Double getLatitude() {
		return latitude;
	}
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	public Double getLongitude() {
		return longitude;
	}
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	
	public String toString() {
		return this.zipCode.concat(" - ".concat(commune));
	}
	
	//Voir doc IGN - Choix de la sphère GRS80 (rayon 6378137m)
	public double distance(InseeInfo b) {
		double retour = 0D ;
		if( ! this.inseeNum.equalsIgnoreCase(b.getInseeNum()) && b.getLatitude() != null && b.getLongitude() != null && getLatitude() != null && getLongitude() != null) {
			retour = 6378D * Math.acos(Math.sin(Math.toRadians(getLatitude()))*Math.sin(Math.toRadians(b.getLatitude())) + Math.cos(Math.toRadians(getLatitude()))*Math.cos(Math.toRadians(b.getLatitude()))*Math.cos(Math.toRadians(getLongitude()-b.getLongitude())) ) ;
		}
		return retour;
	}
	
	/**
	 * Afin d'avoir une liste moins importante à parcourir on va déterminer une boundingBox
	 * dont la distance aux bords en maxDistance
	 * @param maxDistance distance maxi en Km
	 * @return écart maximum en latitude
	 */
	private double maxLatitude( int maxDistance) {
		return Math.toDegrees(2*Math.atan(Math.sqrt(-4*(Math.cos(maxDistance/6378D)+1)*(Math.cos(maxDistance/6378D)-1))/(2*(Math.cos(maxDistance/6378D)+1))));
	}
	
	/**
	 * Afin d'avoir une liste moins importante à parcourir on va déterminer une boundingBox
	 * dont la distance aux bords en maxDistance
	 * @param maxDistance distance maxi en Km
	 * @return écart maximum en longitude
	 */
	private double maxLongitude( int maxDistance) {
		double retour = 0;
		if(getLatitude() != null && getLongitude() != null) {
			retour = Math.toDegrees(Math.acos((Math.cos(maxDistance/6378D)-Math.pow(Math.sin(Math.toRadians(getLatitude())), 2)) / Math.pow(Math.cos(Math.toRadians(getLatitude())),2)));
		}
		// acos(( cos (maxDistance/ 3678D) - sin² lat ) / cos²lat ) - long = retour   
		return retour;
	}
	
	/**
	 * 
	 * @param quantity Nombre maximum de candidats dans la liste
	 * @param maxDistance Distance maximum d'un candidat
	 * @return La Liste des objets InseeInfo Insee info les plus proches 
	 * dans la limite de {@quantity} éléments et d'une distance maxi de 
	 * {@maxDistance} Kms  
	 */
	public List<InseeInfo> getProx( int quantity , int maxDistance) {
		ArrayList<InseeInfo> retour = new ArrayList<InseeInfo>() ;
		double deltalongitude , minlongitude , maxlongitude ;
		double deltalatitude , minlatitude , maxlatitude ;
		
		//On limite à 5000Km - ce qui est déjà beaucoup et présent du coup peu d'intérêt
		maxDistance = ( maxDistance > 5000 ? 5000 : maxDistance );
		deltalongitude = maxLongitude(maxDistance);
		deltalatitude = maxLatitude(maxDistance);
		minlongitude = getLongitude() - deltalongitude ;
		maxlongitude = getLongitude() + deltalongitude ;
		minlatitude = getLatitude() - deltalatitude ;
		maxlatitude = getLatitude() + deltalatitude ;
		
		try { 
			retour = InseeCache.getInseeByCoordinates(minlongitude , maxlongitude , minlatitude , maxlatitude);
			Collections.sort(retour, this);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return retour.subList(0, Math.min(quantity, retour.size()));
		/* On récupère les limites pour notre requête */
		/* On fait appel au cache */
		/* On parcours les candidats et rempli notre liste*/
	}

	@Override
	public int compare(InseeInfo o1, InseeInfo o2) {
		
		return (int)( this.distance(o1)*1000 - this.distance(o2)*1000);
	}
    
    

}
