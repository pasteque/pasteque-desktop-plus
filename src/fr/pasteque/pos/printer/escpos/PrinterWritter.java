//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2007-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.printer.escpos;

import java.io.InputStream;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public abstract class PrinterWritter {
    
    private boolean initialized = false;
    private boolean async = true;

    private ExecutorService exec;
    
    public PrinterWritter(boolean async) {
        exec = Executors.newSingleThreadExecutor();
        this.async = async;
    }
    
    protected abstract void internalWrite(byte[] data);
    protected abstract void internalFlush();
    protected abstract void internalClose();
    
    public void init(final byte[] data) {
        if (!initialized) {
            write(data);
            initialized = true;
        }
    }
       
    public void write(String sValue) {
        write(sValue.getBytes());
    }

    public void write(final byte[] data) {
        if(async) {
	    	exec.execute(new Runnable() {
	            public void run() {
	                internalWrite(data);
	            }
	        });
        } else {
        	internalWrite(data);
        }
    }
    
    public void flush() {
        if(async) {
	        exec.execute(new Runnable() {
	            public void run() {
	                internalFlush();
	            }
	        });
        } else {
        	internalFlush();
        }
    }
    
    public void close() {
    	if(async) {
	        exec.execute(new Runnable() {
	            public void run() {
	                internalClose();
	            }
	        });
    	} else {
    		internalClose();
    	}
        exec.shutdown();
    }

	public void setSync(boolean sync) {
		async = !sync ;
		
	}

	public InputStream getInputStream() {
		return null;
	}
	
}
