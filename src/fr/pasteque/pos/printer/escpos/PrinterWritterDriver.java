package fr.pasteque.pos.printer.escpos;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import javax.print.Doc;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintException;
import javax.print.PrintService;
import javax.print.SimpleDoc;

import fr.pasteque.pos.util.ReportUtils;

public class PrinterWritterDriver extends PrinterWritter {

	private PrintService printservice;
	
	public PrinterWritterDriver(String printername) {
		super(true);
		printservice = ReportUtils.getPrintService(printername);
	}
	
	@Override
	protected void internalWrite(byte[] data) {
		// TODO Auto-generated method stub

		DocPrintJob printjob = printservice.createPrintJob();
		InputStream pis = new ByteArrayInputStream(data);
		
		Doc doc = new SimpleDoc( pis, DocFlavor.INPUT_STREAM.AUTOSENSE, null);
		
        try {
			printjob.print(doc, null);
		} catch (PrintException e) {
			// TODO Auto-generated catch block
			System.err.println(e);
		}
	}

	@Override
	protected void internalFlush() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void internalClose() {
		// TODO Auto-generated method stub

	}

	
}
