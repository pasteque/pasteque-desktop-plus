//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2007-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.printer.escpos;

import java.awt.image.BufferedImage;
import java.util.Date;

import javax.swing.JComponent;

import fr.pasteque.pos.printer.*;
import fr.pasteque.pos.forms.AppLocal;

public class DevicePrinterESCPOS implements DevicePrinter {
      
    private PrinterWritter m_CommOutputPrinter;   
    private Codes m_codes;
    private UnicodeTranslator m_trans;
    private boolean b_micr;
    private boolean waiting=true;
    
    private long lastbuff = 0L ;
    
    private byte[] buffer = new byte[1024];
    
//    private boolean m_bInline;
    private String m_sName;
    
    // Creates new TicketPrinter
    public DevicePrinterESCPOS(PrinterWritter CommOutputPrinter, Codes codes, UnicodeTranslator trans, boolean MICR) throws TicketPrinterException {
        
        m_sName = AppLocal.getIntString("Printer.Serial");
        m_CommOutputPrinter = CommOutputPrinter;
        m_codes = codes;
        m_trans = trans;
        b_micr = MICR;
        
        // Inicializamos la impresora
        m_CommOutputPrinter.setSync(MICR);
        m_CommOutputPrinter.init(ESCPOS.INIT);

        m_CommOutputPrinter.write(ESCPOS.SELECT_PRINTER); // A la impresora
        m_CommOutputPrinter.init(m_codes.getInitSequence());
        m_CommOutputPrinter.write(m_trans.getCodeTable());

        m_CommOutputPrinter.flush();
        
//        m_CommOutputPrinter.write(ESCPOS.SELECT_PRINTER);
//        m_CommOutputPrinter.write(ESCPOS.SELECT_SLIP);
////        m_CommOutputPrinter.write(ESCPOS.SELECT_PAPER_SLIP);
//        m_CommOutputPrinter.write(ESCPOS.SELECT_BACK_SLIP);
//
//        m_CommOutputPrinter.write(m_trans.transString("Test Endorse4"));
//        m_CommOutputPrinter.write(ESCPOS.FF);
        
//        m_CommOutputPrinter.write(ESCPOS.READ_MICR_CMC7);
//        m_CommOutputPrinter.write(ESCPOS.LOAD_CHECK);
//
//
//        m_CommOutputPrinter.write(m_trans.transString("Test Endorse"));
//        m_CommOutputPrinter.write(ESCPOS.CR);
//        m_CommOutputPrinter.write(ESCPOS.EJECT_CHECK);
//        m_CommOutputPrinter.write(ESCPOS.CLEAN_MICR);
        
    }
   
    public String getPrinterName() {
        return m_sName;
    }
    public String getPrinterDescription() {
        return null;
    }   
    public JComponent getPrinterComponent() {
        return null;
    }
    public void reset() {
    }
    
    public void beginReceipt() {
    	waiting = true;
    	m_CommOutputPrinter.init(ESCPOS.INIT);
    	m_CommOutputPrinter.write(ESCPOS.SELECT_PRINTER);
    	if(b_micr) {
    		m_CommOutputPrinter.write(ESCPOS.PRINT_B2T);
    		m_CommOutputPrinter.write(ESCPOS.FONT_A);
    		m_CommOutputPrinter.write(ESCPOS.READ_MICR_CMC7);
    		m_CommOutputPrinter.write(ESCPOS.LOAD_CHECK);
    		m_CommOutputPrinter.write(ESCPOS.SELECT_SLIP);
    		m_CommOutputPrinter.write(ESCPOS.PAGE_MODE);
    		m_CommOutputPrinter.write(ESCPOS.RESOL_SLIP);
    		m_CommOutputPrinter.write(ESCPOS.PAGE_SIZE_CHECK);
    		
    	} else {
    		m_CommOutputPrinter.write(ESCPOS.SELECT_ROLL);
    		m_CommOutputPrinter.write(ESCPOS.PRINT_L2R);
    		m_CommOutputPrinter.write(ESCPOS.FONT_B);
    		m_CommOutputPrinter.write(ESCPOS.PAGE_MODE);
    	}
    	
    	
    }
    
    public void printImage(BufferedImage image) {
        
        //m_CommOutputPrinter.write(ESCPOS.SELECT_PRINTER); 
    	
    	m_CommOutputPrinter.write(ESCPOS.FF);
    	m_CommOutputPrinter.write(ESCPOS.STANDARD_MODE);
        m_CommOutputPrinter.write(m_codes.transImage(image));
        m_CommOutputPrinter.write(ESCPOS.PAGE_MODE);
    }
    
    public void printBarCode(String type, String position, String code) {
        
        //m_CommOutputPrinter.write(ESCPOS.SELECT_PRINTER);
    	m_CommOutputPrinter.write(ESCPOS.FF);
    	m_CommOutputPrinter.write(ESCPOS.ALIGN_CENTER);
        m_codes.printBarcode(m_CommOutputPrinter, type, position, code);
        m_CommOutputPrinter.write(ESCPOS.ALIGN_LEFT);
        m_CommOutputPrinter.write(ESCPOS.PAGE_MODE);
        
    }
    
    public void beginLine(int iTextSize) {

        //m_CommOutputPrinter.write(ESCPOS.SELECT_PRINTER);        
        
        if (iTextSize == DevicePrinter.SIZE_0) {
            m_CommOutputPrinter.write(m_codes.getSize0());
        } else if (iTextSize == DevicePrinter.SIZE_1) {
            m_CommOutputPrinter.write(m_codes.getSize1());
        } else if (iTextSize == DevicePrinter.SIZE_2) {
            m_CommOutputPrinter.write(m_codes.getSize2());
        } else if (iTextSize == DevicePrinter.SIZE_3) {
            m_CommOutputPrinter.write(m_codes.getSize3());
        } else {
            m_CommOutputPrinter.write(m_codes.getSize0());
        }
    }
    
    public void printText(int iStyle, String sText) {

        //m_CommOutputPrinter.write(ESCPOS.SELECT_PRINTER);   

        if ((iStyle & DevicePrinter.STYLE_BOLD) != 0) {
            m_CommOutputPrinter.write(m_codes.getBoldSet());
        }
        if ((iStyle & DevicePrinter.STYLE_UNDERLINE) != 0) {
            m_CommOutputPrinter.write(m_codes.getUnderlineSet());
        }
        m_CommOutputPrinter.write(m_trans.transString(sText));
        if ((iStyle & DevicePrinter.STYLE_UNDERLINE) != 0) {
            m_CommOutputPrinter.write(m_codes.getUnderlineReset());
        }
        if ((iStyle & DevicePrinter.STYLE_BOLD) != 0) {
            m_CommOutputPrinter.write(m_codes.getBoldReset());
        }     
    }
    public void endLine() {
        //m_CommOutputPrinter.write(ESCPOS.SELECT_PRINTER);   
        m_CommOutputPrinter.write(m_codes.getNewLine());
    }
    
    public void endReceipt() {
        //m_CommOutputPrinter.write(ESCPOS.SELECT_PRINTER);   
        
    	if(! b_micr) {
            m_CommOutputPrinter.write(ESCPOS.FF);
	        m_CommOutputPrinter.write(m_codes.getNewLine());
	        m_CommOutputPrinter.write(m_codes.getNewLine());
	        m_CommOutputPrinter.write(m_codes.getNewLine());
	        m_CommOutputPrinter.write(m_codes.getNewLine());
	        m_CommOutputPrinter.write(m_codes.getNewLine());
	        m_CommOutputPrinter.write(m_codes.getCutReceipt());
	        waiting = false;
    	} else {
    		m_CommOutputPrinter.write(ESCPOS.FF);
    		m_CommOutputPrinter.write(ESCPOS.EJECT_CHECK);
    		m_CommOutputPrinter.write(ESCPOS.FF);
    	}
        m_CommOutputPrinter.flush();
        //m_CommOutputPrinter.getSerialPort().close();
    }
    
    public void openDrawer() {

        m_CommOutputPrinter.write(ESCPOS.SELECT_PRINTER);   
        m_CommOutputPrinter.write(m_codes.getOpenDrawer());
        m_CommOutputPrinter.flush();
    }

	@Override
	public boolean isWaiting() {
		if(waiting == false &&  ESCPOS.PRINT_END_OK[0] == buffer[0] && ESCPOS.PRINT_END_OK[1] == buffer[1] && ESCPOS.PRINT_END_OK[2] == buffer[2]) {
        	//System.out.println("waiting fini pour cette fois!");
        } else {
        	//if(lastbuff != 0L && (new Date()).getTime()-lastbuff > 25000) {
        	if(lastbuff != 0L && (new Date()).getTime()-lastbuff > 50) {
        		//On ne bloque pas plus de 25 secondes si l'imprimante ne remonte pas d'info
        		waiting = false;
        	} else {
        		lastbuff = lastbuff != 0L ? lastbuff : (new Date()).getTime();
        		return true;
        	}
        	//System.out.println("waiting toujours!");
        }
		return waiting;
	}
	
}

