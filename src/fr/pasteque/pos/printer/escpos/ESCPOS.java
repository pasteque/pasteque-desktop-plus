//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2007-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.printer.escpos;

public class ESCPOS {

    public static final byte[] INIT = {0x1B, 0x40};
    
    public static final byte[] PAGE_MODE = {0x1B, 0x4C};
    public static final byte[] STANDARD_MODE = {0x1B, 0x53};
    public static final byte[] PRINT_L2R = {0x1B, 0x54, 0x00};
    public static final byte[] PRINT_B2T = {0x1B, 0x54, 0x01};
    public static final byte[] PRINT_R2L = {0x1B, 0x54, 0x02};
    public static final byte[] PRINT_T2B = {0x1B, 0x54, 0x03};
    
    public static final byte[] FONT_A = {0x1B, 0x4D, 0x00};
    public static final byte[] FONT_B = {0x1B, 0x4D, 0x01};
    
        
    public static final byte[] SELECT_PRINTER = {0x1B, 0x3D, 0x01};
    public static final byte[] SELECT_DISPLAY = {0x1B, 0x3D, 0x02}; 
    
    public static final byte[] SELECT_SLIP = {0x1B, 0x63, 0x30 , 0x04};
    public static final byte[] SELECT_ROLL = {0x1B, 0x63, 0x30 , 0x01};
    
    public static final byte[] SLIP_REVERSE_ON = {0x1B, 0x46, 0x01};
    public static final byte[] SLIP_REVERSE_OFF = {0x1B, 0x46, 0x00};
    
    public static final byte[] SELECT_PAPER_SLIP = {0x1B, 0x63, 0x31 , 0x04};
    public static final byte[] SELECT_PAPER_ROLL = {0x1B, 0x63, 0x31 , 0x01};
    
    public static final byte[] READ_MICR_CMC7 = {0x1C, 0x61, 0x30 , 0x01};
    public static final byte[] LOAD_CHECK = {0x1C, 0x61, 0x31};
    public static final byte[] EJECT_CHECK = {0x1C, 0x61, 0x32};
    public static final byte[] RETRANSMIT_MICR = {0x1C, 0x62};
    public static final byte[] CLEAN_MICR = {0x1C, 0x63};
    
    public static final byte[] SELECT_FACE_SLIP = {0x1D, 0x28, 0x47 , 0x02 , 0x00, 0x30 , 0x04};
    public static final byte[] SELECT_BACK_SLIP = {0x1D, 0x28, 0x47 , 0x02 , 0x00, 0x30 , 0x44};

    public static final byte[] HT = {0x09}; // Horizontal Tab
    public static final byte[] LF = {0x0A}; // Print and line feed
    public static final byte[] FF = {0x0C}; // 
    public static final byte[] CR = {0x0D}; // Print and carriage return
        
    public static final byte[] CHAR_FONT_0 = {0x1B, 0x4D, 0x00};
    public static final byte[] CHAR_FONT_1 = {0x1B, 0x4D, 0x01};
    public static final byte[] CHAR_FONT_2 = {0x1B, 0x4D, 0x30};
    public static final byte[] CHAR_FONT_3 = {0x1B, 0x4D, 0x31};
        
    public static final byte[] BAR_HEIGHT_40 = {0x1D, 0x68, 0x40};
    public static final byte[] BAR_WIDTH_4 = {0x1D, 0x77, 0x04};
    public static final byte[] BAR_POSITIONDOWN = {0x1D, 0x48, 0x02};
    public static final byte[] BAR_POSITIONNONE = {0x1D, 0x48, 0x00};
    public static final byte[] BAR_HRIFONT1 = {0x1D, 0x66, 0x01};
    public static final byte[] BAR_HRIFONT0 = {0x1D, 0x66, 0x00};
    
    public static final byte[] BAR_CODE_EAN13 = {0x1D, 0x6B, 0x02}; // 12 numeros fijos
    public static final byte[] BAR_CODE_CODE128 = {0x1D, 0x6B, 0x49}; // 12 numeros fijos
    
    public static final byte[] VISOR_HIDE_CURSOR = {0x1F, 0x43, 0x00};
    public static final byte[] VISOR_SHOW_CURSOR = {0x1F, 0x43, 0x01};
    public static final byte[] VISOR_HOME = {0x0B};
    public static final byte[] VISOR_CLEAR = {0x0C};
        
    public static final byte[] CODE_TABLE_00 = {0x1B, 0x74, 0x00};
    public static final byte[] CODE_TABLE_13 = {0x1B, 0x74, 0x13}; 
    
    public static final byte[] PRINT_END = {0x60, 0x0E, 0x14}; 
    public static final byte[] PRINT_END_OK = {0x60, 0x0F, 0x14}; 
    public static final byte[] MICR_END = {0x40, 0x0F, 0x5F}; 
    
    public static final byte[] RESOL_SLIP = {0x1D, 0x50, (byte) 0xA0 , (byte) 0x90};
    public static final byte[] RESOL_RECEIPT = {0x1D, 0x50, (byte) 0xB4 , (byte) 0xB4};
    
    //Pour le cheque avec une résolution horizontale de 1/160 pouces
    // et verticale de 1/144 pouces
    // avec une surface imprimable de 540/160 x 1408/144
    //on place le point en haut à gauche à x=36 y = 0
    //avec une nouvelle surface de page de 500 par 992 
    public static final byte[] PAGE_SIZE_CHECK = {0x1B, 0x57, 0x24 , 0x00 , 0x00 , 0x00 , (byte) 0xF4 , 0x01 , 0x34 , 0x03};
    
    
    
    //Not in page mode
    public static final byte[] ALIGN_CENTER = {0x1B, 0x61, 0x01};
    public static final byte[] ALIGN_LEFT = {0x1B, 0x61, 0x00};
    public static final byte[] ALIGN_RIGHT = {0x1B, 0x61, 0x02};
    
    private ESCPOS() {       
    }
}
