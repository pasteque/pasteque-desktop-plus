package fr.pasteque.pos.sales;

import javax.swing.JDialog;
import javax.swing.JPanel;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dialog;
import java.awt.Frame;
import java.awt.Window;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JButton;
import com.jgoodies.forms.factories.DefaultComponentFactory;

import fr.pasteque.pos.widgets.WidgetsBuilder;
import java.awt.Color;

public class JDialogAlert extends JDialog {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5475149967585800554L;
	
	JButton btnOk ;
	String title ;
	String content ;
	boolean modal ;
	
    /** Creates new form JProductAttEdit 
     **/
    private JDialogAlert(java.awt.Frame parent, boolean modal) {
    	
        super(parent, modal);
        this.modal = modal ;
    }

    /** Creates new form JProductAttEdit */
    private JDialogAlert(java.awt.Dialog parent, boolean modal) {
        super(parent, modal);
        this.modal = modal ;
    }
    /** Creates new form JProductAttEdit 
     * Fake constructor so I can use UI designer
     * @wbp.parser.constructor*/
    private JDialogAlert() {
    	this.modal = true ;
        init("titre","message");
        
    }
    
    private void init(String title , String content) {

        initComponents(title , content);
        getRootPane().setDefaultButton(btnOk);
    }
    
	public void initComponents(String title , String content ) {
		setUndecorated(true);
		if(modal) {
			setModalityType(ModalityType.APPLICATION_MODAL);
			setUndecorated(true);
			setAlwaysOnTop(true);
		}
		
		JPanel panelMsg = new JPanel();
		panelMsg.setBackground(Color.PINK);
		getContentPane().add(panelMsg, BorderLayout.CENTER);
		
		JLabel lblMessage = new JLabel(content);
		panelMsg.add(lblMessage);
		
		JPanel panelBtn = new JPanel();
		getContentPane().add(panelBtn, BorderLayout.SOUTH);
		
		btnOk = new JButton("OK");
		btnOk.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                m_jButtonOKActionPerformed(evt);
            }
        });
		
		panelBtn.add(btnOk);
		
		JPanel panelTitle = new JPanel();
		getContentPane().add(panelTitle, BorderLayout.NORTH);
		
		JLabel lblTitre = DefaultComponentFactory.getInstance().createTitle(title);
		panelTitle.add(lblTitre);
		
		java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        int minWidth = 360;
        int minHeight = 120;
        int width = WidgetsBuilder.dipToPx(180);
        int height = WidgetsBuilder.dipToPx(180);
        width = Math.max(minWidth, width);
        height = Math.max(minHeight, height);
        // If popup is big enough, make it fullscreen
        if (width > 0.8 * screenSize.width || height > 0.8 * screenSize.height) {
            width = screenSize.width;
            height = screenSize.height;
            this.setUndecorated(true);
        }
        setBounds((screenSize.width-width)/2, (screenSize.height-height)/2, width, height);
	}
	
    protected static Window getWindow(Component parent) {
        if (parent == null) {
            return new JFrame();
        } else if (parent instanceof Frame || parent instanceof Dialog) {
            return (Window)parent;
        } else {
            return getWindow(parent.getParent());
        }
    }
    
    public static JDialogAlert getDialog(Component parent, String title , String content , boolean modal) {
        
        Window window = getWindow(parent);
        
        JDialogAlert myAlert;
        
        if (window instanceof Frame) { 
        	myAlert = new JDialogAlert((Frame) window , modal);
        } else {
        	myAlert = new JDialogAlert((Dialog) window , modal);
        }
        
        myAlert.init(title , content);         
        
        return myAlert;
    } 
    
    public boolean showDialog() {
        
        setVisible(true);    
        return true;
    }  
    
    private void m_jButtonOKActionPerformed(java.awt.event.ActionEvent evt) {
            dispose();
    }


	
	

}
