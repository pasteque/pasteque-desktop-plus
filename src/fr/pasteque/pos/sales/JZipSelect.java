package fr.pasteque.pos.sales;

import javax.swing.JDialog;
import javax.swing.JPanel;

import java.awt.BorderLayout;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListSelectionModel;
import javax.swing.event.MouseInputListener;

import fr.pasteque.basic.BasicException;
import fr.pasteque.pos.admin.InseeInfo;
import fr.pasteque.pos.caching.InseeCache;
import fr.pasteque.pos.widgets.WidgetsBuilder;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

public class JZipSelect extends JDialog {
	public JZipSelect() {
		init();
		//Par défaut - pour tester 8 villes à moins de 50 kms
		length = 8;
		zone = 50;
		inseeNum = "43008";
		
	}
	
	private void loadInitialData() {
		try {
			if( inseeNum != null) {
				InseeInfo insee = InseeCache.getInseeById(inseeNum) ;
				if(insee != null ) {
					listInsee.setModel(new MyListData<InseeInfo>(insee.getProx(length, zone)));
		            if (listInsee.getModel().getSize() > 0) {
		            	listInsee.setSelectedIndex(0);
		            	btnValider.setEnabled(true);
		            }
				}
			}
		} catch (BasicException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			listInsee.setModel(new MyListData<InseeInfo>(new ArrayList<InseeInfo>()));
			btnValider.setEnabled(false);
		}
	}
	
	private void loadData() {
		try {
			int real_length = zipCodeManual.getText().length() > 4 ? -1 : length ;
			listInsee.setModel(new MyListData<InseeInfo>(InseeCache.getInseeByZip(zipCodeManual.getText() , real_length)));
            if (listInsee.getModel().getSize() > 0) {
            	listInsee.setSelectedIndex(0);
            	btnValider.setEnabled(true);
            }
		} catch (BasicException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			listInsee.setModel(new MyListData<InseeInfo>(new ArrayList<InseeInfo>()));
			btnValider.setEnabled(false);
		}
	}

	public JZipSelect(String inseeNum , int length , int zone) {
		init();
		this.length = length;
		this.zone = zone;
		this.inseeNum = inseeNum;
		
		
		
	}
	
    private void jListInseeValueChanged(javax.swing.event.ListSelectionEvent evt) {

    	btnValider.setEnabled(listInsee.getSelectedValue() != null);
    	
    	if(listInsee.getSelectedValue() != null) {
    		setOutputInseeNum(listInsee.getSelectedValue().getInseeNum());
    	}

    }
	
	private void init() {
        int minWidth = 275;
        int minHeight = 275;
        int width = WidgetsBuilder.dipToPx(400);
        int height = WidgetsBuilder.dipToPx(320);
        width = Math.max(minWidth, width);
        height = Math.max(minHeight, height);
        
		setTitle("Saisie Code Postal et Ville");
		setResizable(true);
		setModalityType(ModalityType.DOCUMENT_MODAL);
		setModal(true);
		setSize(new Dimension(width, height));
		setPreferredSize(new Dimension(width, height));
		setMinimumSize(new Dimension(width, height));
		
		JPanel ValidationPanel = new JPanel();
		getContentPane().add(ValidationPanel, BorderLayout.SOUTH);
		
		btnRefus = new JButton("Refus client");
		btnRefus.setToolTipText("Le client a refusé de répondre");
		btnRefus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtnRefusActionPerformed(evt);
            }
        });
		ValidationPanel.add(btnRefus);
		
		btnValider = new JButton("Valider");
		btnValider.setEnabled(false);
		btnValider.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtnValiderActionPerformed(evt);
            }
        });
		ValidationPanel.add(btnValider);
		
		JPanel ManualZipPanel = new JPanel();
		getContentPane().add(ManualZipPanel, BorderLayout.NORTH);
		
		JLabel lblCodePostal = new JLabel("Code Postal :");
		ManualZipPanel.add(lblCodePostal);
		
		zipCodeManual = new JTextField();
		zipCodeManual.addKeyListener(new KeyAdapter() {
			public void keyReleased(KeyEvent e) {
				jzipCodeManualChanged(e);
			}
		});
		zipCodeManual.setToolTipText("Code Postal");
		
		ManualZipPanel.add(zipCodeManual);
		zipCodeManual.setColumns(10);
		
		JScrollPane ListPanel = new JScrollPane();
		getContentPane().add(ListPanel, BorderLayout.CENTER);
		
		listInsee = new JList<InseeInfo>();
		listInsee.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		listInsee.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                jListInseeValueChanged(evt);
            }
        });
		listInsee.addMouseListener(new MouseInputListener() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() == 2) {
					if(listInsee.getSelectedValue() != null) {
						setOutputInseeNum(listInsee.getSelectedValue().getInseeNum());
						dispose();
					}
				}
			}

			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseDragged(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseMoved(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			});
		//ListPanel.add(listInsee);
		ListPanel.setViewportView(listInsee);
        java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();


		setBounds((screenSize.width-width)/2, (screenSize.height-height)/2, width, height);
		
		getRootPane().setDefaultButton(btnValider);
		
	}

	protected void jzipCodeManualChanged(KeyEvent e) {
		if(zipCodeManual.getText().length()>2) {
			loadData();
		}
		
	}

	protected void jbtnRefusActionPerformed(ActionEvent evt) {
		setOutputInseeNum(null);
		dispose();
	}

	protected void jbtnValiderActionPerformed(ActionEvent evt) {
		dispose();
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1032304771028251812L;
	private JTextField zipCodeManual;
	private int length;
	private int zone;
	private String inseeNum ;
	private String outputInseeNum ;
	private JList<InseeInfo> listInsee ;
	JButton btnValider;
	JButton btnRefus;

	public String getInseeNum() {
		return inseeNum;
	}

	public void setInseeNum(String inseeNum) {
		this.inseeNum = inseeNum;
	}

	public String getOutputInseeNum() {
		return outputInseeNum;
	}

	public void setOutputInseeNum(String outputInseeNum) {
		this.outputInseeNum = outputInseeNum;
	}
	
    private static class MyListData<T> extends javax.swing.AbstractListModel<T> {
        
        /**
		 * 
		 */
		private static final long serialVersionUID = -1486879488780187892L;
		private java.util.List<T> m_data;
        
        public MyListData(java.util.List<T> data) {
            m_data = data;
        }
        
        public T getElementAt(int index) {
            return m_data.get(index);
        }
        
        public int getSize() {
            return m_data.size();
        } 
    }

	public String showDialog() {

		loadInitialData();
		setVisible(true);
		
		return getOutputInseeNum();
	} 
	
	//le listener pour la liste
	//le listener pour la zone de saisie

}
