//    POS-Tech
//    Based upon Openbravo POS
//
//    Copyright (C) 2007-2009 Openbravo, S.L.
//                       2012 SARL SCOP Scil (http://scil.coop)
//
//    This file is part of POS-Tech.
//
//    POS-Tech is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    POS-Tech is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with POS-Tech.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.sales;

import fr.pasteque.data.loader.ImageLoader;
import fr.pasteque.pos.forms.AppLocal;
import fr.pasteque.pos.widgets.WidgetsBuilder;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dialog;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Window;
import java.awt.event.ActionEvent;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

public class NotesPicker extends javax.swing.JDialog {

    /**
	 * 
	 */
	private static final long serialVersionUID = 3966059595155219437L;
	private Boolean response;

    private NotesPicker(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
    }

    private NotesPicker(java.awt.Dialog parent, boolean modal) {
        super(parent, modal);
    }

    public static NotesPicker getNotesPicker(Component parent) {
        Window window = SwingUtilities.getWindowAncestor(parent);
        NotesPicker myMsg;
        if (window instanceof Frame) {
            myMsg = new NotesPicker((Frame) window, true);
        } else {
            myMsg = new NotesPicker((Dialog) window, true);
        }
        myMsg.init();
        myMsg.applyComponentOrientation(parent.getComponentOrientation());
        return myMsg;
    }

    private void init() {
        initComponents();
        getRootPane().setDefaultButton(m_jButtonOK);
    }

    public Boolean getNotesResponse() {
        return response;
    }

    private void initComponents() {
        m_jButtonOK = WidgetsBuilder.createButton(ImageLoader.readImageIcon("button_ok.png"),
                AppLocal.getIntString("Button.OK"),
                WidgetsBuilder.SIZE_MEDIUM);
        m_jButtonCancel = WidgetsBuilder.createButton(ImageLoader.readImageIcon("button_cancel.png"),
                AppLocal.getIntString("Button.Cancel"),
                WidgetsBuilder.SIZE_MEDIUM);
        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle(AppLocal.getIntString("Form.TicketNote"));

        this.getContentPane().setLayout(new BorderLayout());
        JPanel container = new JPanel();
        container.setLayout(new GridBagLayout());
        this.getContentPane().add(container, BorderLayout.CENTER);
        GridBagConstraints cstr;

        
        cstr = new GridBagConstraints();
        cstr.fill = GridBagConstraints.NONE;
        cstr.anchor = GridBagConstraints.CENTER;
        cstr.insets = new Insets(0,0,0,0);  
        cstr.gridx = 0;       
        cstr.gridwidth = 2;   
        cstr.gridy = 2;   
        cstr.gridheight = 1;
      	
      	container.add(WidgetsBuilder.createLabel(AppLocal.getIntString("Label.Notes")),cstr);
        
        m_jNotes = new javax.swing.JTextArea();
        m_jNotes.setFont(WidgetsBuilder.getFont(WidgetsBuilder.SIZE_SMALL));
        m_jNotes.setEditable(true);
        m_jNotes.setFocusable(true);
        m_jNotes.setRows(3);
      	m_jNotes.setLineWrap(true);
      	m_jNotes.setWrapStyleWord(true);
      	
      	cstr = new GridBagConstraints();
      	cstr.fill = GridBagConstraints.BOTH;
      	cstr.ipady = 0;       //reset to default
      	cstr.weighty = 2;   //request any extra vertical space
      	cstr.anchor = GridBagConstraints.PAGE_END; //bottom of space
      	cstr.insets = new Insets(5,5,5,5);  //top padding
      	cstr.gridx = 0;       //aligned with button left
      	cstr.gridwidth = 5;   //5 columns wide
      	cstr.weightx = 2;
      	cstr.gridy = 3;       //4th row
      	cstr.gridheight = 3;
      	
      	container.add(m_jNotes,cstr);

        JPanel buttonsContainer = new JPanel();
        m_jButtonOK.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                m_jButtonOKActionPerformed(evt);
            }
        });
        buttonsContainer.add(m_jButtonOK);

        m_jButtonCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                m_jButtonCancelActionPerformed(evt);
            }
        });
        buttonsContainer.add(m_jButtonCancel);

        cstr = new GridBagConstraints();
        cstr.gridx = 0;
        cstr.gridy = 6;
        cstr.gridwidth = 3;
        cstr.fill = GridBagConstraints.HORIZONTAL;
        cstr.anchor = GridBagConstraints.LINE_END;
        container.add(buttonsContainer, cstr);

        java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        setBounds((screenSize.width-609)/2, (screenSize.height-388)/2, 609, 388);
    }

    private void m_jButtonOKActionPerformed(java.awt.event.ActionEvent evt) {
        this.response = true;
        dispose();
    }

    private void m_jButtonCancelActionPerformed(ActionEvent evt) {
    	this.response = false;
        dispose();
    }
    
    public void setNotes(String ticketNotes) {
    	m_jNotes.setText(ticketNotes);
    }
    
    public String getNotes() {
    	return m_jNotes.getText();
    }

    private javax.swing.JButton m_jButtonCancel;
    private javax.swing.JButton m_jButtonOK;
    private JTextArea m_jNotes;

}
