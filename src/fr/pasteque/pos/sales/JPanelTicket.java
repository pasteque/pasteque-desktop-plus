//    POS-Tech
//    Based upon Openbravo POS
//
//    Copyright (C) 2007-2009 Openbravo, S.L.
//                       2012 Scil (http://scil.coop)
//
//    This file is part of POS-Tech.
//
//    POS-Tech is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    POS-Tech is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with POS-Tech.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.sales;

import javax.swing.*;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Date;

import fr.pasteque.data.gui.ComboBoxValModel;
import fr.pasteque.data.gui.MessageInf;
import fr.pasteque.pos.printer.*;
import fr.pasteque.pos.forms.JPanelView;
import fr.pasteque.pos.forms.AppView;
import fr.pasteque.pos.forms.AppLocal;
import fr.pasteque.pos.panels.JProductFinder;
import fr.pasteque.pos.scale.ScaleException;
import fr.pasteque.pos.payment.JPaymentSelect;
import fr.pasteque.basic.BasicException;
import fr.pasteque.beans.EncryptDecrypt;
import fr.pasteque.data.gui.ListKeyed;
import fr.pasteque.data.loader.ImageLoader;
import fr.pasteque.data.user.OseToken;
import fr.pasteque.pos.admin.InseeInfo;
import fr.pasteque.pos.caching.CatalogCache;
import fr.pasteque.pos.caching.InseeCache;
import fr.pasteque.pos.catalog.CatalogSelector;
import fr.pasteque.pos.catalog.JCatalogSubgroups;
import fr.pasteque.pos.customers.CustomerInfoExt;
import fr.pasteque.pos.customers.DataLogicCustomers;
import fr.pasteque.pos.customers.DiscountProfile;
import fr.pasteque.pos.customers.JCustomerFinder;
import fr.pasteque.pos.scripting.ScriptEngine;
import fr.pasteque.pos.scripting.ScriptException;
import fr.pasteque.pos.scripting.ScriptFactory;
import fr.pasteque.pos.forms.DataLogicSystem;
import fr.pasteque.pos.forms.DataLogicSales;
import fr.pasteque.pos.forms.BeanFactoryApp;
import fr.pasteque.pos.forms.BeanFactoryException;
import fr.pasteque.pos.forms.AppConfig;
import fr.pasteque.pos.inventory.TaxCategoryInfo;
import fr.pasteque.pos.payment.JPaymentSelectReceipt;
import fr.pasteque.pos.payment.JPaymentSelectRefund;
import fr.pasteque.pos.payment.PaymentInfo;
import fr.pasteque.pos.payment.PaymentInfoCash;
import fr.pasteque.pos.sales.restaurant.JTicketsBagRestaurant;
import fr.pasteque.pos.sales.restaurant.JTicketsBagRestaurantMap;
import fr.pasteque.pos.ticket.CompositionInfo;
import fr.pasteque.pos.ticket.ProductInfoExt;
import fr.pasteque.pos.ticket.TariffInfo;
import fr.pasteque.pos.ticket.TaxInfo;
import fr.pasteque.pos.ticket.TicketInfo;
import fr.pasteque.pos.ticket.TicketLineInfo;
import fr.pasteque.pos.util.JRPrinterAWT300;
import fr.pasteque.pos.util.ReportUtils;
import fr.pasteque.pos.util.RoundUtils;
import fr.pasteque.pos.util.StringUtils;
import fr.pasteque.pos.widgets.JNumberEvent;
import fr.pasteque.pos.widgets.JNumberEventListener;
import fr.pasteque.pos.widgets.JNumberKeys;
import fr.pasteque.pos.widgets.WidgetsBuilder;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;
import javax.print.PrintService;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.h2.util.Permutations;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRMapArrayDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

/**
 *
 * @author adrianromero
 */
public abstract class JPanelTicket extends JPanel implements JPanelView, BeanFactoryApp, TicketsEditor, DataLogicCustomers.CustomerListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3444628995050147514L;

	private static Logger logger = Logger.getLogger("fr.pasteque.pos.sales.JPanelTicket");

	protected JTicketLines m_ticketlines;

	// private Template m_tempLine;
	private TicketParser m_TTP;

	protected TicketInfo m_oTicket;
	protected Object m_oTicketExt;

	/** True when picking components */
	private boolean m_bIsSubproduct;
	/** Quantity of current composition (when picking a composition) */
	protected double m_dMultiply;
	/** The catalog currently displayed (for compositions) */
	protected Component m_catalog;
	protected CatalogSelector m_cat;
	/** Current composition state (PRODUCT_* values) */
	protected int m_iProduct;

	private StringBuffer m_sBarcode;

	private JTicketsBag m_ticketsbag;

	private ListKeyed<TaxInfo> taxcollection;
	// private ComboBoxValModel m_TaxModel;

	@SuppressWarnings("unused")
	private ListKeyed<TaxCategoryInfo> taxcategoriescollection;
	private ComboBoxValModel<TaxCategoryInfo> taxcategoriesmodel;

	private TaxesLogic taxeslogic;

	private List<TariffInfo> m_TariffList;
	private ComboBoxValModel<TariffInfo> m_TariffModel;

	// private ScriptObject scriptobjinst;
	protected JPanelButtons m_jbtnconfig;

	protected AppView m_App;
	protected DataLogicSystem dlSystem;
	protected DataLogicSales dlSales;
	protected DataLogicCustomers dlCustomers;
	protected Timer clockTimer;

	private JPaymentSelect paymentdialogreceipt;
	private JPaymentSelect paymentdialogrefund;

	// State variables for new automate
	private int m_InputState;
	private static final int I_NOTHING = 0;
	private static final int I_PRICE = 1;
	private static final int I_QUANTITY = 2;
	private static final int I_CUSTOMER = 3;
	private static final int I_LINE_REBATE = 4;
	private static final int I_REBATE = 5;
	private static final int I_NOTE = 6;

	private int m_PriceActualState;
	private int m_PricePreviousState;
	private int m_QuantityActualState;
	private int m_QuantityPreviousState;

	protected JPanel lineEditBtns;

	@SuppressWarnings("unused")
	private DataLogicSales dlsales;
	
	private static final int N_NOTHING = 0;
	private static final int N_ZERO = 1;
	private static final int N_DECIMALZERO = 2;
	private static final int N_NUMBER = 3;
	private static final int N_DECIMALNUMBER = 4;
	private static final int N_DECIMAL = 5;
	private static final int N_ALPHA = 6;

	protected static final int PRODUCT_SINGLE = 0;
	protected static final int PRODUCT_COMPOSITION = 1;
	protected static final int PRODUCT_SUBGROUP = 2;

	private static final char keyBack = '\u0008';
	private static final char keyDel = '\u007f';
	private static final char keySection = '\u00a7';
	private static final char keyEnter = '\n';

	private static final int AUTO_PERFORMED = 0;
	private static final int BUTTON_PERFORMED = 1;
	

	/** Creates new form JTicketView */
	public JPanelTicket() {
		dlsales = new DataLogicSales();
		initComponents();
	}

	public void init(AppView app) throws BeanFactoryException {
		m_App = app;
		this.dlSystem = new DataLogicSystem();
		this.dlSales = new DataLogicSales();
		this.dlCustomers = new DataLogicCustomers();

		m_ticketsbag = getJTicketsBag();
		m_jPanelBag.add(m_ticketsbag.getBagComponent(), BorderLayout.LINE_START);
		add(m_ticketsbag.getNullComponent(), "null");

		m_ticketlines.init(dlSystem.getResourceAsXML("Ticket.Line"));

		m_TTP = new TicketParser(m_App.getDeviceTicket(), dlSystem);

		// Los botones configurables...
		m_jbtnconfig = new JPanelButtons("Ticket.Buttons", this);
		m_jButtonsExt.add(m_jbtnconfig);

		// El panel de los productos o de las lineas...
		m_catalog = getSouthComponent();
		catcontainer.add(getSouthComponent(), BorderLayout.CENTER);
		m_iProduct = PRODUCT_SINGLE;

		// Tariff areas
		m_TariffModel = new ComboBoxValModel<TariffInfo>();

		// ponemos a cero el estado
		eraseAutomator();

		// inicializamos
		m_oTicket = null;
		m_oTicketExt = null;

		m_PriceActualState = N_NOTHING;
		m_QuantityActualState = N_NOTHING;
		m_InputState = I_PRICE;
	}

	public Object getBean() {
		return this;
	}

	protected Component getSouthAuxComponent() {
		m_cat = new JCatalogSubgroups(dlSales, "true".equals(m_jbtnconfig.getProperty("pricevisible")), "true".equals(m_jbtnconfig
				.getProperty("taxesincluded")), Integer.parseInt(m_jbtnconfig.getProperty("img-width", "64")),
				Integer.parseInt(m_jbtnconfig.getProperty("img-height", "54")));
		m_cat.getComponent().setPreferredSize(new Dimension(0, Integer.parseInt(m_jbtnconfig.getProperty("cat-height", "245"))));
		m_cat.addActionListener(new CatalogListener());
		((JCatalogSubgroups) m_cat).setGuideMode(true);
		return m_cat.getComponent();
	}

	public JComponent getComponent() {
		return this;
	}

	public void activate() throws BasicException {

		paymentdialogreceipt = JPaymentSelectReceipt.getDialog(this);
		paymentdialogreceipt.init(m_App);
		paymentdialogrefund = JPaymentSelectRefund.getDialog(this);
		paymentdialogrefund.init(m_App);

		// impuestos incluidos seleccionado ?
		m_jaddtax.setSelected("true".equals(m_jbtnconfig.getProperty("taxesincluded")));

		// Inicializamos el combo de los impuestos.
		java.util.List<TaxInfo> taxlist = this.dlSales.getTaxList();
		taxcollection = new ListKeyed<TaxInfo>(taxlist);
		java.util.List<TaxCategoryInfo> taxcategorieslist = this.dlSales.getTaxCategoriesList();
		taxcategoriescollection = new ListKeyed<TaxCategoryInfo>(taxcategorieslist);

		taxcategoriesmodel = new ComboBoxValModel<TaxCategoryInfo>(taxcategorieslist);
		m_jTax.setModel(taxcategoriesmodel);

		String taxesid = m_jbtnconfig.getProperty("taxcategoryid");
		if (taxesid == null) {
			if (m_jTax.getItemCount() > 0) {
				m_jTax.setSelectedIndex(0);
			}
		} else {
			taxcategoriesmodel.setSelectedKey(taxesid);
		}

		taxeslogic = new TaxesLogic(taxlist);

		// Show taxes options
		if (m_App.getAppUserView().getUser().hasPermission("sales.ChangeTaxOptions")) {
			m_jTax.setVisible(true);
			m_jaddtax.setVisible(true);
		} else {
			m_jTax.setVisible(false);
			m_jaddtax.setVisible(false);
		}

		// Initialize tariff area combobox
		/*
		 * m_TariffList = this.dlSales.getTariffAreaList(); TariffInfo defaultArea = new TariffInfo(-1,
		 * AppLocal.getIntString("Label.DefaultTariffArea")); m_TariffList.add(0, defaultArea);
		 */
		m_TariffList = this.dlSales.getActiveTariffAreaList(m_App);

		m_TariffModel = new ComboBoxValModel<TariffInfo>(m_TariffList);
		m_jTariff.setModel(m_TariffModel);
		if (m_TariffList.size() >= 1) {
			this.updateTariffCombo();
			m_jTariff.setVisible(true);
		} else {
			m_jTariff.setVisible(false);
		}
		// TODO gérer ce droit
		m_jTariff.setEnabled(m_App.getAppUserView().getUser().hasPermission("ticket.EditPrice"));

		// Authorization for buttons
		btnSplit.setEnabled(m_App.getAppUserView().getUser().hasPermission("sales.Total"));
		m_jDelete.setEnabled(m_App.getAppUserView().getUser().hasPermission("sales.EditLines"));
		m_jNumberKeys.setMinusEnabled(m_App.getAppUserView().getUser().hasPermission("sales.EditLines"));
		m_jNumberKeys.setEqualsEnabled(m_App.getAppUserView().getUser().hasPermission("sales.Total"));
		m_jbtnconfig.setPermissions(m_App.getAppUserView().getUser());

		m_ticketsbag.activate();

		this.updateLocationName();
		this.updateConnectedUser();
		// Start clock
		this.updateClock();
		Calendar c = Calendar.getInstance();
		c.add(Calendar.MINUTE, 1);
		c.set(Calendar.SECOND, 0);
		this.clockTimer = new Timer();
		this.clockTimer.schedule(new TimerTask() {
			public void run() {
				updateClock();
			}
		}, c.getTime(), 60000);
	}

	public boolean deactivate() {
		this.clockTimer.cancel();
		this.clockTimer = null;
		return m_ticketsbag.deactivate();
	}

	protected abstract JTicketsBag getJTicketsBag();

	protected abstract Component getSouthComponent();

	protected abstract void resetSouthComponent();

	protected void updateConnectedUser() {
		this.connectedUser.setText( AppLocal.getIntString("label.ConnectedUser",m_App.getAppUserView().getUser().getName()));
	}
	protected void updateClock() {
		Calendar c = Calendar.getInstance();
		int hour = c.get(Calendar.HOUR_OF_DAY);
		int minutes = c.get(Calendar.MINUTE);
		String strHour = String.valueOf(hour);
		if (strHour.length() == 1) {
			strHour = "0" + strHour;
		}
		String strMinute = String.valueOf(minutes);
		if (strMinute.length() == 1) {
			strMinute = "0" + strMinute;
		}
		this.clock.setText(AppLocal.getIntString("Clock.Format", strHour, strMinute));
	}

	public void setActiveTicket(TicketInfo oTicket, Object oTicketExt) {

		m_oTicket = oTicket;
		m_oTicketExt = oTicketExt;

		if (m_oTicket != null) {
			// Asign preeliminary properties to the receipt
			m_oTicket.setUser(m_App.getAppUserView().getUser().getUserInfo());
			m_oTicket.setActiveCash(m_App.getActiveCashIndex());
			m_oTicket.setDate(new Date()); // Set the edition date.
		}

		executeEvent(m_oTicket, m_oTicketExt, "ticket.show");

		refreshTicket();
		this.updateTariffCombo();
		if (m_oTicket!= null && m_oTicket.getCustomer() != null){
			this.messageBox.setText(m_oTicket.getCustomer().printCustInfo());
		}else{
			this.messageBox.setText("");
		}
	}

	public TicketInfo getActiveTicket() {
		return m_oTicket;
	}

	public void setCustomersCount(int count) {
		this.m_oTicket.setCustomersCount(count);
		this.refreshTicketLabel();
	}

	private void refreshTicketLabel() {
		// Better view of customer's name by changing the color of text and
		// remove hour and ticket's id
		String name = null;
		if (m_oTicket.getCustomer() != null) {
			CustomerInfoExt customerName = m_oTicket.getCustomer();
			name = customerName.getName();
			Color green = new Color(33, 67, 92);
			m_jTicketId.setBackground(green);
			m_jTicketId.setForeground(java.awt.Color.WHITE);
		} else {
			name = m_oTicket.getName(m_oTicketExt);
			m_jTicketId.setForeground(java.awt.Color.DARK_GRAY);
			m_jTicketId.setBackground(java.awt.Color.WHITE);
		}
		if (m_oTicket.hasCustomersCount()) {
			name += " (" + m_oTicket.getCustomersCount() + ")";
		}
		m_jTicketId.setText(name);
	}

	private void refreshTicket() {

		CardLayout cl = (CardLayout) (getLayout());

		if (m_oTicket == null) {
			m_jTicketId.setText(null);
			m_ticketlines.clearTicketLines();

			this.subtotalLabel.setText(null);
			this.ticketInfoLabel.setText(null);
			m_jTotalEuros.setText(null);
			this.discountLabel.setText(null);

			eraseAutomator();

			// Muestro el panel de nulos.
			cl.show(this, "null");
			resetSouthComponent();

		} else {
			/*
			 * if (m_oTicket.getTicketType() == TicketInfo.RECEIPT_REFUND) { // Make disable Search and Edit Buttons
			 * m_jEditLine.setVisible(false); m_jList.setVisible(false); }
			 */
			// Refresh ticket taxes
			for (TicketLineInfo line : m_oTicket.getLines()) {
				line.setVAT(taxeslogic.getTaxInfo(line.getProductTaxCategoryID(), m_App.getCashRegister(), m_oTicket.getDate(),
						m_oTicket.getCustomer()));
			}

			// The ticket name
			this.refreshTicketLabel();
			
			// Limpiamos todas las filas y anadimos las del ticket actual
			m_ticketlines.clearTicketLines();

			for (int i = 0; i < m_oTicket.getLinesCount(); i++) {
				m_ticketlines.addTicketLine(m_oTicket.getLine(i) , m_App.getAppUserView().getUser());
			}
			
			printPartialTotals();
			eraseAutomator();

			// Muestro el panel de tickets.
			cl.show(this, "ticket");
			resetSouthComponent();

			// activo el tecleador...
			m_jKeyFactory.setText(null);
			java.awt.EventQueue.invokeLater(new Runnable() {
				public void run() {
					m_jKeyFactory.requestFocus();
				}
			});

			// Show customers count if not set in restaurant mode
			if (this.m_ticketsbag instanceof JTicketsBagRestaurantMap) {
				if (this.m_oTicket.getCustomersCount() == null) {
					if (!AppConfig.loadedInstance.getProperty("ui.autodisplaycustcount").equals("0")) {
						((JTicketsBagRestaurant) this.m_ticketsbag.getBagComponent()).custCountBtnActionPerformed(null);
					}
				}
			}
		}
		updateTicketNoteButton();
	}

	public void updateTariffCombo() {
		updateTariffCombo(m_oTicket);
	}

	/** Update tariff area combo box to select the area of the ticket. */
	public void updateTariffCombo(TicketInfo ticket) {
		Integer tariffOrder = Integer.parseInt(m_jbtnconfig.getProperty("mainTariffAreaOrder", "-1"));
		if (m_jTariff.getItemCount() > 0) {
			m_jTariff.setSelectedIndex(0);
			if (tariffOrder >= 0) {
				for (int i = 0; i < m_jTariff.getItemCount(); i++) {
					try {
						TariffInfo tariff = (TariffInfo) m_jTariff.getItemAt(i);
						if (tariff != null) {
							if (new Integer(tariff.getOrder()).equals(tariffOrder)) {
								m_jTariff.setSelectedIndex(i);
							}
						}
					} catch (ClassCastException e) {
						e.printStackTrace();
					}
				}
			}
			if (ticket != null) {
				// Select the current tariff area
				for (int i = 0; i < m_jTariff.getItemCount(); i++) {
					try {
						TariffInfo tariff = (TariffInfo) m_jTariff.getItemAt(i);
						if (tariff != null) {
							if (new Integer(tariff.getID()).equals(ticket.getTariffArea())) {
								m_jTariff.setSelectedIndex(i);
							}
						}
					} catch (ClassCastException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}

	private void switchTariffArea(TariffInfo area) {
		if (m_oTicket != null) {
			if (area.getID() == -1) {
				m_oTicket.setTariffArea(null);
			} else {
				m_oTicket.setTariffArea(new Integer(area.getID()));
			}
		}
		this.updateTariffCombo();
	}

	// TODO AME: affichage TVA pour ecran
	private void printPartialTotals() {
		if (m_oTicket == null) {
			this.subtotalLabel.setText(null);
			this.ticketInfoLabel.setText(null);
			m_jTotalEuros.setText(null);
			this.discountLabel.setText(null);
		} else {
			this.subtotalLabel.setText(AppLocal.getIntString("label.subtotalLine", m_oTicket.printSubTotal(), m_oTicket.printVAT()));
			if(m_oTicket.getArticlesOrderedCount() > 0D){
				if(m_oTicket.getArticlesDeliveredCount() > 0D) {
					this.ticketInfoLabel.setText(AppLocal.getIntString("label.ticketInfo", m_oTicket.printArticlesStockCount() , m_oTicket.printArticlesOrderCount()));
				} else {
					this.ticketInfoLabel.setText(AppLocal.getIntString("label.ticketInfoOrder", m_oTicket.printArticlesOrderCount()));
				}
			} else {
				this.ticketInfoLabel.setText(AppLocal.getIntString("label.ticketInfoLight", m_oTicket.printArticlesStockCount()));
			}
			m_jTotalEuros.setText(m_oTicket.printTotal());
			if (m_oTicket.getDiscountRate() > 0.0) {
				this.discountLabel.setText(AppLocal.getIntString("label.totalDiscount", m_oTicket.printDiscountRate(),
						m_oTicket.printFullTotal()));
			} else {
				this.discountLabel.setText(null);
			}
		}
	}

	// TODO Voir si il n'y a pas moyen d'optimiser et mieux réutiliser ceci !
	private void syncTicketLine(int index, TicketLineInfo oLine) {
		m_oTicket.setLine(index, oLine);
		m_ticketlines.setTicketLine(index, oLine);
	}

	private void paintTicketLine(int index, TicketLineInfo oLine) {
		// TODO On repassera ici pour le changement ds l'affichage
		if (executeEventAndRefresh("ticket.setline", /* new ScriptArg("index", index), */new ScriptArg("line", oLine)) == null) {
			syncTicketLine(index, oLine);
			m_ticketlines.setSelectedIndex(index);

			visorTicketLine(oLine); // Y al visor tambien...
			printPartialTotals();
			eraseAutomator();

			// event receipt
			executeEventAndRefresh("ticket.change", new ScriptArg("index", index));
		}
	}

	/**
	 * Retourner la ligne d'un type donné correspondant à un produit donné
	 * 
	 * @param product
	 *            le produit concerné
	 * @param type
	 *            le type de ligne de ticket concerné
	 * @return L'objet TicketLineInfo si trouvé - null sinon
	 */
	private int getTicketLine(TicketLineInfo line) {
		int oLine = -1, tmpLine = -1;
		// todo parcourir m_ticketlines
		for (TicketLineInfo ligne : m_oTicket.getLines()) {
			tmpLine++;
			if (ligne.isSimilar(line)) {
				oLine = tmpLine;
				break;
			}
		}
		return oLine;
	}

	private TicketLineInfo createTicketLine(ProductInfoExt oProduct, double dMul, double dPrice) {
		// traitement de la TVA
		TaxInfo tax = taxeslogic.getTaxInfo(oProduct.getTaxCategoryID(), m_App.getCashRegister(), m_oTicket.getDate(),
				m_oTicket.getCustomer());
		// traitement des autres : D3E, Ecomob, ....
		List<TaxInfo> taxes = new ArrayList<TaxInfo>();
		for (String taxCategoryId : oProduct.getTaxCategories()) {
			taxes.add(taxeslogic.getTaxInfo(taxCategoryId, m_App.getCashRegister(), m_oTicket.getDate(), m_oTicket.getCustomer()));
		}
		TicketLineInfo line = new TicketLineInfo(oProduct, dMul, dPrice, tax, taxes,
				(java.util.Properties) (oProduct.getProperties().clone()));
		line.setLocationId(m_App.getInventoryLocation());
		return line;
	}

	private TicketLineInfo addTicketLine(ProductInfoExt oProduct, double dMul, double dPrice) {
		TicketLineInfo line = createTicketLine(oProduct, dMul, dPrice);
		line.setSubproduct(m_bIsSubproduct);

		if (!mergeLine(line, true)) {
			addTicketLine(line);
			if (oProduct.isDiscountEnabled()) {
				double rate = oProduct.getDiscountRate();
				if (rate > 0.005) {
					line.setDiscountRate(rate);
				}
			}
		}

		return line;
	}

	protected void addTicketLine(TicketLineInfo oLine) {
		int i = m_oTicket.getLinesCount();
		// Update price from tariff area if needed
		checkLinePrice(oLine);

		if (executeEventAndRefresh("ticket.addline", new ScriptArg("line", oLine)) == null) {
			if (oLine.isProductCom()) {
				// Comentario entonces donde se pueda
				i = m_ticketlines.getSelectedIndex();

				// me salto el primer producto normal...
				if (i >= 0 && !m_oTicket.getLine(i).isProductCom()) {
					i++;
				}

				// me salto todos los productos auxiliares...
				while (i >= 0 && i < m_oTicket.getLinesCount() && m_oTicket.getLine(i).isProductCom()) {
					i++;
				}

				if (i >= 0) {
					m_oTicket.insertLine(i, oLine);
					// Pintamos la linea en la vista...
					m_ticketlines.insertTicketLine(i, oLine);
				} else {
					Toolkit.getDefaultToolkit().beep();
				}
			} else {
				// Producto normal, entonces al final
				// newline.getMultiply()
				m_oTicket.addLine(oLine);
				// Pintamos la linea en la vista...
				m_ticketlines.addTicketLine(oLine , m_App.getAppUserView().getUser());
			}

			visorTicketLine(oLine);
			printPartialTotals();
			eraseAutomator();

			// event receipt
			executeEventAndRefresh("ticket.change", new ScriptArg("index", i));
		}
	}

	private boolean checkLinePrice(TicketLineInfo oLine) {
		Double initialPrice = oLine.getPrice();

		// EDU On ne tente pas d'ajuster le prix lorsque l'on a affaire à un retour ou lorsqu'il n'y a pas le choix entre plusieurs prix
		if (m_oTicket.getTariffArea() != null && (oLine.getParentLine() == null || oLine.getParentId() == null)) {
			// TODO: don't update price of composition product (which price is 0)
			// Get tariff area price
			// En jouant sur le getTarrifArea on arrive à nos fins
			// soit il dépend du produit et du lieu soit il dépend
			// de l'entrée - pas de cat actif spécifique pour le lieu
			// du choix de la combo-box
			try {
				List<Integer> areaId = new ArrayList<Integer>();
				Double price = this.dlSales.getTariffAreaPrice(m_App, m_oTicket, oLine.getProductID(), areaId);
				if (price != null) {
					oLine.setPrice(price);
					if (areaId.size() > 0) {
						oLine.setTariffAreaId(areaId.get(0));
					}
				}
			} catch (BasicException e) {
				e.printStackTrace();
			}
		}

		return initialPrice.equals(oLine.getPrice());
	}

	private boolean mergeLine(TicketLineInfo line, boolean paint) {
		int origLineNb = getTicketLine(line);
		TicketLineInfo origLine = null;
		boolean stop = origLineNb != -1;

		if (stop) {
			origLine = new TicketLineInfo(m_oTicket.getLine(origLineNb));
			stop = origLine.mergeTicketLine(line);
			line = origLine;
			if (paint) {
				paintTicketLine(origLineNb, origLine);
			} else {
				syncTicketLine(origLineNb, origLine);
			}
		}

		return stop;
	}

	private void toggleOrderTicketLine(int i) {
		TicketLineInfo oLine = m_oTicket.getLine(i);
		oLine.toggleOrder();
		paintTicketLine(i, oLine);

	}

	private void removeTicketLine(int i) {
		
		if (executeEventAndRefresh("ticket.removeline"/* , new ScriptArg("index", i) */) == null) {

			if (m_oTicket.getLine(i).isProductCom()) {
				// Es un producto auxiliar, lo borro y santas pascuas.
				m_oTicket.removeLine(i);
				m_ticketlines.removeTicketLine(i);
			} else {
				// Es un producto normal, lo borro.
				m_oTicket.removeLine(i);
				m_ticketlines.removeTicketLine(i);
				// Y todos lo auxiliaries que hubiera debajo.
				while (i < m_oTicket.getLinesCount() && m_oTicket.getLine(i).isProductCom()) {
					m_oTicket.removeLine(i);
					m_ticketlines.removeTicketLine(i);
				}
			}

			visorTicketLine(null); // borro el visor
			printPartialTotals(); // pinto los totales parciales...
			eraseAutomator();// Pongo a cero

			// event receipt
			executeEventAndRefresh("ticket.change", new ScriptArg("index", (i + 1) * -1));
		}
	}

	private ProductInfoExt getInputProduct() {
		ProductInfoExt oProduct = new ProductInfoExt(); // Es un ticket
		oProduct.setReference(null);
		oProduct.setCode(null);
		oProduct.setName("");
		oProduct.setTaxCategoryID(((TaxCategoryInfo) taxcategoriesmodel.getSelectedItem()).getID());

		oProduct.setPriceSell(includeTaxes(oProduct.getTaxCategoryID(), getInputValue()));

		return oProduct;
	}

	private double includeTaxes(String tcid, double dValue) {
		if (m_jaddtax.isSelected()) {
			TaxInfo tax = taxeslogic.getTaxInfo(tcid, m_App.getCashRegister(), m_oTicket.getDate(), m_oTicket.getCustomer());
			double dTaxRate = tax == null ? 0.0 : tax.getRate();
			return dValue / (1.0 + dTaxRate);
		} else {
			return dValue;
		}
	}

	private double getInputValue() {
		try {
			return Double.parseDouble(m_jPrice.getText());
		} catch (NumberFormatException e) {
			return 0.0;
		}
	}

	private double getPorValue() {
		try {
			return Double.parseDouble(m_jPor.getText().substring(1));
		} catch (NumberFormatException e) {
			return 1.0;
		} catch (StringIndexOutOfBoundsException e) {
			return 1.0;
		}
	}

	private void incProductByCode(String sCode, double quantity, String note) {
		// precondicion: sCode != null
		try {
			ProductInfoExt oProduct = dlSales.getProductInfoByCode(sCode);
			if (oProduct == null) {
				Toolkit.getDefaultToolkit().beep();
				// new MessageInf(MessageInf.SGN_WARNING, AppLocal.getIntString("message.noproduct")).show(this);
				eraseAutomator();
				searchProduct(sCode, quantity);
			} else {
				// Se anade directamente una unidad con el precio y todo
				incProduct(oProduct, quantity, note);
			}
		} catch (BasicException eData) {
			eraseAutomator();
			new MessageInf(eData).show(this);
		}
	}

	private void incProductByCodePrice(String sCode, double dPriceSell) {
		// precondicion: sCode != null
		try {
			ProductInfoExt oProduct = dlSales.getProductInfoByCode(sCode);
			if (oProduct == null) {
				Toolkit.getDefaultToolkit().beep();
				new MessageInf(MessageInf.SGN_WARNING, AppLocal.getIntString("message.noproduct")).show(this);
				eraseAutomator();
			} else {
				// Se anade directamente una unidad con el precio y todo
				if (m_jaddtax.isSelected()) {
					// debemos quitarle los impuestos ya que el precio es con iva incluido...
					TaxInfo tax = taxeslogic.getTaxInfo(oProduct.getTaxCategoryID(), m_App.getCashRegister(), m_oTicket.getDate(),
							m_oTicket.getCustomer());
					addTicketLine(oProduct, 1.0, dPriceSell / (1.0 + tax.getRate()));
				} else {
					addTicketLine(oProduct, 1.0, dPriceSell);
				}
			}
		} catch (BasicException eData) {
			eraseAutomator();
			new MessageInf(eData).show(this);
		}
	}

	private void incProduct(ProductInfoExt prod, double quantity, String note) {
		if (prod.isScale() && m_App.getDeviceScale().existsScale()) {
			try {
				Double value = m_App.getDeviceScale().readWeight();
				if (value != null) {
					incProduct(value.doubleValue(), prod, note);
				}
			} catch (ScaleException e) {
				Toolkit.getDefaultToolkit().beep();
				new MessageInf(MessageInf.SGN_WARNING, AppLocal.getIntString("message.noweight"), e).show(this);
				eraseAutomator();
			}
		} else {
			// No es un producto que se pese o no hay balanza
			incProduct(quantity, prod, note);
		}
	}


	private void incProduct(double dPor, ProductInfoExt prod, String comment) {
		// precondicion: prod != null
		TicketLineInfo line = addTicketLine(prod, dPor, prod.getPriceSell());
		if(comment != null && !comment.isEmpty()) {
			line.setComments(comment);
		}
		/**
		 * Pops automatically the product's attributes screen, if the product has one, when a product is added to the list
		 */
		if (prod.getAttributeSetID() != null) {
			jEditAttributesActionPerformed(AUTO_PERFORMED);
		}
		if (("true".equals(AppConfig.loadedInstance.getProperty("ui.showalerts")) || 
				"1".equals(AppConfig.loadedInstance.getProperty("ui.showalerts")) ) && !prod.getMessagesByCriteria("ALERT" , m_App.getInventoryLocation()).isEmpty()) {
			jShowAlertActionPerformed(AUTO_PERFORMED, prod);
		}
	}

	/** Switch catalog according to composition state */
	public void changeCatalog() {
		catcontainer.removeAll();
		setSubgroupMode(m_iProduct == PRODUCT_SUBGROUP);
		if (m_iProduct == PRODUCT_SUBGROUP) {
			// Set composition catalog
			m_catalog = getSouthAuxComponent();
		} else {
			// Set default catalog
			m_catalog = getSouthComponent();
		}
		catcontainer.add(m_catalog, BorderLayout.CENTER);
		catcontainer.updateUI();
	}

	/** Activate or deactivate input component for subgroups (or not) */
	private void setSubgroupMode(boolean value) {
		this.lineBtnsContainer.setEnabled(!value);
		enableComponents(this.lineBtnsContainer, !value);
		enableComponents(m_jPanEntries, !value);
		if (!value) {
			m_jKeyFactory.requestFocusInWindow();
		}
	}

	private void enableComponents(Container cont, boolean value) {
		for (Component c : cont.getComponents()) {
			try {
				c.setEnabled(value);
				enableComponents((Container) c, value);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	protected void buttonTransition(ProductInfoExt prod) {
		buttonTransition(prod, 1.0D);
	}

	protected void buttonTransition(ProductInfoExt prod, double qty) {
		// precondicion: prod != null
		m_bIsSubproduct = false;

		// Check if picking composition content
		if (m_iProduct == PRODUCT_SUBGROUP && prod != null) {
			// Set component product price to 0
			prod.setCom(true);
			prod.setPriceSell(0.0);
			m_bIsSubproduct = true;
			// Set quantity to the same amount as composition product
			if (m_dMultiply != 1.0) {
				m_jPrice.setText(String.valueOf(m_dMultiply));
			}
		} else if (m_iProduct == PRODUCT_COMPOSITION) {
			// Picked a composition product, start composing
			// Set multiply for components
			m_dMultiply = (getInputValue() == 0) ? qty : getInputValue();
			m_iProduct = PRODUCT_SUBGROUP;
		}

		if (m_PriceActualState == N_NOTHING && m_QuantityActualState == N_NOTHING) {
			// No input, just add product
			incProduct(prod, qty, null);
		} else if (m_PriceActualState != N_NOTHING && m_PriceActualState != N_ZERO && m_PriceActualState != N_DECIMALZERO) {
			// Price input not empty
			incProduct(getInputValue(), prod, null);
		} else {
			Toolkit.getDefaultToolkit().beep();
		}
	}

	/** Returns the automate to its default state */
	public void eraseAutomator() {
		m_InputState = I_PRICE;
		m_PriceActualState = N_NOTHING;
		m_PricePreviousState = N_NOTHING;
		m_QuantityActualState = N_NOTHING;
		m_QuantityPreviousState = N_NOTHING;
		m_sBarcode = new StringBuffer();

		m_jPrice.setText("");
		m_jPor.setText("");
	}

	public boolean validCustomer(String sCode) {
		String note = null;
		String useCode = sCode;
		int notePos = sCode.indexOf('!');
		if(notePos > 0) {
			note = sCode.substring(notePos +1);
			useCode = sCode.substring(0, notePos);
		}
		if (useCode.startsWith("C")) {
			useCode = "c".concat(useCode.substring(1));
		}
		
		if (useCode.startsWith("c")) {
			// barcode of a customers card
			try {
				CustomerInfoExt newcustomer = this.dlCustomers.getCustomerByCard(useCode);
				if (newcustomer == null) {
					Toolkit.getDefaultToolkit().beep();
					new MessageInf(MessageInf.SGN_WARNING, AppLocal.getIntString("message.nocustomer")).show(this);
				} else {
					m_oTicket.setCustomer(newcustomer);
					m_jTicketId.setText(m_oTicket.getName(m_oTicketExt));
					if (newcustomer != null) {
						// Show prepaid and debt in message box
						this.messageBox.setText(newcustomer.printCustInfo());
						// Refresh customer from server
						dlCustomers.updateCustomer(newcustomer.getId(), this);
					}
					if (note != null) {
						this.m_oTicket.setNote(note);
						refreshTicket();
					}
				}
			} catch (BasicException e) {
				Toolkit.getDefaultToolkit().beep();
				new MessageInf(MessageInf.SGN_WARNING, AppLocal.getIntString("message.nocustomer"), e).show(this);
			}
			eraseAutomator();
		}

		return (!useCode.startsWith("c"));
	}
	
	public boolean validRebate(String sCode) {
		if (sCode.startsWith("p") || sCode.startsWith("P")) {
			// A rebate for the whole ticket
			String note = "";
			String useCode = sCode;
			int notePos = sCode.indexOf('!');
			if(notePos > 0) {
				note = sCode.substring(notePos +1);
				useCode = sCode.substring(0, notePos);
			}
			try {
			double discountRate = Double.valueOf(useCode.substring(1)) / 100.0;
			DiscountProfile profile = new DiscountProfile(discountRate);
			
			discountActionPerformed(profile , note);
			} catch (NumberFormatException e) {
				Toolkit.getDefaultToolkit().beep();
				new MessageInf(MessageInf.SGN_WARNING, AppLocal.getIntString("message.notanumber"), e).show(this);
			}
			eraseAutomator();
		}
		return (!(sCode.startsWith("p")|| sCode.startsWith("P")));
	}
	
	public boolean validLineRebate(String sCode) {
		String note = null;
		String useCode = sCode;
		int notePos = sCode.indexOf('!');
		if(notePos > 0) {
			note = sCode.substring(notePos +1);
			useCode = sCode.substring(0, notePos);
		}
		if (sCode.startsWith("l") || sCode.startsWith("L")) {


			try {
			double discountRate = Double.valueOf(useCode.substring(1)) / 100.0;
			lineDiscountActionPerformed(discountRate,note);
			} catch (NumberFormatException e) {
				Toolkit.getDefaultToolkit().beep();
				new MessageInf(MessageInf.SGN_WARNING, AppLocal.getIntString("message.notanumber"), e).show(this);
			}
			eraseAutomator();
			
		}
		if (sCode.startsWith("e")|| sCode.startsWith("E")) {
			// barcode of a customers card
			
			try {
			double discountRate = Double.valueOf(useCode.substring(1));
			lineFixedDiscountActionPerformed(discountRate,note);
			} catch (NumberFormatException e) {
				Toolkit.getDefaultToolkit().beep();
				new MessageInf(MessageInf.SGN_WARNING, AppLocal.getIntString("message.notanumber"), e).show(this);
			}
			eraseAutomator();	
		}
		return (!(sCode.startsWith("l")|| sCode.startsWith("e") || sCode.startsWith("L")|| sCode.startsWith("E")));
	}
	
	public boolean validLineRename(String sCode) {
		String note = null;
		String useCode = sCode;
		int notePos = sCode.indexOf('!');
		if(notePos > 0) {
			note = sCode.substring(notePos +1);
			useCode = sCode.substring(0, notePos);
		}
		if (sCode.startsWith("n") || sCode.startsWith("N")) {

			useCode = useCode.substring(1);
			lineRenameActionPerformed(useCode,note);

			eraseAutomator();
			
		}

		return (!(sCode.startsWith("n")|| sCode.startsWith("N")));
	}
	
	public boolean validNote(String sCode) {
		if (sCode.startsWith("!")) {
			// A Note for the whole ticket

			String note = sCode.substring(1);
			this.m_oTicket.setNote(note);
			refreshTicket();

			eraseAutomator();
		}
		return (!(sCode.startsWith("!")));
	}

	public void usualValid(String sCode) {
		if (sCode.length() == 13 && sCode.startsWith("250")) {
			// barcode of the other machine
			ProductInfoExt oProduct = new ProductInfoExt(); // Es un
															// ticket
			oProduct.setReference(null); // para que no se grabe
			oProduct.setCode(sCode);
			oProduct.setName("Ticket " + sCode.substring(3, 7));
			oProduct.setPriceSell(Double.parseDouble(sCode.substring(7, 12)) / 100);
			oProduct.setTaxCategoryID(((TaxCategoryInfo) taxcategoriesmodel.getSelectedItem()).getID());
			// Se anade directamente una unidad con el precio y todo
			addTicketLine(oProduct, 1.0, includeTaxes(oProduct.getTaxCategoryID(), oProduct.getPriceSell()));
		} else if (sCode.length() == 13 && sCode.startsWith("210")) {
			// barcode of a weigth product
			incProductByCodePrice(sCode.substring(0, 7), Double.parseDouble(sCode.substring(7, 12)) / 100);
		} else {
			// EDU Au cas où il y aurait un intérêt a conserver l'usage du stringbuffer
			// on récupère la string traité que dans le cas qui nous intéresse
			double quantity = 1.0D;
			String note = null;
			int notePos = sCode.indexOf('!');
			if(notePos > 0) {
				note = sCode.substring(notePos +1);
				sCode = sCode.substring(0, notePos);
			}
			if (m_InputState == I_PRICE || m_InputState == I_QUANTITY) {
				sCode = m_jPrice.getText();
			}
			// EDU gérer le cas du multiplicateur
			if (m_InputState == I_QUANTITY) {
				quantity = this.getPorValue();
			}
			incProductByCode(sCode, quantity , note);
		}
	}

	/**
	 * Chooses what to do when a specific key is typed
	 *
	 * @param entered
	 *            contains the key typed
	 */
	public void automator(char entered) {
		// barcode when enter is typed
		if (entered == keyEnter) {
			if (m_sBarcode.length() > 0) {
				String sCode = m_sBarcode.toString();
				if (validCustomer(sCode) && validLineRebate(sCode) && validRebate(sCode) && validNote(sCode) && validLineRename(sCode)) {
					usualValid(sCode);
				}
			} else {
				Toolkit.getDefaultToolkit().beep();
			}
		}
		// Other character
		else {
			m_sBarcode.append(entered);

			// goes to the fonction processPrice if the key typed matches the
			// key condition and
			// if the input is made in quantity's label
			if ((entered == '0' || entered == '1' || entered == '2' || entered == '3' || entered == '4' || entered == '5' || entered == '6'
					|| entered == '7' || entered == '8' || entered == '9' || entered == '.' || entered == keyBack || entered == keyDel || entered == '/')
					&& (m_InputState == I_NOTHING || m_InputState == I_PRICE)) {
				m_InputState = I_PRICE;
				processPrice(entered);

				// if '*' is typed and nothing in price's label, sets the
				// price's label text to 0,
				// sets 'x' to quantity's label and goes to the quantity's label
			} else if (entered == '*' && (m_InputState == I_NOTHING || m_InputState == I_PRICE) && m_PriceActualState == N_NOTHING) {
				m_PriceActualState = N_ZERO;
				m_InputState = I_QUANTITY;
				m_QuantityActualState = N_NOTHING;
				m_jPrice.setText("0");
				m_jPor.setText("x");

				// if '*' is typed and something in price's label, sets 'x' to
				// quantity's label and goes to the quantity's label
			} else if (entered == '*' && m_InputState == I_PRICE && m_PriceActualState != N_NOTHING) {
				m_InputState = I_QUANTITY;
				m_QuantityActualState = N_NOTHING;
				m_jPor.setText("x");

				// goes to the fonction processQuantity if the key typed matches
				// the key condition and
				// if the input is made in quantity's label
			} else if ((entered == '0' || entered == '1' || entered == '2' || entered == '3' || entered == '4' || entered == '5'
					|| entered == '6' || entered == '7' || entered == '8' || entered == '9' || entered == '.' || entered == keyBack
					|| entered == keyDel || entered == '/')
					&& m_InputState == I_QUANTITY) {
				processQuantity(entered);

				// + without input: increment selected line quantity
			} else if (entered == '+'  &&  m_InputState != I_NOTE && m_PriceActualState == N_NOTHING && m_QuantityActualState == N_NOTHING) {
				int i = m_ticketlines.getSelectedIndex();
				if (i < 0) {
					Toolkit.getDefaultToolkit().beep();
				} else {
					TicketLineInfo newline = new TicketLineInfo(m_oTicket.getLine(i));
					// If it's a refund + button means one unit less
					if (m_oTicket.getTicketType() == TicketInfo.RECEIPT_REFUND) {
						newline.setMultiply(newline.getMultiply() - 1.0);
						if (newline.getMultiply() == 0.0) {
							removeTicketLine(i);
						} else {
							newline.updateParent(m_oTicket.getLine(i));
							paintTicketLine(i, newline);
						}
					} else {
						// add one unit to the selected line
						newline.setMultiply(newline.getMultiply() + 1.0);
						if (newline.getMultiply() == 0.0) {
							removeTicketLine(i); // elimino la linea
						} else {
							newline.updateParent(m_oTicket.getLine(i));
							paintTicketLine(i, newline);
						}
					}
				}

				// - without input: decrement selected line quantity
				// Remove line if quantity is set to 0
			} else if (entered == '-'  &&  m_InputState != I_NOTE && m_PriceActualState == N_NOTHING && m_QuantityActualState == N_NOTHING
					&& m_App.getAppUserView().getUser().hasPermission("sales.EditLines")) {

				int i = m_ticketlines.getSelectedIndex();
				if (i < 0) {
					Toolkit.getDefaultToolkit().beep();
				} else {
					TicketLineInfo newline = new TicketLineInfo(m_oTicket.getLine(i));
					// If it's a refund - button means one unit more
					if (m_oTicket.getTicketType() == TicketInfo.RECEIPT_REFUND) {
						newline.setMultiply(newline.getMultiply() + 1.0);
						if (newline.getMultiply() == 0.0) {
							removeTicketLine(i);
						} else {
							newline.updateParent(m_oTicket.getLine(i));
							paintTicketLine(i, newline);
						}
					} else {
						// substract one unit to the selected line
						newline.setMultiply(newline.getMultiply() - 1.0);
						if (newline.getMultiply() == 0.0) {
							removeTicketLine(i); // elimino la linea
						} else {
							newline.updateParent(m_oTicket.getLine(i));
							paintTicketLine(i, newline);
						}
					}
				}

				// + with multiply input (without price): replace quantity
			} else if (entered == '+'  &&  m_InputState != I_NOTE && (m_PriceActualState == N_NOTHING || m_PriceActualState == N_ZERO)
					&& m_QuantityActualState != N_NOTHING && m_QuantityActualState != N_ZERO && m_QuantityActualState != N_DECIMALZERO
					&& m_InputState == I_QUANTITY) {
				int i = m_ticketlines.getSelectedIndex();
				if (i < 0) {
					Toolkit.getDefaultToolkit().beep();
				} else {
					double dPor = getPorValue();
					TicketLineInfo newline = new TicketLineInfo(m_oTicket.getLine(i));
					if (m_oTicket.getTicketType() == TicketInfo.RECEIPT_REFUND) {
						newline.setMultiply(-dPor);
						newline.setPrice(Math.abs(newline.getPrice()));
						newline.updateParent(m_oTicket.getLine(i));
						paintTicketLine(i, newline);
					} else {
						newline.setMultiply(dPor);
						newline.setPrice(Math.abs(newline.getFullPrice()));
						newline.updateParent(m_oTicket.getLine(i));
						paintTicketLine(i, newline);
					}
				}
				// - with multiply input (without price): set negative quantity
			} else if (entered == '-'  &&  m_InputState != I_NOTE && (m_PriceActualState == N_NOTHING || m_PriceActualState == N_ZERO)
					&& m_QuantityActualState != N_NOTHING && m_QuantityActualState != N_ZERO && m_QuantityActualState != N_DECIMALZERO
					&& m_InputState == I_QUANTITY && m_App.getAppUserView().getUser().hasPermission("sales.EditLines")) {
				int i = m_ticketlines.getSelectedIndex();
				if (i < 0) {
					Toolkit.getDefaultToolkit().beep();
				} else {
					double dPor = getPorValue();
					TicketLineInfo newline = new TicketLineInfo(m_oTicket.getLine(i));
					if (m_oTicket.getTicketType() == TicketInfo.RECEIPT_NORMAL) {
						newline.setMultiply(-dPor);
						newline.updateParent(m_oTicket.getLine(i));
						paintTicketLine(i, newline);
					}
				}

				// + with price input (without multiply): create an empty line
				// with entered price
			} else if (entered == '+'  &&  m_InputState != I_NOTE && m_PriceActualState != N_NOTHING && m_PriceActualState != N_ZERO
					&& m_PriceActualState != N_DECIMALZERO && m_QuantityActualState == N_NOTHING
					&& m_App.getAppUserView().getUser().hasPermission("sales.EditLines")) {
				ProductInfoExt product = getInputProduct();
				addTicketLine(product, 1.0, product.getPriceSell());

				// - with price input (without multiply): create an empty line
				// with negative entered price
			} else if (entered == '-'  &&  m_InputState != I_NOTE && m_PriceActualState != N_NOTHING && m_PriceActualState != N_ZERO
					&& m_PriceActualState != N_DECIMALZERO && m_QuantityActualState == N_NOTHING
					&& m_App.getAppUserView().getUser().hasPermission("sales.EditLines")) {
				ProductInfoExt product = getInputProduct();
				addTicketLine(product, 1.0, -product.getPriceSell());

				// + with price and multiply: create an empty line with entered
				// price
				// and quantity set to entered multiply
			} else if (entered == '+'  &&  m_InputState != I_NOTE && m_PriceActualState != N_NOTHING && m_PriceActualState != N_ZERO
					&& m_PriceActualState != N_DECIMALZERO && m_QuantityActualState != N_NOTHING && m_QuantityActualState != N_ZERO
					&& m_PriceActualState != N_DECIMALZERO && m_App.getAppUserView().getUser().hasPermission("sales.EditLines")) {
				ProductInfoExt product = getInputProduct();
				addTicketLine(product, getPorValue(), product.getPriceSell());

				// - with price and multiply: create an empty line with entered
				// negative price and quantity set to entered multiply
			} else if (entered == '-'  &&  m_InputState != I_NOTE && m_PriceActualState != N_NOTHING && m_PriceActualState != N_ZERO
					&& m_PriceActualState != N_DECIMALZERO && m_QuantityActualState != N_NOTHING && m_QuantityActualState != N_ZERO
					&& m_PriceActualState != N_DECIMALZERO && m_App.getAppUserView().getUser().hasPermission("sales.EditLines")) {
				ProductInfoExt product = getInputProduct();
				addTicketLine(product, getPorValue(), -product.getPriceSell());

				// Space bar or = : go to payments
			} else if ((entered == ' ' || entered == '=') &&  m_InputState != I_NOTE) {
				if (m_oTicket.getLinesCount() > 0) {
					if (closeTicket(m_oTicket, m_oTicketExt)) {
						// Ends edition of current receipt
						m_ticketsbag.deleteTicket();
					} else {
						// repaint current ticket
						refreshTicket();
					}
				} else {
					Toolkit.getDefaultToolkit().beep();
				}
				// Scale button pressed and a number typed as a price
				// c ou C et nous sommes sur le premier char -> saisie client
				//on evite d'interpreter quoi que ce soit jusqu'à la touche enter
			}  else if ((entered == 'c' || entered == 'C') && m_sBarcode.length() == 1) {			
				m_InputState = I_CUSTOMER;
				processPrice(entered);
				// % et nous sommes sur le premier char -> saisie remise ticket en %
				//on evite d'interpreter quoi que ce soit jusqu'à la touche enter
			}  else if ((entered == 'p'  || entered == 'P') && m_sBarcode.length() == 1) {
				m_InputState = I_REBATE;
				processPrice(entered);
				// l et nous sommes sur le premier char -> saisie remise en % à la ligne
				//on evite d'interpreter quoi que ce soit jusqu'à la touche enter
			}  else if ((entered == 'l' || entered == 'L') && m_sBarcode.length() == 1) {
				m_InputState = I_LINE_REBATE;
				processPrice(entered);		
				// e et nous sommes sur le premier char -> saisie remise en € à la ligne
				//on evite d'interpreter quoi que ce soit jusqu'à la touche enter
			}  else if ((entered == 'e' || entered == 'E') && m_sBarcode.length() == 1) {
				m_InputState = I_LINE_REBATE;
				processPrice(entered);
			}  else if ((entered == 'n' || entered == 'N') && m_sBarcode.length() == 1) {
				m_InputState = I_NOTE;
				processPrice(entered);
			}  else if (entered == '!' ) {
				m_InputState = I_NOTE;
				processPrice(entered);
			}else if( m_PriceActualState == N_ALPHA) {
				processPrice(entered);
			}
		}
	}

	/**
	 * Manages the automate's state for the price's label.
	 * 
	 * @author Loïc Dumont
	 * @since since 20/02/2013
	 * @param entered
	 *            contains the key typed
	 */
	public void processPrice(char entered) {
		// 0 stays 0
		// EDU PRévoir le cas où une ref commence par '0'
		// C'est assez confus - on poursuit néanmoins tel quel en ajoutant
		// les deux cas qui nous intéressent
		// TODO Refactor
		if ((entered == 'c' || entered == 'p' || entered == 'l'  || entered == 'e' || entered == 'C' || entered == 'P' || entered == 'L'  || entered == 'E' || entered == 'n' || entered == 'N') && (m_PriceActualState == N_NOTHING)) {
			m_PriceActualState = N_ALPHA;
			m_jPrice.setText(String.valueOf(entered));
		} else if (entered == keyDel || entered == '/') {
			eraseAutomator(); // sets the automate to its default state

			// erases numbers one by one
		} else if (entered == keyBack) {
			String price = m_jPrice.getText();
			int back = price.length() - 1;
			if (back - 1 == price.indexOf('.') && m_PriceActualState == N_DECIMAL) {
				m_PriceActualState = m_PricePreviousState;
			} else if (m_PriceActualState == N_DECIMALZERO) {
				m_PriceActualState = N_ZERO;
			} else if (m_PriceActualState == N_DECIMALNUMBER) {
				m_PriceActualState = N_NUMBER;
			} else if (m_PriceActualState == N_ZERO) {
				m_PriceActualState = N_NOTHING;
				eraseAutomator(); // sets the automate to its default state
			} else if (back == 0 && m_PriceActualState == N_NUMBER) {
				m_PriceActualState = N_NOTHING;
				eraseAutomator(); // sets the automate to its default state
			}
			if (back > 0) {
				price = price.substring(0, back);
				m_jPrice.setText(price);
			} else if (back < 0) {
				eraseAutomator(); // sets the automate to its default state
			}
		} else if (m_PriceActualState == N_ALPHA) {
			m_jPrice.setText(m_jPrice.getText() + entered);
		} else if (entered == '0' && (m_PriceActualState == N_NOTHING)) {
			m_PriceActualState = N_ZERO;
			m_jPrice.setText("0");
		} else if (entered == '0' && (m_PriceActualState == N_ZERO)) {
			m_jPrice.setText(m_jPrice.getText() + entered);
		} else if ((entered == '1' || entered == '2' || entered == '3' || entered == '4' || entered == '5' || entered == '6'
				|| entered == '7' || entered == '8' || entered == '9')
				&& (m_PriceActualState == N_NOTHING)) {
			m_jPrice.setText(Character.toString(entered));
			m_PriceActualState = N_NUMBER;
		} else if ((entered == '1' || entered == '2' || entered == '3' || entered == '4' || entered == '5' || entered == '6'
				|| entered == '7' || entered == '8' || entered == '9')
				&& (m_PriceActualState == N_ZERO)) {
			m_jPrice.setText(m_jPrice.getText() + entered);
			m_PriceActualState = N_NUMBER;
		} else if ((entered == '0' || entered == '1' || entered == '2' || entered == '3' || entered == '4' || entered == '5'
				|| entered == '6' || entered == '7' || entered == '8' || entered == '9')
				&& (m_PriceActualState == N_NUMBER || m_PriceActualState == N_DECIMAL)) {
			m_jPrice.setText(m_jPrice.getText() + entered);
		} else if ((entered == '0' || entered == '1' || entered == '2' || entered == '3' || entered == '4' || entered == '5'
				|| entered == '6' || entered == '7' || entered == '8' || entered == '9')
				&& (m_PriceActualState == N_DECIMALZERO)) {
			m_jPrice.setText(m_jPrice.getText() + entered);
			m_PriceActualState = N_DECIMAL;
			m_PricePreviousState = N_DECIMALZERO;
		} else if ((entered == '0' || entered == '1' || entered == '2' || entered == '3' || entered == '4' || entered == '5'
				|| entered == '6' || entered == '7' || entered == '8' || entered == '9')
				&& (m_PriceActualState == N_DECIMALNUMBER)) {
			m_jPrice.setText(m_jPrice.getText() + entered);
			m_PriceActualState = N_DECIMAL;
			m_PricePreviousState = N_DECIMALNUMBER;
		} else if (entered == '.' && m_PriceActualState == N_NOTHING) {
			m_jPrice.setText("0.");
			m_PriceActualState = N_DECIMALZERO;
			m_PricePreviousState = N_ZERO;
		} else if (entered == '.' && (m_PriceActualState == N_ZERO || m_PriceActualState == N_NUMBER)) {
			m_jPrice.setText(m_jPrice.getText() + ".");
			if (m_PriceActualState == N_ZERO) {
				m_PriceActualState = N_DECIMALZERO;
				m_PricePreviousState = N_ZERO;
			} else if (m_PriceActualState == N_NUMBER) {
				m_PriceActualState = N_DECIMALNUMBER;
				m_PricePreviousState = N_NUMBER;
			}

			// erase all numbers
		} 
	}

	/**
	 * Manages the automate's state for the quantity's label.
	 * 
	 * @author Loïc Dumont
	 * @since since 20/02/2013
	 * @param entered
	 *            contains the key typed
	 */
	public void processQuantity(char entered) {
		// 0 stays 0
		if (entered == '0' && (m_QuantityActualState == N_NOTHING || m_QuantityActualState == N_ZERO)) {
			m_jPor.setText("x0");
			m_QuantityActualState = N_ZERO;
		} else if ((entered == '1' || entered == '2' || entered == '3' || entered == '4' || entered == '5' || entered == '6'
				|| entered == '7' || entered == '8' || entered == '9')
				&& (m_QuantityActualState == N_NOTHING || m_QuantityActualState == N_ZERO)) {
			m_jPor.setText("x" + Character.toString(entered));
			m_QuantityActualState = N_NUMBER;
		} else if ((entered == '0' || entered == '1' || entered == '2' || entered == '3' || entered == '4' || entered == '5'
				|| entered == '6' || entered == '7' || entered == '8' || entered == '9')
				&& (m_QuantityActualState == N_NUMBER || m_QuantityActualState == N_DECIMAL)) {
			m_jPor.setText(m_jPor.getText() + entered);
		} else if ((entered == '0' || entered == '1' || entered == '2' || entered == '3' || entered == '4' || entered == '5'
				|| entered == '6' || entered == '7' || entered == '8' || entered == '9')
				&& (m_QuantityActualState == N_DECIMALZERO)) {
			m_jPor.setText(m_jPor.getText() + entered);
			m_QuantityActualState = N_DECIMAL;
			m_QuantityPreviousState = N_DECIMALZERO;
		} else if ((entered == '0' || entered == '1' || entered == '2' || entered == '3' || entered == '4' || entered == '5'
				|| entered == '6' || entered == '7' || entered == '8' || entered == '9')
				&& (m_QuantityActualState == N_DECIMALNUMBER)) {
			m_jPor.setText(m_jPor.getText() + entered);
			m_QuantityActualState = N_DECIMAL;
			m_QuantityPreviousState = N_DECIMALNUMBER;
		} else if (entered == '.' && m_QuantityActualState == N_NOTHING) {
			m_jPor.setText("x0.");
			m_QuantityActualState = N_DECIMALZERO;
			m_QuantityPreviousState = N_ZERO;
		} else if (entered == '.' && (m_QuantityActualState == N_ZERO || m_QuantityActualState == N_NUMBER)) {
			m_jPor.setText(m_jPor.getText() + ".");
			if (m_QuantityActualState == N_ZERO) {
				m_QuantityActualState = N_DECIMALZERO;
				m_QuantityPreviousState = N_ZERO;
			} else if (m_QuantityActualState == N_NUMBER) {
				m_QuantityActualState = N_DECIMALNUMBER;
				m_QuantityPreviousState = N_NUMBER;
			}

			// erases all numbers
		} else if (entered == keyDel || entered == '/') {
			m_QuantityActualState = N_NOTHING;
			m_InputState = I_PRICE;
			m_jPor.setText("");

			// erases numbers one by one
		} else if (entered == keyBack) {
			String quantity = m_jPor.getText();
			int back = quantity.length() - 1;
			if (back - 1 == quantity.indexOf('.') && m_QuantityActualState == N_DECIMAL) {
				m_QuantityActualState = m_QuantityPreviousState;
			} else if (m_QuantityActualState == N_DECIMALZERO) {
				m_QuantityActualState = N_ZERO;
			} else if (m_QuantityActualState == N_DECIMALNUMBER) {
				m_QuantityActualState = N_NUMBER;
			} else if (back == 1 && m_QuantityActualState == N_ZERO) {
				m_QuantityActualState = N_NOTHING;
			} else if (back == 1 && m_QuantityActualState == N_NUMBER) {
				m_QuantityActualState = N_NOTHING;
			} else if (back == 0 && m_QuantityActualState == N_NOTHING) {
				m_InputState = I_PRICE;
			}
			if (back >= 0) {
				quantity = quantity.substring(0, back);
				m_jPor.setText(quantity);
			}
		}
	}

	private boolean closeTicket(TicketInfo ticket, Object ticketext) {

		boolean resultok = false , increment = false;

		if (m_App.getAppUserView().getUser().hasPermission("sales.Total")) {

			try {
				// reset the payment info
				taxeslogic.calculateTaxes(ticket);
				if (ticket.getTotal() >= 0.0) {
					ticket.resetPayments(); // Only reset if is sale
				}
				ticket.updateDiscountRate();
				if (executeEvent(ticket, ticketext, "ticket.total") == null) {

					// Muestro el total
					printTicket("Printer.TicketTotal", ticket, ticketext);

					// Select the Payments information
					JPaymentSelect paymentdialog = ticket.getTicketType() == TicketInfo.RECEIPT_NORMAL ? paymentdialogreceipt
							: paymentdialogrefund;
					String printSelectedConfigBtn = m_jbtnconfig.getProperty("printselected");
					boolean printSelected;
					if (printSelectedConfigBtn != null) {
						printSelected = printSelectedConfigBtn.equals("true");
					} else {
						printSelected = AppConfig.loadedInstance.getProperty("ui.printticketbydefault").equals("1");
					}
					paymentdialog.setPrintSelected(printSelected);

					paymentdialog.setTransactionID(ticket.getTransactionID());

					// TODO EDU C'est ici qu'on s'assure qu'on dispose du code postal / insee en fait
					// Si le customer en a un on le prend
					// Sinon on ouvre une boite de dialogue pour demander
					// On met éventuellement à jour le client ?
					if (ticket.getInseeNum() == null) {
						ticket.setInseeNum(ticket.getCustomerInsee());
						if (ticket.getInseeNum() == null) {
							// EDU Pas de client ou pas de quoi remonter à l'INSEE
							// On pose la question
							// TODO EDU : enlever la liste de 8 villes en dur ainsi que le rayon de recherche par défaut à 50Kms
							ticket.setInseeNum(new JZipSelect(this.getLocationInsee(), 8, 50).showDialog());
							// On monte une fenêtre de dialogue modale
							// basée sur le num insee de la vente pour la préselection
							// plus une fenêtre de saisie manuelle pour ajuster
							// Avec auto ajustement de la liste
						}
					}

					if (paymentdialog.showDialog(ticket.getTotal(), ticket.getCustomer(), ticket.HasOrders() ||
							ticket.isDiscounted())) {

						// assign the payments selected and calculate taxes.
						ticket.setPayments(paymentdialog.getSelectedPayments());

						// Asigno los valores definitivos del ticket...
						ticket.setUser(m_App.getAppUserView().getUser().getUserInfo()); // El
																						// usuario
																						// que
																						// lo
																						// cobra
						ticket.setActiveCash(m_App.getActiveCashIndex());

						// TODO - S'assurer que nous souhaitons que date new reflete la date de dernière édition
						ticket.setDate(new Date()); // Le pongo la fecha de
						// cobro

						//EDU 2021 nous sommes ici après validation d'un encaissement
						//Soit nous avons ticket.getTicketId() == 0 -> nous sommes sur un ticket normal, on lui attribue le prochain numéro
						//On incrémente notre numéro de ticket
						//Et c'est fini
						//Soit on a un numéro de ticket , nous sommes donc sur une modif
						//Dans ce cas : on lui attribue le prochain numéro
						//On incrémente le prochain numéro de ticket
						//On le fait une fois de plus ( ticket retour)						
						
						increment = ticket.getTicketId() != 0;
						ticket.setTicketId(m_App.getCashRegister().getNextTicketId());
						

						
						//TODO EDU Attention avant cette ligne était dans le bloc
						executeEvent(ticket, ticketext, "ticket.close", new ScriptArg("print", paymentdialog.isPrintSelected()));
						
						// Print checks
						int index = 0;
						for(PaymentInfo payment : ticket.getPayments()) {
							if(payment.getName().equals(PaymentInfo.Type.CHEQUE)) {
								
								
									Date echeance = payment.getEcheance() == null ? new Date() : payment.getEcheance() ;
							        DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.FRANCE);
							        otherSymbols.setDecimalSeparator(',');
							        DecimalFormat decf = new DecimalFormat("#.00", otherSymbols);
							        SimpleDateFormat hdf = new SimpleDateFormat("dd/MM/yyyy");
							        String dateEcheance = hdf.format(echeance);
									String value = decf.format(payment.getTotal());
									
									String lettersValue = convert2Letters(value.split(","));
									String date = hdf.format(new Date());
									index++;
								if(JDialogPrintCheck.getDialog(this, AppLocal.getIntString("label.printCheck"), 
										AppLocal.getIntString("message.printcheck" , index , value , dateEcheance), true).showDialog()) {	
									printCheck("Printer.check",date,dateEcheance,value,lettersValue,this.getLocationCity());
									
									while(m_TTP.waiting()) {
										
									}
									String CMC7 = m_TTP.getCMC7();
									if(!CMC7.isEmpty()) {
										payment.setTransactionID(CMC7);
									}
								}
							}
						}
						
						//On sort l'ensemble de bloc afin de pouvoir mettre à jour un eventuel CMC7
						
						if (executeEvent(ticket, ticketext, "ticket.save") == null) {
							// Save the receipt and assign a receipt number
							try {
								dlSales.saveTicket(ticket, m_App.getInventoryLocation(), m_App.getActiveCashSession().getId(), m_oTicket.getCustomer());
								// Refresh customer if any
								if (ticket.getCustomer() != null && !ticket.getCustomer().getLevelSearch().equals(2)) {
									dlCustomers.updateCustomer(ticket.getCustomer().getId(), null);
								}
							} catch (BasicException eData) {
								MessageInf msg = new MessageInf(MessageInf.SGN_NOTICE, AppLocal.getIntString("message.nosaveticket"), eData);
								msg.show(this);
							}



							// Print receipt or just cashIn.
							String sourceName = "Printer.Ticket" ;
							
							if( !paymentdialog.isPrintSelected() && ! paymentdialog.isPrintInvoiceSelected()) {
								sourceName = "Printer.Ticket2" ;
								printTicket(sourceName , ticket, ticketext);
							}
							for (int nbPrint = paymentdialog.getPrints() ; nbPrint-- > 0 && paymentdialog.isPrintSelected() ; ) {
								printTicket(sourceName , ticket, ticketext);
								sourceName = "Printer.TicketPreview";
							}
							
							//Print Invoice if needed
							sourceName = "Printer.Invoice" ;
							for (int nbPrint = paymentdialog.getPrints() ; nbPrint-- > 0 && paymentdialog.isPrintInvoiceSelected() ; ) {
								
								printTicket(sourceName , ticket, ticketext);
								sourceName = "Printer.InvoicePreview" ;
							}
							
							resultok = true;

							// Update message box
							boolean msgBoxUpdated = false;
							for (PaymentInfo p : paymentdialog.getSelectedPayments()) {
								if (p instanceof PaymentInfoCash) {
									this.messageBox
											.setText(AppLocal.getIntString("MsgBox.LastChange", ((PaymentInfoCash) p).printChange()));
									msgBoxUpdated = true;
								}
							}
							if (!msgBoxUpdated) {
								this.messageBox.setText("");
							}

							// Increment next ticket id
							m_App.getCashRegister().incrementNextTicketId();
							if(increment) {
								m_App.getCashRegister().incrementNextTicketId();
							}
						}
						
					}
					ticket.setOpenedPaymentDialog(true);	
				}
			} catch (TaxesException e) {
				MessageInf msg = new MessageInf(MessageInf.SGN_WARNING, AppLocal.getIntString("message.cannotcalculatetaxes"));
				msg.show(this);
				resultok = false;
			}

			// reset the payment info
			m_oTicket.resetTaxes();
			m_oTicket.resetPayments();
		}

		// cancelled the ticket.total script
		// or canceled the payment dialog
		// or canceled the ticket.close script
		return resultok;
	}

	private void printCheck(String sresourcename, String date, String dateEcheance, String value, String lettersValue, String place) {

		String sresource = dlSystem.getResourceAsXML(sresourcename);

		if (sresource == null) {
			MessageInf msg = new MessageInf(MessageInf.SGN_WARNING, AppLocal.getIntString("message.cannotprintCheck"));
			msg.show(JPanelTicket.this);
		} else {
			try {
				ScriptEngine script = ScriptFactory.getScriptEngine(ScriptFactory.VELOCITY);
				script.put("date", date);
				script.put("dateecheance", dateEcheance);
				script.put("value", value);
				script.put("lettersvalue", lettersValue);
				script.put("place", place);
				script.put("stringutils", new StringUtils());
				m_TTP.printTicket(script.eval(sresource).toString());
			} catch (ScriptException e) {
				MessageInf msg = new MessageInf(MessageInf.SGN_WARNING, AppLocal.getIntString("message.cannotprintcheck"), e);
				msg.show(JPanelTicket.this);
			} catch (TicketPrinterException e) {
				MessageInf msg = new MessageInf(MessageInf.SGN_WARNING, AppLocal.getIntString("message.cannotprintcheck"), e);
				msg.show(JPanelTicket.this);
			}
		}
	}

	//EDU Metode pour obtenir une version en toute lettres d'un nombre entier
	//TODO : replacer au bon endroit et ouvrir la possibilité d'internationaliser
	public static String convert2Letters(String[] strings) {
		int mainValue = Integer.parseInt(strings[0]);
		int centsValue = Integer.parseInt(strings[1]);
		String retour ;
		retour = convertiInt(mainValue);
		retour = retour.concat("euro");
		if(mainValue > 1) {
			retour = retour.concat("s");
		}
		retour = retour.concat(" et ");
		retour = retour.concat(convertiInt(centsValue));
		retour = retour.concat("centime");
		if(centsValue > 1) {
			retour = retour.concat("s");
		}
		return retour;
	}

	private static String convertiInt(int chiffre) {
		String lettre="";
		int centaine, dizaine, unite, reste, y;
		boolean dix = false;
		reste = chiffre / 1;
 
		for(int i=1000000000; i>=1; i/=1000)
		{
			y = reste/i;
		    if(y!=0)
		    	{
		            centaine = y/100;
		            dizaine  = (y - centaine*100)/10;
		            unite = y-(centaine*100)-(dizaine*10);
		            switch(centaine)
		            {
		                case 0:
		                    break;
		                case 1:
		                    lettre += "cent ";
		                    break;
		                case 2:
		                    if((dizaine == 0)&&(unite == 0)) lettre +="deux cents ";
		                    else lettre +="deux cent ";
		                    break;
		                case 3:
		                    if((dizaine == 0)&&(unite == 0)) lettre += "trois cents ";
		                    else lettre += "trois cent ";
		                    break;
		                case 4:
		                    if((dizaine == 0)&&(unite == 0)) lettre+="quatre cents ";
		                    else lettre +="quatre cent ";
		                    break;
		                case 5:
		                    if((dizaine == 0)&&(unite == 0)) lettre+="cinq cents ";
		                    else lettre+="cinq cent ";
		                    break;
		                case 6:
		                    if((dizaine == 0)&&(unite == 0)) lettre+="six cents ";
		                    else lettre += "six cent ";
		                    break;
		                case 7:
		                    if((dizaine == 0)&&(unite == 0)) lettre+="sept cents ";
		                    else lettre+="sept cent ";
		                    break;
		                case 8:
		                    if((dizaine == 0)&&(unite == 0)) lettre+="huit cents ";
		                    else lettre+="huit cent ";
		                    break;
		                case 9:
		                    if((dizaine == 0)&&(unite == 0)) lettre+="neuf cents ";
		                    else lettre+="neuf cent ";
		                    break;
		            }// endSwitch(centaine)
 
		            switch(dizaine)
		            {
		                case 0:
		                    break;
		                case 1:
		                    dix = true;
		                    break;
		                case 2:
		                    lettre+="vingt ";
		                    break;
		                case 3:
		                    lettre+="trente ";
		                    break;
		                case 4:
		                    lettre+="quarante ";
		                    break;
		                case 5:
		                    lettre+="cinquante ";
		                    break;
		                case 6:
		                    lettre+="soixante ";
		                    break;
		                case 7:
		                    dix = true;
		                    lettre+="soixante ";
		                    break;
		                case 8:
		                    lettre+="quatre-vingt ";
		                    break;
		                case 9:
		                    dix = true;
		                    lettre+="quatre-vingt ";
		                    break;
		            } // endSwitch(dizaine)
 
		            switch(unite)
		            {
		                case 0:
		                    if(dix) lettre+="dix ";
		                    break;
		                case 1:
		                    if(dix) lettre+="onze ";
		                    else    lettre+="un ";
		                    break;
		                case 2:
		                    if(dix) lettre+="douze ";
		                    else    lettre+="deux ";
		                    break;
		                case 3:
		                    if(dix) lettre+="treize ";
		                    else    lettre+="trois ";
		                    break;
		                case 4:
		                    if(dix) lettre+="quatorze ";
		                    else    lettre+="quatre ";
		                    break;
		                case 5:
		                    if(dix) lettre+="quinze ";
		                    else    lettre+="cinq ";
		                    break;
		                case 6:
		                    if(dix) lettre+="seize ";
		                    else    lettre+="six ";
		                    break;
		                case 7:
		                    if(dix) lettre+="dix-sept ";
		                    else    lettre+="sept ";
		                    break;
		                case 8:
		                    if(dix) lettre+="dix-huit ";
		                    else    lettre+="huit ";
		                    break;
		                case 9:
		                    if(dix) lettre+="dix-neuf ";
		                    else    lettre+="neuf ";
		                    break;
		            } // endSwitch(unite)
 
		            switch (i)
		            {
		                case 1000000000:
		                    if(y>1) lettre+="milliards ";
		                    else lettre+="milliard ";
		                    break;
		                case 1000000:
		                    if(y>1) lettre+="millions ";
		                    else lettre+="million ";
		                    break;
		                case 1000:
		                    lettre+="mille ";
		                    break;
		            }
		        } // end if(y!=0)
		        reste -= y*i;
		        dix = false;
		    } // end for
		    if(lettre.isEmpty()) lettre+="zero "; 
		   
		return lettre;
	}

	private void printTicket(String sresourcename, TicketInfo ticket, Object ticketext) {

		String sresource = dlSystem.getResourceAsXML(sresourcename);
		try {
			taxeslogic.calculateTaxes(ticket);
		} catch (TaxesException e) {
			e.printStackTrace();
		}
		if (sresource == null) {
			MessageInf msg = new MessageInf(MessageInf.SGN_WARNING, AppLocal.getIntString("message.cannotprintticket"));
			msg.show(JPanelTicket.this);
		} else {
			try {
				ScriptEngine script = ScriptFactory.getScriptEngine(ScriptFactory.VELOCITY);
				script.put("taxes", taxcollection);
				script.put("taxeslogic", taxeslogic);
				script.put("ticket", ticket);
				script.put("place", ticketext);
				m_TTP.printTicket(script.eval(sresource).toString());
			} catch (ScriptException e) {
				MessageInf msg = new MessageInf(MessageInf.SGN_WARNING, AppLocal.getIntString("message.cannotprintticket"), e);
				msg.show(JPanelTicket.this);
			} catch (TicketPrinterException e) {
				MessageInf msg = new MessageInf(MessageInf.SGN_WARNING, AppLocal.getIntString("message.cannotprintticket"), e);
				msg.show(JPanelTicket.this);
			}
		}
	}

	private void printReport(String resourcefile, TicketInfo ticket, Object ticketext) {

		try {

			JasperReport jr;

			InputStream in = getClass().getResourceAsStream(resourcefile + ".ser");
			if (in == null) {
				// read and compile the report
				JasperDesign jd = JRXmlLoader.load(getClass().getResourceAsStream(resourcefile + ".jrxml"));
				jr = JasperCompileManager.compileReport(jd);
			} else {
				// read the compiled reporte
				ObjectInputStream oin = new ObjectInputStream(in);
				jr = (JasperReport) oin.readObject();
				oin.close();
			}

			// Construyo el mapa de los parametros.
			Map<String, Object> reportparams = new HashMap<String, Object>();
			// reportparams.put("ARG", params);
			try {
				reportparams.put("REPORT_RESOURCE_BUNDLE", ResourceBundle.getBundle(resourcefile + ".properties"));
			} catch (MissingResourceException e) {
			}
			reportparams.put("TAXESLOGIC", taxeslogic);

			Map<String, Object> reportfields = new HashMap<String, Object>();
			reportfields.put("TICKET", ticket);
			reportfields.put("PLACE", ticketext);

			JasperPrint jp = JasperFillManager.fillReport(jr, reportparams, new JRMapArrayDataSource(new Object[] { reportfields }));

			PrintService service = ReportUtils.getPrintService(m_App.getProperties().getProperty("machine.printername"));

			JRPrinterAWT300.printPages(jp, 0, jp.getPages().size() - 1, service);

		} catch (Exception e) {
			MessageInf msg = new MessageInf(MessageInf.SGN_WARNING, AppLocal.getIntString("message.cannotloadreport"), e);
			msg.show(this);
		}
	}

	private void visorTicketLine(TicketLineInfo oLine) {
		if (oLine == null) {
			m_App.getDeviceTicket().getDeviceDisplay().clearVisor();
		} else {
			try {
				ScriptEngine script = ScriptFactory.getScriptEngine(ScriptFactory.VELOCITY);
				script.put("ticketline", oLine);
				m_TTP.printTicket(script.eval(dlSystem.getResourceAsXML("Printer.TicketLine")).toString());
			} catch (ScriptException e) {
				MessageInf msg = new MessageInf(MessageInf.SGN_WARNING, AppLocal.getIntString("message.cannotprintline"), e);
				msg.show(JPanelTicket.this);
			} catch (TicketPrinterException e) {
				MessageInf msg = new MessageInf(MessageInf.SGN_WARNING, AppLocal.getIntString("message.cannotprintline"), e);
				msg.show(JPanelTicket.this);
			}
		}
	}

	private Object evalScript(ScriptObject scr, String resource, ScriptArg... args) {

		// resource here is guaratied to be not null
		try {
			scr.setSelectedIndex(m_ticketlines.getSelectedIndex());
			return scr.evalScript(dlSystem.getResourceAsXML(resource), args);
		} catch (ScriptException e) {
			MessageInf msg = new MessageInf(MessageInf.SGN_WARNING, AppLocal.getIntString("message.cannotexecute"), e);
			msg.show(this);
			return msg;
		}
	}

	public void evalScriptAndRefresh(String resource, ScriptArg... args) {

		if (resource == null) {
			MessageInf msg = new MessageInf(MessageInf.SGN_WARNING, AppLocal.getIntString("message.cannotexecute"));
			msg.show(this);
		} else {
			ScriptObject scr = new ScriptObject(m_oTicket, m_oTicketExt);
			scr.setSelectedIndex(m_ticketlines.getSelectedIndex());
			evalScript(scr, resource, args);
			refreshTicket();
			setSelectedIndex(scr.getSelectedIndex());
		}
	}

	public void printTicket(String resource) {
		printTicket(resource, m_oTicket, m_oTicketExt);
	}

	private Object executeEventAndRefresh(String eventkey, ScriptArg... args) {

		for (ScriptArg arg : args) 
		{
			if (arg.getKey().equals("index")) {
				int index = (int) arg.getValue();
				checkLines(index);
				updateOrderOfLinesTicket();
				// Sélectionner la derniére ligne ajouter
				m_ticketlines.setSelectedIndex(index);
			}
		}
		String resource = m_jbtnconfig.getEvent(eventkey);
		if (resource == null) {
			return null;
		} else {
			ScriptObject scr = new ScriptObject(m_oTicket, m_oTicketExt);
			scr.setSelectedIndex(m_ticketlines.getSelectedIndex());
			Object result = evalScript(scr, resource, args);
			refreshTicket();
			setSelectedIndex(scr.getSelectedIndex());
			return result;
		}
	}
	/**
	 *  ça permet de garder l'ordre d'ajout d'un ticketLine
	 */
	private void updateOrderOfLinesTicket() {
		
		Collections.sort(m_oTicket.getLines());
		
	    m_ticketlines.clearTicketLines();

		for (int i = 0; i < m_oTicket.getLinesCount(); i++) {
			m_ticketlines.addTicketLine(m_oTicket.getLine(i), m_App.getAppUserView().getUser());
		}
		
	}

	private Object executeEvent(TicketInfo ticket, Object ticketext, String eventkey, ScriptArg... args) {

		String resource = m_jbtnconfig.getEvent(eventkey);
		if (resource == null) {
			return null;
		} else {
			ScriptObject scr = new ScriptObject(ticket, ticketext);
			return evalScript(scr, resource, args);
		}
	}

	public String getResourceAsXML(String sresourcename) {
		return dlSystem.getResourceAsXML(sresourcename);
	}

	public BufferedImage getResourceAsImage(String sresourcename) {
		return dlSystem.getResourceAsImage(sresourcename);
	}

	private void setSelectedIndex(int i) {

		if (i >= m_oTicket.getLinesCount()) {
			i = m_oTicket.getLinesCount() - 1;
		}
		if (i >= 0 && i < m_oTicket.getLinesCount()) {
			m_ticketlines.setSelectedIndex(i);
		}
	}

	public static class ScriptArg {
		private String key;
		private Object value;

		public ScriptArg(String key, Object value) {
			this.key = key;
			this.value = value;
		}

		public String getKey() {
			return key;
		}

		public Object getValue() {
			return value;
		}
	}

	public class ScriptObject {

		private TicketInfo ticket;
		private Object ticketext;

		private int selectedindex;

		private ScriptObject(TicketInfo ticket, Object ticketext) {
			this.ticket = ticket;
			this.ticketext = ticketext;
		}

		public double getInputValue() {
			if (m_PriceActualState != N_NOTHING && m_PriceActualState != N_ZERO && m_QuantityActualState == N_NOTHING) {
				return JPanelTicket.this.getInputValue();
			} else {
				return 0.0;
			}
		}

		public int getSelectedIndex() {
			return selectedindex;
		}

		public void setSelectedIndex(int i) {
			selectedindex = i;
		}

		public void printReport(String resourcefile) {
			JPanelTicket.this.printReport(resourcefile, ticket, ticketext);
		}

		public void printTicket(String sresourcename) {
			JPanelTicket.this.printTicket(sresourcename, ticket, ticketext);
		}

		public Object evalScript(String code, ScriptArg... args) throws ScriptException {

			ScriptEngine script = ScriptFactory.getScriptEngine(ScriptFactory.BEANSHELL);
			script.put("ticket", ticket);
			script.put("place", ticketext);
			script.put("taxes", taxcollection);
			script.put("taxeslogic", taxeslogic);
			script.put("user", m_App.getAppUserView().getUser());
			script.put("sales", this);

			// more arguments
			for (ScriptArg arg : args) {
				script.put(arg.getKey(), arg.getValue());
			}

			return script.eval(code);
		}
	}

	protected class CatalogListener implements ActionListener {
		private void reloadCatalog() {
			changeCatalog();
			try {
				m_cat.loadCatalog();
			} catch (BasicException e) {
				e.printStackTrace();
			}
		}

		public void actionPerformed(ActionEvent e) {
			if ((e.getSource()).getClass().equals(ProductInfoExt.class)) {
				// Clicked on a product
				ProductInfoExt prod = ((ProductInfoExt) e.getSource());
				// Special command to end composition
				if (e.getActionCommand().equals("-1")) {
					m_iProduct = PRODUCT_SINGLE;
					reloadCatalog();
				} else {
					if (prod.getCategoryID().equals("0")) {
						// Clicked on a composition
						m_iProduct = PRODUCT_COMPOSITION;
						buttonTransition(prod);
						reloadCatalog();
						m_cat.showCatalogPanel(prod.getID());
					} else {
						// Clicked on a regular product
						buttonTransition(prod);
					}
				}
			} else {
				// Si se ha seleccionado cualquier otra cosa...
				// Si es una orden de cancelar la venta de una composición
				if (e.getActionCommand().equals("cancelSubgroupSale")) {
					int i = m_oTicket.getLinesCount();
					TicketLineInfo line = m_oTicket.getLine(--i);
					// Quito todas las líneas que son subproductos
					// (puesto que está recién añadido, pertenecen
					// al menú que estamos cancelando)
					while ((i > 0) && (line.isSubproduct())) {
						m_oTicket.removeLine(i);
						m_ticketlines.removeTicketLine(i);
						line = m_oTicket.getLine(--i);
					}
					// Quito la línea siguiente, perteneciente al menú en sí
					if (i >= 0) {
						m_oTicket.removeLine(i);
						m_ticketlines.removeTicketLine(i);
					}
					// Actualizo el interfaz
					m_iProduct = PRODUCT_SINGLE;
					reloadCatalog();
				}
			}
		}

	}

	protected class CatalogSelectionListener implements ListSelectionListener {
		public void valueChanged(ListSelectionEvent e) {

			if (!e.getValueIsAdjusting()) {
				int i = m_ticketlines.getSelectedIndex();

				// Buscamos el primer producto no Auxiliar.
				while (i >= 0 && m_oTicket.getLine(i).isProductCom()) {
					i--;
				}

				// Mostramos el panel de catalogo adecuado...
				if (i >= 0) {
					m_cat.showCatalogPanel(m_oTicket.getLine(i).getProductID());
					// TODO Ajuster les cas où on peut ou pas prendre en commande
					//m_jOrder.setEnabled(isOrderAvailable() && m_oTicket.getLine(i).isOrderAvailable());
					m_jOrder.setEnabled(m_oTicket.getLine(i).isOrderAvailable());
				} else {
					m_cat.showCatalogPanel(null);
				}
			}
		}
	}

	public void customerLoaded(CustomerInfoExt customer) {
		if (m_oTicket != null && m_oTicket.getCustomer() != null && customer != null
				&& m_oTicket.getCustomer().getId().equals(customer.getId())) {
			// Loading went well and the customer is still the one on the ticket
			logger.log(Level.INFO, "Customer refreshed from server.");
			m_oTicket.setCustomer(customer);
			// Refresh message box
			this.messageBox.setText(customer.printCustInfo());
		} else {
			logger.log(Level.INFO, "Customer refresh failed or customer changed.");
		}
	}

	private void initComponents() {
		AppConfig cfg = AppConfig.loadedInstance;
		int btnspacing = WidgetsBuilder.pixelSize(Float.parseFloat(cfg.getProperty("ui.touchbtnspacing")));
		
        jButtonNumPad = new javax.swing.JButton();
        numpadZone = new javax.swing.JPanel();


        jButtonNumPad.setFocusPainted(false);
        jButtonNumPad.setFocusable(false);
        jButtonNumPad.setMargin(new java.awt.Insets(2,2,2,2));
        jButtonNumPad.setRequestFocusEnabled(false);
        jButtonNumPad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonNumPadActionPerformed(evt);
            }
        });
		
            menu_open = ImageLoader.readImageIcon("menu-up.png");
            menu_close = ImageLoader.readImageIcon("menu-down.png");

        assignNumPadButtonIcon();    

		JPanel m_jPanContainer = new JPanel(); // The main container
		m_jButtonsExt = new JPanel();
		m_jPanelBag = new JPanel();
		lineBtnsContainer = new JPanel();
		m_jUp = WidgetsBuilder.createButton(ImageLoader.readImageIcon("button_up.png"), AppLocal.getIntString("Button.m_jUpSales.toolTip"));
		m_jDown = WidgetsBuilder.createButton(ImageLoader.readImageIcon("button_down.png"),
				AppLocal.getIntString("Button.m_jDownSales.toolTip"));
		m_jDelete = WidgetsBuilder.createButton(ImageLoader.readImageIcon("tkt_line_delete.png"),
				AppLocal.getIntString("Button.m_jDelete.toolTip"));
		m_jList = WidgetsBuilder.createButton(ImageLoader.readImageIcon("tkt_search.png"), AppLocal.getIntString("Button.m_jList.toolTip"));
		m_jEditLine = WidgetsBuilder.createButton(ImageLoader.readImageIcon("tkt_line_edit.png"),
				AppLocal.getIntString("Button.m_jEditLine.toolTip"));
		m_jbtnLineDiscount = WidgetsBuilder.createButton(ImageLoader.readImageIcon("tkt_line_discount.png"),
				AppLocal.getIntString("Button.m_jbtnLineDiscount.toolTip"));
		m_jbtnLineFixedDiscount = WidgetsBuilder.createButton(ImageLoader.readImageIcon("tkt_line_euro_discount.png"),
				AppLocal.getIntString("Button.m_jbtnLineDiscount.toolTip"));
		jEditAttributes = WidgetsBuilder.createButton(ImageLoader.readImageIcon("tkt_line_attr.png"),
				AppLocal.getIntString("Button.jEditAttributes.toolTip"));
		try {
			m_jOrder = WidgetsBuilder.createButton(
					new ImageIcon(ImageIO.read(ImageLoader.class.getResource("/fr/pasteque/images/order.png"))),
					AppLocal.getIntString("label.Command"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		m_jPanTotals = new JPanel();
		m_jTotalEuros = WidgetsBuilder.createImportantLabel();
		this.discountLabel = WidgetsBuilder.createLabel();
		this.subtotalLabel = WidgetsBuilder.createSmallLabel(AppLocal.getIntString("label.subtotalLine"));
		this.ticketInfoLabel = WidgetsBuilder.createImportantLabel(AppLocal.getIntString("label.ticketInfoLight"));
		m_jPanEntries = new JPanel();
		m_jNumberKeys = new JNumberKeys();
		jPanel9 = new JPanel();
		m_jPrice = WidgetsBuilder.createLabel();
		m_jPor = WidgetsBuilder.createLabel();
		m_jEnter = WidgetsBuilder.createButton(ImageLoader.readImageIcon("barcode.png"), AppLocal.getIntString("Button.m_jEnter.toolTip"));
		m_jTax = new JComboBox<TaxCategoryInfo>();
		m_jaddtax = new JToggleButton();
		m_jKeyFactory = new JTextField();
		catcontainer = new JPanel();
		m_jInputContainer = new JPanel();
		m_jTariff = WidgetsBuilder.createComboBox();
		/*
		 * Unused JLabel tariffLbl = WidgetsBuilder.createLabel(AppLocal.getIntString("Label.TariffArea"));
		 */

		this.setBackground(new Color(255, 204, 153));
		this.setLayout(new CardLayout());

		m_jPanContainer.setLayout(new GridBagLayout());
		GridBagConstraints cstr = null;

		// Main container is broken into 4 vertical parts
		// Brand header
		// Ticket header
		// Input
		// Footer

		// /////////////
		// Brand header
		// /////////////
		JPanel brandHeader = new JPanel();
		brandHeader.setLayout(new GridBagLayout());
		ImageIcon brand = ImageLoader.readImageIcon("logo_flat.png");
		JLabel brandLabel = new JLabel(brand);
		this.clock = WidgetsBuilder.createLabel("00:00");
		this.connectedUser = WidgetsBuilder.createLabel("-");
		this.messageBox = WidgetsBuilder.createTextArea();
		this.messageBox.setRequestFocusEnabled(false);
		this.messageBox.setFocusable(false);
		
		cstr = new GridBagConstraints();
		cstr.gridy = 0;
		cstr.gridheight = 2;
		brandHeader.add(brandLabel, cstr);
		cstr = new GridBagConstraints();
		cstr.gridy = 1;
		cstr.fill = GridBagConstraints.BOTH;
		cstr.weightx = 1.0;
		cstr.gridheight = 1;
		cstr.gridwidth = 2;
		cstr.insets = new Insets(5, 20, 5, 20);
		brandHeader.add(this.messageBox, cstr);
		cstr = new GridBagConstraints();
		cstr.gridy = 0;
		cstr.gridheight = 1;
		cstr.insets = new Insets(5, 20, 5, 20);
		brandHeader.add(this.clock, cstr);
		cstr = new GridBagConstraints();
		cstr.gridy = 0;
		cstr.gridheight = 1;
		cstr.insets = new Insets(5, 20, 5, 20);
		brandHeader.add(this.connectedUser, cstr);
		cstr = new GridBagConstraints();
		cstr.gridy = 0;
		cstr.insets = new Insets(5, 5, 5, 5);
		cstr.fill = GridBagConstraints.HORIZONTAL;
		m_jPanContainer.add(brandHeader, cstr);
		
		messageBox.addMouseListener(new MouseAdapter()
        {
            public void mouseClicked(MouseEvent me)
            {
            	if (m_oTicket.getCustomer().getLevelSearch() != 2) {
	            	 if (m_oTicket.getCustomer() != null && Desktop.isDesktopSupported()) {
	                     final Desktop dt = Desktop.getDesktop();
	                     //TODO rendre ceci paramétrable via les ressources côté serveur
	                     //TODO gérer l'authentification de l'utilisateur
	                     //http://127.0.0.1:8080/ose-server/dashboard#/products/fd897f61ef4cbc924fe2d0ed6abb3436/form
	                     
	                     String backOfficeUrl;
						try {
							backOfficeUrl = AppConfig.loadedInstance.getProperty("server.backoffice") + "/" + AppConfig.loadedInstance.getProperty("server.racine") +"?token="+EncryptDecrypt.encrypt(new OseToken(m_App.getAppUserView().getUser().getName()).toString())+"#/customers/" + m_oTicket.getCustomer().getId() + "/form?_action=view";
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							backOfficeUrl = AppConfig.loadedInstance.getProperty("server.backoffice") + "/" + AppConfig.loadedInstance.getProperty("server.racine") +"#/customers/" + m_oTicket.getCustomer().getId() + "/form?_action=view";
						}
	                    if (dt.isSupported(Desktop.Action.BROWSE)) {
	                         try {
	                             dt.browse(new URI(backOfficeUrl));
	                         } catch (URISyntaxException exc) {
	                             //
	                         } catch (IOException exc) {
	                             //
	                         }
	                    }
	                 }
            	}else if (m_oTicket.getCustomer().getLevelSearch() == 2){
                	javax.swing.JOptionPane.showMessageDialog(null,"La fiche client n'est pas disponible pour le moment"); 
                }
            }
        });
		// ////////////////////
		// Ticket info/buttons
		// ////////////////////
		JPanel ticketHeader = new JPanel();

		ticketHeader.setLayout(new GridBagLayout());
		// LocationName
		locationName = WidgetsBuilder.createLabel();
		locationName.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		locationName.setFont(new Font(Font.DIALOG, Font.BOLD, 25));
		locationName.setRequestFocusEnabled(false);
		cstr = new GridBagConstraints();
		cstr.weightx = 1.0;
		cstr.gridy = 0;
		cstr.anchor = GridBagConstraints.LINE_START;
		ticketHeader.add(locationName, cstr);
		// Ticket id
		m_jTicketId = WidgetsBuilder.createLabel();
		m_jTicketId.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

		// m_jTicketId.setOpaque(true);
		m_jTicketId.setFont(new Font(Font.DIALOG, Font.BOLD, 25));
		m_jTicketId.setPreferredSize(new java.awt.Dimension(160, 25));
		m_jTicketId.setRequestFocusEnabled(false);
		cstr = new GridBagConstraints();
		cstr.weightx = 1.0;
		cstr.gridx = 1;
		cstr.gridy = 0;
		ticketHeader.add(m_jTicketId, cstr);
		// Customer button
		btnCustomer = WidgetsBuilder.createButtonTooltip(ImageLoader.readImageIcon("tkt_assign_customer.png"),
				AppLocal.getIntString("Button.btnCustomer.toolTip"), WidgetsBuilder.SIZE_MEDIUM);
		btnCustomer.setFocusPainted(false);
		btnCustomer.setFocusable(false);
		btnCustomer.setRequestFocusEnabled(false);
		btnCustomer.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				btnCustomerActionPerformed(evt);
			}
		});
		cstr = new GridBagConstraints();
		cstr.gridx = 2;
		cstr.gridy = 0;
		cstr.insets = new Insets(0, btnspacing, 0, btnspacing);
		ticketHeader.add(btnCustomer, cstr);

		// Split button
		btnSplit = WidgetsBuilder.createButtonTooltip(ImageLoader.readImageIcon("tkt_split.png"),
				AppLocal.getIntString("Button.btnSplit.toolTip"), WidgetsBuilder.SIZE_MEDIUM);
		btnSplit.setFocusPainted(false);
		btnSplit.setFocusable(false);
		btnSplit.setRequestFocusEnabled(false);
		btnSplit.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				btnSplitActionPerformed(evt);
			}
		});
		cstr.gridx++;
		ticketHeader.add(btnSplit, cstr);
		// Ticket bag extra buttons
		cstr.gridx++;
		ticketHeader.add(m_jPanelBag, cstr);
		// Script extra buttons
		cstr.gridx++;
		ticketHeader.add(m_jButtonsExt, cstr);
		// Add container
		cstr = new GridBagConstraints();
		cstr.gridy = 1;
		cstr.fill = GridBagConstraints.HORIZONTAL;
		m_jPanContainer.add(ticketHeader, cstr);

		// //////////
		// Main zone
		// //////////
		JPanel mainZone = new JPanel();
		mainZone.setLayout(new GridBagLayout());

		// Catalog
		// ////////
		catcontainer.setBorder(javax.swing.BorderFactory.createEmptyBorder(5, 5, 5, 5));
		catcontainer.setLayout(new java.awt.BorderLayout());
		cstr = new GridBagConstraints();
		cstr.gridx = 0;
		cstr.gridy = 0;
		cstr.gridheight = 3;
		cstr.weightx = 1.0;
		cstr.weighty = 1.0;
		cstr.fill = GridBagConstraints.BOTH;
		mainZone.add(catcontainer, cstr);

		// Ticket zone
		// ////////////
		JPanel ticketZone = new JPanel();
		ticketZone.setLayout(new GridBagLayout());

		// Tariff area
		m_jTariff.setFocusable(false);
		m_jTariff.setRequestFocusEnabled(false);
		m_jTariff.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				m_jTariffActionPerformed(evt);
			}
		});
		cstr = new GridBagConstraints();
		cstr.gridx = 0;
		cstr.gridy = 0;
		cstr.gridwidth = 2;
		cstr.fill = GridBagConstraints.HORIZONTAL;
		cstr.insets = new Insets(btnspacing, btnspacing, btnspacing, btnspacing);
		ticketZone.add(m_jTariff, cstr);

		// Ticket lines
		m_ticketlines = new JTicketLines(m_App != null ? m_App.getAppUserView().getUser() : null);
		m_ticketlines.setBorder(javax.swing.BorderFactory.createEmptyBorder(5, 5, 5, 5));
		if (cfg != null || !cfg.getProperty("ui.ticketlineminwidth").equals("0") || !cfg.getProperty("ui.ticketlineminheight").equals("0")) {
			int lineMinWidth = WidgetsBuilder.pixelSize(Float.parseFloat(cfg.getProperty("ui.ticketlineminwidth")));
			int lineMinHeight = WidgetsBuilder.pixelSize(Float.parseFloat(cfg.getProperty("ui.ticketlineminheight")));
			m_ticketlines.setPreferredSize(new Dimension(lineMinWidth, lineMinHeight));
		}
		cstr = new GridBagConstraints();
		cstr.gridx = 0;
		cstr.gridy = 1;
		cstr.fill = GridBagConstraints.BOTH;
		cstr.weightx = 1.0;
		cstr.weighty = 2.0;
		ticketZone.add(m_ticketlines, cstr);

		lineEditBtns = new JPanel();
		lineEditBtns.setLayout(new GridBagLayout());
		//lineEditBtns.setMinimumSize(new Dimension(400,400));
		m_jOrder.setFocusPainted(false);
		m_jOrder.setFocusable(false);
		m_jOrder.setRequestFocusEnabled(false);
		m_jOrder.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				m_jOrderActionPerformed(evt);
			}
		});
		cstr = new GridBagConstraints();
		cstr.gridx = 0;
		cstr.insets = new Insets(0, 0, btnspacing, btnspacing);
		lineEditBtns.add(m_jOrder, cstr);
		// Up/down buttons
		// if (cfg == null
		// // || cfg.getProperty("ui.showupdownbuttons").equals("1")) {
		m_jUp.setFocusPainted(false);
		m_jUp.setFocusable(false);
		m_jUp.setRequestFocusEnabled(false);
		m_jUp.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				m_jUpActionPerformed(evt);
			}
		});
		cstr = new GridBagConstraints();
		cstr.gridx = 0;
		cstr.insets = new Insets(0, 0, btnspacing, btnspacing);
		lineEditBtns.add(m_jUp, cstr);
		m_jDown.setFocusPainted(false);
		m_jDown.setFocusable(false);
		m_jDown.setRequestFocusEnabled(false);
		m_jDown.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				m_jDownActionPerformed(evt);
			}
		});
		cstr = new GridBagConstraints();
		cstr.gridx = 0;
		cstr.insets = new Insets(0, 0, btnspacing, btnspacing);
		lineEditBtns.add(m_jDown, cstr);
		// }
		// Delete line
		m_jDelete.setFocusPainted(false);
		m_jDelete.setFocusable(false);
		m_jDelete.setRequestFocusEnabled(false);
		m_jDelete.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				try {
					m_jDeleteActionPerformed(evt);
				} catch (BasicException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		cstr = new GridBagConstraints();
		cstr.gridx = 0;
		cstr.insets = new Insets(0, 0, btnspacing, btnspacing);
		lineEditBtns.add(m_jDelete, cstr);
		// Find product
		m_jList.setFocusPainted(false);
		m_jList.setFocusable(false);
		m_jList.setRequestFocusEnabled(false);
		m_jList.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				m_jListActionPerformed(evt);
			}
		});
		cstr = new GridBagConstraints();
		cstr.gridx = 0;
		cstr.insets = new Insets(0, 0, btnspacing, btnspacing);
		lineEditBtns.add(m_jList, cstr);
		// Edit line
		m_jEditLine.setFocusPainted(false);
		m_jEditLine.setFocusable(false);
		m_jEditLine.setRequestFocusEnabled(false);
		m_jEditLine.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				m_jEditLineActionPerformed(evt);
			}
		});
		cstr = new GridBagConstraints();
		cstr.gridx = 0;
		cstr.insets = new Insets(0, 0, btnspacing, btnspacing);
		lineEditBtns.add(m_jEditLine, cstr);
		// Attributes
		jEditAttributes.setFocusPainted(false);
		jEditAttributes.setFocusable(false);
		jEditAttributes.setRequestFocusEnabled(false);
		jEditAttributes.setEnabled(false); // TODO: set attributes
		jEditAttributes.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jEditAttributesActionPerformed(BUTTON_PERFORMED);
			}
		});
		cstr = new GridBagConstraints();
		cstr.gridx = 0;
		cstr.insets = new Insets(0, 0, btnspacing, btnspacing);
		// disabled until come back
		// lineEditBtns.add(jEditAttributes, cstr);
		// Line discount button
		m_jbtnLineDiscount.setFocusPainted(false);
		m_jbtnLineDiscount.setFocusable(false);
		m_jbtnLineDiscount.setRequestFocusEnabled(false);
		m_jbtnLineDiscount.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				m_jbtnLineDiscountActionPerformed(evt);
			}
		});
		cstr = new GridBagConstraints();
		cstr.gridx = 0;
		cstr.insets = new Insets(0, 0, btnspacing, btnspacing);
		lineEditBtns.add(m_jbtnLineDiscount, cstr);

		m_jbtnLineFixedDiscount.setFocusPainted(false);
		m_jbtnLineFixedDiscount.setFocusable(false);
		m_jbtnLineFixedDiscount.setRequestFocusEnabled(false);
		m_jbtnLineFixedDiscount.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				m_jbtnLineFixedDiscountActionPerformed(evt);
			}
		});
		cstr = new GridBagConstraints();
		cstr.gridx = 0;
		cstr.insets = new Insets(0, 0, btnspacing, btnspacing);
		lineEditBtns.add(m_jbtnLineFixedDiscount, cstr);
		// Add line edit container
		cstr = new GridBagConstraints();
		cstr.gridx = 1;
		cstr.gridy = 1;
		cstr.gridheight = 4;
		cstr.weighty = 2.0;
		//cstr.fill = GridBagConstraints.VERTICAL;
		cstr.insets = new Insets(5, 0, 0, btnspacing);
		cstr.anchor = GridBagConstraints.NORTH;
		ticketZone.add(lineEditBtns, cstr);

		// Total zone
		// ///////////
		JPanel totalZone = new JPanel();
		totalZone.setLayout(new GridBagLayout());
		//Ticket Info
		this.ticketInfoLabel.setRequestFocusEnabled(false);
		cstr = new GridBagConstraints();
		cstr.gridx = 0;
		cstr.gridy = 0;
		cstr.weightx = 1.0;
		cstr.gridwidth = 4;
		cstr.anchor = GridBagConstraints.FIRST_LINE_START;
		cstr.insets = new java.awt.Insets(5, 5, 5, 5);
		totalZone.add(this.ticketInfoLabel, cstr);
		// Ticket discount
		btnTicketDiscount = WidgetsBuilder.createButtonTooltip(ImageLoader.readImageIcon("tkt_discount.png"),
				AppLocal.getIntString("Button.btnTicketDiscount.toolTip"), WidgetsBuilder.SIZE_BIG);
		btnTicketDiscount.setFocusPainted(false);
		btnTicketDiscount.setFocusable(false);
		btnTicketDiscount.setRequestFocusEnabled(false);
		btnTicketDiscount.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				btnTicketDiscountActionPerformed(evt);
			}
		});
		cstr = new GridBagConstraints();
		cstr.gridheight = 2;
		cstr.gridy = 1;
		cstr.insets = new Insets(0, 5, 0, btnspacing);
		cstr.anchor = GridBagConstraints.SOUTH;
		totalZone.add(btnTicketDiscount, cstr);
		
		//Zone Sud Est - On impbrique plusieurs gridbag plutôt que de faire des choses intordables avec les poids 
		JPanel southeastZone = new JPanel();
		southeastZone.setLayout(new GridBagLayout());
		// Ticket Note
		btnTicketNote = WidgetsBuilder.createButtonTooltip(ImageLoader.readImageIcon("tkt_line_edit.png"),
				AppLocal.getIntString("Button.btnTicketNote.toolTip"), WidgetsBuilder.SIZE_BIG);
		btnTicketNote.setFocusPainted(false);
		btnTicketNote.setFocusable(false);
		btnTicketNote.setRequestFocusEnabled(false);
		btnTicketNote.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				btnTicketNotesActionPerformed(evt);
			}
		});
//		cstr = new GridBagConstraints();
//		cstr.gridheight = 3;
//		cstr.insets = new Insets(0, 5, 0, btnspacing);
//		cstr.anchor = GridBagConstraints.SOUTH;
		cstr.gridx = 2;
		cstr.anchor = GridBagConstraints.SOUTHEAST;
		southeastZone.add(btnTicketNote, cstr);
		
		this.subtotalLabel.setRequestFocusEnabled(false);
		cstr = new GridBagConstraints();
		cstr.gridx = 0;
		cstr.gridy = 1;
		cstr.weightx = 1.0;
		cstr.gridwidth = 2;
		cstr.anchor = GridBagConstraints.FIRST_LINE_END;
		cstr.insets = new java.awt.Insets(5, 5, 5, 5);
		southeastZone.add(this.subtotalLabel, cstr);
		
		
		cstr = new GridBagConstraints();
		cstr.gridx = 2;
		cstr.gridy = 1;
		cstr.weightx = 1.0;
		cstr.gridwidth = 2;
		cstr.anchor = GridBagConstraints.FIRST_LINE_END;
		//cstr.insets = new java.awt.Insets(5, 5, 5, 5);
		totalZone.add(southeastZone, cstr);
		// Discount
		this.discountLabel.setRequestFocusEnabled(false);
		cstr = new GridBagConstraints();
		cstr.gridx = 1;
		cstr.gridy = 1;
		cstr.anchor = GridBagConstraints.SOUTHWEST;
		cstr.fill = GridBagConstraints.HORIZONTAL;
		totalZone.add(this.discountLabel, cstr);
		// Total
		m_jTotalEuros.setRequestFocusEnabled(false);
		cstr = new GridBagConstraints();
		cstr.gridx = 3;
		cstr.gridy = 0;
		cstr.gridwidth = 2;
		cstr.anchor = GridBagConstraints.SOUTHEAST;
		cstr.insets = new java.awt.Insets(5, 5, 5, 5);
		cstr.weightx = 1.0;
		// cstr.fill = GridBagConstraints.HORIZONTAL;
		totalZone.add(m_jTotalEuros, cstr);
		// Subtotal and total (label and amount)

		// Add total zone
		cstr = new GridBagConstraints();
		cstr.gridx = 0;
		cstr.gridy = 2;
		cstr.gridwidth = 1;
		cstr.fill = GridBagConstraints.HORIZONTAL;
		ticketZone.add(totalZone, cstr);

		//mainZone.add(ticketZone, cstr);
		// Barcode and manual input zone
		if (cfg == null || cfg.getProperty("ui.showbarcode").equals("1")) {
			JPanel barcodeZone = new JPanel();
			barcodeZone.setLayout(new GridBagLayout());
			m_jPrice.setBackground(java.awt.Color.white);
			m_jPrice.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
			m_jPrice.setBorder(javax.swing.BorderFactory.createCompoundBorder(
					javax.swing.BorderFactory.createLineBorder(javax.swing.UIManager.getDefaults().getColor("Button.darkShadow")),
					javax.swing.BorderFactory.createEmptyBorder(1, 4, 1, 4)));
			m_jPrice.setOpaque(true);
			m_jPrice.setPreferredSize(new java.awt.Dimension(100, 22));
			m_jPrice.setRequestFocusEnabled(false);
			cstr = new java.awt.GridBagConstraints();
			cstr.gridx = 0;
			cstr.gridy = 0;
			cstr.gridwidth = 2;
			cstr.insets = new Insets(btnspacing, btnspacing, btnspacing, btnspacing);
			cstr.fill = java.awt.GridBagConstraints.BOTH;
			cstr.weightx = 1.0;
			cstr.weighty = 1.0;
			barcodeZone.add(m_jPrice, cstr);
			m_jPor.setBackground(java.awt.Color.white);
			m_jPor.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
			m_jPor.setBorder(javax.swing.BorderFactory.createCompoundBorder(
					javax.swing.BorderFactory.createLineBorder(javax.swing.UIManager.getDefaults().getColor("Button.darkShadow")),
					javax.swing.BorderFactory.createEmptyBorder(1, 4, 1, 4)));
			m_jPor.setOpaque(true);
			m_jPor.setPreferredSize(new java.awt.Dimension(22, 22));
			m_jPor.setRequestFocusEnabled(false);
			cstr = new java.awt.GridBagConstraints();
			cstr.gridx = 2;
			cstr.gridy = 0;
			cstr.fill = java.awt.GridBagConstraints.BOTH;
			cstr.weightx = 1.0;
			cstr.weighty = 1.0;
			cstr.insets = new java.awt.Insets(btnspacing, 0, btnspacing, 0);
			barcodeZone.add(m_jPor, cstr);
			m_jEnter.setFocusPainted(false);
			m_jEnter.setFocusable(false);
			m_jEnter.setRequestFocusEnabled(false);
			m_jEnter.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					m_jEnterActionPerformed(evt);
				}
			});
			cstr = new java.awt.GridBagConstraints();
			cstr.gridx = 3;
			cstr.gridy = 0;
			cstr.insets = new java.awt.Insets(btnspacing, btnspacing, btnspacing, btnspacing);
			barcodeZone.add(m_jEnter, cstr);
			cstr = new GridBagConstraints();
			cstr.gridx = 0;
			cstr.gridy = 3;
			cstr.gridwidth = 1;
			cstr.fill = GridBagConstraints.HORIZONTAL;
			//mainZone.add(barcodeZone, cstr);
			ticketZone.add(barcodeZone, cstr);
		}

		// Numpad zone
		//numpadZone.setLayout(new GridBagLayout());
		m_jNumberKeys.addJNumberEventListener(new JNumberEventListener() {
			public void keyPerformed(JNumberEvent evt) {
				m_jNumberKeysKeyPerformed(evt);
			}
		});

		cstr = new GridBagConstraints();
		cstr.gridx = 0;
		cstr.gridy = 4;
		cstr.gridwidth = 1;
		ticketZone.add(jButtonNumPad,cstr);
		
		cstr = new GridBagConstraints();
		cstr.gridx = 0;
		cstr.gridy = 5;
		cstr.gridwidth = 1;
		cstr.fill = GridBagConstraints.HORIZONTAL;
		numpadZone.add(m_jNumberKeys);
		ticketZone.add(numpadZone,cstr);
		
		// Add ticket zone
		cstr = new GridBagConstraints();
		cstr.gridy = 0;
		cstr.gridx = 1;
		cstr.fill = GridBagConstraints.BOTH;
		cstr.weighty = 1.0;
		ticketZone.setPreferredSize(new Dimension(650,450));
		ticketZone.setMinimumSize(new Dimension(650,450));
		mainZone.add(ticketZone, cstr);
//		cstr = new GridBagConstraints();
//		cstr.gridx = 1;
//		cstr.gridy = 2;
		//mainZone.add(m_jNumberKeys, cstr);
		// Add main zone container
		cstr = new GridBagConstraints();
		cstr.gridx = 0;
		cstr.weightx = 1.0;
		cstr.weighty = 1.0;
		cstr.fill = GridBagConstraints.BOTH;
		m_jPanContainer.add(mainZone, cstr);

		// ////////////
		// Footer line
		// ////////////

		m_jPanTotals.setLayout(new java.awt.GridBagLayout());

		m_jPanEntries.setLayout(new javax.swing.BoxLayout(m_jPanEntries, javax.swing.BoxLayout.Y_AXIS));

		m_jTax.setFocusable(false);
		m_jTax.setRequestFocusEnabled(false);
		cstr = new java.awt.GridBagConstraints();
		cstr.gridx = 0;
		cstr.gridy = 1;
		cstr.gridwidth = 2;
		cstr.fill = java.awt.GridBagConstraints.BOTH;
		cstr.weightx = 1.0;
		cstr.weighty = 1.0;
		cstr.insets = new java.awt.Insets(5, 0, 0, 0);
		jPanel9.add(m_jTax, cstr);

		m_jaddtax.setText("+");
		m_jaddtax.setFocusPainted(false);
		m_jaddtax.setFocusable(false);
		m_jaddtax.setRequestFocusEnabled(false);
		cstr = new java.awt.GridBagConstraints();
		cstr.gridx = 2;
		cstr.gridy = 1;
		cstr.fill = java.awt.GridBagConstraints.BOTH;
		cstr.weightx = 1.0;
		cstr.weighty = 1.0;
		cstr.insets = new java.awt.Insets(5, 5, 0, 0);
		jPanel9.add(m_jaddtax, cstr);

		jPanel9.setMaximumSize(new java.awt.Dimension(m_jNumberKeys.getMaximumSize().width, jPanel9.getPreferredSize().height));
		m_jPanEntries.add(jPanel9);

		m_jKeyFactory.setBackground(javax.swing.UIManager.getDefaults().getColor("Panel.background"));
		m_jKeyFactory.setForeground(javax.swing.UIManager.getDefaults().getColor("Panel.background"));
		m_jKeyFactory.setBorder(null);
		m_jKeyFactory.setCaretColor(javax.swing.UIManager.getDefaults().getColor("Panel.background"));
		m_jKeyFactory.setPreferredSize(new java.awt.Dimension(1, 1));
		m_jKeyFactory.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyTyped(java.awt.event.KeyEvent evt) {
				m_jKeyFactoryKeyTyped(evt);
			}

			public void keyPressed(java.awt.event.KeyEvent evt) {
				m_jKeyFactoryKeyPressed(evt);
			}
		});
		m_jPanEntries.add(m_jKeyFactory);

		cstr = new GridBagConstraints();
		cstr.gridx = 2;
		cstr.gridy = 0;
		cstr.gridheight = 2;
		m_jInputContainer.add(m_jPanEntries, cstr);

		cstr = new GridBagConstraints();
		cstr.gridx = 0;
		cstr.gridy = 1;
		cstr.weightx = 1.0;
		cstr.fill = GridBagConstraints.HORIZONTAL;
		m_jPanContainer.add(m_jInputContainer, cstr);

		this.add(m_jPanContainer, "ticket");
	}

	@SuppressWarnings("unused")
	private void m_jbtnScaleActionPerformed(java.awt.event.ActionEvent evt) {

		automator(keySection);

	}

	private void m_jEditLineActionPerformed(java.awt.event.ActionEvent evt) {

		int i = m_ticketlines.getSelectedIndex();
		if (i < 0) {
			Toolkit.getDefaultToolkit().beep(); // no line selected
		} else {
			try {
				TicketLineInfo newline = JProductLineEdit.showMessage(this, m_App, m_oTicket.getLine(i));
				if (newline != null) {
					// line has been modified
					paintTicketLine(i, newline);
				}
			} catch (BasicException e) {
				new MessageInf(e).show(this);
			}
		}
	}

	private void btnTicketDiscountActionPerformed(ActionEvent evt) {
		DiscountProfilePicker finder = DiscountProfilePicker.getDiscountProfilePicker(this);
		String notes = "";
		finder.setNotes(this.m_oTicket.getNote());
		finder.setVisible(true);
		

		DiscountProfile profile = finder.getSelectedProfile();
		if (profile != null) {
			notes = finder.getNotes();
		}
		discountActionPerformed(profile , notes);
		
			
	}
	
	private void btnTicketNotesActionPerformed(ActionEvent evt) {
		NotesPicker finder = NotesPicker.getNotesPicker(this);
		finder.setNotes(this.m_oTicket.getNote());
		finder.setVisible(true);
		
		if (finder.getNotesResponse() ) {
			this.m_oTicket.setNote(finder.getNotes());
			updateTicketNoteButton();
		}
		
	}
	
	private void updateTicketNoteButton() {
		if(this.m_oTicket != null && this.m_oTicket.getNote() != null && !this.m_oTicket.getNote().isEmpty()) {
			btnTicketNote.setBackground(Color.YELLOW);
		} else {
			btnTicketNote.setBackground(null);
		}
	}
	
	private void discountActionPerformed(DiscountProfile profile , String notes) {

		if (profile != null) {
			if (profile.getRate() <= 1.0)
			{
				// Set discount profile and rate to ticket
				this.m_oTicket.setDiscountRate(profile.getRate());
				this.m_oTicket.setDiscountProfileId(profile.getId());
				this.m_oTicket.setNote(notes);
			}else{
				// on ne peut pas faire une remise > à 100%
				MessageInf msg = new MessageInf(MessageInf.SGN_WARNING, AppLocal.getIntString("message.discountHigherPriceTicket"));
				msg.show(this);
				java.awt.Toolkit.getDefaultToolkit().beep();
				this.refreshTicket();
			}
		} else {
			// Reset discount profile and rate
			this.m_oTicket.setDiscountProfileId(null);
			this.m_oTicket.setDiscountRate(0.0);
			this.m_oTicket.setNote("");
		}
		refreshTicket();
	}

	private void m_jbtnLineDiscountActionPerformed(java.awt.event.ActionEvent evt) {
		double discountRate = this.getInputValue() / 100.0;
		String note = null;
		if(m_sBarcode.toString().indexOf("!") > 0) {
			note = m_sBarcode.toString().substring(m_sBarcode.toString().indexOf("!")+1);
		}
		lineDiscountActionPerformed(discountRate , note);

	}
	
	
	private void lineRenameActionPerformed(String newName , String note) {		

		int index = m_ticketlines.getSelectedIndex();
		if (index >= 0) {
			TicketLineInfo line = m_oTicket.getLine(index);
			if (note != null) {
				line.setComments(note);
				
			}
			if (newName != null && line.getProductID() == null) {
				line.setProperty("product.name", newName);
			}
			this.refreshTicket();
			
		} 
	}
	private void lineDiscountActionPerformed(double discountRate , String note) {		
		/* EDU 2016-02 - Adaptation afin que 9.99 TTC - 10% 
		 *  ne corresponde plus à 8.991 mais à 8.99 TTC
		 *  il faut donc adapter le 10% en 10.01001001 %
		 */

		int index = m_ticketlines.getSelectedIndex();
		if (index >= 0) {
			TicketLineInfo line = m_oTicket.getLine(index);
			if (note != null) {
				line.setComments(note);
				this.refreshTicket();
			}
			if (discountRate <= 1.0)
			{
				
				if (discountRate > 0.005) {
					double discountAmount = RoundUtils.round(line.getFullPriceTax() * discountRate) ;
					discountRate = discountAmount / line.getFullPriceTax();
					
					line.setDiscountRate(discountRate);
					this.refreshTicket();
				} else {
					
					if("0".equals(this.m_jPrice.getText()) || discountRate == 0D) {
						//Explicit 0
						line.setDiscountRate(0);
						this.refreshTicket();
					} else {
						// No rate 
						MessageInf msg = new MessageInf(MessageInf.SGN_WARNING, AppLocal.getIntString("message.selectratefordiscount"));
		
						msg.show(this);
						java.awt.Toolkit.getDefaultToolkit().beep();
					}
				}
			}else{
				// on ne peut pas faire une remise > à 100%
				MessageInf msg = new MessageInf(MessageInf.SGN_WARNING, AppLocal.getIntString("message.discountHigherPriceline"));
				msg.show(this);
				java.awt.Toolkit.getDefaultToolkit().beep();
				this.refreshTicket();
			}
		} else {
			// No item or discount selected
			MessageInf msg = new MessageInf(MessageInf.SGN_WARNING, AppLocal.getIntString("message.selectlinefordiscount"));
			msg.show(this);
			java.awt.Toolkit.getDefaultToolkit().beep();
		}
	}

	private void m_jbtnLineFixedDiscountActionPerformed(java.awt.event.ActionEvent evt) {
		double discountAmount = this.getInputValue();
		String note = null;
		if(m_sBarcode.toString().indexOf("!") > 0) {
			note = m_sBarcode.toString().substring(m_sBarcode.toString().indexOf("!")+1);
		}
		lineFixedDiscountActionPerformed(discountAmount, note);
	}
	
	private void lineFixedDiscountActionPerformed(double discountAmount , String note) {
		int index = m_ticketlines.getSelectedIndex();

		if (index >= 0) {
			TicketLineInfo line = m_oTicket.getLine(index);
			double discountRate = discountAmount / line.getFullPriceTax();
			if (note != null) {
				line.setComments(note);
				this.refreshTicket();
			}
			if (discountRate <= 1.0)
			{
				if (discountRate > 0.005) {
					line.setDiscountRate(discountRate);
					this.refreshTicket();
				} else {
					if("0".equals(this.m_jPrice.getText())) {
						//Explicit 0
						line.setDiscountRate(0);
						this.refreshTicket();
					} else {
						// No rate 
						MessageInf msg = new MessageInf(MessageInf.SGN_WARNING, AppLocal.getIntString("message.selectratefordiscount"));
						msg.show(this);
						java.awt.Toolkit.getDefaultToolkit().beep();
					}
				}
			}else{
				// on ne peut pas faire une remise > à 100%
				MessageInf msg = new MessageInf(MessageInf.SGN_WARNING, AppLocal.getIntString("message.discountHigherPriceline"));
				msg.show(this);
				java.awt.Toolkit.getDefaultToolkit().beep();
				this.refreshTicket();
			}
		} else {
			// No item or discount selected
			MessageInf msg = new MessageInf(MessageInf.SGN_WARNING, AppLocal.getIntString("message.selectlinefordiscount"));
			msg.show(this);
			java.awt.Toolkit.getDefaultToolkit().beep();
		}

	}

	private void m_jTariffActionPerformed(java.awt.event.ActionEvent evt) {
		try {
			TariffInfo tariff = (TariffInfo) m_jTariff.getSelectedItem();
			if (tariff != null) {
				this.switchTariffArea(tariff);
			}
		} catch (java.lang.ClassCastException e) {

		}
	}

	private void m_jEnterActionPerformed(java.awt.event.ActionEvent evt) {

		automator(keyEnter);

	}

	private void m_jNumberKeysKeyPerformed(JNumberEvent evt) {

		automator(evt.getKey());

	}

	private void m_jKeyFactoryKeyTyped(java.awt.event.KeyEvent evt) {
		m_jKeyFactory.setText(null);
		automator(evt.getKeyChar());
	}

	private void m_jKeyFactoryKeyPressed(java.awt.event.KeyEvent evt) {
		// Adding shortcuts for product's selection in panel sales screen
		if (evt.getKeyCode() == KeyEvent.VK_UP || evt.getKeyCode() == KeyEvent.VK_PAGE_UP) {
			m_ticketlines.selectionUp();
		} else if (evt.getKeyCode() == KeyEvent.VK_DOWN || evt.getKeyCode() == KeyEvent.VK_PAGE_DOWN) {
			m_ticketlines.selectionDown();
		}
	}

	private void m_jDeleteActionPerformed(java.awt.event.ActionEvent evt) throws BasicException {

		int i = m_ticketlines.getSelectedIndex();
		if (i < 0) {
			Toolkit.getDefaultToolkit().beep(); // No hay ninguna seleccionada
		} else {
			try{
				dlSales.saveTicketLinesDeleted(null,m_oTicket.getLine(i), m_oTicket.getName(m_oTicketExt),m_oTicket.isOpenedPaymentDialog());
		 	} catch (Exception e) {
		 		logger.log(Level.WARNING, "Unable to save TicketLinesDeleted dans la base: "+ e.getMessage());
		 		logger.log(Level.INFO, "Queued ticketLinesDeleted ");
		 		
		 	}
			removeTicketLine(i); // elimino la linea
		}

	}

	private void m_jOrderActionPerformed(java.awt.event.ActionEvent evt) {

		int i = m_ticketlines.getSelectedIndex();
		if (i < 0) {
			Toolkit.getDefaultToolkit().beep(); // No hay ninguna seleccionada
		} else if (isOrderAvailable()){ // si le ticket est déja lié à un client
			toggleOrderTicketLine(i); // On change le statut
		}else{
			 if (this.linkTicketToCustomer()!=null){
				 toggleOrderTicketLine(i);
			 }
		}

	}

	private void m_jUpActionPerformed(java.awt.event.ActionEvent evt) {

		m_ticketlines.selectionUp();

	}

	private void m_jDownActionPerformed(java.awt.event.ActionEvent evt) {

		m_ticketlines.selectionDown();

	}

	private void m_jListActionPerformed(java.awt.event.ActionEvent evt) {
		searchProduct(null);
	}

	private void searchProduct(String ref) {
		searchProduct(ref, 1.0D);
	}

	private void searchProduct(String ref, double qty) {
		ProductInfoExt prod = JProductFinder.showMessage(JPanelTicket.this, dlSales, ref,m_App.getAppUserView().getUser());
		if (prod != null) {
			buttonTransition(prod, qty);
		}
	}

	private void btnCustomerActionPerformed(java.awt.event.ActionEvent evt) {
		
		this.linkTicketToCustomer();
		
	}

	private CustomerInfoExt linkTicketToCustomer() {
		JCustomerFinder finder = JCustomerFinder.getCustomerFinder(this, dlCustomers,m_App.getAppUserView().getUser());
		finder.search(m_oTicket.getCustomer());
		finder.setVisible(true);

		CustomerInfoExt customer = finder.getSelectedCustomer();
		CustomerInfoExt oldCustomer = this.m_oTicket.getCustomer();
		if(customer == null && oldCustomer != null && this.m_oTicket.HasOrders()) {
			//EDU 2020-12 - Cas du cancel alors qu'on est sur le client d'une commande
			customer = oldCustomer;
		}
		this.m_oTicket.setCustomer(finder.getSelectedCustomer());
		if (customer != null && customer.getDiscountProfileId() != null) {
			// Set discount profile and rate to ticket
			try {
				DiscountProfile profile = dlCustomers.getDiscountProfile(customer.getDiscountProfileId());
				this.m_oTicket.setDiscountRate(profile.getRate());
				this.m_oTicket.setDiscountProfileId(profile.getId());
			} catch (BasicException e) {
				e.printStackTrace();
			}
		} else {
			// Reset discount profile and rate
			//this.m_oTicket.setDiscountProfileId(null);
			//this.m_oTicket.setDiscountRate(0.0);
			// Attention pas de reset si nous sommes sur un retour
			//TODO En plus on n'a pas le même comportement si entrée à partir d'une carte
			//Il faudra uniformiser avec le retour de l'automator ! en attendant je commente le reset
		}
		if (customer != null) {
			// Show prepaid and debt in message box
			this.messageBox.setText(customer.printCustInfo());
			if (customer.getLevelSearch() != 2) {
				// Refresh customer from server
				dlCustomers.updateCustomer(customer.getId(), this);
			}
		}
		refreshTicket();
		return customer;
		
	}

	private void btnSplitActionPerformed(java.awt.event.ActionEvent evt) {

		if (m_oTicket.getLinesCount() > 0) {
			ReceiptSplit splitdialog = ReceiptSplit.getDialog(this, dlSystem.getResourceAsXML("Ticket.Line"), dlSales, dlCustomers,
					taxeslogic,m_App.getAppUserView().getUser());

			TicketInfo ticket1 = m_oTicket.copyTicket();
			TicketInfo ticket2 = new TicketInfo();
			ticket2.setCustomer(m_oTicket.getCustomer());

			if (splitdialog.showDialog(ticket1, ticket2, m_oTicketExt)) {
				if (closeTicket(ticket2, m_oTicketExt)) { // already checked
															// that number of
															// lines > 0
					setActiveTicket(ticket1, m_oTicketExt);// set result ticket
				}
			}
		}
	}

	private void jEditAttributesActionPerformed(int cameFrom) {

		int i = m_ticketlines.getSelectedIndex();
		if (i < 0) {
			Toolkit.getDefaultToolkit().beep(); // no line selected
		} else {
			try {
				TicketLineInfo line = m_oTicket.getLine(i);
				JProductAttEdit attedit = JProductAttEdit.getAttributesEditor(this);
				attedit.editAttributes(line.getProductAttSetId(), line.getProductAttSetInstId());
				attedit.setVisible(true);
				if (attedit.isOK()) {
					// The user pressed OK
					line.setProductAttSetInstId(attedit.getAttributeSetInst());
					line.setProductAttSetInstDesc(attedit.getAttributeSetInstDescription());
					paintTicketLine(i, line);
				}
			} catch (BasicException ex) {
				if (cameFrom == BUTTON_PERFORMED) {
					MessageInf msg = new MessageInf(MessageInf.SGN_WARNING, AppLocal.getIntString("message.cannotfindattributes"), ex);
					msg.show(this);
				}
			}
		}
	}

	private void jShowAlertActionPerformed(int cameFrom, ProductInfoExt prod) {

		int i = m_ticketlines.getSelectedIndex();
		if (i < 0) {
			Toolkit.getDefaultToolkit().beep(); // no line selected
		} else {
			TicketLineInfo line = m_oTicket.getLine(i);
			// TODO Préparer la fenêtre modale qui va bien
			JDialogAlert msgedit = JDialogAlert.getDialog(this, prod.getName(), prod.getMessagesByCriteria("ALERT" , m_App.getInventoryLocation()).get(0).getContent(), true);
			Toolkit.getDefaultToolkit().beep();
			msgedit.setVisible(true);
			// The user pressed OK
			paintTicketLine(i, line);
		}
	}

	public void updateLocationName() {
		String name = " ";
		if (m_App != null) {
			name += m_App.getCashRegister().getLocationName();

			if (m_App.getSelectedSalesLocation() != null) {
				name = " " + m_App.getSelectedSalesLocation().toString();
			}
			this.locationName.setText(name);
		}
	}

	public String getLocationInsee() {
		String name = "";
		if (m_App != null) {
			name = m_App.getCashRegister().getZipCode();

			if (m_App.getSelectedSalesLocation() != null) {
				name = m_App.getSelectedSalesLocation().getInseeNb();
			}
		}

		return name;
	}
	
	public String getLocationCity() {
		String name = "";
		if (m_App != null) {
			name = m_App.getCashRegister().getLocationName();
			
			InseeInfo insee;
			try {
				insee = InseeCache.getInseeById(m_App.getCashRegister().getZipCode());
				if(insee != null) {
					name = insee.getCommune();
				}
			} catch (BasicException e) {
				e.printStackTrace();
			}

			if (m_App.getSelectedSalesLocation() != null) {
				name = m_App.getSelectedSalesLocation().getCity();
			}
		}

		return name;
	}

	public boolean isOrderAvailable() {
		return (getActiveTicket().getCustomerId() != null);
	}

	/**
	 * On va vérifier que chaque élément composé dispose de sa liste de dépendence avec les quantités adéquates On va vérifier que chaque
	 * dépendence est bien ratachée à un élément
	 * 
	 * On parcours une liste dans l'ordre et on vérifie : si il y a un manque on le comble si il y a un intru on le vire si il y a un écart
	 * de quantité on rectifie
	 * 
	 * @param index
	 *            la ligne où il s'est passé un truc ! C'est elle qui détient la vérité!
	 * 
	 *            paintTicketLine ligne 526
	 */
	private void checkLines(int index) {
		ProductInfoExt composedProduct = null, elementProduct = null;
		List<CompositionInfo> elements = null;
		boolean goOn = true;
		double eco = 0D;
		int i;
		// int rank = 0 , delta = m_ticketlines.getSelectedIndex();
		// boolean isOrder = false;
		boolean isRemoval = index < 0;
		TicketLineInfo oLine;
		List<TicketLineInfo> singleOffers, severalOffers;
		ArrayList<ArrayList<TicketLineInfo>> balanced = null;
		List<ProductInfoExt> singleOffersProducts, severalOffersProducts;
		
		if (isRemoval) {
			index = (index + 1) * -1;
		}
		// Attention liens depus children vers une ligne plus dans la liste !
		// en cas de supression de ligne

		// Dans ce cas un retour au cas général suffit

		// si index < 0 ... c'est qu'on a enlevé la dernière ligne on n'a donc plus rien à faire
		if (index >= 0 && index <= m_oTicket.getLinesCount()) {
			
			// si on n'entre pas dans ce bloc c'est que nous avons supprimé la dernière ligne du ticket
			// rien de spécial à faire car soit nous avons supprimé une ligne 'seule'
			// soit nous avons supprimé une ligne de composant auquel cas nous sommes aussi dans le cas standard
			if (index < m_oTicket.getLinesCount()) {
				oLine = m_oTicket.getLine(index);
				
				// POSTULAT avant d'arriver on est ok !
				// Donc seules les conséquences de notre modification ont pu altérer le bazard

				if (isRemoval) {
					try {
						// si on a enlevé une ligne nous sommes désormais sur la ligne suivante
						// si c'est une ligne de composant alors on remonte jusqu'à trouver la ligne maître.
						if (oLine.getParent() != null) {
							i = index - 1;
							TicketLineInfo ticketLine = oLine.getParent();

							if (i >= m_oTicket.getLinesCount()) {
								i = m_oTicket.getLinesCount() - 1;
							}

							while (i >= 0 && !ticketLine.equals(m_oTicket.getLine(i--)))
								;
							if (i < 0) {
								// si on ne la trouve pas (ligne du dessus inexistante ou composant avec maitre différent ou produit normal)
								// -------------------
								// c'est que c'est elle qui a sauté on re descend et vire tous les composants
								i = index;
								if (i >= m_oTicket.getLinesCount()) {
									i = m_oTicket.getLinesCount() - 1;
								}
								while (i >= 0 && ticketLine.equals(m_oTicket.getLine(i).getParent())) {
									m_ticketlines.removeTicketLine(i);
									m_oTicket.removeLine(i--);
								}
							} else {
								// si on la trouve c'est qu'on a mis à zero un composant on la vire puis transforme tous les composants
								// restant en lignes normales -> cas standard
								// TODO Gérer : !attention si remise!

							}

						} else {
							// sinon on regarde la ligne précédente :
							// c'est un article normal : RAS on a forcément viré un article normal (puisque sur une ligne) -> cas standard
							// c'est un agregat : on a viré son seul composant -> on vire la ligne -> cas standard !attention si remise!
							// c'est un composant : on remonte jusqu'à l'agrégat et on vérifie les lignes de composants le nombre suffit
							// imho
							// c'est ok : on a viré une ligne normal à la suite d'un agrégat rAS -> cas standard
							// c'est ko : on a viré un composant : on vire l'agrégat et tous les composant deviennent des lignes normales ->
							// cas standard !attention si remise!
							//
							composedProduct = dlSales.getProductInfo(oLine.getProductID());
						}

					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				} else {

					// si on a ajouté une ligne ou modifié les quantités ( !isRemoval ) notre ligne d'index est juste / sert de référentiel
					try {
						composedProduct = dlSales.getProductInfo(oLine.getProductID());
						if (composedProduct != null && composedProduct.getChildren() != null && composedProduct.getChildren().size() > 0) {
							// si index est une ligne dont le produit est un agrégat
							// on recalcule toutes les quantités des composants ------------------------------

							elements = composedProduct.getChildren();
							if (oLine.getChildren() != null) {
								for (TicketLineInfo ticketLine : oLine.getChildren()) {
									// on fait le tour pour vérifier / ajuster les quantités
									i = -1;
									while (!elements.get(++i).getChildId().equals(ticketLine.getProductID())) {	
									}
									// Le cas où l'élément ne serait pas trouvé remet en cause notre postulat de départ
									// ça voudrait dire qu'il y a une ligne de sous produit qui n'entre pas dans la composition !
									// ça se conclu par une erreur - On possède toutefois un catch fourre tout
									ticketLine.setMultiply(oLine.getMultiply() * elements.get(i).getQuantity());
									if (oLine.isOrder() && ticketLine.isOrderAvailable()) {
										ticketLine.setType(TicketLineInfo.ORDER);
									} else {
										//EDU - 2021 11 si nous sommes là c'est qu'on vient de modifier cette ligne... donc on passe les composants à vente
										ticketLine.setType(TicketLineInfo.SALE);
									}

									// modifier dans ticketLine !
									// TODO à enlever pus tard mais utils pour 'visuels intermediaires'
									// On part du principe que les lignes sont insérées dans le même ordre
									// dans children et dans les tables de visu
									m_ticketlines.removeTicketLine(index + i + 1);
									m_ticketlines.insertTicketLine(index + i + 1, ticketLine);

									elements.remove(i);
								}
							}
							// il reste des éléments (primo insertion ?)
							if (elements.size() > 0) {
								for (CompositionInfo elem : elements) {
									// On crée la ligne qui va bien - prix standard !
									// *Sans* la logique de taxes et Cie ? Oui ce sera réajusté à la fin !
									elementProduct = dlSales.getProductInfo(elem.getChildId());
									TicketLineInfo ticketLine = createTicketLine(elementProduct, oLine.getMultiply() * elem.getQuantity(),
											elementProduct.getPriceSell());

									checkLinePrice(ticketLine);

									if (oLine.isOrder() && ticketLine.isOrderAvailable()) {
										ticketLine.setType(TicketLineInfo.ORDER);
									}

									ticketLine.setParent(oLine);
									ticketLine.setSubproduct(true);
									if (oLine.getChildren() == null) {
										oLine.setChildren(new ArrayList<TicketLineInfo>());
									}
									oLine.getChildren().add(ticketLine);
									m_oTicket.insertLine(++index, ticketLine);
									// Pintamos la linea en la vista...
									m_ticketlines.insertTicketLine(index, ticketLine);

									// TODO cas des remises ?
									// delta++;
								}
							}
						}
						
						//EDU 2021-11 
						//Gérer le cas du passage en commande ( ou le contraire sur une offre à 1 seul élément)
						//On force ainsi la même action sur la ligne parent.
						if(oLine.getParent() != null && oLine.getParent().getChildren() != null && oLine.getParent().getChildren().size() == 1) {
							oLine.getParent().setType(oLine.getType());
						}

					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					// si index est une ligne dont le produit est un standard RAS -> cas standard
					// si index est une ligne dont le produit est un composant
					// on vire la ligne du produit maitre (avant index) et on passe toutes les lignes de composant en lignes normales -> cas
					// standard

				}
			}

			// cas standard -> on ramène notre ticket à une collection de produits vendus au prix normal
			// au cas d'un retour ticketLine.getMultiply() < 0 on fait rien
			i = -1;
			HashMap<String,Double> mapLotsRemises = new HashMap<String,Double>(); // ça permet de garder les remise appliquer à un lot pour le réapliquer ou cas ou il dera toujours present aprés updates lots
			
			while (++i < m_oTicket.getLinesCount()) {
				TicketLineInfo ticketLine = m_oTicket.getLine(i);
				//final int order = ticketLine.getM_iLine();
				
				if (ticketLine.getChildren() != null && ticketLine.getChildren().size() > 0 && ( (isRemoval && i==(index-1)) || ticketLine.isChildQuantityAnInt()) && ticketLine.getMultiply()>0) {
					if (ticketLine.hasDiscount()){
						mapLotsRemises.put(ticketLine.getProductID(), ticketLine.getDiscountRate());
					}
					// TODO un petit test pour voir si ristourne accordée et si modif dans les éléments ?
					m_ticketlines.removeTicketLine(i);
					m_oTicket.removeLine(i--);
				}
				if (ticketLine.getParent() != null && ticketLine.getParent().isChildQuantityAnInt() && ticketLine.getParent().getMultiply()>0) {
					ticketLine.setParent(null);
					ticketLine.setSubproduct(false);
					m_ticketlines.removeTicketLine(i);
					m_ticketlines.insertTicketLine(i, ticketLine);
				}
			}

			// Danger le vendeur a fait une ristourne sur une offre ... que faisons nous ?
			// une ligne avec réduction n'est jamais remise en cause ! sauf suppression !

			// Maintenant on check ce qu'on peut faire

			// On fait le tour des lignes présentes :
			//
			// TODO
			// Une ligne qui est une promo n'est là que parce qu'il a été jugé bon de la conserver dans le passé
			// Probablement car elle bénéficie d'une réduction - Elle est conservée avec toutes ses dépendences
			//
			//TODO 
			// On ne doit pas remettre en cause une ligne avec une commande
			// Ref 1 -> réduction
			//Ref 1 -> on ne cumule pas
			//
			// Ref 1 -> commande
			//Ref 1 -> on ne cumule pas			
			
		
			i = -1;
			singleOffers = new ArrayList<TicketLineInfo>();
			severalOffers = new ArrayList<TicketLineInfo>();
			singleOffersProducts = new ArrayList<ProductInfoExt>();
			severalOffersProducts = new ArrayList<ProductInfoExt>();

			while (++i < m_oTicket.getLinesCount()) {
				TicketLineInfo ticketLine = m_oTicket.getLine(i);
				try {
					composedProduct = dlSales.getProductInfo(ticketLine.getProductID());
				} catch (BasicException e) {
					composedProduct = null;
				}
				if (!ticketLine.hasDiscount() && !ticketLine.isSubproduct() && composedProduct != null
						&& composedProduct.getParents() != null /*&& !ticketLine.isOrder()*/) {
					if (composedProduct.getParents().size() == 1) {
						// Une ligne qui a un unique promo potentielle est stocké dans une liste dédiée - L1
						singleOffers.add(ticketLine);
						singleOffersProducts.add(composedProduct);
						m_ticketlines.removeTicketLine(i);
						m_oTicket.removeLine(i--);
					}
					if (composedProduct.getParents().size() > 1) {
						// Une ligne qui peut intégrer plusieur promos différentes est stockée dans une dernière liste - L2
						severalOffers.add(ticketLine);
						severalOffersProducts.add(composedProduct);
						m_ticketlines.removeTicketLine(i);
						m_oTicket.removeLine(i--);
					}

				}

				// Une ligne qui n'a pas de promo potentielle est conservée de suite
				// Ou qui a été manuellement promotionnée
				// Ou qui est un sous produit - cas de la promo avec rabais
				// Ou qui a une version composite fractionnaire

			}

			while (goOn) {
				eco += addEvidentOffers(singleOffers, singleOffersProducts, severalOffers, severalOffersProducts);
				
				if (balanced == null) {
					balanced = new ArrayList<ArrayList<TicketLineInfo>>();
				} else {
					if (balanced.size() == 0) {
						goOn = false;
						// On est dans un cas de bascule - tout est retournée en L2
					}
				}

				if (goOn) {
					// On fait le tour de L2 - On vire toutes les promos ne pouvant pas être réalisées avec les produits qui restent
					goOn = purgeSeveralOffers(singleOffers, singleOffersProducts, severalOffers, severalOffersProducts, balanced);
				}

				// Si on a enlevé des promos
				// On injecte les lignes de L2 sans promos dans la liste
				// On injecte les lignes de L2 avec une unique promo dans le test précédent
				// On boucle
			}
			// Si on n'a pas enlevé de promos - ou si on détecte une boucle
			// détection boucle - grâce à la liste des bascules

			// ça se corse - normalement il ne devrait plus y avoir trop de cas on les teste tous ?
			// et compare l'économie réalisée pour choisir le meilleur
			// Algo naïf = non optimal : on commence par la promo qui a la plus grosse économie
			eco += computeComplexOffers(singleOffers, singleOffersProducts, severalOffers, severalOffersProducts);
			// On n'oublie pas de faire le tour des prix à la fin !
			// une fonction de MàJ doit exister ça se remet en place sur une mini manip
			// ( ajout autre produit par exemple)
			// m_ticketlines.setSelectedIndex(delta);

			m_oTicket.setOffersEconomy(eco);
			i = -1;
			while (++i < m_oTicket.getLinesCount()) {
				TicketLineInfo ticketLine = m_oTicket.getLine(i);
				if (mapLotsRemises.containsKey(ticketLine.getProductID())){
					ticketLine.setDiscountRate(mapLotsRemises.get(ticketLine.getProductID()));
				}
			}
			refreshTicket();
			
		}

	}

	/**
	 * Il reste toutes les lignes qu'on n'a pas pu gérer de manière évidente En particulier celles pour lesquelles il va falloir faire un
	 * choix sur l'offre à appliquer
	 * 
	 * @param singleOffers
	 * @param singleOffersProducts
	 * @param severalOffers
	 * @param severalOffersProducts
	 * @return l'économie réalisée
	 */
	private double computeComplexOffers(List<TicketLineInfo> singleOffers, List<ProductInfoExt> singleOffersProducts,
			List<TicketLineInfo> severalOffers, List<ProductInfoExt> severalOffersProducts) {
		double eco = 0D, tmpEco;
		int nbOffers;
		ProductInfoExt candidateOffer = null;
		List<Integer> candidatesFromSingle, candidatesFromSeveral;
		ArrayList<TicketLineInfo> tmpTicket = new ArrayList<TicketLineInfo>();
		ArrayList<TicketLineInfo> ticket = new ArrayList<TicketLineInfo>();
		ArrayList<ProductInfoExt> candidatesOffers = new ArrayList<ProductInfoExt>();
		List<TicketLineInfo> tmpSingleOffers = new ArrayList<TicketLineInfo>();
		List<TicketLineInfo> tmpSeveralOffers = new ArrayList<TicketLineInfo>();
		List<TicketLineInfo> newSingleOffers = new ArrayList<TicketLineInfo>();
		List<TicketLineInfo> newSeveralOffers = new ArrayList<TicketLineInfo>();
		List<Double> multiplyList = new ArrayList<Double>();
		ProductInfoExt[] offersTested;
		Permutations<ProductInfoExt> permutations;

		// On commence par éliminer le cas simple - rien à faire
		if (severalOffers.size() != 0 || singleOffers.size() != 0) {
			// Le plus simple est de comparer toutes les possibilités
			// On commence par lister toutes les offres potentielles avec les produits impliqués
			// On va stocker les retours
			for (ProductInfoExt singleProduct : singleOffersProducts) {
				candidatesFromSingle = new ArrayList<Integer>();
				candidatesFromSeveral = new ArrayList<Integer>();
				try {
					candidateOffer = dlSales.getProductInfo(singleProduct.getParents().get(0).getParentId()) ;
					nbOffers = candidateOffer == null ? 0 : getNbOffers(singleOffers , severalOffers ,
							candidateOffer, candidatesFromSingle , candidatesFromSeveral );
					if(nbOffers != 0 && !candidatesOffers.contains(candidateOffer)) {
						candidatesOffers.add(candidateOffer);
						// TODO On en stocke plus ?
					}
				} catch (BasicException e1) {
					// Ne devrait jamais pouvoir arriver
					candidateOffer = null;
					nbOffers = 0;
				}
			}
			for (ProductInfoExt singleProduct : severalOffersProducts) {
				for (CompositionInfo composition : singleProduct.getParents()) {
					candidatesFromSingle = new ArrayList<Integer>();
					candidatesFromSeveral = new ArrayList<Integer>();
					try {
						candidateOffer = dlSales.getProductInfo(composition.getParentId()) ;
						nbOffers = candidateOffer ==null ? 0 : getNbOffers(singleOffers , severalOffers ,
								candidateOffer, candidatesFromSingle , candidatesFromSeveral );
						if(nbOffers != 0 && !candidatesOffers.contains(candidateOffer)) {
							candidatesOffers.add(candidateOffer);
							// TODO On en stocke plus ?
						}
					} catch (BasicException e1) {
						// Ne devrait jamais pouvoir arriver
						candidateOffer = null;
						nbOffers = 0;
					}
				}
			}
			// On va tout tester !
			// Un cas typique : Offre 1 : A + B = -5€
			// Offre 2 : A * 2 = -6€
			// On a 2 elements de A et 2 éléments de B ?
			// On a 6 éléméents de A et 1 de B ?

			// ATTENTION!! on a n! cas à traiter ! où n est le nombre de promos potentielles !
			// A partir de 8 (40320 cas) ça me semble trop lourd

			// TODO il pourra être utile de revenir dessus pour tenter de scinder en sous ensembles indépendants
			// mieux vaut 3! + 3! + 2! = 18 cas que 8! = 40320 cas
			if (candidatesOffers != null && candidatesOffers.size() > 0) {
				offersTested = (ProductInfoExt[]) candidatesOffers.toArray(new ProductInfoExt[candidatesOffers.size()]);
				permutations = Permutations.create(
						(ProductInfoExt[]) candidatesOffers.toArray(new ProductInfoExt[candidatesOffers.size()]), offersTested);
				while (permutations.next()) {
					// pour chaque combinaison on regarde le 'rendement' - eco réalisée
					tmpSingleOffers.clear();
					tmpSingleOffers.addAll(singleOffers);
					tmpSingleOffers.addAll(newSingleOffers);
					newSingleOffers.clear();
					tmpSeveralOffers.clear();
					tmpSeveralOffers.addAll(severalOffers);
					tmpSeveralOffers.addAll(newSeveralOffers);
					newSeveralOffers.clear();
					tmpTicket.clear();
					tmpEco = appliquePromos(offersTested, tmpSingleOffers, tmpSeveralOffers, tmpTicket, newSingleOffers, newSeveralOffers);
					// si le rendement est meilleurs que le dernier score on conserve.
					if (tmpEco > eco) {
						eco = tmpEco;
						ticket.clear();
						ticket.addAll(tmpSingleOffers);
						ticket.addAll(tmpSeveralOffers);
						ticket.addAll(tmpTicket);
						// gérer le cas où un split va impacter les quantités
						multiplyList.clear();
						for (int i = 0; i < ticket.size(); i++) {
							TicketLineInfo tmpTicketLine = ticket.get(i);
							multiplyList.add(tmpTicketLine.getMultiply());
						}
					}
				}

				// On ajoute la meilleure solution à notre ticket
				if (eco != 0) {
					for (int i = 0; i < ticket.size(); i++) {
						TicketLineInfo tmpTicketLine = ticket.get(i);
						tmpTicketLine.setParent(null);
						tmpTicketLine.setSubproduct(false);
					}
					for (int i = 0; i < ticket.size(); i++) {
						TicketLineInfo tmpTicketLine = ticket.get(i);
						tmpTicketLine.setMultiply(multiplyList.get(i));
						// les parents ont pu bouger et ne plus refléter la bonne organisation
						// ce qui n'est pas possible pour les enfant des produits composés
						if (tmpTicketLine.getChildren() != null && tmpTicketLine.getChildren().size() != 0) {
							for (TicketLineInfo childTicketLine : tmpTicketLine.getChildren()) {
								childTicketLine.setParent(tmpTicketLine);
								childTicketLine.setSubproduct(true);
							}
						}
						if (!mergeLine(tmpTicketLine, false)) {
							m_oTicket.addLine(tmpTicketLine);
							m_ticketlines.addTicketLine(tmpTicketLine, m_App.getAppUserView().getUser());
						}
					}
				}
			}
		}
		return eco;
	}

	/**
	 * Applique toutes les promos/offres présentes dans la liste dans le même ordre
	 * 
	 * @param offersTested
	 *            - liste des offres à tester
	 * @param singleOffers
	 *            - liste des lignes pouvant concourrir à une seule offre
	 * @param severalOffers
	 *            - liste des lignes pouvant concourrir à plusieurs offres
	 * @param tmpTicket
	 *            - le ticket que ça donnerait
	 * @return l'économie réalisée
	 */
	private double appliquePromos(ProductInfoExt[] offersTested, List<TicketLineInfo> singleOffers, List<TicketLineInfo> severalOffers,
			ArrayList<TicketLineInfo> tmpTicket, List<TicketLineInfo> newSingleOffers, List<TicketLineInfo> newSeveralOffers) {
		double eco = 0D;
		for (int index = 0; index < offersTested.length; index++) {
			eco += appliquePromo(offersTested[index], singleOffers, severalOffers, tmpTicket, newSingleOffers, newSeveralOffers);
		}
		return Math.abs(eco);
	}

	/**
	 * Applique l'offre
	 * 
	 * @param productInfoExt
	 *            - l'offre à tester
	 * @param singleOffers
	 *            - liste des lignes pouvant concourrir à une seule offre
	 * @param severalOffers
	 *            - liste des lignes pouvant concourrir à plusieurs offres
	 * @param tmpTicket
	 *            - le ticket que ça donnerait
	 * @param newSeveralOffers
	 *            - les nouvelles lignes issues d'une décomposition / split dans single
	 * @param newSingleOffers
	 *            - les nouvelles lignes issues d'une décomposition / split dans several
	 * @return l'économie réalisée
	 */
	private double appliquePromo(ProductInfoExt candidateOffer, List<TicketLineInfo> singleOffers, List<TicketLineInfo> severalOffers,
			ArrayList<TicketLineInfo> tmpTicket, List<TicketLineInfo> newSingleOffers, List<TicketLineInfo> newSeveralOffers) {
		double eco = 0D, noOfferPrice, newPrice;
		List<Integer> candidatesFromSingle, candidatesFromSeveral;
		TicketLineInfo ticketLine, tmpTicketLine;
		int nbOffers, j;
		candidatesFromSingle = new ArrayList<Integer>();
		candidatesFromSeveral = new ArrayList<Integer>();
		nbOffers = getNbOffers(singleOffers, severalOffers, candidateOffer, candidatesFromSingle, candidatesFromSeveral);

		// Aucune possibilité de placer cette offre -> on ne change rien aux entrées -
		// tous les produits demeurent dispo pour la suite
		if (nbOffers != 0) {

			// TODO Voir si ça vaut le coup de tout sortir dans une fonction - 2 appels un brin différents
			// ici et dans addEvidentOffers mais là bas ce n'est pas exactement pareil
			// on envoie directement dans le ticket - ça pourrait passer en paramètre
			// on envoi le surplus dans le ticket - ça peut changer
			noOfferPrice = 0;
			newPrice = 0;
			for (Integer index : candidatesFromSingle) {
				noOfferPrice += singleOffers.get(index).getValue();
			}
			for (Integer index : candidatesFromSeveral) {
				noOfferPrice += severalOffers.get(index).getValue();
			}
			// On prépare notre offre
			ticketLine = createTicketLine(candidateOffer, nbOffers, candidateOffer.getPriceSell());
			checkLinePrice(ticketLine);
			// Par défaut c'est une commande - Si un seul composant ne l'est pas ça n'en est plus
			ticketLine.setType(TicketLineInfo.ORDER);

			for (j = 0; j < candidateOffer.getChildren().size(); j++) {
				// On fait le tour de tous les composants à trouver
				CompositionInfo element = candidateOffer.getChildren().get(j);
				double multiply = element.getQuantity() * nbOffers;

				for (Integer index : candidatesFromSingle) {
					// On fait le tour de toutes les lignes pour déplacer les composants
					tmpTicketLine = singleOffers.get(index);
					if (tmpTicketLine.getProductID().equals(element.getChildId())) {
						// si multiply == 0 on laisse la ligne dispo pour la suite
						if (multiply == 0) {
							// On a déjà ajouté ce qu'il fallait pour ce composant !
							newPrice += tmpTicketLine.getValue();
							// On ren la ligne de nouveau dispo
							singleOffers.add(tmpTicketLine);
						} else {
							// On va en ajouter un peu
							multiply -= tmpTicketLine.getMultiply();
							tmpTicketLine.setSubproduct(true);
							tmpTicketLine.setParent(ticketLine);
							ticketLine.setType((ticketLine.isOrder() && tmpTicketLine.isOrder() ? TicketLineInfo.ORDER
									: TicketLineInfo.SALE));
							ticketLine.addChild(tmpTicketLine);
							if (multiply * nbOffers < 0) {
								// ... un peu trop parfois !
								// On cree une ligne du delta
								tmpTicketLine = tmpTicketLine.splitTicketLine(multiply * -1);
								tmpTicketLine.setSubproduct(false);
								newPrice += tmpTicketLine.getValue();
								// EDU Il faut l'ajouter convenablement pour qu'elle demeure dispo pour la suite
								singleOffers.add(tmpTicketLine);
								newSingleOffers.add(tmpTicketLine);
								multiply = 0;
							}
						}
					}
				}
				// Several
				for (Integer index : candidatesFromSeveral) {
					// On fait le tour de toutes les lignes pour déplacer les composants
					tmpTicketLine = severalOffers.get(index);
					if (tmpTicketLine.getProductID().equals(element.getChildId())) {
						// si multiply == 0 on laisse la ligne dispo pour la suite
						if (multiply == 0) {
							// On a déjà ajouté ce qu'il fallait pour ce composant !
							newPrice += tmpTicketLine.getValue();
							// On ren la ligne de nouveau dispo
							severalOffers.add(tmpTicketLine);
						} else {
							// On va en ajouter un peu
							multiply -= tmpTicketLine.getMultiply();
							tmpTicketLine.setSubproduct(true);
							tmpTicketLine.setParent(ticketLine);
							ticketLine.setType((ticketLine.isOrder() && tmpTicketLine.isOrder() ? TicketLineInfo.ORDER
									: TicketLineInfo.SALE));
							ticketLine.addChild(tmpTicketLine);
							if (multiply * nbOffers < 0) {
								// ... un peu trop parfois !
								// On cree une ligne du delta
								tmpTicketLine = tmpTicketLine.splitTicketLine(multiply * -1);
								tmpTicketLine.setSubproduct(false);
								newPrice += tmpTicketLine.getValue();
								// EDU Il faut l'ajouter convenablement pour qu'elle demeure dispo pour la suite
								severalOffers.add(tmpTicketLine);
								newSeveralOffers.add(tmpTicketLine);
								multiply = 0;
							}
						}
					}
				}
			}
			Collections.sort(candidatesFromSingle);
			// Ne pas oublier de virer les entrées traitées
			for (int deltaLines = 0; deltaLines < candidatesFromSingle.size(); deltaLines++) {
				singleOffers.remove(candidatesFromSingle.get(deltaLines) - deltaLines);
			}
			Collections.sort(candidatesFromSeveral);
			// Ne pas oublier de virer les entrées traitées
			for (int deltaLines = 0; deltaLines < candidatesFromSeveral.size(); deltaLines++) {
				severalOffers.remove(candidatesFromSeveral.get(deltaLines) - deltaLines);
			}

			newPrice += ticketLine.getValue();
			tmpTicket.add(ticketLine);
			for (TicketLineInfo childTicketLine : ticketLine.getChildren()) {
				tmpTicket.add(childTicketLine);
			}
			eco += noOfferPrice - newPrice;

		}

		return eco;
	}

	/**
	 * Purger la liste des candidats à de multiples offres en ne conservant que celles qu'il est possible de valider Et déplaçant ceux qui
	 * ne seraient plus candidat qu'à une unique offre dans la iste adéquate
	 * 
	 * @param singleOffers
	 * @param singleOffersProducts
	 * @param severalOffers
	 * @param severalOffersProducts
	 * @param balanced
	 *            la liste des mouvements lors du dernier passage - vidée si on rejoue la même partition
	 * @return true si il y a eu du mouvement
	 */
	private boolean purgeSeveralOffers(List<TicketLineInfo> singleOffers, List<ProductInfoExt> singleOffersProducts,
			List<TicketLineInfo> severalOffers, List<ProductInfoExt> severalOffersProducts, ArrayList<ArrayList<TicketLineInfo>> balanced) {
		int i = -1, number = severalOffers.size(), nbOffers, j;
		List<Integer> candidatesFromSingle, candidatesFromSeveral;
		ProductInfoExt candidateOffer = null;
		ArrayList<TicketLineInfo> tmpBalanced = new ArrayList<TicketLineInfo>();

		while (++i < severalOffersProducts.size()) {
			j = -1;
			while (++j < severalOffersProducts.get(i).getParents().size()) {
				candidatesFromSingle = new ArrayList<Integer>();
				candidatesFromSeveral = new ArrayList<Integer>();
				try {
					candidateOffer = dlSales.getProductInfo(severalOffersProducts.get(i).getParents().get(j).getParentId());
					nbOffers = getNbOffers(singleOffers, severalOffers, candidateOffer, candidatesFromSingle, candidatesFromSeveral);
				} catch (BasicException e1) {
					// Ne devrait jamais pouvoir arriver
					candidateOffer = null;
					nbOffers = 0;
				}

				// On vire une offre potentielle de la liste ... mais on ne vire pas la dernière
				// ainsi on en a toujours au moins une et ça peut de nouveaux passer par la case
				// insertion évidente.
				if ((nbOffers == 0 || severalOffersProducts.get(i).getParents().get(j).getQuantity() < 1)
						&& severalOffersProducts.get(i).getParents().size() > 1) {
					severalOffersProducts.get(i).getParents().remove(j--);
				}
			}
			// il n'y a plus qu'une possibilité - on bascule
			// TODO gérer les bascules circulaires - un ligne qui n'a qu'un possibilité
			// mais colle avec une autre qui en a plusieurs et toues les deux sont ok
			// la première va descendre en L2 puis aussitôt remonter en L1 et ça boucle
			if (j == 1) {
				singleOffersProducts.add(severalOffersProducts.get(i));
				singleOffers.add(severalOffers.get(i));
				tmpBalanced.add(severalOffers.get(i));
				severalOffersProducts.remove(i);
				severalOffers.remove(i--);
			}
		}

		// Gestion des bascules
		if(balanced.size() > 0) {
			for(i = 0 ; i< balanced.size(); i++) {
				ArrayList<TicketLineInfo> elemBalanced = balanced.get(i);
				if (tmpBalanced.size() == elemBalanced.size()) {
					// On a le même nombre d'éléments qu'une des dernières fois c'est suspect.
					if (tmpBalanced.containsAll(elemBalanced)) {
						//si tous les éléments sont communs c'est qu'on a déjà rencontré ce cas de figure
						balanced.clear();
					}
				}
			}
			if(balanced.size() > 0) {
				balanced.add(tmpBalanced);
			}
		} else {
			balanced.add(tmpBalanced);
		}

		return (number != severalOffers.size() ? true : false);
	}

	/**
	 * Une fonction qui gère le cas des insertions evidentes de promos.
	 * 
	 * @param singleOffers
	 *            liste des lignes n'ayant qu'une offre potentielle
	 * @param singleOffersProducts
	 *            liste des produits concernés
	 * @param severalOffers
	 *            liste de lignes pouvant aller dans plusieurs offres
	 * @param severalOffersProducts
	 *            liste de produits concernés On fait le tour de L1 Si la promo concernée peut être complétée Si c'est uniquement avec
	 *            d'autres lignes de L1 On vérifie que le prix total (en prenant les lignes non promotionnées en priorité) est inférieur à
	 *            la somme des prix des composants Si toujours OK on ajoute la promo - et stocke tant qu'à faire la réduction réalisée Et
	 *            passe toutes les lignes comme dépendances Sinon on ajoute la ligne telle quelle - une seule ligne si elle bénéficie d'une
	 *            promo Toutes sinon Sinon on ajoute les lignes de L1 entrant dans la compo à L2 Sinon (Promo non-complétable) Toutes les
	 *            lignes de L1 entrant potentiellement dans la compo de cette promo sont ajoutées telles quelles
	 **
	 **
	 **            Note : Cas d'une remise salarié ? il y a une promo mais potentiellement promo applicquable sur tout = Même les autres
	 *            promos !
	 * 
	 *            Note 2 : Dans la première version, on ne remet pas en cause une réductions appliquée manuellement
	 */
	private double addEvidentOffers(List<TicketLineInfo> singleOffers, List<ProductInfoExt> singleOffersProducts,
			List<TicketLineInfo> severalOffers, List<ProductInfoExt> severalOffersProducts) {
		int i = -1, nbOffers, deltaLines, j;
		double eco = 0, noOfferPrice, newPrice , childCount;
		TicketLineInfo ticketLine, tmpTicketLine;
		List<Integer> candidatesFromSingle, candidatesFromSeveral;
		ProductInfoExt candidateOffer = null;

		while (++i < singleOffersProducts.size()) {
			candidatesFromSingle = new ArrayList<Integer>();
			candidatesFromSeveral = new ArrayList<Integer>();
			try {
				candidateOffer = dlSales.getProductInfo(singleOffersProducts.get(i).getParents().get(0).getParentId()) ;
				nbOffers = candidateOffer == null ? 0 : getNbOffers(singleOffers , severalOffers ,
						candidateOffer, candidatesFromSingle , candidatesFromSeveral );
				if(candidateOffer == null) {
					candidatesFromSingle.add(i);
				}
			} catch (BasicException e1) {
				// Ne devrait jamais pouvoir arriver
				// Et si ...... :-(
				// AME : BUG du 06/08/2015, sur ajout produit 2810, on check les offres, ce qui est normal
				// en revanche il trouve qu'il y a une offre avec avec le 8069 (...) or le 8069 est a le statut supprimé,
				// donc il ne remonte pas sur le POS... donc productInfoExt est nulll
				// donc nullPointerException ...  -> non catcher par BasicException :-(
				// Pour fixer le bug, j'ai pas fait un simple test sur productInfoExt, car effectivement, ca cas ne devrait pas se
				// produire mais j'ai initialisé candidatesFromSingle pour pouvoir rajouter le produit dans le ticket
				// Il faut par contre se poser au moins 2 questions:
				// - pourquoi 2810 ressort une offre qui n'est plus valable dans le temps (car produit supprimé), pour historique ticket ??? 
				// peut etre avoir une liste des offres, et une liste des offres actives ... pas de sens en cas de syncho des data du cache
				// de la caisse
				// - la durée de vie des caches de la caisse, en effet sur Andrezieux, il n'y a pas de plantage, car le produit 8069
				// est sur la caisse, après avoir vidé le cache, il n'y est plus...
				candidatesFromSingle.add(0);
				candidateOffer = null;
				nbOffers = 0;
			}

			//TODO ajuster les critères d'élimination des comositions fractionnaires
			//Ma compo = 1/2 du produit
			//En plus le prix est supérieur du coup
			//Ainsi que des compos unitaires - une ref avec remise pour pouvoir faire une offre 1 acheté 1 gratuit avec deux produits
			//de différente taille
			// on avait ajouté un malheureux 
			//|| ( singleOffersProducts.get(i).getParents().get(0).getQuantity() == 1 && singleOffersProducts.get(i).getParents().size() == 1)
			//en fait il faut retrouver le nombre d'enfants pour le premier parent.
			try {
				childCount = CatalogCache.getProduct(singleOffersProducts.get(i).getParents().get(0).getParentId()).countChildren() ;
			} catch (BasicException e) {
				// TODO Auto-generated catch block
				childCount = 0 ;
			}
			Collections.sort(candidatesFromSingle);
			
			if(nbOffers == 0 || singleOffersProducts.get(i).getParents().get(0).getQuantity() < 1 
					||  childCount< 2D ) {

				deltaLines = 0;
				
				for (Integer index : candidatesFromSingle) {
					tmpTicketLine = singleOffers.get(index - deltaLines);
					if (!mergeLine(tmpTicketLine, false)) {
						m_oTicket.addLine(tmpTicketLine);
						m_ticketlines.addTicketLine(tmpTicketLine , m_App.getAppUserView().getUser());
					}
					singleOffers.remove(index - deltaLines);
					singleOffersProducts.remove(index - deltaLines++);
				}
				// normalement c'est toujours vrai car la première ligne fait toujours partie de l'offre proposée
				if (deltaLines > 0) {
					i--;
				}
			} else {
				if (candidatesFromSeveral.size() == 0) {
					noOfferPrice = 0;
					newPrice = 0;
					for (Integer index : candidatesFromSingle) {
						noOfferPrice += singleOffers.get(index).getValue();
					}
					// On prépare notre offre
					ticketLine = createTicketLine(candidateOffer, nbOffers, candidateOffer.getPriceSell());



					checkLinePrice(ticketLine);
					// Par défaut c'est une commande - Si un seul composant ne l'est pas ça n'en est plus
					ticketLine.setType(TicketLineInfo.ORDER);
					for (j = 0; j < candidateOffer.getChildren().size(); j++) {
						// On fait le tour de tous les composants à trouver
						CompositionInfo element = candidateOffer.getChildren().get(j);
						double multiply = element.getQuantity() * nbOffers;
						for (Integer index : candidatesFromSingle) {
							// On fait le tour de toutes les lignes pour déplacer les composants
							tmpTicketLine = singleOffers.get(index);
							if (tmpTicketLine.getProductID().equals(element.getChildId())) {
								if (multiply == 0) {
									// On a déjà ajouté ce qu'il fallait pour ce composant !
									// On envoi toute cette ligne dans le ticket
									newPrice += tmpTicketLine.getValue();
									if (!mergeLine(tmpTicketLine, false)) {
										m_oTicket.addLine(tmpTicketLine);
										m_ticketlines.addTicketLine(tmpTicketLine, m_App.getAppUserView().getUser());
									}

								} else {
									// On va en ajouter un peu
									multiply -= tmpTicketLine.getMultiply();
									tmpTicketLine.setSubproduct(true);
									tmpTicketLine.setParent(ticketLine);
									ticketLine.setType((ticketLine.isOrder() && tmpTicketLine.isOrder() ? TicketLineInfo.ORDER
											: TicketLineInfo.SALE));
									ticketLine.addChild(tmpTicketLine);
									if (multiply * nbOffers < 0) {
										// ... un peu trop parfois !
										// On cree une ligne du delta
										tmpTicketLine = tmpTicketLine.splitTicketLine(multiply * -1);
										tmpTicketLine.setSubproduct(false);
										newPrice += tmpTicketLine.getValue();
										if (!mergeLine(tmpTicketLine, false)) {
											m_oTicket.addLine(tmpTicketLine);
											m_ticketlines.addTicketLine(tmpTicketLine, m_App.getAppUserView().getUser());
										}
										multiply = 0;
									}
								}
							}
						}
					}

					// Ne pas oublier de virer les entrées traitées
					//Collections.sort(candidatesFromSingle);

					for (deltaLines = 0; deltaLines < candidatesFromSingle.size(); deltaLines++) {
						singleOffers.remove(candidatesFromSingle.get(deltaLines) - deltaLines);
						singleOffersProducts.remove(candidatesFromSingle.get(deltaLines) - deltaLines);
					}
					if (deltaLines > 0) {
						i--;
					}

					newPrice += ticketLine.getValue();

					// normalement pas beasoin de merge... je ne peux pas passer plusieurs fois sur la même promo
					m_oTicket.addLine(ticketLine);
					m_ticketlines.addTicketLine(ticketLine, m_App.getAppUserView().getUser());
					for (TicketLineInfo childTicketLine : ticketLine.getChildren()) {
						if (!mergeLine(childTicketLine, false)) {
							m_oTicket.addLine(childTicketLine);
							m_ticketlines.addTicketLine(childTicketLine, m_App.getAppUserView().getUser());
						}
					}
					eco += noOfferPrice - newPrice;

				} else {
					// TODO on peut faire mieux : plutôt que de tout basculer, ne basculer que ce qui pourrait entrer dans une compo
					deltaLines = 0;
					for (Integer index : candidatesFromSingle) {
						severalOffers.add(singleOffers.get(index - deltaLines));
						severalOffersProducts.add(singleOffersProducts.get(index - deltaLines));
						singleOffers.remove(index - deltaLines);
						singleOffersProducts.remove(index - deltaLines++);
					}
				}
			}
		}
		return eco;
	}

	/**
	 * Obtenir le nombre d'offre qu'on peut compléter avec la liste de produits disponibles
	 * 
	 * @param singleOffers
	 *            liste des lignes de ventes à offre potentielle unique
	 * @param severalOffers
	 *            liste des lignes de ventes à offres potenitelles multiples
	 * @param productInfoExt
	 *            l'offre qu'on essaie de satisfaire
	 * @param candidatesFromSingle
	 *            la liste des lignes qu'on va prendre dans la liste 1
	 * @param candidatesFromSeveral
	 *            la liste des lignes qu'on va prendre dans la liste 2
	 * @return 0 si on ne peut pas compléter une offre - le nombre d'unité qu'on peut faire sinon
	 * 
	 *         Là encore on va perdre un peu de place en embarquant la liste des produits - ça évite des requêtes complémentaires dans le
	 *         cache en disposant déjà des objets de decription des produits
	 * 
	 *         Attention au cas des produits + - : une reprise et une vente !
	 */
	private int getNbOffers(List<TicketLineInfo> singleOffers, List<TicketLineInfo> severalOffers, ProductInfoExt productInfoExt,
			List<Integer> candidatesFromSingle, List<Integer> candidatesFromSeveral) {
		
		double  quantity = 0 , sign = (singleOffers == null || singleOffers.size() == 0 ? ( severalOffers == null || severalOffers.size() == 0 ? 1 
				: severalOffers.get(0).getMultiply() ) 
				: singleOffers.get(0).getMultiply()) , 
				tmpOffersNb = sign * -1 ;
		int index ;
		TicketLineInfo ticketLine ;
		
		
			for(CompositionInfo childComp : productInfoExt.getChildren()) {
				quantity = 0 ;
				for( index = 0 ; index < singleOffers.size() ; index++) {
					ticketLine = singleOffers.get(index);
					if(childComp.getChildId().equals(ticketLine.getProductID()) && sign * ticketLine.getMultiply() > 0) {
						quantity +=ticketLine.getMultiply();
						candidatesFromSingle.add(index);
					}
				}
				for( index = 0 ; index < severalOffers.size() ; index++) {
					ticketLine = severalOffers.get(index);
					if(childComp.getChildId().equals(ticketLine.getProductID()) && sign * ticketLine.getMultiply() > 0) {
						quantity +=ticketLine.getMultiply();
						candidatesFromSeveral.add(index);
					}
				}
				if(productInfoExt.countChildren() > 1 ) {
					quantity /= childComp.getQuantity() ;
				} else {
					quantity = 0;
				}
				tmpOffersNb = (tmpOffersNb * sign < 0 ? quantity : sign/Math.abs(sign)*Math.min(Math.abs(tmpOffersNb), Math.abs(quantity))) ;
			}
		return (int) (tmpOffersNb * sign < 0 ? 0 : tmpOffersNb);
	}
	
	private void jButtonNumPadActionPerformed(java.awt.event.ActionEvent evt) {

	    setNumPadVisible(!numpadZone.isVisible());
	    
	}
	
    private void setNumPadVisible(boolean value) {
        
    	numpadZone.setVisible(value);
        assignNumPadButtonIcon();
        revalidate();
    }
    
    private void assignNumPadButtonIcon() {
    	jButtonNumPad.setIcon(numpadZone.isVisible()
                ? menu_close
                : menu_open);
    }

    public Object getM_oTicketExt() {
		return m_oTicketExt;
	}

	public void setM_oTicketExt(Object m_oTicketExt) {
		this.m_oTicketExt = m_oTicketExt;
	}

	private Icon menu_open;
    private Icon menu_close;
    private javax.swing.JButton jButtonNumPad;
    private javax.swing.JPanel numpadZone;
	private javax.swing.JButton btnCustomer;
	protected javax.swing.JButton btnSplit;
	private javax.swing.JPanel catcontainer;
	private javax.swing.JButton jEditAttributes;
	private javax.swing.JPanel lineBtnsContainer;
	private javax.swing.JPanel jPanel9;
	@SuppressWarnings("unused")
	private javax.swing.JPanel m_jButtons;
	private javax.swing.JPanel m_jButtonsExt;
	private javax.swing.JButton m_jDelete;
	private javax.swing.JButton m_jDown;
	private javax.swing.JButton m_jEditLine;
	private javax.swing.JButton m_jEnter;
	private javax.swing.JTextField m_jKeyFactory;
	private javax.swing.JButton m_jList;
	private JNumberKeys m_jNumberKeys;
	private javax.swing.JPanel m_jPanEntries;
	private javax.swing.JPanel m_jPanTotals;
	private javax.swing.JPanel m_jPanelBag;
	private javax.swing.JLabel m_jPor;
	private javax.swing.JLabel m_jPrice;
	private javax.swing.JComboBox<TaxCategoryInfo> m_jTax;
	private javax.swing.JLabel m_jTicketId;
	private javax.swing.JLabel m_jTotalEuros;
	private javax.swing.JLabel subtotalLabel;
	private javax.swing.JLabel ticketInfoLabel;
	private javax.swing.JLabel discountLabel;
	private javax.swing.JButton m_jUp;
	private javax.swing.JButton m_jOrder;
	private javax.swing.JToggleButton m_jaddtax;
	private javax.swing.JButton m_jbtnLineDiscount;
	private javax.swing.JButton m_jbtnLineFixedDiscount;
	protected javax.swing.JButton btnTicketDiscount;
	protected javax.swing.JButton btnTicketNote;
	private javax.swing.JPanel m_jInputContainer;
	private javax.swing.JComboBox<TariffInfo> m_jTariff;
	private javax.swing.JLabel clock;
	private javax.swing.JLabel connectedUser;
	private javax.swing.JTextArea messageBox;

	private javax.swing.JLabel locationName;

}
