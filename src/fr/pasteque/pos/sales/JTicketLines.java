//    POS-Tech
//    Based upon Openbravo POS
//
//    Copyright (C) 2007-2009 Openbravo, S.L.
//                       2012 Scil (http://scil.coop)
//
//    This file is part of POS-Tech.
//
//    POS-Tech is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    POS-Tech is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with POS-Tech.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.sales;

import fr.pasteque.beans.EncryptDecrypt;
import fr.pasteque.data.loader.LocalRes;
import fr.pasteque.data.user.OseToken;

import java.awt.Component;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.BorderLayout;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.io.StringReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.MouseInputListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import fr.pasteque.pos.scripting.ScriptEngine;
import fr.pasteque.pos.scripting.ScriptException;
import fr.pasteque.pos.scripting.ScriptFactory;
import fr.pasteque.pos.forms.AppConfig;
import fr.pasteque.pos.forms.AppLocal;
import fr.pasteque.pos.forms.AppUser;
import fr.pasteque.pos.ticket.TicketLineInfo;
import fr.pasteque.pos.widgets.WidgetsBuilder;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class JTicketLines extends javax.swing.JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6333219232377847216L;

	private static Logger logger = Logger.getLogger("fr.pasteque.pos.sales.JTicketLines");

	private static SAXParser m_sp = null;

	private TicketTableModel m_jTableModel;
	
	private AppUser appUser;


	/** Creates new form JLinesTicket */
	public JTicketLines(AppUser appUser) {
		this.appUser = appUser;
		initComponents();  
	}

	public void init(String ticketline) {
		ColumnTicket[] acolumns = new ColumnTicket[0];

		if (ticketline != null) {
			try {
				if (m_sp == null) {
					SAXParserFactory spf = SAXParserFactory.newInstance();
					m_sp = spf.newSAXParser();
				}
				ColumnsHandler columnshandler = new ColumnsHandler();
				m_sp.parse(new InputSource(new StringReader(ticketline)), columnshandler);
				acolumns = columnshandler.getColumns();

			} catch (ParserConfigurationException ePC) {
				logger.log(Level.WARNING, LocalRes.getIntString("exception.parserconfig"), ePC);
			} catch (SAXException eSAX) {
				logger.log(Level.WARNING, LocalRes.getIntString("exception.xmlfile"), eSAX);
			} catch (IOException eIO) {
				logger.log(Level.WARNING, LocalRes.getIntString("exception.iofile"), eIO);
			}
		}

		m_jTableModel = new TicketTableModel(acolumns);    
		m_jTicketTable.setModel(m_jTableModel);        

		//m_jTicketTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		TableColumnModel jColumns = m_jTicketTable.getColumnModel();
		for (int i = 0; i < acolumns.length; i++) {
			jColumns.getColumn(i).setPreferredWidth(acolumns[i].width);
			jColumns.getColumn(i).setResizable(false);
		}       

		m_jScrollTableTicket.getVerticalScrollBar().setPreferredSize(new Dimension(35, 35));
		TableCellRenderer defaultHeaderRenderer = m_jTicketTable.getTableHeader().getDefaultRenderer();
		m_jTicketTable.getTableHeader().setDefaultRenderer(new TicketHeaderRenderer(defaultHeaderRenderer, acolumns));
		m_jTicketTable.getTableHeader().setReorderingAllowed(false);         
		m_jTicketTable.setDefaultRenderer(Object.class, new TicketCellRenderer(acolumns));
		m_jTicketTable.setRowHeight(WidgetsBuilder.dipToPx(56));
		m_jTicketTable.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION); 
		m_jScrollTableTicket.invalidate();
		// reseteo la tabla...
		m_jTableModel.clear();
	}

	public void addListSelectionListener(ListSelectionListener l) {        
		m_jTicketTable.getSelectionModel().addListSelectionListener(l);
	}
	public void removeListSelectionListener(ListSelectionListener l) {
		m_jTicketTable.getSelectionModel().removeListSelectionListener(l);
	}

	public void clearTicketLines() {                   
		m_jTableModel.clear();
	}

	public void setTicketLine(int index, TicketLineInfo oLine){
		m_jTableModel.setRow(index, oLine); 
		setSelectedIndex(index);
	}

	public void addTicketLine(TicketLineInfo oLine , AppUser appUser) {
		this.appUser = appUser;
		m_jTableModel.addRow(oLine);

		// Selecciono la que acabamos de anadir.            
		setSelectedIndex(m_jTableModel.getRowCount() - 1);
	}    

	public void insertTicketLine(int index, TicketLineInfo oLine) {

		m_jTableModel.insertRow(index, oLine);

		// Selecciono la que acabamos de anadir.            
		setSelectedIndex(index);   
	}     
	public void removeTicketLine(int i){

		m_jTableModel.removeRow(i);

		// Escojo una a seleccionar
		if (i >= m_jTableModel.getRowCount()) {
			i = m_jTableModel.getRowCount() - 1;
		}

		if ((i >= 0) && (i < m_jTableModel.getRowCount())) {
			// Solo seleccionamos si podemos.
			setSelectedIndex(i);
		}
	}

	public void setSelectedIndex(int i){
		if (i>=0 && i<m_jTableModel.getRowCount()){
			// Seleccionamos
			m_jTicketTable.getSelectionModel().setSelectionInterval(i, i);

			// Hacemos visible la seleccion.
			Rectangle oRect = m_jTicketTable.getCellRect(i, 0, true);
			m_jTicketTable.scrollRectToVisible(oRect);
		}  
	}

	public javax.swing.JTable getM_jTicketTable() {
		return m_jTicketTable;
	}

	public int getSelectedIndex() {
		return m_jTicketTable.getSelectionModel().getMinSelectionIndex(); // solo sera uno, luego no importa...
	}

	public void selectionDown() {

		int i = m_jTicketTable.getSelectionModel().getMaxSelectionIndex();
		if (i < 0){
			i =  0; // No hay ninguna seleccionada
		} else {
			i ++;
			if (i >= m_jTableModel.getRowCount()) {
				i = m_jTableModel.getRowCount() - 1;
			}
		}

		if ((i >= 0) && (i < m_jTableModel.getRowCount())) {
			// Solo seleccionamos si podemos.

			setSelectedIndex(i);
		}
	}

	public void selectionUp() {

		int i = m_jTicketTable.getSelectionModel().getMinSelectionIndex();
		if (i < 0){
			i = m_jTableModel.getRowCount() - 1; // No hay ninguna seleccionada
		} else {
			i --;
			if (i < 0) {
				i = 0;
			}
		}

		if ((i >= 0) && (i < m_jTableModel.getRowCount())) {
			// Solo seleccionamos si podemos.
			setSelectedIndex(i);
		}
	}

	private static class TicketCellRenderer extends DefaultTableCellRenderer {

		/**
		 * 
		 */
		private static final long serialVersionUID = 5003368489702484940L;
		private ColumnTicket[] m_acolumns;        

		public TicketCellRenderer(ColumnTicket[] acolumns) {
			m_acolumns = acolumns;
		}

		@Override
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column){

			JLabel aux = (JLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
			WidgetsBuilder.setupLabel(aux, WidgetsBuilder.SIZE_MEDIUM);
			aux.setVerticalAlignment(javax.swing.SwingConstants.TOP);
			aux.setHorizontalAlignment(m_acolumns[column].align);
			return aux;
		}
	}

	private static class TicketHeaderRenderer extends DefaultTableCellRenderer {

		/**
		 * 
		 */
		private static final long serialVersionUID = -7159750895250486123L;
		private ColumnTicket[] m_acolumns;
		private TableCellRenderer baseRenderer;

		public TicketHeaderRenderer(TableCellRenderer baseRenderer, ColumnTicket[] acolumns) {
			this.baseRenderer = baseRenderer;
			m_acolumns = acolumns;
		}

		@Override
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
			JLabel aux = (JLabel) this.baseRenderer.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
			WidgetsBuilder.setupLabel(aux, WidgetsBuilder.SIZE_SMALL);
			aux.setVerticalAlignment(javax.swing.SwingConstants.TOP);
			aux.setHorizontalAlignment(m_acolumns[column].align);
			return aux;
		}
	}

	private static class TicketTableModel extends AbstractTableModel {

		/**
		 * 
		 */
		private static final long serialVersionUID = 2907602851338039646L;
		//        private AppView m_App;
		private ColumnTicket[] m_acolumns;
		private ArrayList<String[]> m_rows = new ArrayList<String[]>();
		private ArrayList<String> m_productsId = new ArrayList<String>();

		public TicketTableModel(ColumnTicket[] acolumns) {
			m_acolumns = acolumns;
		}
		public int getRowCount() {
			return m_rows.size();
		}
		public int getColumnCount() {
			return m_acolumns.length;
		}
		@Override
		public String getColumnName(int column) {
			return AppLocal.getIntString(m_acolumns[column].name);
			// return m_acolumns[column].name;
		}
		public Object getValueAt(int row, int column) {
			return ((String[]) m_rows.get(row))[column];
		}

		public String getProductIdAt(int row) {
			return m_productsId.get(row);
		}

		@Override
		public boolean isCellEditable(int row, int column) {
			return false;
		}

		public void clear() {
			int old = getRowCount();
			if (old > 0) { 
				m_rows.clear();
				m_productsId.clear();
				fireTableRowsDeleted(0, old - 1);
			}
		}

		public void setRow(int index, TicketLineInfo oLine){

			String[] row = (String []) m_rows.get(index);
			m_productsId.set(index, oLine.getProductID());
			for (int i = 0; i < m_acolumns.length; i++) {
				try {
					ScriptEngine script = ScriptFactory.getScriptEngine(ScriptFactory.VELOCITY);
					script.put("ticketline", oLine);
					row[i] = script.eval(m_acolumns[i].value).toString();
				} catch (ScriptException e) {
					row[i] = null;
				} 
				fireTableCellUpdated(index, i);
			}             
		}        

		public void addRow(TicketLineInfo oLine) {

			insertRow(m_rows.size(), oLine);
		}

		public void insertRow(int index, TicketLineInfo oLine) {

			String[] row = new String[m_acolumns.length];
			for (int i = 0; i < m_acolumns.length; i++) {
				try {
					ScriptEngine script = ScriptFactory.getScriptEngine(ScriptFactory.VELOCITY);
					script.put("ticketline", oLine);
					row[i] = script.eval(m_acolumns[i].value).toString();
				} catch (ScriptException e) {
					row[i] = null;
				}  
			} 

			m_rows.add(index, row);
			m_productsId.add(index, oLine.getProductID());
			fireTableRowsInserted(index, index);
		}

		public void removeRow(int row) {
			m_rows.remove(row);
			m_productsId.remove(row);
			fireTableRowsDeleted(row, row);
		}

	}

	private static class ColumnsHandler extends DefaultHandler {

		private ArrayList<ColumnTicket> m_columns = null;

		public ColumnTicket[] getColumns() {
			return (ColumnTicket[]) m_columns.toArray(new ColumnTicket[m_columns.size()]);
		}
		@Override
		public void startDocument() throws SAXException { 
			m_columns = new ArrayList<ColumnTicket>();
		}
		@Override
		public void endDocument() throws SAXException {}    
		@Override
		public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException{
			if ("column".equals(qName)){
				ColumnTicket c = new ColumnTicket();
				c.name = attributes.getValue("name");
				c.width = Integer.parseInt(attributes.getValue("width"));
				String sAlign = attributes.getValue("align");
				if ("right".equals(sAlign)) {
					c.align = javax.swing.SwingConstants.RIGHT;
				} else if ("center".equals(sAlign)) {
					c.align = javax.swing.SwingConstants.CENTER;
				} else {
					c.align = javax.swing.SwingConstants.LEFT;
				}
				c.value = attributes.getValue("value");
				m_columns.add(c);
			}
		}      
		@Override
		public void endElement(String uri, String localName, String qName) throws SAXException {}
		@Override
		public void characters(char[] ch, int start, int length) throws SAXException {}
	}

	private static class ColumnTicket {
		public String name;
		public int width;
		public int align;
		public String value;
	}

	private void initComponents() {
		setLayout(new BorderLayout());

		m_jScrollTableTicket = new javax.swing.JScrollPane();
		m_jTicketTable = new javax.swing.JTable();

		m_jScrollTableTicket.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		m_jScrollTableTicket.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		m_jTicketTable.setFocusable(false);
		m_jTicketTable.setIntercellSpacing(new java.awt.Dimension(0, 1));
		m_jTicketTable.setRequestFocusEnabled(false);
		m_jTicketTable.setShowVerticalLines(false);
		m_jScrollTableTicket.setViewportView(m_jTicketTable);
		m_jTicketTable.setPreferredScrollableViewportSize(new java.awt.Dimension(10,10));

		m_jTicketTable.addMouseListener(new MouseInputListener() {
			@Override
			public void mouseClicked(MouseEvent e) {

				if (e.getClickCount() == 2) {
					JTable target = (JTable)e.getSource();
					int row = target.getSelectedRow();
					TicketTableModel tTM = (TicketTableModel) target.getModel();
					//TODO Voir pour n'avoir la ref en dur qu'à un unique endroit ! (JProductFinder l'utilise aussi)
					//	Peut être rendre ceci paramétrable via les ressources côté serveur
					if (Desktop.isDesktopSupported()) {
						final Desktop dt = Desktop.getDesktop();


						String backOfficeUrl;
						try {
							backOfficeUrl = AppConfig.loadedInstance.getProperty("server.backoffice") + "/"+ AppConfig.loadedInstance.getProperty("server.racine") +"?token="+EncryptDecrypt.encrypt(new OseToken(appUser.getName()).toString())+"#/products/" + tTM.getProductIdAt(row) + "/form";
						} catch (Exception ex) {
							// TODO Auto-generated catch block
							ex.printStackTrace();
							backOfficeUrl = AppConfig.loadedInstance.getProperty("server.backoffice") + "/"+ AppConfig.loadedInstance.getProperty("server.racine") +"#/products/" + tTM.getProductIdAt(row) + "/form";
						}
						if (dt.isSupported(Desktop.Action.BROWSE)) {
							try {
								dt.browse(new URI(backOfficeUrl));
							} catch (URISyntaxException exc) {
								//
							} catch (IOException exc) {
								//
							}
						}
					}
				}
			}

			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseDragged(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseMoved(MouseEvent e) {
				// TODO Auto-generated method stub

			}
		});
		add(m_jScrollTableTicket, BorderLayout.CENTER);

	}

	private javax.swing.JScrollPane m_jScrollTableTicket;
	private javax.swing.JTable m_jTicketTable;
}
