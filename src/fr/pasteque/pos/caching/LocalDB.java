//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2007-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.caching;

import fr.pasteque.pos.forms.AppConfig;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/** Local database cache. Not intended for multithreading or multiuser. */
public class LocalDB {

    private static Logger logger = Logger.getLogger("fr.pasteque.pos.caching.LocalDB");
    private static final int VERSION = 9;

    private static Connection conn = null;

    private static String path() {
        return AppConfig.loadedInstance.getDataDir() + "/db_cache";
    }

    private LocalDB() {}

    private static Connection getConnection() throws SQLException {
        if (conn == null) {
            try {
                Class.forName("org.h2.Driver");
                String url = "jdbc:h2:" + path();
                conn = DriverManager.getConnection(url, "pasteque", "");
            } catch (ClassNotFoundException e) {
                // Should never happen
                logger.log(Level.SEVERE,
                        "Unable to run local cache database", e);
            }
        }
        return conn;
    }

    public static void init() throws SQLException {
        Connection c = getConnection();
        Statement stmt = c.createStatement();
        try {
            ResultSet rs = stmt.executeQuery("SELECT version FROM meta");
            // Update code
            if (rs.next()) {
            	
                int version = rs.getInt("version");
                switch (version) {
                case 1:
                    // Upgrade from version 1 to 2
                    stmt.execute("ALTER TABLE customers "
                            + "ALTER COLUMN number VARCHAR(255)");
                    stmt.execute("UPDATE meta SET version = 2");
                    // no break to keep upgrading until latest version
                case 2:
                    stmt.execute("CREATE TABLE insee ("
                    	+ "NUM_INSEE VARCHAR(64) NOT NULL, "
                    	+ "ZIPCODE VARCHAR(45) DEFAULT NULL, "
                    	+ "COMMUNE VARCHAR(255) DEFAULT NULL, "
                    	+ "LATITUDE DOUBLE DEFAULT NULL, "
                    	+ "LONGITUDE DOUBLE DEFAULT NULL, data BINARY(500000), "
                    	+ "PRIMARY KEY (NUM_INSEE))");
                	stmt.execute("UPDATE meta SET version = 3");
                case 3:
                	stmt.execute("ALTER TABLE meta "
                			+ "ADD COLUMN lastUpdate BIGINT");
                	stmt.execute("UPDATE meta SET version = 4");
                case 4:
                    stmt.execute("CREATE TABLE tariffAreaValidity ("
                            + "id INTEGER(255), areaId INTEGER(255), "
                    		+ "cashRegisterId VARCHAR(255), start BIGINT, "
                            + "end BIGINT, priority BIGINT, "
                            + "PRIMARY KEY (id))");
                	stmt.execute("UPDATE meta SET version = 5");
                case 5:
                	stmt.execute("ALTER TABLE products "
                			+ "ADD COLUMN isActive BOOLEAN DEFAULT TRUE");
                	stmt.execute("ALTER TABLE products "
                        	+ "ADD COLUMN isComposed BOOLEAN DEFAULT FALSE");
                	stmt.execute("UPDATE meta SET version = 6");
                case 6:
                	stmt.execute("CREATE TABLE linesDeletedQueue ("
                            + " id int(11) NOT NULL AUTO_INCREMENT, ticketId VARCHAR(255), isOpenedPayment BOOLEAN, data BINARY(500000))");
                	stmt.execute("UPDATE meta SET version = 7");
                // PG MAJ du schema de la base pour ajout colonne lastname et firstname sur la table customers
                // afin de pouvoir les utiliser lors de la recherche client
                case 7:
                    stmt.execute("ALTER TABLE customers "
                            + "ADD COLUMN lastname VARCHAR(255)");
                    stmt.execute("ALTER TABLE customers "
                            + "ADD COLUMN firstname VARCHAR(255)");
                    stmt.execute("ALTER TABLE customers "
                            + "ADD COLUMN dateofbirth date");
                    stmt.execute("ALTER TABLE customers "
                            + "ADD COLUMN adresse VARCHAR(255)");
                    stmt.execute("ALTER TABLE customers "
                            + "ADD COLUMN adresse2 VARCHAR(255)");
                    stmt.execute("ALTER TABLE customers "
                            + "ADD COLUMN postal VARCHAR(255)");
                    stmt.execute("ALTER TABLE customers "
                            + "ADD COLUMN city VARCHAR(255)");
                    stmt.execute("ALTER TABLE customers "
                            + "ADD COLUMN region VARCHAR(255)");
                    stmt.execute("ALTER TABLE customers "
                            + "ADD COLUMN country VARCHAR(255)");
                    stmt.execute("ALTER TABLE customers "
                            + "ADD COLUMN email VARCHAR(255)");
                    stmt.execute("ALTER TABLE customers "
                            + "ADD COLUMN phone VARCHAR(255)");
                    stmt.execute("ALTER TABLE customers "
                            + "ADD COLUMN phone2 VARCHAR(255)");
                    stmt.execute("ALTER TABLE customers "
                            + "ADD COLUMN notes VARCHAR(255)");
                    stmt.execute("ALTER TABLE customers "
                            + "ADD COLUMN civilite VARCHAR(255)");
                    stmt.execute("ALTER TABLE customers "
                            + "ADD COLUMN type VARCHAR(255)");
                    stmt.execute("ALTER TABLE customers "
                            + "ADD COLUMN source VARCHAR(255)");
                    stmt.execute("ALTER TABLE customers "
                            + "ADD COLUMN newsletter BOOLEAN");
                    stmt.execute("ALTER TABLE customers "
                            + "ADD COLUMN partenaires BOOLEAN");
                    stmt.execute("ALTER TABLE customers "
                            + "ADD COLUMN dateCreation datetime");
                    stmt.execute("ALTER TABLE customers "
                            + "ADD COLUMN dateUpdate datetime");
                    stmt.execute("CREATE TABLE customerQueue ("
                            + "customerId VARCHAR(255), data BINARY(500000), "
                            + "PRIMARY KEY (customerId))");
                    stmt.execute("UPDATE meta SET version = 8");
                case 8 :
                	stmt.execute("ALTER TABLE customers "
                            + "ADD COLUMN isParent BOOLEAN");
                    stmt.execute("UPDATE meta SET version = 9");
                }
            }
        } catch (SQLException e) {
            // Create the database
            stmt.execute("CREATE TABLE meta (version INTEGER , lastUpdate BIGINT)");
            stmt.execute("INSERT INTO meta (version) VALUES (" + VERSION + ")");
            stmt.execute("CREATE TABLE cashRegisters ("
                    + "id INTEGER(255), data BINARY(500000), "
                    + "PRIMARY KEY (id))");
            stmt.execute("CREATE TABLE products ("
                    + "id VARCHAR(255), ref VARCHAR(255), label VARCHAR(255), "
                    + "barcode VARCHAR(255), categoryId VARCHAR(255), "
                    + "isActive BOOLEAN, isComposed BOOLEAN, "
                    + "dispOrder INTEGER(255), data BINARY(5000000), "
                    + "PRIMARY KEY (id))");
            stmt.execute("CREATE TABLE categories ("
                    + "id VARCHAR(255), label VARCHAR(255), "
                    + "parentId VARCHAR(255), "
                    + "dispOrder INTEGER(255), data BINARY(5000000), "
                    + "PRIMARY KEY (id))");
            stmt.execute("CREATE TABLE taxCats ("
                    + "id VARCHAR(255), data BINARY(500000), "
                    + "PRIMARY KEY (id))");
            stmt.execute("CREATE TABLE taxes ("
                    + "id VARCHAR(255), taxCatId VARCHAR(255), "
                    + "data BINARY(500000), "
                    + "PRIMARY KEY (id))");
            stmt.execute("CREATE TABLE currencies ("
                    + "id INTEGER(255), main BOOLEAN, data BINARY(500000), "
                    + "PRIMARY KEY (id))");
            // PG MAJ du schema de la base pour ajout colonne lastname et firstname sur la table customers
            // afin de pouvoir les utiliser lors de la recherche client
            stmt.execute("CREATE TABLE customers ("
                    + "id VARCHAR(255), number VARCHAR(255), key VARCHAR(255), "
                    + "name VARCHAR(255), lastname VARCHAR(255), firstname VARCHAR(255), "
                    + "dateofbirth date, dateCreation datetime, dateUpdate datetime, address VARCHAR(255), address2 VARCHAR(255), "
                    + "postal VARCHAR(255), city VARCHAR(255), region VARCHAR(255), country VARCHAR(255), "
                    + "email VARCHAR(255), phone VARCHAR(255), phone2 VARCHAR(255), "
                    + "civilite VARCHAR(255), type VARCHAR(255), source VARCHAR(255), newsletter BOOLEAN, partenaires BOOLEAN, "
                    + "notes VARCHAR(255), card VARCHAR(255), isParent BOOLEAN , data BINARY(500000), "
                    + "PRIMARY KEY (id))");
            stmt.execute("CREATE TABLE customerRanking ("
                    + "id VARCHAR(255), rank INTEGER(255), "
                    + "PRIMARY KEY (id))");
            stmt.execute("CREATE TABLE tariffAreas ("
                    + "id INTEGER(255), data BINARY(500000), "
                    + "PRIMARY KEY (id))");
            stmt.execute("CREATE TABLE tariffAreaValidity ("
                    + "id INTEGER(255), areaId INTEGER(255), "
            		+ "locationId VARCHAR(255), start BIGINT, "
                    + "end BIGINT, priority BIGINT, "
                    + "PRIMARY KEY (id))");
            stmt.execute("CREATE TABLE tariffAreaPrices ("
                    + "areaId INTEGER(255), prdId VARCHAR(255), price DOUBLE, "
                    + "PRIMARY KEY (areaId, prdId))");
            stmt.execute("CREATE TABLE subgroups ("
                    + "id INTEGER(255), compositionId VARCHAR(255), "
                    + "dispOrder INTEGER(255), data BINARY (500000), "
                    + "PRIMARY KEY (id))");
            stmt.execute("CREATE TABLE subgroupProds ("
                    + "groupId INTEGER(255), prdId VARCHAR(255), "
                    + "dispOrder INTEGER(255), "
                    + "PRIMARY KEY (groupId, prdId))");
            stmt.execute("CREATE TABLE sharedTickets ("
                    + "id VARCHAR(255), data BINARY(500000), "
                    + "PRIMARY KEY (id))");
            stmt.execute("CREATE TABLE sharedTicketQueue ("
                    + "id VARCHAR(255), operation INTEGER(255), "
                    + "data BINARY(500000), "
                    + "PRIMARY KEY (id))");
            stmt.execute("CREATE TABLE ticketQueue ("
                    + "ticketId BIGINT, data BINARY(500000), "
                    + "PRIMARY KEY (ticketId))");
            stmt.execute("CREATE TABLE messages ("
                    + "ticketId BIGINT, data BINARY(500000), "
                    + "PRIMARY KEY (ticketId))");
            stmt.execute("CREATE TABLE barcodes ("
                    + "prdId VARCHAR(255), barcode VARCHAR(255), "
                    + "PRIMARY KEY (prdId , barcode))");
            stmt.execute("CREATE TABLE productComp ("
                    + "masterPrdId VARCHAR(255), childPrdId VARCHAR(255), "
                    + "quantity DOUBLE, data BINARY(500000), "
                    + "PRIMARY KEY (masterPrdId , childPrdId))");
            stmt.execute("CREATE TABLE servMessages ( msgId IDENTITY, "
                    + "content VARCHAR(255), locationId VARCHAR(255), "
                    + "peopleId VARCHAR(255), productId VARCHAR(255), "
                    + "type VARCHAR(255), startDate DATETIME, "
                    + "endDate DATETIME, data BINARY(500000), "
                    + "PRIMARY KEY (msgId))");
            stmt.execute("CREATE TABLE insee ("
            		+ "NUM_INSEE VARCHAR(64) NOT NULL, "
            		+ "ZIPCODE VARCHAR(45) DEFAULT NULL, "
            		+ "COMMUNE VARCHAR(255) DEFAULT NULL, "
            		+ "LATITUDE DOUBLE DEFAULT NULL, "
            		+ "LONGITUDE DOUBLE DEFAULT NULL, data BINARY(500000), "
            		+ "PRIMARY KEY (NUM_INSEE))");
            stmt.execute("CREATE TABLE linesDeletedQueue ("
                    + " id int(11) NOT NULL AUTO_INCREMENT,ticketId VARCHAR(255), isOpenedPayment BOOLEAN, data BINARY(500000))");
            stmt.execute("CREATE TABLE customerQueue ("
                    + "customerId VARCHAR(255), data BINARY(500000), "
                    + "PRIMARY KEY (customerId))");
            logger.log(Level.INFO, "Initialized database version " + VERSION);
        }
    }

	public static void close() {
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                // Can't we get anything from it...
                logger.log(Level.SEVERE,
                        "Cannot close local database connection", e);
                e.printStackTrace();
            }
            conn = null;
        }
    }

    public static PreparedStatement prepare(String sql) throws SQLException {
        Connection c = getConnection();
        return c.prepareStatement(sql);
    }

    public static void execute(String sql) throws SQLException {
        Connection c = getConnection();
        Statement stmt = c.createStatement();
        stmt.execute(sql);
    }
    
    public static ResultSet executeQuery(String sql) throws SQLException {
        Connection c = getConnection();
        Statement stmt = c.createStatement();
        return stmt.executeQuery(sql);
    }
    
    public static Date getLastUpdate() {
    	Date retour = null;
        Connection c;
		try {
			c = getConnection();
			Statement stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT lastUpdate FROM meta");
            // Update code
            if (rs.next()) {
                retour = new Date(rs.getLong("lastUpdate"));
            }
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	return retour;
    }
    
    public static void setLastUpdate(Date newDate) throws SQLException {
    	Connection c = getConnection();
    	PreparedStatement stmt = c.prepareStatement("UPDATE meta SET lastUpdate = ?");
    	stmt.clearParameters();
    	stmt.setLong(1, newDate.getTime());
    	stmt.execute();
    }
}
