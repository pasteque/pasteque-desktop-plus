package fr.pasteque.pos.caching;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import fr.pasteque.basic.BasicException;
import fr.pasteque.pos.admin.InseeInfo;

public class InseeCache {

	private static Logger logger = Logger.getLogger("fr.pasteque.pos.caching.InseeCache");
	private static PreparedStatement inseeByCoordinates;
	private static PreparedStatement inseeById;
	private static PreparedStatement inseeByZipandCity;
	private static PreparedStatement inseeByZip;
	
    private static void init() throws SQLException {
    	inseeByCoordinates = LocalDB.prepare("SELECT data FROM insee "
                + "WHERE LATITUDE BETWEEN ? AND ? AND "
    			+ "LONGITUDE BETWEEN ? AND ?");
    	inseeById = LocalDB.prepare("SELECT data FROM insee "
                + "WHERE NUM_INSEE = ?");
    	inseeByZipandCity = LocalDB.prepare("SELECT data FROM insee "
                + "WHERE ( ZIPCODE = ? OR ZIPCODE LIKE ? OR ZIPCODE LIKE ? OR ZIPCODE LIKE ? ) AND ( COMMUNE = ? OR COMMUNE = ? )");
    	inseeByZip = LocalDB.prepare("SELECT data FROM insee "
                + "WHERE ZIPCODE LIKE ? OR ZIPCODE LIKE ? LIMIT ?");
    }
    /** Update Insee Data */
    public static void refreshInsee(List<InseeInfo> crs , Date lastUpdate)
            throws BasicException {
    try{
    	
    	if(lastUpdate.getTime() == 0) {
    		LocalDB.execute("TRUNCATE TABLE insee");
    	}
    	logger.log(Level.INFO , "RefreshInsee");
    	PreparedStatement stmt = LocalDB.prepare("MERGE INTO "
    			+ "insee (NUM_INSEE, ZIPCODE, COMMUNE, LATITUDE, LONGITUDE, data) VALUES (?, ?, ?, ?, ?, ?)");
    	for (InseeInfo cr : crs) {
    		stmt.setString(1, cr.getInseeNum());
    		stmt.setString(2, cr.getZipCode());
    		stmt.setString(3, cr.getCommune());
    		stmt.setDouble(4, (cr.getLatitude() == null ? 0D : cr.getLatitude() ));
    		stmt.setDouble(5, (cr.getLongitude() == null ? 0D : cr.getLongitude() ));
    		ByteArrayOutputStream bos = new ByteArrayOutputStream(5120);
    		ObjectOutputStream os = new ObjectOutputStream(bos);
    		os.writeObject(cr);
    		stmt.setBytes(6, bos.toByteArray());
    		os.close();
    		stmt.addBatch();
    	}
    	stmt.executeBatch();
	} catch (SQLException e) {
	    throw new BasicException(e);
	} catch (IOException e) {
	    throw new BasicException(e);
	}
    }


    /**
     * Retourner une liste de communes connues dans un espace délimité
     * @param minlongitude 
     * @param maxlongitude
     * @param minlatitude
     * @param maxlatitude
     * @return une liste des villes dans la surface du globe ainsi définie
     */
	public static ArrayList<InseeInfo> getInseeByCoordinates(double minlongitude, double maxlongitude, double minlatitude,
			double maxlatitude)         
			throws BasicException {
		        try {
		            if (inseeByCoordinates == null) {
		                init();
		            }
		            inseeByCoordinates.clearParameters();
		            inseeByCoordinates.setDouble(1, minlatitude);
		            inseeByCoordinates.setDouble(2, maxlatitude);
		            inseeByCoordinates.setDouble(3, minlongitude);
		            inseeByCoordinates.setDouble(4, maxlongitude);
		            ResultSet rs = inseeByCoordinates.executeQuery();
		            ArrayList<InseeInfo> irs = readInseeResult(rs);
		            return irs;
		        } catch (SQLException e) {
		            throw new BasicException(e);
		        }
	}
	
	public static InseeInfo getInseeById(String inseeNum)         
			throws BasicException {
		        try {
		            if (inseeById == null) {
		                init();
		            }
		            inseeById.clearParameters();
		            inseeById.setString(1, inseeNum);
		            ResultSet rs = inseeById.executeQuery();
		            ArrayList<InseeInfo> irs = readInseeResult(rs);
		            return ( irs.size() == 0 ? null : irs.get(0));
		        } catch (SQLException e) {
		            throw new BasicException(e);
		        }
	}
	
	private static ArrayList<InseeInfo> readInseeResult(ResultSet rs)
        throws BasicException {
        try {
            ArrayList<InseeInfo> irs = new ArrayList<InseeInfo>();
            while (rs.next()) {
                byte[] data = rs.getBytes("data");
                ByteArrayInputStream bis = new ByteArrayInputStream(data);
                ObjectInputStream os = new ObjectInputStream(bis);
                InseeInfo cr = (InseeInfo) os.readObject();
                irs.add(cr);
            }
            return irs;
        } catch (SQLException e) {
            throw new BasicException(e);
        } catch (IOException e) {
            throw new BasicException(e);
        } catch (ClassNotFoundException e) {
            // Should never happen
            throw new BasicException(e);
        }
	}
	
	public static InseeInfo getInseeByZipAndName(String zipCode, String city)
		throws BasicException {
	        try {
	            if (inseeByZipandCity == null) {
	                init();
	            }
	            inseeByZipandCity.clearParameters();
	            inseeByZipandCity.setString(1, zipCode);
	            inseeByZipandCity.setString(2, zipCode+"/%");
	            inseeByZipandCity.setString(3, "%/"+zipCode);
	            inseeByZipandCity.setString(4, "%/"+zipCode+"/%");
	            inseeByZipandCity.setString(5, city.toUpperCase());
	            inseeByZipandCity.setString(6, city.toUpperCase().replaceAll("-", " ").replaceAll("SAINT", "ST").replaceAll("'", " "));
	            ResultSet rs = inseeByZipandCity.executeQuery();
	            ArrayList<InseeInfo> irs = readInseeResult(rs);
	            return ( irs.size() == 0 ? null : irs.get(0));
	        } catch (SQLException e) {
	            throw new BasicException(e);
	        }
	}
	
	/**
	 * 
	 * @param zipCode le code à rechercher qui peut être incomplet
	 * @param number nombre maximum d'enregistrements à retourner - null ou négatif pour pas de limite
	 * @return une liste des communes pouvant correspondre dans la limite de number lignes
	 * @throws BasicException
	 */
	public static List<InseeInfo> getInseeByZip(String zipCode , Integer number)
			throws BasicException {
		        try {
		            if (inseeByZip == null) {
		                init();
		            }
		            if(number == null) number = -1; 
		            inseeByZip.clearParameters();
		            inseeByZip.setString(1, zipCode+"%");
		            inseeByZip.setString(2, "%/"+zipCode+"%");
		            inseeByZip.setInt(3, number);
		            ResultSet rs = inseeByZip.executeQuery();
		            ArrayList<InseeInfo> irs = readInseeResult(rs);
		            return irs;
		        } catch (SQLException e) {
		            throw new BasicException(e);
		        }
		}

}
