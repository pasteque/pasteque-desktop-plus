//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2007-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.caching;

import fr.pasteque.data.loader.ServerLoader;
import fr.pasteque.pos.customers.CustomerInfoExt;
import fr.pasteque.pos.sales.SharedTicketInfo;
import fr.pasteque.pos.ticket.TicketInfo;
import fr.pasteque.pos.ticket.TicketLineInfo;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;

/** Message queue for disconnected usage */
public class CallQueue {

    private static Logger logger = Logger.getLogger("fr.pasteque.pos.caching.CallQueue");

    private static final int OP_EDIT = 1;
    private static final int OP_DELETE = 2;

    private static PreparedStatement sharedTicket;
    private static PreparedStatement delSTOperations;
    private static PreparedStatement addTicket;
    private static PreparedStatement getSTOps;
    private static PreparedStatement getTktOps;
    private static PreparedStatement delTktOp;
    private static PreparedStatement countSTOps;
    private static PreparedStatement countTktOps;
    private static PreparedStatement addLinesDeleted;
    private static PreparedStatement getLinesDeleted;
    private static PreparedStatement delLinesDeleted;
    private static PreparedStatement maxTicketId;
    private static PreparedStatement addCustomer;
    private static PreparedStatement getCustomer;
    private static PreparedStatement delCustomer;
    private static PreparedStatement getCustomerCache;

    private static String cashId;
    private static boolean offline;
    private static Timer recoverTimer;

    public static void setup(String cashId) {
        CallQueue.cashId = cashId;
    }

    private static void init() throws SQLException {
        sharedTicket = LocalDB.prepare("INSERT INTO sharedTicketQueue "
                + "(id, operation, data) VALUES (?, ?, ?)");
        delSTOperations = LocalDB.prepare("DELETE FROM sharedTicketQueue "
                + "WHERE id = ?");
        addTicket = LocalDB.prepare("INSERT INTO ticketQueue (ticketId , data) "
                + "VALUES (?, ?)");
        getSTOps = LocalDB.prepare("SELECT * FROM sharedTicketQueue");
        getTktOps = LocalDB.prepare("SELECT * FROM ticketQueue");
        delTktOp = LocalDB.prepare("DELETE FROM ticketQueue "
                + "WHERE ticketId = ?");
        countSTOps = LocalDB.prepare("SELECT count(id) AS num "
                + "FROM sharedTicketQueue");
        countTktOps = LocalDB.prepare("SELECT count(ticketId) as num "
                + "FROM ticketQueue");
        
        addLinesDeleted = LocalDB.prepare("INSERT INTO linesDeletedQueue (ticketId,isOpenedPayment,data) "
                + "VALUES (?,?,?)");
        getLinesDeleted = LocalDB.prepare("SELECT * FROM linesDeletedQueue");
        delLinesDeleted = LocalDB.prepare("DELETE FROM linesDeletedQueue "
                + "WHERE id = ?");
        
        maxTicketId = LocalDB.prepare("SELECT max(ticketId) as num "
                + "FROM ticketQueue");
        addCustomer = LocalDB.prepare("INSERT INTO customerQueue (customerId , data) "
                + "VALUES (?, ?)");
        getCustomer = LocalDB.prepare("SELECT * FROM customerQueue");
        delCustomer = LocalDB.prepare("DELETE FROM customerQueue "
                + "WHERE customerId = ?");
        getCustomerCache = LocalDB.prepare("SELECT * FROM customers "
        		+ "WHERE id = ?");
    }

    /** Switch offline and enable timed recovery. Queuing an operation
     * automatically turns offline. Does nothing if already offline. */
    public static void turnOffline() {
        if (!offline) {
            logger.log(Level.INFO, "Turning offline");
            offline = true;
            // Start the timer to recover
            recoverTimer = new Timer();
            recoverTimer.schedule(new TimerTask(){
                    public void run() {
                        checkRecovery();
                    }
                }, 6000, 60000);
        }
    }
    private static void checkRecovery() {
        ServerLoader loader = new ServerLoader();
        ServerLoader.Response r;
        try {
            r = loader.read("VersionAPI", "");
            if (r.getStatus().equals(ServerLoader.Response.STATUS_OK)) {
                // Server is there!
                logger.log(Level.INFO, "Recovery started");
                if (recover()) {
                    recoverTimer.cancel();
                    recoverTimer = null;
                }
            }
        } catch (Exception e) {
            // Server unavailable
        }
    }

    /** Get total count of pending operations */
    public static synchronized int getOperationsCount() {
        try {
            if (countSTOps == null) {
                init();
            }
            int count = 0;
            ResultSet strs = countSTOps.executeQuery();
            if (strs.next()) {
                count += strs.getInt("num");
            }
            strs.close();
            ResultSet tktrs = countTktOps.executeQuery();
            if (tktrs.next()) {
                count += tktrs.getInt("num");
            }
            tktrs.close();
            return count;
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }
    }
    
    /** Get max teckid from pending operations */
    public static synchronized long getMaxTicketId() {
        try {
            if (maxTicketId == null) {
                init();
            }
            long count = 0;
            ResultSet strs = maxTicketId.executeQuery();
            if (strs.next()) {
                count = strs.getLong("num");
            }
            strs.close();
            return count;
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }
    }

    /** Recover from offline mode. Try to send all pending operations, turns
     * off offline mode once everything is sent.
     */
    public static synchronized boolean recover() {
    	
    	ServerLoader loader = new ServerLoader();
    	
        if (getCustomer == null) {
            try {
                init();
            } catch (SQLException e) {
                e.printStackTrace();
                return false;
            }
        }
        
        // Save Customer 
        try {
            ResultSet customerRs = getCustomer.executeQuery();
            while (customerRs.next()) {
                try {
                    byte[] data = customerRs.getBytes("data");
                    ByteArrayInputStream bis = new ByteArrayInputStream(data);
                    ObjectInputStream os = new ObjectInputStream(bis);
                    CustomerInfoExt customer = (CustomerInfoExt) os.readObject();
                    os.close();
                    
                    if (getCustomerCache == null) {
                        try {
                            init();
                        } catch (SQLException e) {
                            e.printStackTrace();
                            return false;
                        }
                    }
                    
                    getCustomerCache.clearParameters();
                    getCustomerCache.setString(1, customer.getId());
                    String customerId = "";
                    ResultSet customerCacheRs = getCustomerCache.executeQuery();
                    if (customerCacheRs.next()) {
                    	customerId = customerCacheRs.getString("id");
                    }
                    if (customerId.isEmpty()) {
                    	
                    	CustomersCache.createCustomers(customer);
                    	
                    }
                    
                    ServerLoader.Response r1;
                    r1 = loader.write("CustomersAPI", "saveCustomer", "", customer.toJSON().toString());
                    if (!r1.getStatus().equals(ServerLoader.Response.STATUS_OK)) {
                        logger.log(Level.WARNING, "Recovery failed, "
                                + "server error: "
                                + r1.getResponse().toString());
                        try {
                        	customerRs.close();
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                        return false;
                    }
                    
                    
                    // Save Customer went well, delete from queue
                    delCustomer.clearParameters();
                    delCustomer.setString(1, customer.getId());
                    delCustomer.execute();
                    logger.log(Level.INFO, "Customer " + customer.getId()
                            + " saved");
                } catch (Exception e) {
                    logger.log(Level.INFO, "Recovery failed: "
                            + e.getMessage());
                    try {
                    	customerRs.close();
                    } catch (SQLException e2) {
                        e2.printStackTrace();
                    }
                    return false;
                }
            }
        } catch (SQLException fuck) {
            fuck.printStackTrace();
            return false;
        }
    	
        if (getSTOps == null) {
            try {
                init();
            } catch (SQLException e) {
                e.printStackTrace();
                return false;
            }
        }
        
        // Send shared tickets operations
        try {
            ResultSet strs = getSTOps.executeQuery();
            while (strs.next()) {
                int op = strs.getInt("operation");
                String id = strs.getString("id");
                if (op == OP_EDIT) {
                    // Edit shared ticket
                    try {
                        byte[] data = strs.getBytes("data");
                        ByteArrayInputStream bis = new ByteArrayInputStream(data);
                        ObjectInputStream os = new ObjectInputStream(bis);
                        SharedTicketInfo tkt = (SharedTicketInfo) os.readObject();
                        os.close();
                        ServerLoader.Response r;
                        r = loader.write("TicketsAPI", "share", "ticket",
                                tkt.toJSON().toString());
                        if (!r.getStatus().equals(ServerLoader.Response.STATUS_OK)) {
                            logger.log(Level.WARNING, "Recovery failed, "
                                    + "server error: "
                                    + r.getResponse().toString());
                            try {
                                strs.close();
                            } catch (SQLException e) {
                                e.printStackTrace();
                            }
                            return false;
                        }
                        // Shared ticket edited, remove from queue
                        deletePreviousOperations(id);
                        logger.log(Level.INFO, "Shared ticket " + id
                                + " edited");
                    } catch (Exception e) {
                        logger.log(Level.INFO, "Recovery failed: "
                                + e.getMessage());
                        try {
                            strs.close();
                        } catch (SQLException e2) {
                            e2.printStackTrace();
                        }
                        return false;
                    }
                } else {
                    // Delete shared ticket
                    try {
                        ServerLoader.Response r;
                        r = loader.write("TicketsAPI", "delShared", "id", id);
                        if (!r.getStatus().equals(ServerLoader.Response.STATUS_OK)) {
                            logger.log(Level.WARNING, "Shared ticket "
                                    + "recovery failed, server error: "
                                    + r.getResponse().toString());
                            strs.close();
                            return false;
                        }
                        // Shared ticket deleted, remove from queue
                        deletePreviousOperations(id);
                        logger.log(Level.INFO, "Shared ticket " + id
                                + " deleted");
                    } catch (Exception e) {
                        logger.log(Level.INFO, "Recovery failed: "
                                + e.getMessage());
                        strs.close();
                        return false;
                    }
                }
            }
            // Everything went well, send ticket operations
            ResultSet tktrs = getTktOps.executeQuery();
            while (tktrs.next()) {
                try {
                    byte[] data = tktrs.getBytes("data");
                    ByteArrayInputStream bis = new ByteArrayInputStream(data);
                    ObjectInputStream os = new ObjectInputStream(bis);
                    TicketInfo tkt = (TicketInfo) os.readObject();
                    os.close();
                    ServerLoader.Response r;
                    r = loader.write("TicketsAPI", "save",
                            "ticket", tkt.toJSON().toString(),
                            "cashId", cashId);
                    if (!r.getStatus().equals(ServerLoader.Response.STATUS_OK)) {
                        logger.log(Level.WARNING, "Ticket recovery failed, "
                                + "server error: "
                                + r.getResponse().toString());
                        try {
                            tktrs.close();
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                        return false;
                    }
                    // Save ticket went well, delete from queue
                    delTktOp.clearParameters();
                    delTktOp.setLong(1, tkt.getTicketId());
                    delTktOp.execute();
                    logger.log(Level.INFO, "Ticket " + tkt.getTicketId()
                            + " saved");
                } catch (Exception e) {
                    logger.log(Level.INFO, "Recovery failed: "
                            + e.getMessage());
                    e.printStackTrace();
                    try {
                        tktrs.close();
                    } catch (SQLException e2) {
                        e2.printStackTrace();
                    }
                    return false;
                }
            }
            ResultSet linesDeleted = getLinesDeleted.executeQuery();
            System.out.println("linesDeleted" +linesDeleted);
            while (linesDeleted.next()) {
                try {
                    byte[] data = linesDeleted.getBytes("data");
                    String ticketId = linesDeleted.getString("ticketId");
                    Boolean isOpenedPayment = linesDeleted.getBoolean("isOpenedPayment");
                    int id = linesDeleted.getInt("id");
                    ByteArrayInputStream bis = new ByteArrayInputStream(data);
                    ObjectInputStream os = new ObjectInputStream(bis);
                    TicketLineInfo line = (TicketLineInfo) os.readObject();
                    os.close();
                    ServerLoader.Response r;
                    TicketLineInfo copy = line.copyTicketLine();
	        		String lineJSON = copy.lineDeletedToJSON().toString();
                    r = loader.write("TicketLinesDeletedAPI", "save",
    	                    "ticketLinesDeleted", null,
    	                    "ticketLineDeleted",lineJSON,
    	                    "ticketId",ticketId,"isOpenedPayment",String.valueOf(isOpenedPayment));
    	            if (!r.getStatus().equals(ServerLoader.Response.STATUS_OK)) {
    	            	logger.log(Level.WARNING, "TicketLinesDeleted recovery failed, "
                                + "server error: "
                                + r.getResponse().toString());
    	            }
                    // Save ticket went well, delete from queue
    	            delLinesDeleted.clearParameters();
    	            delLinesDeleted.setInt(1, id);
    	            delLinesDeleted.execute();
                    logger.log(Level.INFO, "TicketlinesDeleted  saved");
                } catch (Exception e) {
                    logger.log(Level.INFO, "TicketLinesDeleted Recovery failed: "
                            + e.getMessage());
                    e.printStackTrace();
                    try {
                    	linesDeleted.close();
                    } catch (SQLException e2) {
                        e2.printStackTrace();
                    }
                    return false;
                }
            }
        } catch (SQLException fuck) {
            fuck.printStackTrace();
            return false;
        }
        logger.log(Level.INFO, "Recovered from offline mode");
        offline = false;
        return true;
    }

    public static boolean isOffline() {
        return offline;
    }

    /** Delete pending operation on a shared ticket. */
    private static synchronized void deletePreviousOperations(String id) throws SQLException {
        if (delSTOperations == null) {
            init();
        }
        delSTOperations.clearParameters();
        delSTOperations.setString(1, id);
        delSTOperations.execute();
    }

    /** Queue a call to save a shared ticket. If a previous call for the ticket
     * is already queued it is discarded. Turns on offline mode.
     */
    public static synchronized void queueSharedTicketSave(String id,
            SharedTicketInfo ticket) {
        turnOffline();
        try {
            deletePreviousOperations(id);        
            // Replace with the new ticket
            sharedTicket.clearParameters();
            sharedTicket.setString(1, id);
            sharedTicket.setInt(2, OP_EDIT);
            ByteArrayOutputStream bos = new ByteArrayOutputStream(5120);
            ObjectOutputStream os = new ObjectOutputStream(bos);
            os.writeObject(ticket);
            byte[] data = bos.toByteArray();
            os.close();
            sharedTicket.setBytes(3, data);
            sharedTicket.execute();
            logger.log(Level.INFO, "Queued shared ticket " + id);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    /** Queue a call to delete a shared ticket. All previous calls for the
     * ticket are discarded. Turns on offline mode.
     */
    public static synchronized void queueDeleteSharedTicket(String id) {
        turnOffline();
        try {
            deletePreviousOperations(id);
            sharedTicket.clearParameters();
            sharedTicket.setString(1, id);
            sharedTicket.setInt(2, OP_DELETE);
            sharedTicket.setBytes(3, null);
            sharedTicket.execute();
            logger.log(Level.INFO, "Queued shared ticket " + id + " delete");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    /** Queue a call to save a ticket. Turns on offline mode. */
    public static synchronized void queueCustomerSave(CustomerInfoExt customer) {
//        turnOffline();
        try {
            if (addCustomer == null) {
                init();
            }
            addCustomer.clearParameters();
            addCustomer.setString(1, customer.getId());
            ByteArrayOutputStream bos = new ByteArrayOutputStream(5120);
            ObjectOutputStream os = new ObjectOutputStream(bos);
            os.writeObject(customer);
            byte[] data = bos.toByteArray();
            os.close();
            addCustomer.setBytes(2, data);
            addCustomer.execute();
            logger.log(Level.INFO, "Queued Customer " + customer.getId());
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /** Queue a call to save a ticket. Turns on offline mode. */
    public static synchronized void queueTicketSave(TicketInfo ticket) {
        turnOffline();
        try {
            if (addTicket == null) {
                init();
            }
            addTicket.clearParameters();
            addTicket.setLong(1, ticket.getTicketId());
            ByteArrayOutputStream bos = new ByteArrayOutputStream(5120);
            ObjectOutputStream os = new ObjectOutputStream(bos);
            os.writeObject(ticket);
            byte[] data = bos.toByteArray();
            os.close();
            addTicket.setBytes(2, data);
            addTicket.execute();
            logger.log(Level.INFO, "Queued ticket " + ticket.getTicketId());
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

	public static void queueTicketLinesDeletedSave(List<TicketLineInfo> lines, String ticketId, boolean isOpenedPayment) {
		turnOffline();
        try {
            if (addLinesDeleted == null) {
                init();
            }
            for (TicketLineInfo line : lines){
	            addLinesDeleted.clearParameters();
	            addLinesDeleted.setString(1, ticketId);
	            addLinesDeleted.setBoolean(2, isOpenedPayment);
	            ByteArrayOutputStream bos = new ByteArrayOutputStream(5120);
	            ObjectOutputStream os = new ObjectOutputStream(bos);
	            os.writeObject(line);
	            byte[] data = bos.toByteArray();
	            os.close();
	            addLinesDeleted.setBytes(3, data);
	            addLinesDeleted.execute();
	            logger.log(Level.INFO, "Queued ticketLinesDeleted ticket id : " + ticketId);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
		
	}
}