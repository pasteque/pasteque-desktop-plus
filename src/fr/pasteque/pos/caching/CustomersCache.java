//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2007-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.caching;

import fr.pasteque.basic.BasicException;
import fr.pasteque.pos.customers.CustomerInfoExt;
import fr.pasteque.pos.customers.DataLogicCustomers;
import fr.pasteque.pos.widgets.Criteria;
import fr.pasteque.pos.widgets.SearchCacheAbstract;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.ListModel;

public class CustomersCache extends SearchCacheAbstract {

    //private static Logger logger = Logger.getLogger("fr.pasteque.pos.caching.CustomersCache");

    private static PreparedStatement customers;
    private static PreparedStatement customer;
    private static PreparedStatement customerIds;
    private static PreparedStatement customerCard;
    private static PreparedStatement ranking;
    private static PreparedStatement search;
    private static PreparedStatement searchLast;
    private static PreparedStatement update;

    private CustomersCache() {}

    private static void init() throws SQLException {
        customers = LocalDB.prepare("SELECT data FROM customers");
        customer = LocalDB.prepare("SELECT data FROM customers "
                + "WHERE id = ?");
        customerIds = LocalDB.prepare("SELECT data FROM customers "
                + "WHERE id IN (SELECT id FROM TABLE(x VARCHAR = ?) T INNER JOIN customers ON T.X=customers.ID)");
        customerCard = LocalDB.prepare("SELECT data FROM customers "
                + "WHERE isParent AND card = ?");
        ranking = LocalDB.prepare("SELECT data FROM customers, customerRanking "
                + "WHERE customers.id = customerRanking.id "
                + "ORDER BY rank ASC");
        search = LocalDB.prepare("SELECT data FROM customers "
                + "WHERE isParent AND LOWER(number) LIKE LOWER(?) "
                + "AND LOWER(key) LIKE LOWER(?) AND LOWER(name) LIKE LOWER(?) ORDER BY ID DESC");
        searchLast = LocalDB.prepare("SELECT TOP ? data FROM customers WHERE isParent "
                + " ORDER BY DATEUPDATE DESC");
        update = LocalDB.prepare("UPDATE customers SET data = ? WHERE id = ?");
        
    }

    private static List<CustomerInfoExt> readCustomerResult(ResultSet rs)
        throws BasicException {
        try {
            List<CustomerInfoExt> custs = new ArrayList<CustomerInfoExt>();
            while (rs.next()) {
                byte[] data = rs.getBytes("data");
                ByteArrayInputStream bis = new ByteArrayInputStream(data);
                ObjectInputStream os = new ObjectInputStream(bis);
                CustomerInfoExt cust = (CustomerInfoExt) os.readObject();
                custs.add(cust);
            }
            return custs;
        } catch (SQLException e) {
            throw new BasicException(e);
        } catch (IOException e) {
            throw new BasicException(e);
        } catch (ClassNotFoundException e) {
            // Should never happen
            throw new BasicException(e);
        }
    }
    
    /** Create customers. */
    public static void createCustomers(CustomerInfoExt customer)
        throws BasicException {
        try {
        	// PG rempli les champs lastname et firstname dans la table customers
            // afin de pouvoir les utiliser lors de la recherche client
            PreparedStatement stmt = LocalDB.prepare("INSERT INTO customers "
                    + "(id, number, key, name, card, lastname, firstname, address, address2, postal, city, region, country, email, phone, phone2, notes, dateofbirth, civilite, type, source, newsletter, partenaires, dateCreation, dateUpdate, isParent ,data) VALUES "
                    + "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
            
            customer.setLevelSearch(0);
            stmt.setString(1, customer.getId());
            stmt.setString(2, customer.getTaxid());
            stmt.setString(3, customer.getSearchkey());
            stmt.setString(4, customer.getName());
            stmt.setString(5, customer.getCard());
            stmt.setString(6, customer.getLastname());
            stmt.setString(7, customer.getFirstname());
            stmt.setString(8, customer.getAddress());
            stmt.setString(9, customer.getAddress2());
            stmt.setString(10, customer.getPostal());
            stmt.setString(11, customer.getCity());
            stmt.setString(12, customer.getRegion());
            stmt.setString(13, customer.getCountry());
            stmt.setString(14, customer.getEmail());
            stmt.setString(15, customer.getPhone());
            stmt.setString(16, customer.getPhone2());
            stmt.setString(17, customer.getNotes());
            if (customer.getDateofbirth() != null) {
            	stmt.setDate(18, new java.sql.Date(customer.getDateofbirth().getTime()));
            }else {
            	stmt.setDate(18,null);
            }
            stmt.setString(19, customer.getCivilite());
            stmt.setString(20, customer.getType());
            stmt.setString(21, customer.getSource());
            stmt.setBoolean(22, customer.isNewsletter());
            stmt.setBoolean(23, customer.isPartenaires());
            if (customer.getDateCreation() != null) {
            	stmt.setDate(24, new java.sql.Date(customer.getDateCreation().getTime()));
            }else {
            	stmt.setDate(24,null);
            }
            if (customer.getDateUpdate() != null) {
            	stmt.setDate(25, new java.sql.Date(customer.getDateUpdate().getTime()));
            }else {
            	stmt.setDate(25,null);
            }
            stmt.setBoolean(26, customer.isParent());
            ByteArrayOutputStream bos = new ByteArrayOutputStream(5120);
            ObjectOutputStream os = new ObjectOutputStream(bos);
            os.writeObject(customer);
            stmt.setBytes(27, bos.toByteArray());
            os.close();
            stmt.addBatch();
        	
        	stmt.executeBatch();
        } catch (SQLException e) {
            throw new BasicException(e);
        } catch (IOException e) {
            throw new BasicException(e);
        }
    }

    /** Clear and replace customers. */
    public static void refreshCustomers(List<CustomerInfoExt> customers, Date lastUpdate)
        throws BasicException {
        try {
        	if(lastUpdate.getTime() == 0) {
        		LocalDB.execute("TRUNCATE TABLE customers");
        	}
        	// PG rempli les champs lastname et firstname dans la table customers
            // afin de pouvoir les utiliser lors de la recherche client
            PreparedStatement stmt = LocalDB.prepare("MERGE INTO customers "
                    + "(id, number, key, name, card, lastname, firstname, address, address2, postal, city, region, country, email, phone, phone2, notes, dateofbirth, civilite, type, source, newsletter, partenaires, dateCreation, dateUpdate, isParent ,data) VALUES "
                    + "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
            for (CustomerInfoExt cust : customers) {
            	cust.setLevelSearch(0);
                stmt.setString(1, cust.getId());
                stmt.setString(2, cust.getTaxid());
                stmt.setString(3, cust.getSearchkey());
                stmt.setString(4, cust.getName());
                stmt.setString(5, cust.getCard());
                stmt.setString(6, cust.getLastname());
                stmt.setString(7, cust.getFirstname());
                stmt.setString(8, cust.getAddress());
                stmt.setString(9, cust.getAddress2());
                stmt.setString(10, cust.getPostal());
                stmt.setString(11, cust.getCity());
                stmt.setString(12, cust.getRegion());
                stmt.setString(13, cust.getCountry());
                stmt.setString(14, cust.getEmail());
                stmt.setString(15, cust.getPhone());
                stmt.setString(16, cust.getPhone2());
                stmt.setString(17, cust.getNotes());
                if (cust.getDateofbirth() != null) {
                	stmt.setDate(18, new java.sql.Date(cust.getDateofbirth().getTime()));
                }else {
                	stmt.setDate(18,null);
                }
                stmt.setString(19, cust.getCivilite());
                stmt.setString(20, cust.getType());
                stmt.setString(21, cust.getSource());
                stmt.setBoolean(22, cust.isNewsletter());
                stmt.setBoolean(23, cust.isPartenaires());
                if (cust.getDateCreation() != null) {
                	stmt.setTimestamp(24, new Timestamp(cust.getDateCreation().getTime()));
                }else {
                	stmt.setTimestamp(24,null);
                }
                if (cust.getDateUpdate() != null) {
                	stmt.setTimestamp(25, new Timestamp(cust.getDateUpdate().getTime()));
                }else {
                	stmt.setTimestamp(25,null);
                }
                stmt.setBoolean(26, cust.isParent());
                ByteArrayOutputStream bos = new ByteArrayOutputStream(5120);
                ObjectOutputStream os = new ObjectOutputStream(bos);
                os.writeObject(cust);
                stmt.setBytes(27, bos.toByteArray());
                os.close();
                stmt.addBatch();
        	}
        	stmt.executeBatch();
        } catch (SQLException e) {
            throw new BasicException(e);
        } catch (IOException e) {
            throw new BasicException(e);
        }
    }
    
    /** Update a customer */
    public static void refreshCustomer(CustomerInfoExt cust)
        throws BasicException {
        try {
            if (update == null) {
                init();
            }
            update.clearParameters();
            update.setString(2, cust.getId());
            ByteArrayOutputStream bos = new ByteArrayOutputStream(5120);
            ObjectOutputStream os = new ObjectOutputStream(bos);
            os.writeObject(cust);
            update.setBytes(1, bos.toByteArray());
            os.close();
            update.execute();
        } catch (SQLException e) {
            throw new BasicException(e);
        } catch (IOException e) {
            throw new BasicException(e);
        }
    }
    /** Clear and replace customer ranking. */
    public static void refreshRanking(List<String> ids)
        throws BasicException {
        try {
            LocalDB.execute("TRUNCATE TABLE customerRanking");
            PreparedStatement stmt = LocalDB.prepare("INSERT "
                    + "INTO customerRanking "
                    + "(id, rank) VALUES (?, ?)");
            for (int i = 0; i < ids.size(); i++) {
                stmt.setString(1, ids.get(i));
                stmt.setInt(2, i);
                stmt.execute();
            }
        } catch (SQLException e) {
            throw new BasicException(e);
        }
    }

    /** Get cached data if any, null otherwise */
    public static List<CustomerInfoExt> getCustomers() throws BasicException {
        try {
            if (customers == null) {
                init();
            }
            ResultSet rs = customers.executeQuery();
            return readCustomerResult(rs);
        } catch (SQLException e) {
            throw new BasicException(e);
        }
    }

    public static CustomerInfoExt getCustomer(String id) throws BasicException {
        try {
            if (customer == null) {
                init();
            }
            customer.clearParameters();
            customer.setString(1, id);
            ResultSet rs = customer.executeQuery();
            List<CustomerInfoExt> custs = readCustomerResult(rs);
            if (custs.size() > 0) {
                return custs.get(0);
            } else {
                return null;
            }
        } catch (SQLException e) {
            throw new BasicException(e);
        }
    }

    public static CustomerInfoExt getCustomerByCard(String card)
        throws BasicException {
        try {
            if (customerCard == null) {
                init();
            }
            customerCard.clearParameters();
            customerCard.setString(1, card);
            ResultSet rs = customerCard.executeQuery();
            List<CustomerInfoExt> custs = readCustomerResult(rs);
            if (custs.size() > 0) {
                return custs.get(0);
            } else {
            	List<String> values = new ArrayList<String>();
            	values.add(card);
            	Criteria criteria = new Criteria("Numéro client", "card", values);
            	DefaultListModel<Criteria> listModel = new DefaultListModel<Criteria>();
            	listModel.addElement(criteria);
            	JList<Criteria> test = new JList<Criteria>(listModel);
            	return DataLogicCustomers.searchCustomersByFidelite(test.getModel(), card);
            }
        } catch (SQLException e) {
            throw new BasicException(e);
        }
    }

    /** Get cached data if any, null otherwise */
    public static List<CustomerInfoExt> getTopCustomers()
        throws BasicException {
        try {
            if (ranking == null) {
                init();
            }
            ResultSet rs = ranking.executeQuery();
            return readCustomerResult(rs);
        } catch (SQLException e) {
            throw new BasicException(e);
        }
    }
    
    /**Modification de la recherche client */
    public static List<CustomerInfoExt> searchCustomersOpt(ListModel<Criteria> listModel, String table) throws BasicException {

        try {
        	CustomersCache searchCustomers = new CustomersCache();
            ResultSet rs = searchCustomers.searchOpt(listModel, table).executeQuery();
            return readCustomerResult(rs);
        } catch (SQLException e) {
            throw new BasicException(e);
        }
    }

    public static List<CustomerInfoExt> searchCustomers(String number,
            String searchkey, String name) throws BasicException {
        if (number == null) {
            number = "%%";
        } else {
            number = "%" + number + "%";
        }
        if (searchkey == null) {
            searchkey = "%%";
        } else {
            searchkey = "%" + searchkey + "%";
        }
        if (name == null) {
            name = "%%";
        } else {
            name = "%" + name + "%";
        }        
        try {
            if (search == null) {
                init();
            }
            search.clearParameters();
            search.setString(1, number);
            search.setString(2, searchkey);
            search.setString(3, name);
            ResultSet rs = search.executeQuery();
            return readCustomerResult(rs);
        } catch (SQLException e) {
            throw new BasicException(e);
        }
    }
    
    public static List<CustomerInfoExt> searchCustomersSplitted(String number,
            String searchkey, String name) throws BasicException {
    	String nameJok;
    	String query = "SELECT data FROM CUSTOMERS " ;
        String order = "ORDER BY ID DESC;" ;
    	String AND = " AND ";
    	String WHERE = " WHERE ";
    	String liaison = WHERE ;
    	
    	if (number != null) {
            number = "%" + number + "%";
            query = query.concat(liaison).concat("LOWER(number) LIKE LOWER('"+ number +"') ");
            if(WHERE.equals(liaison)) {
            	liaison = AND;
            }
        }
        if (searchkey != null) {
            searchkey = "%" + searchkey + "%";
            query = query.concat(liaison).concat("LOWER(key) LIKE LOWER('"+ searchkey +"') ");
            if(WHERE.equals(liaison)) {
            	liaison = AND;
            }
        }
        
		if (name != null) {
            nameJok = "%" + name + "%";
            query = query.concat(liaison).concat("LOWER(name) NOT LIKE LOWER('"+ nameJok +"') ");
            if(WHERE.equals(liaison)) {
            	liaison = AND;
            }
        }
        try {
        	if (name != null) {
	            for(String item : name.split("\\s+")) {
	            	item = "%" + item + "%" ;
	            	query = query.concat("AND LOWER(name) LIKE LOWER('"+ item+ "') ");
	            }
        	}
             ResultSet rs = LocalDB.executeQuery(query + order);
             return readCustomerResult(rs);
        } catch (SQLException e) {
            throw new BasicException(e);
        }
        
    }
    
    /**Recherche liste de customer**/
    public static List<String> searchCustomersList(List<String> listCustomerIdsLocal) throws BasicException {
    	try {
    		List<String> listCustomerIds = new ArrayList<String>();
    		if(listCustomerIdsLocal.size() > 0) {
	            if (customerIds == null) {
	                init();
	            }
	            customerIds.clearParameters();
	            customerIds.setObject(1, listCustomerIdsLocal.toArray());
	            ResultSet rs = customerIds.executeQuery();
	            List<CustomerInfoExt> listCustomers = readCustomerResult(rs);
	            
	            for (CustomerInfoExt customer : listCustomers) {
	            	listCustomerIds.add(customer.getId());
	            }
    		}
	        return listCustomerIds;
    		
        } catch (SQLException e) {
            throw new BasicException(e);
        }
    }
    
    /**Modification de la recherche client*/
    public static List<CustomerInfoExt> searchCustomersExactOpt(ListModel<Criteria> listModel, String table) throws BasicException {
    	try {
        	CustomersCache searchCustomers = new CustomersCache();
            ResultSet rs = searchCustomers.searchExactOpt(listModel, table).executeQuery();
            return readCustomerResult(rs);
        } catch (SQLException e) {
            throw new BasicException(e);
        }
    }
    
    public static List<CustomerInfoExt> searchCustomersExact(String number,
            String searchkey, String name) throws BasicException {
        if (number == null) {
            number = "%%";
        } 
        if (searchkey == null) {
            searchkey = "%%";
        } 
        if (name == null) {
            name = "%%";
        } 
        
        try {
            if (search == null) {
                init();
            }
            search.clearParameters();
            search.setString(1, number);
            search.setString(2, searchkey);
            search.setString(3, name);
            ResultSet rs = search.executeQuery();
            return readCustomerResult(rs);
        } catch (SQLException e) {
            throw new BasicException(e);
        }
    }

	public static List<CustomerInfoExt> getLastCustomers(Integer maxNumber) throws BasicException {
        if (maxNumber == null) {
        	maxNumber = 10;
        } 
        try {
            if (searchLast == null) {
                init();
            }
            searchLast.clearParameters();
            searchLast.setInt(1, maxNumber);
            ResultSet rs = searchLast.executeQuery();
            return readCustomerResult(rs);
        } catch (SQLException e) {
            throw new BasicException(e);
        }
	}

	@Override
	public PreparedStatement searchExactOpt(ListModel<Criteria> listModel) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PreparedStatement searchOpt(ListModel<Criteria> listModel) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}
}
