//    POS-Tech
//    Based upon Openbravo POS
//
//    Copyright (C) 2007-2009 Openbravo, S.L.
//                       2012 Scil (http://scil.coop)
//
//    This file is part of POS-Tech.
//
//    POS-Tech is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    POS-Tech is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with POS-Tech.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.forms;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Locale;
import java.util.Properties;
import java.util.logging.Logger;

/**
 *
 * @author adrianromero
 */
public class AppConfig implements AppProperties {

    private static Logger logger = Logger.getLogger("fr.pasteque.pos.forms.AppConfig");

    private static final String DEFAULT_SERVER = "http://localhost:8080/ose-server";
    private static final String DEMO_USER = "demo";
    private static final String DEMO_PASSWORD = "demo";
    private static final String DEFAULT_MACHINE = "Caisse";

    private static final String DEFAULT_DIR = System.getProperty("user.home")
            + "/." + AppLocal.APP_ID;
    public static AppConfig loadedInstance;

    private Properties m_propsconfig;
    private File configfile;

    public AppConfig(String[] args) {
        if (args.length == 0) {
            this.init(this.getDefaultConfig());
        } else {
            this.init(new File(args[0]));
        }
    }

    public String getDataDir() {
        return DEFAULT_DIR;
    }

    public AppConfig(File configfile) {
        this.init(configfile);
    }

    private void init(File configfile) {
        this.configfile = configfile;
        m_propsconfig = new Properties();

        logger.info("Reading configuration file: " + configfile.getAbsolutePath());
        File dir = new File(this.getDataDir());
        if (!dir.exists()) {
            dir.mkdirs();
        }
        //TODO EDU JPOS Mettre dans le fichier de config
        //System.setProperty(JposPropertiesConst.JPOS_POPULATOR_FILE_PROP_NAME, "C:\\Applis\\EPSON\\JAVAPos\\SetupPOS\\jpos.xml");
    }

    private File getDefaultConfig() {
        return new File(DEFAULT_DIR + "/config.properties");
    }
    private File getDefaultRestoreConfig() {
        return new File(DEFAULT_DIR + "/config.properties.restore");
    }

    public String getProperty(String sKey) {
        String prop = m_propsconfig.getProperty(sKey);
        if (prop == null) {
            prop = DEFAULT_VALUES.get(sKey);
        }
        return prop;
    }

    /** Check if user is using the demo account. */
    public boolean isDemo() {
        //TODO EDU - la plupart du temps dans un premier temps on sera avec le user demo et le default server
    	return false;
    	//return this.getProperty("server.backoffice").startsWith(DEFAULT_SERVER)
        //        && this.getProperty("db.user").equals(DEMO_USER);
    }

    /** Shortcut to getProperty("users.activated") with parsing.
     * @returns The list of activated user ids or null if all.
     */
    public String[] getEnabledUsers() {
        String prop = getProperty("users.activated");
        if (prop.equals("all")) {
            return null;
        }
        String ids[] = prop.split(",");
        for (String id : ids) {
            id = id.trim();
        }
        return ids;
    }

    public String getHost() {
        return this.getProperty("machine.hostname");
    }

    public File getConfigFile() {
        return this.configfile;
    }

    public void setProperty(String sKey, String sValue) {
        if (sValue == null) {
            m_propsconfig.remove(sKey);
        } else {
            m_propsconfig.setProperty(sKey, sValue);
        }
    }

    public String getLocale() {
        String slang = this.getProperty("user.language");
        String scountry = this.getProperty("user.country");
        //String svariant = this.getProperty("user.variant"); Unused
        String locCode = slang;
        if (scountry != null) {
            locCode += "_" + scountry;
        }
        return locCode;
    }

    public boolean delete() {
        this.loadDefault();
        return this.configfile.delete();
    }
    public boolean canRestore() {
        File restore = new File(this.configfile.getAbsolutePath() + ".restore");
        File defaultRestore = this.getDefaultRestoreConfig();
        return restore.exists() || defaultRestore.exists();
    }
    public void restore() throws IOException {
        File restore = new File(this.configfile.getAbsolutePath() + ".restore");
        File currentConfig = this.configfile;
        if (restore.exists()) {
            this.configfile = restore;
            this.load();
            this.configfile = currentConfig; // keep same file for save
            logger.info("Restored configuration from: " + restore.getAbsolutePath());
        } else {
            // Try with default restore file
            File defaultConfig = this.getDefaultRestoreConfig();
            if (defaultConfig.exists()) {
                this.configfile = defaultConfig;
                this.load();
                this.configfile = currentConfig;
                logger.info("Restored configuration from: " + defaultConfig.getAbsolutePath());
            } else {
                // No custom restore settings, use default.
                this.loadDefault();
                logger.info("Restored default configuration");
            }
        }
        this.save();
    }

    public void load() {
        loadDefault();
        try {
            InputStream in = new FileInputStream(configfile);
            if (in != null) {
                m_propsconfig.load(in);
                in.close();
            }
        } catch (IOException e){
            this.loadDefault();
        }
        AppConfig.loadedInstance = this;
    }

    public void save() throws IOException {
        OutputStream out = new FileOutputStream(configfile);
        if (out != null) {
            m_propsconfig.store(out, AppLocal.APP_NAME
                    + ". Configuration file.");
            out.close();
        }
    }

    /** The defaults values. These values are taken when the property is not
     * found in file. These values are not saved in properties files if they
     * are not set.
     */
    private static HashMap<String, String> DEFAULT_VALUES;
    static {
        DEFAULT_VALUES = new HashMap<String, String>();
        DEFAULT_VALUES.put("ui.touchbtnminwidth", "0.4"); // in inches
        DEFAULT_VALUES.put("ui.touchbtnminheight", "0.4"); // in inches
        DEFAULT_VALUES.put("ui.touchbigbtnminwidth", "0.5"); // in inches
        DEFAULT_VALUES.put("ui.touchbigbtnminheight", "0.5"); // in inches
        DEFAULT_VALUES.put("ui.touchhudgebtnminwidth", "0.8");  // in inches
        DEFAULT_VALUES.put("ui.touchhudgebtnminheight", "0.6"); // in inches
        DEFAULT_VALUES.put("ui.touchsmallbtnminwidth", "0.3"); // in inches
        DEFAULT_VALUES.put("ui.touchsmallbtnminheight", "0.3"); // in inches
        DEFAULT_VALUES.put("ui.touchbtnspacing", "0.08"); // in inches
        DEFAULT_VALUES.put("ui.fontsize", "12");
        DEFAULT_VALUES.put("ui.fontsizebig", "14");
        DEFAULT_VALUES.put("ui.fontsizesmall", "10");
        DEFAULT_VALUES.put("ui.showupdownbuttons", "0");
        DEFAULT_VALUES.put("ui.margintype", "percent");
        DEFAULT_VALUES.put("prices.setmode", "taxed");
        DEFAULT_VALUES.put("prices.roundto", "0");
        DEFAULT_VALUES.put("ui.printticketbydefault", "1");
        DEFAULT_VALUES.put("ui.printinvoicebydefault", "0");
        DEFAULT_VALUES.put("ui.autodisplaycustcount", "1");
        DEFAULT_VALUES.put("ui.ticketlineminwidth", "4.0"); // in inches
        DEFAULT_VALUES.put("ui.ticketlineminheight", "2.0"); // in inches
        DEFAULT_VALUES.put("printer.receipt.nb", "2"); // in inches
        DEFAULT_VALUES.put("server.racine", ""); // in inches
        DEFAULT_VALUES.put("cache.loadCustomerSince", "5"); // in minutes
    }

    /** Load "default file", which values are expanded or overriden by the
     * actually property file.
     * The properties that then saved along other in the properties file.
     */
    private void loadDefault() {
        m_propsconfig = new Properties();
        String dirname = System.getProperty("dirname.path");
        dirname = dirname == null ? "./" : dirname;

        Locale l = Locale.getDefault();
        m_propsconfig.setProperty("user.language", l.getLanguage());
        m_propsconfig.setProperty("user.country", l.getCountry());
        m_propsconfig.setProperty("user.variant", l.getVariant());

        m_propsconfig.setProperty("users.activated", "all");

        m_propsconfig.setProperty("swing.defaultlaf", System.getProperty("swing.defaultlaf", "javax.swing.plaf.metal.MetalLookAndFeel"));

        m_propsconfig.setProperty("machine.printer", "screen");
        m_propsconfig.setProperty("machine.printer.2", "Not defined");
        m_propsconfig.setProperty("machine.printer.3", "Not defined");
        m_propsconfig.setProperty("machine.display", "screen");
        m_propsconfig.setProperty("machine.scale", "screen");
        m_propsconfig.setProperty("machine.screenmode", "window"); // fullscreen / window
        m_propsconfig.setProperty("machine.screentype", "touchscreen");
        m_propsconfig.setProperty("machine.ticketsbag", "standard");
        m_propsconfig.setProperty("machine.scanner", "Not defined");

        m_propsconfig.setProperty("payment.gateway", "external");
        m_propsconfig.setProperty("payment.magcardreader", "Not defined");
        m_propsconfig.setProperty("payment.testmode", "false");
        m_propsconfig.setProperty("payment.commerceid", "");
        m_propsconfig.setProperty("payment.commercepassword", "password");

        m_propsconfig.setProperty("machine.printername", "(Default)");

        // Receipt printer paper set to 80mmx297mm
        m_propsconfig.setProperty("paper.receipt.x", "12");
        m_propsconfig.setProperty("paper.receipt.y", "22");
        m_propsconfig.setProperty("paper.receipt.width", "210");
        m_propsconfig.setProperty("paper.receipt.height", "820");
        m_propsconfig.setProperty("paper.receipt.mediasizename", "A4");
        m_propsconfig.setProperty("paper.receipt.orientation", "portrait");
        
        // Check printer paper set to 80mmx175mm
        m_propsconfig.setProperty("paper.check.x", "12");
        m_propsconfig.setProperty("paper.check.y", "22");
        m_propsconfig.setProperty("paper.check.width", "210");
        m_propsconfig.setProperty("paper.check.height", "451");
        m_propsconfig.setProperty("paper.check.mediasizename", "A4");
        m_propsconfig.setProperty("paper.check.orientation", "landscape");

        // Normal printer paper for A4
        m_propsconfig.setProperty("paper.standard.x", "12");
        m_propsconfig.setProperty("paper.standard.y", "22");
        m_propsconfig.setProperty("paper.standard.width", "451");
        m_propsconfig.setProperty("paper.standard.height", "820");
        m_propsconfig.setProperty("paper.standard.mediasizename", "A4");
        m_propsconfig.setProperty("paper.standard.orientation", "portrait");

        m_propsconfig.setProperty("machine.uniqueinstance", "false");

        // UI stuff
        m_propsconfig.setProperty("machine.screendensity", "72"); // In pixel per inch
        m_propsconfig.setProperty("ui.autohidemenu", "1");
        m_propsconfig.setProperty("ui.countmoney", "1");
        m_propsconfig.setProperty("ui.showbarcode", "1");
        
        m_propsconfig.setProperty("ui.appwidth", "1024");
        m_propsconfig.setProperty("ui.appheight", "700");
        m_propsconfig.setProperty("ui.showalerts", "0");

        // Server stuff
        //m_propsconfig.setProperty("server.backoffice",
        //        DEFAULT_SERVER + "/" + AppLocal.DB_VERSION);
        m_propsconfig.setProperty("server.backoffice",
                DEFAULT_SERVER + "/" );
        m_propsconfig.setProperty("db.user", DEMO_USER);
        m_propsconfig.setProperty("db.password", DEMO_PASSWORD);
        m_propsconfig.setProperty("machine.hostname", DEFAULT_MACHINE);
        
        m_propsconfig.setProperty("printer.receipt.nb" , "2");
        
        m_propsconfig.setProperty("server.backoffice.timeout" , "5");
        m_propsconfig.setProperty("file.userjson" , "C:\\appli\\POS/user_search.json");
        m_propsconfig.setProperty("server.racine" , DEFAULT_VALUES.get("server.racine"));
        
        m_propsconfig.setProperty("cache.loadCustomerSince" , DEFAULT_VALUES.get("cache.loadCustomerSince"));
    }
}
