package fr.pasteque.pos.forms;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import fr.pasteque.pos.util.AltEncrypter;

public class RESTCall<T> {

	private static MultiValueMap<String, String> params(String api, String action, String... params) {
		MultiValueMap<String, String> ret = new LinkedMultiValueMap<String, String>();
		String user = AppConfig.loadedInstance.getProperty("db.user");
		String password = AppConfig.loadedInstance.getProperty("db.password");
		if (password != null && password.startsWith("crypt:")) {
			// the password is encrypted
			AltEncrypter cypher = new AltEncrypter("cypherkey" + user);
			password = cypher.decrypt(password.substring(6));
		}
		
		ret.add("login", user);
		ret.add("password", password);
		ret.add("p", api);
		ret.add("action", action);
		for (int i = 0; i < params.length; i += 2) {
			if (params[i + 1] != null) {
				String key = params[i];
				String value = params[i + 1];
				ret.add(key, value);
			}
		}
		return ret;
	}

	public RESTResponse<T> getData(String api, String action, String... params) {
		String url = AppConfig.loadedInstance.getProperty("server.backoffice");
		//url += "api.php";
		RestTemplate restTemplate = new RestTemplate();
		UriComponentsBuilder fromUriString = UriComponentsBuilder.fromUriString(url);
		fromUriString.queryParams(RESTCall.params(api, action, params));
		ResponseEntity<RESTResponse<T>> response = restTemplate.exchange(fromUriString.build().toUri(), HttpMethod.GET,
				null, new ParameterizedTypeReference<RESTResponse<T>>() {
				});
		return response.getBody();
	}

}
