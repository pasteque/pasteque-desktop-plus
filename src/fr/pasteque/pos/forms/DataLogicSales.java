//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2007-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.
//
//    cashin/cashout notes by Henri Azar

package fr.pasteque.pos.forms;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONArray;
import org.json.JSONObject;



import fr.pasteque.basic.BasicException;
import fr.pasteque.data.loader.ServerLoader;
import fr.pasteque.pos.admin.CurrencyInfo;
import fr.pasteque.pos.caching.CallQueue;
import fr.pasteque.pos.caching.CatalogCache;
import fr.pasteque.pos.caching.CurrenciesCache;
import fr.pasteque.pos.caching.CustomersCache;
import fr.pasteque.pos.caching.LocalDB;
import fr.pasteque.pos.caching.TariffAreasCache;
import fr.pasteque.pos.caching.TaxesCache;
import fr.pasteque.pos.customers.CustomerInfoExt;
import fr.pasteque.pos.inventory.TaxCategoryInfo;
import fr.pasteque.pos.ticket.CashMove;
import fr.pasteque.pos.ticket.CashSession;
import fr.pasteque.pos.ticket.CategoryInfo;
import fr.pasteque.pos.ticket.CompositionInfo;
import fr.pasteque.pos.ticket.ProductInfoExt;
import fr.pasteque.pos.ticket.SubgroupInfo;
import fr.pasteque.pos.ticket.TariffInfo;
import fr.pasteque.pos.ticket.TariffValidity;
import fr.pasteque.pos.ticket.TaxInfo;
import fr.pasteque.pos.ticket.TicketInfo;
import fr.pasteque.pos.ticket.TicketLineInfo;
import fr.pasteque.pos.ticket.ZTicket;
import fr.pasteque.pos.util.URLTextGetter.ServerException;

/**
 *
 * @author adrianromero
 */
public class DataLogicSales {

    private static Logger logger = Logger.getLogger("fr.pasteque.pos.forms.DatalogicSales");
    private static final String TYPE_CAT = "getCat";
    private static final String TYPE_PRD = "getPrd";

    /** Creates a new instance of SentenceContainerGeneric */
    public DataLogicSales() {
    }

    private byte[] loadImage(String type, String id) {
        try {
            ServerLoader loader = new ServerLoader();
            return loader.readBinary(type, id);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public boolean preloadCategories() {
        try {
            logger.log(Level.INFO, "Preloading categories");
            ServerLoader loader = new ServerLoader();
            ServerLoader.Response r = loader.read("CategoriesAPI", "getAll");
            if (r.getStatus().equals(ServerLoader.Response.STATUS_OK)) {
                JSONArray a = r.getArrayContent();
                List<CategoryInfo> categories = new ArrayList<CategoryInfo>();
                for (int i = 0; i < a.length(); i++) {
                    JSONObject o = a.getJSONObject(i);
                    CategoryInfo cat = new CategoryInfo(o);
                    categories.add(cat);
                    try {
                        if (o.getBoolean("hasImage")) {
                            byte[] img = this.loadImage(TYPE_CAT, cat.getID());
                            CatalogCache.storeCategoryImage(cat.getID(), img);
                        }
                    } catch (BasicException e) {
                        logger.log(Level.WARNING,
                                "Unable to get category image for "
                                + cat.getID(), e);
                    }
                }
                CatalogCache.refreshCategories(categories);
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    public boolean preloadProducts(Date lastUpdate) {
        try {
            logger.log(Level.INFO, "Preloading products");
            ServerLoader loader = new ServerLoader();
            ServerLoader.Response r ;
            if(lastUpdate.getTime() == 0) {
            	r = loader.read("ProductsAPI", "getAll");
            } else {
            	r = loader.read("ProductsAPI", "getAll" , "date" , String.valueOf(lastUpdate.getTime()));
            }
            
            if (r.getStatus().equals(ServerLoader.Response.STATUS_OK)) {
                JSONArray a = r.getArrayContent();
                List<ProductInfoExt> products = new ArrayList<ProductInfoExt>();
                for (int i = 0; i < a.length(); i++) {
                    JSONObject o = a.getJSONObject(i);
                    
                    if (!o.getBoolean("visible")) {
                        // Don't add products not sold
                        continue;
                    }
                    
                    ProductInfoExt prd = new ProductInfoExt(o);
                    products.add(prd);
                    try {
                        if (o.getBoolean("hasImage")) {
                            byte[] img = this.loadImage(TYPE_PRD, prd.getID());
                            CatalogCache.storeProductImage(prd.getID(), img);
                        }
                    } catch (BasicException e) {
                        logger.log(Level.WARNING,
                                "Unable to get product image for "
                                + prd.getID(), e);
                    }

                }
                CatalogCache.refreshProducts(products , lastUpdate);
                CatalogCache.refreshBarCodes(products , lastUpdate);
                CatalogCache.refreshCompositions(products , lastUpdate);
                CatalogCache.refreshMessages(products , lastUpdate);
                
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /** Get a product by ID */
    public final ProductInfoExt getProductInfo(String id)
        throws BasicException {
        return CatalogCache.getProduct(id);
    }

    /** Get a product by code */
    public final ProductInfoExt getProductInfoByCode(String sCode) throws BasicException {
        return CatalogCache.getProductByCode(sCode);
    }

    public List<ProductInfoExt> searchProducts(String label, String reference)
        throws BasicException {        
    	List<ProductInfoExt> result = CatalogCache.searchProductsExact(label, reference);
    	List<ProductInfoExt> resultJok = CatalogCache.searchProducts(label, reference); 

    		if(resultJok.size() > 0) {
    			resultJok.removeAll(result);
    			result.addAll(resultJok);
    		}
    		if(label != null) {
    			result.addAll(CatalogCache.searchProductsSplitted(label, reference));
    		}
    	
        return result;
    }

    /** Get root categories. Categories must be preloaded. */
    public final List<CategoryInfo> getRootCategories() throws BasicException {
        return CatalogCache.getRootCategories();
    }

    public final CategoryInfo getCategory(String id) throws BasicException {
        return CatalogCache.getCategory(id);
    }

    /** Get subcategories from parent ID. Categories must be preloaded. */
    public final List<CategoryInfo> getSubcategories(String category) throws BasicException  {
        return CatalogCache.getSubcategories(category);
    }

    public final List<CategoryInfo> getCategories() throws BasicException {
        return CatalogCache.getCategories();
    }

    public boolean preloadCompositions() {
        try {
            logger.log(Level.INFO, "Preloading compositions");
            ServerLoader loader = new ServerLoader();
            ServerLoader.Response r = loader.read("CompositionsAPI", "getAll");
            if (r.getStatus().equals(ServerLoader.Response.STATUS_OK)) {
                JSONArray a = r.getArrayContent();
                Map<String, List<SubgroupInfo>> compos = new HashMap<String, List<SubgroupInfo>>();
                Map<Integer, List<String>> groups = new HashMap<Integer, List<String>>();
                for (int i = 0; i < a.length(); i++) {
                    JSONObject o = a.getJSONObject(i);
                    String prdId = o.getString("id");
                    compos.put(prdId, new ArrayList<SubgroupInfo>());
                    JSONArray grps = o.getJSONArray("groups");
                    for (int j = 0; j < grps.length(); j++) {
                        JSONObject oGrp = grps.getJSONObject(j);
                        SubgroupInfo subgrp = new SubgroupInfo(oGrp);
                        compos.get(prdId).add(subgrp);
                        groups.put(subgrp.getID(), new ArrayList<String>());
                        JSONArray choices = oGrp.getJSONArray("choices");
                        for (int k = 0; k < choices.length(); k++) {
                            JSONObject oPrd = choices.getJSONObject(k);
                            groups.get(subgrp.getID()).add(oPrd.getString("productId"));
                        }
                    }
                }
                CatalogCache.refreshSubgroups(compos);
                CatalogCache.refreshSubgroupProds(groups);
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    public final List<SubgroupInfo> getSubgroups(String composition)
        throws BasicException  {
        return CatalogCache.getSubgroups(composition);
    }
    public final List<ProductInfoExt> getSubgroupCatalog(Integer subgroup)
        throws BasicException  {
        return CatalogCache.getSubgroupProducts(subgroup);
    }

    /** Get products from a category ID. Products must be preloaded. */
    public List<ProductInfoExt> getProductCatalog(String category) throws BasicException  {
        return CatalogCache.getProductsByCategory(category);
    }

    /** Get products associated to a first one by ID */
    public List<ProductInfoExt> getProductComments(String id) throws BasicException {
        return new ArrayList<ProductInfoExt>();
        // TODO: enable product comments
        /*return new PreparedSentence(s,
            "SELECT P.ID, P.REFERENCE, P.CODE, P.NAME, P.ISCOM, P.ISSCALE, "
            + "P.PRICEBUY, P.PRICESELL, P.TAXCAT, P.CATEGORY, "
            + "P.ATTRIBUTESET_ID, P.IMAGE, P.ATTRIBUTES, P.DISCOUNTENABLED, "
            + "P.DISCOUNTRATE "
            + "FROM PRODUCTS P, PRODUCTS_CAT O, PRODUCTS_COM M "
            + "WHERE P.ID = O.PRODUCT AND P.ID = M.PRODUCT2 "
            + "AND M.PRODUCT = ? AND P.ISCOM = " + s.DB.TRUE() + " "
            + "ORDER BY O.CATORDER, P.NAME",
            SerializerWriteString.INSTANCE,
            ProductInfoExt.getSerializerRead()).list(id);*/
    }

    /** Get the list of tickets from opened cash sessions. */
    public List<TicketInfo> getSessionTickets() throws BasicException {
        try {
            ServerLoader loader = new ServerLoader();
            ServerLoader.Response r = loader.read("TicketsAPI", "getOpen");
            if (r.getStatus().equals(ServerLoader.Response.STATUS_OK)) {
                JSONArray a = r.getArrayContent();
                List<String> listCustomerIdsLocal = new ArrayList<String>();
                for (int i = 0; i < a.length(); i++) {
                    JSONObject o = a.getJSONObject(i);
                    if (!o.isNull("customerId")) {
                    	listCustomerIdsLocal.add(o.getString("customerId"));
                    }
                }
                
                if (listCustomerIdsLocal.size() > 0) {
                	importCustomersTicket(listCustomerIdsLocal);
                }
                                        
                List<TicketInfo> list = new ArrayList<TicketInfo>();
                for (int i = 0; i < a.length(); i++) {
                    JSONObject o = a.getJSONObject(i);
                    list.add(new TicketInfo(o));
                }
                return list;
            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new BasicException(e);
        }
    }
    
    //PGA : Import dans la base locale de la caisse les clients créés depuis plus
    // de 30 jours et qui est affectée à un ticket
	public void importCustomersTicket(List<String> listCustomerIdsManquant) throws BasicException {
    	
		if(listCustomerIdsManquant.size() > 0) {
			//PGA: Suppression de la liste les id customer en double.
			Set<String> dedoublonnage  = new HashSet<String>(listCustomerIdsManquant);
			List<String> listCustomerIdsManquantdedouble = new ArrayList<String>(dedoublonnage);
			listCustomerIdsManquant = listCustomerIdsManquantdedouble;
			
			
			List<String> listCustomerIdsLocal = CustomersCache.searchCustomersList(listCustomerIdsManquant); 
			listCustomerIdsManquant.removeAll(listCustomerIdsLocal);
		}
		
		if (listCustomerIdsManquant.size()>0) {
			
			ServerLoader loader = new ServerLoader();
			List<CustomerInfoExt> listCustomers = new ArrayList<CustomerInfoExt>();
			for (String id : listCustomerIdsManquant) {
				try {
					ServerLoader.Response r = loader.read("CustomersAPI", "get", "id", id);
					 if (r.getStatus().equals(ServerLoader.Response.STATUS_OK)) {
						 JSONObject o = r.getObjContent();
			             CustomerInfoExt customer = new CustomerInfoExt(o);
			             listCustomers.add(customer);
					 }
					
				} catch (SocketTimeoutException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ServerException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			Date lastUpdate = LocalDB.getLastUpdate();
			CustomersCache.refreshCustomers(listCustomers,lastUpdate);
			
		}
    }

    //Tickets and Receipt list
    public List<TicketInfo> searchTickets(Long tktId, Integer ticketType,
            String cashId, Date start, Date stop, String customerId,
            String userId) throws BasicException {
        try {
            ServerLoader loader = new ServerLoader();
            ServerLoader.Response r = loader.read("TicketsAPI", "search",
                    "ticketId", tktId != null ? tktId.toString() : null,
                    "ticketType",
                    ticketType != null ? ticketType.toString() : null,
                    "cashId", cashId,
                    "dateStart",
                    start != null ? String.valueOf(start.getTime()) : null,
                    "dateStop",
                    stop != null ? String.valueOf(stop.getTime()) : null,
                    "customerId", customerId, "userId", userId);
            if (r.getStatus().equals(ServerLoader.Response.STATUS_OK)) {
                JSONArray a = r.getArrayContent();
                List<String> listCustomerIdsLocal = new ArrayList<String>();
                for (int i = 0; i < a.length(); i++) {
                    JSONObject o = a.getJSONObject(i);
                    if (!o.isNull("customerId")) {
                    	listCustomerIdsLocal.add(o.getString("customerId"));
                    }
                }
                
                if (listCustomerIdsLocal.size() > 0) {
                	importCustomersTicket(listCustomerIdsLocal);
                }
                                        
                List<TicketInfo> list = new ArrayList<TicketInfo>();
                for (int i = 0; i < a.length(); i++) {
                    JSONObject o = a.getJSONObject(i);
                    list.add(new TicketInfo(o));
                }
                return list;
            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new BasicException(e);
        }
    }

    public boolean preloadTaxes() {
        try {
            logger.log(Level.INFO, "Preloading taxes");
            ServerLoader loader = new ServerLoader();
            ServerLoader.Response r = loader.read("TaxesAPI", "getAll");
            if (r.getStatus().equals(ServerLoader.Response.STATUS_OK)) {
                JSONArray a = r.getArrayContent();
                List<TaxCategoryInfo> taxCats = new ArrayList<TaxCategoryInfo>();
                List<TaxInfo> taxes = new ArrayList<TaxInfo>();
                for (int i = 0; i < a.length(); i++) {
                    JSONObject o = a.getJSONObject(i);
                    TaxCategoryInfo taxCat = new TaxCategoryInfo(o);
                    taxCats.add(taxCat);
                    JSONArray a2 = o.getJSONArray("taxes");
                    for (int j = 0; j < a2.length(); j++) {
                        JSONObject o2 = a2.getJSONObject(j);
                        TaxInfo tax = new TaxInfo(o2);
                        taxes.add(tax);
                    }
                }
                TaxesCache.refreshTaxCategories(taxCats);
                TaxesCache.refreshTaxes(taxes);
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public final List<TaxInfo> getTaxList() throws BasicException {
        return TaxesCache.getTaxes();
    }
    public TaxInfo getTax(String taxId) throws BasicException {
        return TaxesCache.getTax(taxId);
    }

    public final List<TaxCategoryInfo> getTaxCategoriesList()
        throws BasicException {
        return TaxesCache.getTaxCats();
    }

    /*public final SentenceList getAttributeSetList() {
        return null;
        // TODO: reenable attributes
        return new StaticSentence(s
            , "SELECT ID, NAME FROM ATTRIBUTESET ORDER BY NAME"
            , null
            , new SerializerRead() { public Object readValues(DataRead dr) throws BasicException {
                return new AttributeSetInfo(dr.getString(1), dr.getString(2));
                }});
                }*/


    public boolean preloadCurrencies() {
        try {
            logger.log(Level.INFO, "Preloading currencies");
            ServerLoader loader = new ServerLoader();
            ServerLoader.Response r = loader.read("CurrenciesAPI", "getAll");
            if (r.getStatus().equals(ServerLoader.Response.STATUS_OK)) {
                JSONArray a = r.getArrayContent();
                List<CurrencyInfo> currencies = new ArrayList<CurrencyInfo>();
                for (int i = 0; i < a.length(); i++) {
                    JSONObject o = a.getJSONObject(i);
                    CurrencyInfo currency = new CurrencyInfo(o);
                    currencies.add(currency);
                }
                CurrenciesCache.refreshCurrencies(currencies);
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public final List<CurrencyInfo> getCurrenciesList() throws BasicException {
        return CurrenciesCache.getCurrencies();
    }

    public CurrencyInfo getCurrency(int currencyId) throws BasicException {
        return CurrenciesCache.getCurrency(currencyId);
    }

    public CurrencyInfo getMainCurrency() throws BasicException {
        return CurrenciesCache.getMainCurrency();
    }

    public final boolean isCashActive(String id) throws BasicException {
        DataLogicSystem dlSystem = new DataLogicSystem();
        CashSession session = dlSystem.getCashSessionById(id);
        return session.isOpened();
    }

    public ZTicket getZTicket(String cashSessionId) throws BasicException {
        try {
            ServerLoader loader = new ServerLoader();
            ServerLoader.Response r = loader.read("CashesAPI", "zticket",
                    "id", cashSessionId);
            if (r.getStatus().equals(ServerLoader.Response.STATUS_OK)) {
                JSONObject o = r.getObjContent();
                if (o == null) {
                    return null;
                } else {
                    return new ZTicket(o);
                }
            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new BasicException(e);
        }
    }


    /** Save or edit ticket */
    public final void saveTicket(final TicketInfo ticket,
            final String locationId,
            final String cashId, final CustomerInfoExt customer) throws BasicException {
    	Integer levelSearchCustomer = null;
        if (CallQueue.isOffline()) {
        	if (customer != null) {
        		CallQueue.queueCustomerSave(customer);
        	}
            // Don't try to send and wait for recovery
            CallQueue.queueTicketSave(ticket);
            return;
        }
        try {
        	ServerLoader loader = new ServerLoader();
            ServerLoader.Response r;
            
            r = loader.read("VersionAPI", "getServer");
            
        	//PG Test connexion au BO Local
        	if (!r.getStatus().equals(ServerLoader.Response.STATUS_OK)) {
                throw new BasicException("Bad server response");
            }
        	if (customer != null) {
        		levelSearchCustomer = customer.getLevelSearch();
	        	if (customer.getLevelSearch().equals(1) || customer.getLevelSearch().equals(2)) {
	        		try {
	                	 CustomersCache.createCustomers(customer);
	                } catch (BasicException e) {
	                    e.printStackTrace();
	                }
	        	}
	        	if (levelSearchCustomer.equals(2)) {	
	        		r = loader.write("CustomersAPI", "saveCustomer", "", customer.toJSON().toString());
	                if (!r.getStatus().equals(ServerLoader.Response.STATUS_OK)) {
	                    throw new BasicException("Bad server response");
	                }
	        	}
        	}
            
            r = loader.write("TicketsAPI", "save",
                    "ticket", ticket.toJSON().toString(), "cashId", cashId);
            if (!r.getStatus().equals(ServerLoader.Response.STATUS_OK)) {
                throw new BasicException("Bad server response");
            }
        } catch (Exception e) {
        	if (customer != null) {
        		CallQueue.queueCustomerSave(customer);
        	}
            // Unable to save, queue it
            logger.log(Level.WARNING, "Unable to save ticket: "
                    + e.getMessage());
            CallQueue.queueTicketSave(ticket);
            throw new BasicException(e);
        }
    }
    /**
     * Save line ticket deleted
     * @param line
     * @param ticketId 
     * @throws BasicException 
     */
    public void saveTicketLinesDeleted(List<TicketLineInfo> lines,TicketLineInfo line, String ticketId, boolean isOpenedPayment) throws BasicException {
	    	List<TicketLineInfo> linesInfo  = new  ArrayList<TicketLineInfo>();
			 if (line != null){
				linesInfo.add(line);
			 }else{
				linesInfo = lines;
			 } 	
			 if (CallQueue.isOffline()) {
		 		// Don't try to send and wait for recovery
	            CallQueue.queueTicketLinesDeletedSave(linesInfo,ticketId,isOpenedPayment);
	            return;
	        }
	        try {
	        	String linesJSON = null;
	        	String lineJSON = null;
	        	ArrayList<JSONObject> listJSON;
				if (lines !=null){
	        		listJSON= new ArrayList<JSONObject>();
	        		for(TicketLineInfo ligne:lines){ 
	        			listJSON.add(ligne.lineDeletedToJSON());
	        		}
	        		linesJSON=listJSON.toString();
	        	}else if (line != null){
	        		TicketLineInfo copy = line.copyTicketLine();
	        		lineJSON = copy.lineDeletedToJSON().toString();
	        	}
	            ServerLoader loader = new ServerLoader();
	            ServerLoader.Response r;
	            r = loader.write("TicketLinesDeletedAPI", "save",
	                    "ticketLinesDeleted", linesJSON,
	                    "ticketLineDeleted",lineJSON,
	                    "ticketId",ticketId,"isOpenedPayment",String.valueOf(isOpenedPayment));
	            if (!r.getStatus().equals(ServerLoader.Response.STATUS_OK)) {
	                throw new BasicException("Bad server response");
	            }
	        } catch (Exception e) {
	            // Unable to save, queue it
	            logger.log(Level.WARNING, "Unable to save TicketLinesDeleted : "
	                    + e.getMessage());
	            CallQueue.queueTicketLinesDeletedSave(linesInfo,ticketId,isOpenedPayment);
	            throw new BasicException(e);
	        }
		
	}
    public void deleteTicket(TicketInfo ticket) throws BasicException {
        try {
            ServerLoader loader = new ServerLoader();
            ServerLoader.Response r;
            r = loader.write("TicketsAPI", "delete",
                    "id", ticket.getId());
            if (!r.getStatus().equals(ServerLoader.Response.STATUS_OK)) {
                throw new BasicException("Bad server response");
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new BasicException(e);
        }
    }

    public boolean preloadTariffAreas() {
        try {
            logger.log(Level.INFO, "Preloading tariff areas");
            ServerLoader loader = new ServerLoader();
            ServerLoader.Response r = loader.read("TariffAreasAPI", "getAll");
            if (r.getStatus().equals(ServerLoader.Response.STATUS_OK)) {
                JSONArray a = r.getArrayContent();
                Map<Integer, Map<String, Double>> prices = new HashMap<Integer, Map<String, Double>>();
                List<TariffInfo> areas = new ArrayList<TariffInfo>();
                List<TariffValidity> lValidity = new ArrayList<TariffValidity>();
                for (int i = 0; i < a.length(); i++) {
                    JSONObject o = a.getJSONObject(i);
                    TariffInfo area = new TariffInfo(o);
                    areas.add(area);
                    Map<String, Double> areaPrices = new HashMap<String, Double>();
                    JSONArray aPrices = o.getJSONArray("prices");
                    for (int j = 0; j < aPrices.length(); j++) {
                        JSONObject oPrice = aPrices.getJSONObject(j);
                        String prdId = oPrice.getString("productId");
                        double price = oPrice.getDouble("price");
                        areaPrices.put(prdId, price);
                    }
                    prices.put(area.getID(), areaPrices);
                    
                    JSONArray aValidity = o.getJSONArray("validity");
                    for (int j = 0; j < aValidity.length(); j++) {
                        JSONObject oValidity = aValidity.getJSONObject(j);
                        TariffValidity validity = new TariffValidity(oValidity);
                        validity.setPriority(area.getOrder());
                       	lValidity.add(validity);
                        
                    }
                }
                TariffAreasCache.refreshTariffAreas(areas);
                TariffAreasCache.refreshValidity(lValidity);
                TariffAreasCache.refreshPrices(prices);
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public Double getTariffAreaPrice(int tariffAreaId, String productId) throws BasicException {
        return TariffAreasCache.getPrice(tariffAreaId, productId);
    }

    public final List<TariffInfo> getTariffAreaList() throws BasicException {
        return TariffAreasCache.getAreas();
    }

    public boolean saveMove(CashMove move) throws BasicException {
        try {
            ServerLoader loader = new ServerLoader();
            ServerLoader.Response r = loader.write("CashMvtsAPI", "move",
                    "cashId", move.getCashId(),
                    "date", String.valueOf(move.getDate().getTime()),
                    "note", move.getNote(),
                    "payment", move.getPayment().toJSON().toString());
            if (r.getStatus().equals(ServerLoader.Response.STATUS_OK)) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new BasicException(e);
        }
    }

    /**
     * Obtenir le prix de vente par défaut pour un produit
     * 	- Attention à chaque instant on peut avoir plusieurs tariffaires valides
     * Dans ce cas il faut regarder la priorité pour choisir
     *  - Attention certain produits ne sont pas disponibles à la vente si ils ne sont pas sur un tarrifaire actif
     * 
     * @param m_App - l'appli pour pouvoir à partir de la caisse, avoir les tariffaires actifs
     * @param m_oTicket - le ticket en cours pour pouvoir éventuellement intervenir sur le tariffaires actif
     * @param productID - l'id du produit pour avoir le prix pratiqué
     * @return - le prix
     * @throws BasicException
     */
	public Double getTariffAreaPrice(AppView m_App, TicketInfo m_oTicket, String productID , List<Integer> areaId) throws BasicException {
		Double retour = null;
		List<Integer> tarriffAreasIds = TariffAreasCache.getActiveArreas(m_App.getCashRegister().getLocationId());
		tarriffAreasIds.add(m_oTicket.getTariffArea());
		
		//On parcours la liste des tariffaires actifs
		//On s'arrête dèq que l'on a un prix
		for( int i = 0 ; retour == null && i < tarriffAreasIds.size() ; i++) {
			retour =  TariffAreasCache.getPrice(tarriffAreasIds.get(i), productID);
			if (areaId != null) {
				areaId.add(0 , tarriffAreasIds.get(i)) ;
			}
		}
		//On retourne null si on n'a pas de prix = retour au prix par défaut !
		
		//TODO Voir si on permet dans certains cas - paramètre config par exemple ? 
		// ou droits utilisateur  de donner la priorité au tariffaire sélectionné par l'opérateur.
		
		return retour;
	}
	
    public final List<TariffInfo> getActiveTariffAreaList(AppView m_App) throws BasicException {
    	List<Integer> tarriffAreasIds = TariffAreasCache.getActiveArreas(m_App.getCashRegister().getLocationId());
    	List<TariffInfo> retour = new ArrayList<TariffInfo>();
    	for( int i = tarriffAreasIds.size() ; i-- > 0 ; ) {
    		retour.add(TariffAreasCache.getArea(tarriffAreasIds.get(i)));
    	}
    	
    	return retour;
    }

	public boolean checkComposedProductsAvailability(String m_sInventoryLocation) {
		// TODO implementer
		HashMap<String,ProductInfoExt> updatedProducts = new HashMap<String,ProductInfoExt>();
		List<ProductInfoExt> products = new ArrayList<ProductInfoExt>();
		List<Integer> tarriffAreasIds = new ArrayList<Integer>(); 
				
		Boolean allProducts = false ;
		Boolean updateCandidate ; 
		Double price;
		
		//On regarde tous les produits qui sont des compos
		try {
			tarriffAreasIds = TariffAreasCache.getActiveArreas(m_sInventoryLocation);
			products = CatalogCache.getComposedProducts();
		} catch (BasicException e1) {
			
			e1.printStackTrace();
		}

		//Si il n'y en a aucun (premier lancement après update depuis v5) on regarde tous les produits tout court
		if(products.isEmpty()) {
			try {
				allProducts = true ;
				products = CatalogCache.searchProducts("%", "%");
			} catch (BasicException e) {
				e.printStackTrace();
			}
		}
		
		for(ProductInfoExt product : products) {
			updateCandidate = false ;
			//On vérifie que ce soit une compo
			if(product.getAllChildren().isEmpty()) {
				//On verifie que ça colle avec le bool is composed
				if(!allProducts ){
				//	-> si non on corrige et place le flag de l'update à true
					updateCandidate = true ;
				}
			} else {
			//On verifie que ça colle avec le bool is composed
				if(allProducts ){
				//	-> si non on corrige et place le flag de l'update à true
					updateCandidate = true ;
				}
				
				
				//On regarde si elle est dans un tariffaire actif
				//On parcours la liste des tariffaires actifs
				//On s'arrête dèq que l'on a un prix
				price = null ;
				for( int i = 0 ; price == null && i < tarriffAreasIds.size() ; i++) {
					try {
						price =  TariffAreasCache.getPrice(tarriffAreasIds.get(i), product.getID());
					} catch (BasicException e) {
						e.printStackTrace();
					}
				}
				
				//On vérifie que ce retour soit identique au statut actif actuel
				//	-> si non on corrige et place le flag de l'update à true
				//	La correction = modifier le boolean mais surtout modifier les listes de parents de tous les enfants
				//	ajouter les enfants à la liste des update à faire
				if(price == null && product.isActive()) {
					updateCandidate = true ;
					product.setActive(false);
					for(CompositionInfo child : product.getAllChildren()) {
					//faire le tour des enfants
						ProductInfoExt childProduct = null ;
						try {
							childProduct = updatedProducts.get(child.getChildId()) ;
							if(childProduct == null ) {
								childProduct = CatalogCache.getProduct(child.getChildId());
							} else {
								updatedProducts.remove(childProduct.getID());
							}
							childProduct.removeActiveParent(child);
							updatedProducts.put(childProduct.getID() ,childProduct);
						} catch (BasicException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					//ajouter l'enfant aux produits à mettre à jour
					//enlever ce parent de la liste des parents actifs
					}
				}
				
				if(price != null && !product.isActive()) {
					updateCandidate = true ;
					product.setActive(true);
					for(CompositionInfo child : product.getAllChildren()) {
					//faire le tour des enfants
						ProductInfoExt childProduct = null ;
						try {
							childProduct = updatedProducts.get(child.getChildId()) ;
							if(childProduct == null ) {
								childProduct = CatalogCache.getProduct(child.getChildId());
								
							} else {
								updatedProducts.remove(childProduct.getID());
							}
							childProduct.addActiveParent(child);
							updatedProducts.put(childProduct.getID() ,childProduct);
							
						} catch (BasicException e) {
							e.printStackTrace();
						}
					//ajouter l'enfant aux produits à mettre à jour
					//ajouter ce parent dans la liste des parents actifs
					}
				}		
			}
			
			if(updateCandidate) {
				updatedProducts.put(product.getID() ,product);
			}
		}

		
		//Si la liste des update est non vide, on update 
		if( ! updatedProducts.isEmpty()) {
			try {
				CatalogCache.refreshProducts(updatedProducts.values() , new Date());
			} catch (BasicException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return true;
		
	}

}
