//    POS-Tech
//    Based upon Openbravo POS
//
//    Copyright (C) 2007-2009 Openbravo, S.L.
//                       2012 SARL SCOP Scil (http://scil.coop)
//
//    This file is part of POS-Tech.
//
//    POS-Tech is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    POS-Tech is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with POS-Tech.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.pos.forms;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.xml.bind.DatatypeConverter;

import org.json.JSONArray;
import org.json.JSONObject;

import fr.pasteque.basic.BasicException;
import fr.pasteque.data.loader.ImageUtils;
import fr.pasteque.data.loader.ServerLoader;
import fr.pasteque.format.Formats;
import fr.pasteque.pos.admin.InseeInfo;
import fr.pasteque.pos.caching.CallQueue;
import fr.pasteque.pos.caching.CashRegistersCache;
import fr.pasteque.pos.caching.InseeCache;
import fr.pasteque.pos.caching.ResourcesCache;
import fr.pasteque.pos.caching.RolesCache;
import fr.pasteque.pos.caching.UsersCache;
import fr.pasteque.pos.ticket.CashRegisterInfo;
import fr.pasteque.pos.ticket.CashSession;
import fr.pasteque.pos.ticket.SalesLocationInfo;
import fr.pasteque.pos.util.ThumbNailBuilder;
import fr.pasteque.pos.util.URLTextGetter.ServerException;

/**
 *
 * @author adrianromero
 */
public class DataLogicSystem {

	private static Logger logger = Logger.getLogger("fr.pasteque.pos.forms.DataLogicSystem");

	/** Creates a new instance of DataLogicSystem */
	public DataLogicSystem() {
	}

	public final String findDbVersion() throws BasicException {
		try {
			ServerLoader loader = new ServerLoader();
			ServerLoader.Response r = loader.read("VersionAPI", "");
			if (r.getStatus().equals(ServerLoader.Response.STATUS_OK)) {
				JSONObject v = r.getObjContent();
				return v.getString("level");
			} else {
				return null;
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new BasicException(e);
		}
	}
	public final void execDummy() throws BasicException {
		try {
			ServerLoader loader = new ServerLoader();
			ServerLoader.Response r = loader.read("" ,"");
			if (r.getStatus().equals(ServerLoader.Response.STATUS_REJECTED)) {
				return;
			} else {
				throw new BasicException("Bad server response");
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new BasicException(e);
		}
	}

	/** Get users from server */
	private List<AppUser> loadUsers() throws BasicException {
		try {
			ServerLoader loader = new ServerLoader();
			ServerLoader.Response r = loader.read("UsersAPI", "getAll");
			final ThumbNailBuilder tnb = new ThumbNailBuilder(32, 32,
					"default_user.png");
			if (r.getStatus().equals(ServerLoader.Response.STATUS_OK)) {
				JSONArray a = r.getArrayContent();
				List<AppUser> users = new LinkedList<AppUser>();
				for (int i = 0; i < a.length(); i++) {
					JSONObject jsU = a.getJSONObject(i);
					String password = null;
					String card = null;
					String nom = null;
					String prenom = null;
					if (!jsU.isNull("password")) {
						password = jsU.getString("password");
					}
					if (!jsU.isNull("card")) {
						card = jsU.getString("card");
					}
					if (!jsU.isNull("nom")) {
						nom = jsU.getString("nom");
					}
					if (!jsU.isNull("prenom")) {
						prenom = jsU.getString("prenom");
					}
					ImageIcon icon;
					if (jsU.getBoolean("hasImage")) {
						byte[] data = loader.readBinary("user",
								jsU.getString("id"));
						icon = new ImageIcon(tnb.getThumbNail(ImageUtils.readImage(data)));
					} else {
						icon = new ImageIcon(tnb.getThumbNail(null));
					}
					AppUser u = new AppUser(jsU.getString("id"),
							jsU.getString("name"), nom , prenom ,
							password, card,
							jsU.getString("roleId"), icon,
							jsU.getBoolean("visible"));
					users.add(u);
				}
				return users;
			} else {
				return null;
			}
		} catch (Exception e) {
			throw new BasicException(e);
		}
	}
	/** Preload and update cache if possible. Return true if succes. False
	 * otherwise and cache is not modified.
	 */
	public boolean preloadUsers() {
		try {
			logger.log(Level.INFO, "Preloading users");
			List<AppUser> data = this.loadUsers();
			if (data == null) {
				return false;
			}
			try {
				UsersCache.save(data);
			} catch (IOException e) {
				e.printStackTrace();
				return false;
			}
			return true;
		} catch (BasicException e) {
			e.printStackTrace();
			return false;
		}
	}
	/** Get all users */
	public final List<AppUser> listPeople() throws BasicException {
		List<AppUser> data = null;
		try {
			data = UsersCache.load();
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (data == null) {
			data = this.loadUsers();
			if (data != null) {
				try {
					UsersCache.save(data);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return data;
	}
	/** Get visible users */
	public final List<AppUser> listPeopleVisible() throws BasicException {
		List<AppUser> allUsers = this.listPeople();
		List<AppUser> visUsers = new LinkedList<AppUser>();
		for (AppUser user : allUsers) {
			if (user.isVisible()) {
				visUsers.add(user);
			}
		}
		return visUsers;
	}
	public final AppUser getPeople(String id) throws BasicException {
		List<AppUser> allUsers = this.listPeople();
		for (AppUser user : allUsers) {
			if (user.getId() != null && user.getId().equals(id)) {
				return user;
			}
		}
		return null;
	}
	/** Get user by card. Return null if nothing is found. */
	public final AppUser findPeopleByCard(String card) throws BasicException {
		List<AppUser> allUsers = this.listPeople();
		for (AppUser user : allUsers) {
			if (user.getCard() != null && user.getCard().equals(card)) {
				return user;
			}
		}
		return null;
	}

	private final Map<String, String> loadRoles() throws BasicException {
		try {
			ServerLoader loader = new ServerLoader();
			ServerLoader.Response r = loader.read("RolesAPI", "getAll");
			if (r.getStatus().equals(ServerLoader.Response.STATUS_OK)) {
				JSONArray a = r.getArrayContent();
				Map<String, String> roles = new HashMap<String, String>();
				for (int i = 0; i < a.length(); i++) {
					JSONObject o = a.getJSONObject(i);
					roles.put(o.getString("id"), o.getString("permissions"));
				}
				return roles;
			} else {
				return null;
			}
		} catch (Exception e) {
			throw new BasicException(e);
		}
	}
	public boolean preloadRoles() {
		try {
			logger.log(Level.INFO, "Preloading roles");
			Map<String, String> data = this.loadRoles();
			if (data == null) {
				return false;
			}
			try {
				RolesCache.save(data);
			} catch (IOException e) {
				e.printStackTrace();
				return false;
			}
			return true;
		} catch (BasicException e) {
			e.printStackTrace();
			return false;
		}
	}
	public final String findRolePermissions(String sRole)
			throws BasicException {
		Map<String, String> data = null;
		try {
			data = RolesCache.load();
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (data == null) {
			data = this.loadRoles();
			if (data != null) {
				try {
					RolesCache.save(data);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		if (data != null) {
			return data.get(sRole);
		} else {
			return null;
		}
	}

	public final String changePassword(String userId, String oldPwd,
			String newPwd) throws BasicException {
		try {
			ServerLoader loader = new ServerLoader();
			ServerLoader.Response r = loader.write("UsersAPI", "updPwd",
					"id", userId, "oldPwd", oldPwd, "newPwd", newPwd);
			if (r.getStatus().equals(ServerLoader.Response.STATUS_OK)) {
				return r.getResponse().getString("content");
			} else {
				return null;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/** Load resource from server */
	private final byte[] loadResource(String name) throws BasicException {
		ServerLoader loader = new ServerLoader();
		byte[] resource;
		// Check resource from server
		try {
			ServerLoader.Response r = loader.read("ResourcesAPI", "get",
					"label", name);
			if (r.getStatus().equals(ServerLoader.Response.STATUS_OK)) {
				JSONObject o = r.getObjContent();
				String strRes = o.getString("content");
				if (o.getInt("type") == 0) {
					resource = strRes.getBytes();
				} else {
					resource = DatatypeConverter.parseBase64Binary(strRes);
				}
			} else {
				return null;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resource = null;
		}
		return resource;
	}
	/** Preload and update cache if possible. Return true if succes. False
	 * otherwise and cache is not modified.
	 */
	public boolean preloadResource(String name) {
		try {
			logger.log(Level.INFO, "Preloading resource " + name);
			byte[] data = this.loadResource(name);
			if (data == null) {
				return false;
			}
			try {
				ResourcesCache.save(name, data);
			} catch (IOException e) {
				e.printStackTrace();
				return false;
			}
			return true;
		} catch (BasicException e) {
			e.printStackTrace();
			return false;
		}
	}
	private byte[] getResource(String name) {
		byte[] data = null;
		try {
			data = ResourcesCache.load(name);
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (data == null) {
			try {
				data = this.loadResource(name);
				if (data != null) {
					try {
						ResourcesCache.save(name, data);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			} catch (BasicException e) {
				e.printStackTrace();
			}
		}
		return data;
	}

	public final byte[] getResourceAsBinary(String sName) {
		return getResource(sName);
	}

	public final String getResourceAsText(String sName) {
		return Formats.BYTEA.formatValue(getResource(sName));
	}

	public final String getResourceAsXML(String sName) {
		return Formats.BYTEA.formatValue(getResource(sName));
	}

	public final BufferedImage getResourceAsImage(String sName) {
		try {
			byte[] img = getResource(sName); // , ".png"
			return img == null ? null : ImageIO.read(new ByteArrayInputStream(img));
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	public final Properties getResourceAsProperties(String sName) {

		Properties p = new Properties();
		try {
			byte[] img = getResourceAsBinary(sName);
			if (img != null) {
				p.loadFromXML(new ByteArrayInputStream(img));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return p;
	}


	public boolean preloadCashRegisters() {
		try {
			logger.log(Level.INFO, "Preloading cash registers");
			ServerLoader loader = new ServerLoader();
			ServerLoader.Response r = loader.read("CashRegistersAPI", "getAll");

			if (r.getStatus().equals(ServerLoader.Response.STATUS_OK)) {
				JSONArray a = r.getArrayContent();
				List<CashRegisterInfo> crs = new LinkedList<CashRegisterInfo>();
				for (int i = 0; i < a.length(); i++) {
					JSONObject o = a.getJSONObject(i);
					CashRegisterInfo cr = new CashRegisterInfo(o);
					crs.add(cr);
				}

				CashRegistersCache.refreshCashRegisters(crs);
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public boolean preloadInsee(Date lastUpdate) {
		try {
			logger.log(Level.INFO, "Preloading Insee");
			ServerLoader loader = new ServerLoader();
			ServerLoader.Response r ;
			if(lastUpdate.getTime() == 0) {
				r = loader.read("InseeAPI", "getAll");
			} else {
				r = loader.read("InseeAPI", "getAll" , "date" , String.valueOf(lastUpdate.getTime()));
			}
			if (r.getStatus().equals(ServerLoader.Response.STATUS_OK)) {
				JSONArray a = r.getArrayContent();
				List<InseeInfo> crs = new ArrayList<InseeInfo>();
				for (int i = 0; i < a.length(); i++) {
					JSONObject o = a.getJSONObject(i);
					InseeInfo cr = new InseeInfo(o);
					crs.add(cr);
				}
				InseeCache.refreshInsee(crs , lastUpdate);
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}


	public final CashRegisterInfo getCashRegister(int id) throws BasicException {
		return CashRegistersCache.getCashRegister(id);
	}
	public final CashRegisterInfo getCashRegister(String host)
			throws BasicException {
		try {
			ServerLoader loader = new ServerLoader();
			ServerLoader.Response r = loader.read("CashRegistersAPI", "get",
					"label", host);
			if (r.getStatus().equals(ServerLoader.Response.STATUS_OK)) {
				JSONObject o = r.getObjContent();
				if (o == null) {
					return null;
				}
				CashRegisterInfo retour = new CashRegisterInfo(o);

				retour.setSalesLocations(getSalesLocation(o));
				//2016-06 : Cas d'une mise en file d'atente de ticket sur un incident alors que le serveur est toujours disponible.
				// il faut au démarrage suivant de la caisse repartir du numéro du dernier ticket édité 
				// et non du dernier ticket enregistré sur le serveur
				retour.setNextTicketId(Math.max(retour.getNextTicketId() , CallQueue.getMaxTicketId()+1));
				return retour;
			} else {
				return null;
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new BasicException(e);
		}
	}

	public final JSONArray getSalesLocationArray(JSONObject o) throws SocketTimeoutException, ServerException, IOException {

		ServerLoader loader = new ServerLoader();
		SimpleDateFormat hdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		ServerLoader.Response r = loader.read("SalesLocationsAPI", "getCriteria",
				"from", hdf.format(new Date(o.getLong("startDate"))) , "to" ,  hdf.format(new Date(o.getLong("endDate"))) , "tour" , String.valueOf(o.getJSONObject("tour").getInt("id")));
		if (r.getStatus().equals(ServerLoader.Response.STATUS_OK)) {
			JSONArray retour = r.getArrayContent();
			return retour;
		} else {
			return null;
		}
	}

	public final ArrayList<SalesLocationInfo> getSalesLocation(JSONObject o)
			throws BasicException {
		try {
			ArrayList<SalesLocationInfo> returnedList = new ArrayList<SalesLocationInfo>();
			o = o.getJSONObject("location") ;
			JSONArray toursList = o.getJSONArray("tours");
			JSONArray citiesList;
			JSONObject tmpJSON;
			int maxTours = toursList.length();
			int maxCities = 0 ;
			while(0 < maxTours-- ) {
				tmpJSON = toursList.getJSONObject(maxTours).getJSONObject("tour");
				citiesList = getSalesLocationArray(toursList.getJSONObject(maxTours));

				maxCities = citiesList == null ? 0 : citiesList.length();
				while(0 < maxCities--) {
					returnedList.add(new SalesLocationInfo(citiesList.getJSONObject(maxCities) , tmpJSON.getString("nom")) );
				}
			}
			Collections.sort(returnedList, new SalesLocationInfoStartDateComparator());

			return returnedList;
		} catch (Exception e) {
			e.printStackTrace();
			throw new BasicException(e);
		}
	}

	public CashSession getCashSession(int cashRegId) throws BasicException {
		try {
			ServerLoader loader = new ServerLoader();
			ServerLoader.Response r = loader.read("CashesAPI", "getByCashRegister",
					"cashRegisterId", String.valueOf(cashRegId));
			if (r.getStatus().equals(ServerLoader.Response.STATUS_OK)) {
				JSONObject o = r.getObjContent();
				if (o == null) {
					return null;
				} else {
					return new CashSession(o);
				}
			} else {
				return null;
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new BasicException(e);
		}
	}

	public List<CashSession> searchCashSession(String host, Date start,
			Date stop) throws BasicException {
		try {
			ServerLoader loader = new ServerLoader();
			String startParam = null;
			String stopParam = null;
			if (start != null) {
				startParam = String.valueOf(start.getTime());
			}
			if (stop != null) {
				stopParam = String.valueOf(stop.getTime());
			}
			ServerLoader.Response r = loader.read("CashesAPI", "search",
					"cashRegisterId", host, "dateStart", startParam,
					"dateStop", stopParam);
			if (r.getStatus().equals(ServerLoader.Response.STATUS_OK)) {
				List<CashSession> sess = new LinkedList<CashSession>();
				JSONArray a = r.getArrayContent();
				for (int i = 0; i < a.length(); i++) {
					JSONObject o = a.getJSONObject(i);
					sess.add(new CashSession(o));
				}
				return sess;
			} else {
				return null;
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new BasicException(e);
		}
	}

	public CashSession getCashSessionById(String id) throws BasicException {
		try {
			ServerLoader loader = new ServerLoader();
			ServerLoader.Response r = loader.read("CashesAPI", "get",
					"id", id);
			if (r.getStatus().equals(ServerLoader.Response.STATUS_OK)) {
				JSONObject o = r.getObjContent();
				if (o == null) {
					return null;
				} else {
					return new CashSession(o);
				}
			} else {
				return null;
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new BasicException(e);
		}
	}

	/** Send a cash session to the server and return the updated session
	 * (id may have been set)
	 */
	public final CashSession saveCashSession(CashSession cashSess)
			throws BasicException {
		try {
			ServerLoader loader = new ServerLoader();
			ServerLoader.Response r = loader.write("CashesAPI", "update",
					"cash", cashSess.toJSON().toString());
			if (r.getStatus().equals(ServerLoader.Response.STATUS_OK)) {
				JSONObject o = r.getObjContent();
				if (o == null) {
					return null;
				} else {
					return new CashSession(o);
				}
			} else {
				return null;
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new BasicException(e);
		}
	}

	public final String findLocationName(String locationId)
			throws BasicException {
		try {
			ServerLoader loader = new ServerLoader();
			ServerLoader.Response r = loader.read("LocationsAPI", "get",
					"id", locationId);
			if (r.getStatus().equals(ServerLoader.Response.STATUS_OK)) {
				JSONObject o = r.getObjContent();
				return o.getString("label");
			} else {
				return null;
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new BasicException(e);
		}
	}
}


class SalesLocationInfoStartDateComparator implements Comparator<SalesLocationInfo> {
	public int compare(SalesLocationInfo sL1, SalesLocationInfo sL2) {
		return sL1.getStartDate().compareTo(sL2.getEndDate());
	}
}