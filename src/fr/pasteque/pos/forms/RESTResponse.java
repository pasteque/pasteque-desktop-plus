package fr.pasteque.pos.forms;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class RESTResponse<T> {

	private String status;
	private T content;

	@JsonCreator
	public RESTResponse(@JsonProperty("content") T content, @JsonProperty("status") String status) {
		this.status = status;
		this.content = content;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public T getContent() {
		return content;
	}

	public void setContent(T content) {
		this.content = content;
	}

}
