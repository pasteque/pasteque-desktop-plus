package fr.pasteque.data.user;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class OseToken implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	String userName;
	
	String today;
	
   

	public OseToken(String userName) {
		this.userName = userName;
		Date date = Calendar.getInstance().getTime();
        // Display a date in day, month, year format
        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        today = formatter.format(date);
	}

	public String toString(){
		return this.userName+"_"+today;
	}
}
