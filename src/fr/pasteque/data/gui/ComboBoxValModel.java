//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2007-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.

package fr.pasteque.data.gui;

import javax.swing.*;

import java.util.*;

import fr.pasteque.data.loader.IKeyGetter;
import fr.pasteque.data.loader.KeyGetterBuilder;

/**
 *
 * @author  adrian
 * @param <T>
 */
public class ComboBoxValModel<T> extends AbstractListModel<T> implements ComboBoxModel<T> {  
   
    /**
	 * 
	 */
	private static final long serialVersionUID = -3587619780160887246L;
	
	private List<T> m_aData;
    private IKeyGetter m_keygetter;
    private T m_selected;
    
    /** Creates a new instance of ComboBoxValModel */
    public ComboBoxValModel(List<T> aData, IKeyGetter keygetter) {
        m_aData = aData;
        m_keygetter = keygetter;
        m_selected = null;
    }
    public ComboBoxValModel(List<T> aData) {
        this(aData, KeyGetterBuilder.INSTANCE);
    }
    public ComboBoxValModel(IKeyGetter keygetter) {
        this(new ArrayList<T>(), keygetter);
    }
    public ComboBoxValModel() {
        this(new ArrayList<T>(), KeyGetterBuilder.INSTANCE);
    }
    
    public void add(T c) {
        m_aData.add(c);
    }

    public void add(int index, T c) {
        m_aData.add(index, c);
    }

    public void del(T c) {
        m_aData.remove(c);
    }    

    public void refresh(List<T> aData) {
        m_aData = aData;
        m_selected = null;
    }
    
    public Object getSelectedKey() {
        if (m_selected == null) {
            return null;
        } else {
            return m_keygetter.getKey(m_selected);  // Si casca, excepcion parriba
        }
    }

    public String getSelectedText() {
        if (m_selected == null) {
            return null;
        } else {
            return m_selected.toString();
        }
    }
    
    public void setSelectedKey(Object aKey) {
        setSelectedItem(getElementByKey(aKey));
    }
    
    public void setSelectedFirst() {
        m_selected = (m_aData.size() == 0) ? null : m_aData.get(0);
    }
    
    public T getElementByKey(Object aKey) {
        if (aKey != null) {
            Iterator<T> it = m_aData.iterator();
            while (it.hasNext()) {
                T value = it.next();
                if (aKey.equals(m_keygetter.getKey(value))) {
                    return value;
                }
            }           
        }
        return null;
    }
    
    public T getElementAt(int index) {
        return m_aData.get(index);
    }
    
    public T getSelectedItem() {
        return m_selected;
    }
    
    public int getSize() {
        return m_aData.size();
    }
    
    @SuppressWarnings("unchecked")
	public void setSelectedItem(Object anItem) {
        
        if ((m_selected != null && !m_selected.equals(anItem)) || m_selected == null && anItem != null) {
            m_selected = (T) anItem;
            fireContentsChanged(this, -1, -1);
        }
    }
    
}
